-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 02, 2019 at 05:12 AM
-- Server version: 5.7.24
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `generic-octobercms-upgrade`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_logs`
--

CREATE TABLE `api_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `result` longtext COLLATE utf8mb4_unicode_ci,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `request_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `rtime` double(8,2) NOT NULL,
  `response_code` smallint(6) NOT NULL,
  `authorized` text COLLATE utf8mb4_unicode_ci,
  `user_agent` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `query_log` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `is_superuser`) VALUES
(1, 'Admin', 'Person', 'admin', 'admin@domain.tld', '$2y$10$WRa2qO9n58TjiCES3o6dguUuHn95qgcxUu3944X7e3EV52KcO30vS', NULL, NULL, NULL, '', 1, 2, NULL, NULL, '2019-12-02 05:11:56', '2019-12-02 05:11:56', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_roles`
--

CREATE TABLE `backend_users_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2019-12-02 05:11:56', '2019-12-02 05:11:56', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2019-12-02 05:11:56', '2019-12-02 05:11:56'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2019-12-02 05:11:56', '2019-12-02 05:11:56');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_templates`
--

CREATE TABLE `cms_theme_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_audittrail_audit_logs`
--

CREATE TABLE `legato_audittrail_audit_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backend_users_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_backend_blacklist_ip`
--

CREATE TABLE `legato_backend_blacklist_ip` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unblocked_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_backend_user_ip_attempts`
--

CREATE TABLE `legato_backend_user_ip_attempts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_backend_user_resetpasswords`
--

CREATE TABLE `legato_backend_user_resetpasswords` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_backend_whitelist_ip`
--

CREATE TABLE `legato_backend_whitelist_ip` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_content_pages`
--

CREATE TABLE `legato_content_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_import`
--

CREATE TABLE `legato_import` (
  `id` int(10) UNSIGNED NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imported` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_news`
--

CREATE TABLE `legato_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `html_content` text COLLATE utf8mb4_unicode_ci,
  `is_published` tinyint(4) DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_pushaws_complete_individual`
--

CREATE TABLE `legato_pushaws_complete_individual` (
  `id` int(10) UNSIGNED NOT NULL,
  `arn` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complete_individual_id` int(10) UNSIGNED NOT NULL,
  `sns_status` smallint(6) NOT NULL DEFAULT '1',
  `error_reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_pushaws_complete_topic`
--

CREATE TABLE `legato_pushaws_complete_topic` (
  `id` int(10) UNSIGNED NOT NULL,
  `arn` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complete_topic_id` int(10) UNSIGNED NOT NULL,
  `sns_status` smallint(6) NOT NULL DEFAULT '1',
  `error_reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_pushaws_endpoint`
--

CREATE TABLE `legato_pushaws_endpoint` (
  `id` int(10) UNSIGNED NOT NULL,
  `arn` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(10) UNSIGNED NOT NULL,
  `sns_status` smallint(6) NOT NULL DEFAULT '1',
  `error_reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_pushaws_subscription`
--

CREATE TABLE `legato_pushaws_subscription` (
  `id` int(10) UNSIGNED NOT NULL,
  `arn` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscription_id` int(10) UNSIGNED NOT NULL,
  `sns_status` smallint(6) NOT NULL DEFAULT '1',
  `error_reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_pushaws_topic`
--

CREATE TABLE `legato_pushaws_topic` (
  `id` int(10) UNSIGNED NOT NULL,
  `arn` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `sns_status` smallint(6) NOT NULL DEFAULT '1',
  `error_reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_complete_individual`
--

CREATE TABLE `legato_push_complete_individual` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED DEFAULT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `device_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_complete_topic`
--

CREATE TABLE `legato_push_complete_topic` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_device`
--

CREATE TABLE `legato_push_device` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_type` tinyint(4) NOT NULL DEFAULT '1',
  `language` int(11) NOT NULL DEFAULT '0',
  `os_type` int(11) NOT NULL DEFAULT '0',
  `custom_field` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `platform_arn_position` int(11) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_message`
--

CREATE TABLE `legato_push_message` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_time` datetime NOT NULL,
  `push_platform` tinyint(4) NOT NULL DEFAULT '2',
  `badge` int(11) DEFAULT NULL,
  `sound` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` bigint(20) NOT NULL DEFAULT '172800',
  `content_available` tinyint(4) NOT NULL DEFAULT '0',
  `mutable_content` tinyint(4) NOT NULL DEFAULT '0',
  `attachment_url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_message_tag`
--

CREATE TABLE `legato_push_message_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_message_tag_link`
--

CREATE TABLE `legato_push_message_tag_link` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_id` int(10) UNSIGNED DEFAULT NULL,
  `tag_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_progress_individual`
--

CREATE TABLE `legato_push_progress_individual` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED DEFAULT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `device_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_progress_individual_user`
--

CREATE TABLE `legato_push_progress_individual_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED DEFAULT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_progress_topic`
--

CREATE TABLE `legato_push_progress_topic` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_subscription`
--

CREATE TABLE `legato_push_subscription` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_topic`
--

CREATE TABLE `legato_push_topic` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` int(10) UNSIGNED NOT NULL,
  `topic_group_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_push_topic_group`
--

CREATE TABLE `legato_push_topic_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_complete`
--

CREATE TABLE `legato_sms_complete` (
  `id` int(10) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `sms_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_total_retry` int(11) NOT NULL,
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_complete_archive`
--

CREATE TABLE `legato_sms_complete_archive` (
  `id` int(10) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_total_retry` int(11) NOT NULL,
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_cores`
--

CREATE TABLE `legato_sms_cores` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_delivery`
--

CREATE TABLE `legato_sms_delivery` (
  `id` int(10) UNSIGNED NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_delivery_archive`
--

CREATE TABLE `legato_sms_delivery_archive` (
  `id` int(10) UNSIGNED NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_message`
--

CREATE TABLE `legato_sms_message` (
  `msg_id` int(10) UNSIGNED NOT NULL,
  `sms_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_message` int(11) NOT NULL,
  `num_success` int(11) NOT NULL,
  `num_fail` int(11) NOT NULL,
  `num_processing` int(11) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `num_delivered` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OA` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suffix` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_message_archive`
--

CREATE TABLE `legato_sms_message_archive` (
  `msg_id` int(10) UNSIGNED NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_message` int(11) NOT NULL,
  `num_success` int(11) NOT NULL,
  `num_fail` int(11) NOT NULL,
  `num_processing` int(11) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `num_delivered` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_progress`
--

CREATE TABLE `legato_sms_progress` (
  `id` int(10) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `sms_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_host_retry` int(11) NOT NULL DEFAULT '0',
  `num_total_retry` int(11) NOT NULL DEFAULT '0',
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_receiver`
--

CREATE TABLE `legato_sms_receiver` (
  `id` int(10) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `receiver_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legato_sms_receiver_archive`
--

CREATE TABLE `legato_sms_receiver_archive` (
  `id` int(10) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field', 1),
(26, '2013_10_01_000001_Db_Backend_Users', 2),
(27, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(28, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(29, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(30, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(31, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(32, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(33, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(34, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(35, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(36, '2018_12_16_000011_Db_Backend_Add_Deleted_At', 2),
(37, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(38, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(39, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
(40, '2018_11_01_000001_Db_Cms_Theme_Templates', 3),
(41, '2018_08_30_000001_Db_Common_Imports', 4);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_version`
--

CREATE TABLE `mobile_version` (
  `id` int(10) UNSIGNED NOT NULL,
  `version` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `force_update` smallint(6) NOT NULL DEFAULT '0' COMMENT '1=Yes;0=No',
  `platform` smallint(6) NOT NULL COMMENT '1=iOS;2=Android',
  `popup_message` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `download_link` text COLLATE utf8mb4_unicode_ci,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0=disabled;1=active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_attributes`
--

CREATE TABLE `rainlab_translate_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_indexes`
--

CREATE TABLE `rainlab_translate_indexes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_locales`
--

CREATE TABLE `rainlab_translate_locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_locales`
--

INSERT INTO `rainlab_translate_locales` (`id`, `code`, `name`, `is_default`, `is_enabled`, `sort_order`) VALUES
(1, 'en', 'English', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_messages`
--

CREATE TABLE `rainlab_translate_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `options`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2019-12-02 05:11:56', '2019-12-02 05:11:56'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2019-12-02 05:11:56', '2019-12-02 05:11:56');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'PopcornPHP.ImageCompress', 'comment', '1.0.1', 'First version of ImageCompress', '2019-12-02 05:11:52'),
(2, 'PopcornPHP.ImageCompress', 'comment', '1.0.2', 'Adding settings and change compress logic (look UPGRADE.md)', '2019-12-02 05:11:52'),
(3, 'PopcornPHP.ImageCompress', 'comment', '1.0.3', 'Adding enlarge setting for small images', '2019-12-02 05:11:52'),
(4, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2019-12-02 05:11:52'),
(5, 'Legato.Braintree', 'comment', '1.0.1', 'Initialize plugin.', '2019-12-02 05:11:52'),
(6, 'Legato.SMS', 'comment', '1.0.1', 'First version of SMS', '2019-12-02 05:11:52'),
(7, 'Legato.SMS', 'script', '1.0.2', 'create_cores_table.php', '2019-12-02 05:11:52'),
(8, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_complete_archive_table.php', '2019-12-02 05:11:52'),
(9, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_complete_table.php', '2019-12-02 05:11:52'),
(10, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_delivery_archive_table.php', '2019-12-02 05:11:53'),
(11, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_delivery_table.php', '2019-12-02 05:11:53'),
(12, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_messages_archive_table.php', '2019-12-02 05:11:53'),
(13, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_progress_table.php', '2019-12-02 05:11:53'),
(14, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_receiver_archive_table.php', '2019-12-02 05:11:53'),
(15, 'Legato.SMS', 'script', '1.0.2', 'create_legato_sms_receiver_table.php', '2019-12-02 05:11:53'),
(16, 'Legato.SMS', 'script', '1.0.2', 'create_sms_messages_table.php', '2019-12-02 05:11:53'),
(17, 'Legato.SMS', 'comment', '1.0.2', 'init DataBase', '2019-12-02 05:11:53'),
(18, 'Legato.Content', 'comment', '1.0.1', 'First version of Content', '2019-12-02 05:11:53'),
(19, 'Legato.Content', 'script', '1.0.2', 'create_pages_table.php', '2019-12-02 05:11:53'),
(20, 'Legato.Content', 'comment', '1.0.2', 'Edited plugin info and general namings', '2019-12-02 05:11:53'),
(21, 'Legato.Content', 'comment', '1.0.2', 'Edited schema create function', '2019-12-02 05:11:53'),
(22, 'Legato.Backend', 'comment', '1.0.1', 'First version of Backend', '2019-12-02 05:11:53'),
(23, 'Legato.Backend', 'comment', '1.0.2', 'Strong password protection for Backend User', '2019-12-02 05:11:53'),
(24, 'Legato.Backend', 'script', '1.0.3', 'create_attempts_table.php', '2019-12-02 05:11:53'),
(25, 'Legato.Backend', 'script', '1.0.3', 'create_blacklists_table.php', '2019-12-02 05:11:53'),
(26, 'Legato.Backend', 'comment', '1.0.3', 'Block IP for Z hours after X times login failed within Y hours.', '2019-12-02 05:11:53'),
(27, 'Legato.Backend', 'script', '1.0.4', 'create_whitelists_table.php', '2019-12-02 05:11:53'),
(28, 'Legato.Backend', 'comment', '1.0.4', 'Support White List', '2019-12-02 05:11:53'),
(29, 'Legato.Backend', 'script', '1.0.5', 'create_resetpasswords_table.php', '2019-12-02 05:11:53'),
(30, 'Legato.Backend', 'comment', '1.0.5', 'Limit reset password function', '2019-12-02 05:11:53'),
(31, 'Legato.Backend', 'script', '1.0.6', 'add_common_attirbutes_to_attempts_table.php', '2019-12-02 05:11:53'),
(32, 'Legato.Backend', 'comment', '1.0.6', 'IP attempts table missed fields', '2019-12-02 05:11:53'),
(33, 'Legato.Backend', 'script', '1.0.7', 'create_backend_users_roles_table.php', '2019-12-02 05:11:53'),
(34, 'Legato.Backend', 'comment', '1.0.7', 'Backend user multi roles', '2019-12-02 05:11:53'),
(35, 'Legato.Push', 'comment', '1.0.1', 'Initialize plugin.', '2019-12-02 05:11:53'),
(36, 'Legato.Push', 'script', '1.0.2', 'create_push_message_table.php', '2019-12-02 05:11:53'),
(37, 'Legato.Push', 'script', '1.0.2', 'create_push_device_table.php', '2019-12-02 05:11:54'),
(38, 'Legato.Push', 'script', '1.0.2', 'create_push_message_tag_table.php', '2019-12-02 05:11:54'),
(39, 'Legato.Push', 'script', '1.0.2', 'create_push_message_tag_link_table.php', '2019-12-02 05:11:54'),
(40, 'Legato.Push', 'script', '1.0.2', 'create_push_progress_individual_user_table.php', '2019-12-02 05:11:54'),
(41, 'Legato.Push', 'script', '1.0.2', 'create_push_progress_individual_table.php', '2019-12-02 05:11:54'),
(42, 'Legato.Push', 'script', '1.0.2', 'create_push_complete_individual_table.php', '2019-12-02 05:11:54'),
(43, 'Legato.Push', 'script', '1.0.2', 'create_push_topic_gorup_table.php', '2019-12-02 05:11:54'),
(44, 'Legato.Push', 'script', '1.0.2', 'create_push_topic_table.php', '2019-12-02 05:11:54'),
(45, 'Legato.Push', 'script', '1.0.2', 'create_push_subscription_table.php', '2019-12-02 05:11:54'),
(46, 'Legato.Push', 'script', '1.0.2', 'create_push_progress_topic_table.php', '2019-12-02 05:11:54'),
(47, 'Legato.Push', 'script', '1.0.2', 'create_push_complete_topic_table.php', '2019-12-02 05:11:54'),
(48, 'Legato.Push', 'comment', '1.0.2', 'Init DB', '2019-12-02 05:11:54'),
(49, 'Legato.News', 'comment', '1.0.1', 'First version of news', '2019-12-02 05:11:54'),
(50, 'Legato.News', 'script', '1.0.2', 'create_news_table.php', '2019-12-02 05:11:54'),
(51, 'Legato.News', 'comment', '1.0.2', 'Initialize plugin.', '2019-12-02 05:11:54'),
(52, 'legato.Api', 'comment', '1.0.1', 'First version of api', '2019-12-02 05:11:54'),
(53, 'legato.Api', 'script', '1.0.2', 'create_api_logs_table.php', '2019-12-02 05:11:54'),
(54, 'legato.Api', 'comment', '1.0.2', 'api log', '2019-12-02 05:11:54'),
(55, 'legato.Api', 'script', '1.0.3', 'create_access_token_table.php', '2019-12-02 05:11:54'),
(56, 'legato.Api', 'comment', '1.0.3', 'access token', '2019-12-02 05:11:54'),
(57, 'Legato.TranslateRelationships', 'comment', '1.0.1', 'First version of TranslateRelationships', '2019-12-02 05:11:54'),
(58, 'Legato.AuditTrail', 'comment', '1.0.1', 'First version of AuditTrail', '2019-12-02 05:11:54'),
(59, 'Legato.AuditTrail', 'script', '1.0.2', 'create_audit_logs_table.php', '2019-12-02 05:11:55'),
(60, 'Legato.AuditTrail', 'comment', '1.0.2', 'Fixed schema create issue', '2019-12-02 05:11:55'),
(61, 'Legato.AuditTrail', 'comment', '1.0.3', 'Update For latest common modules', '2019-12-02 05:11:55'),
(62, 'Legato.MobileVersion', 'comment', '1.0.1', 'Initialize plugin.', '2019-12-02 05:11:55'),
(63, 'Legato.MobileVersion', 'script', '1.0.2', 'create_mobile_version_table.php', '2019-12-02 05:11:55'),
(64, 'Legato.MobileVersion', 'comment', '1.0.2', 'Created table mobile_version', '2019-12-02 05:11:55'),
(65, 'Bedard.Debugbar', 'comment', '1.0.1', 'First version of Debugbar', '2019-12-02 05:11:55'),
(66, 'Bedard.Debugbar', 'comment', '1.0.2', 'Debugbar facade aliased (Alxy)', '2019-12-02 05:11:55'),
(67, 'Bedard.Debugbar', 'comment', '1.0.3', 'Added ajax debugging', '2019-12-02 05:11:55'),
(68, 'Bedard.Debugbar', 'comment', '1.0.4', 'Only display to backend authenticated users', '2019-12-02 05:11:55'),
(69, 'Bedard.Debugbar', 'comment', '1.0.5', 'Use elevated privileges', '2019-12-02 05:11:55'),
(70, 'Bedard.Debugbar', 'comment', '1.0.6', 'Fix fatal error when cms.page.beforeDisplay is fired multiple times (mnishihan)', '2019-12-02 05:11:55'),
(71, 'Bedard.Debugbar', 'comment', '1.0.7', 'Allow plugin to be installed via Composer (tim0991)', '2019-12-02 05:11:55'),
(72, 'Bedard.Debugbar', 'comment', '1.0.8', 'Fix debugbar dependency', '2019-12-02 05:11:55'),
(73, 'Bedard.Debugbar', 'comment', '2.0.0', '!!! Upgrade for compatibility with Laravel 5.5 (PHP 7+, October 420+)', '2019-12-02 05:11:55'),
(74, 'Bedard.Debugbar', 'comment', '2.0.1', 'Add config file to prevent exceptions from being thrown (credit alxy)', '2019-12-02 05:11:55'),
(75, 'Alxy.Captcha', 'comment', '1.0.1', 'First version of Captcha', '2019-12-02 05:11:55'),
(76, 'Alxy.Captcha', 'comment', '1.0.2', 'Added setting permissions by @drtzack', '2019-12-02 05:11:55'),
(77, 'Alxy.Captcha', 'comment', '1.0.3', 'Register Snippet for Rainlab.Pages support', '2019-12-02 05:11:55'),
(78, 'TamerHassan.ScheduleList', 'comment', '1.0.1', 'First version of October Schedule List Plugin', '2019-12-02 05:11:55'),
(79, 'Jc91715.Oss', 'comment', '1.0.1', 'First version of oss', '2019-12-02 05:11:55'),
(80, 'RainLab.Translate', 'script', '1.0.1', 'create_messages_table.php', '2019-12-02 05:11:55'),
(81, 'RainLab.Translate', 'script', '1.0.1', 'create_attributes_table.php', '2019-12-02 05:11:55'),
(82, 'RainLab.Translate', 'script', '1.0.1', 'create_locales_table.php', '2019-12-02 05:11:55'),
(83, 'RainLab.Translate', 'comment', '1.0.1', 'First version of Translate', '2019-12-02 05:11:55'),
(84, 'RainLab.Translate', 'comment', '1.0.2', 'Languages and Messages can now be deleted.', '2019-12-02 05:11:55'),
(85, 'RainLab.Translate', 'comment', '1.0.3', 'Minor updates for latest October release.', '2019-12-02 05:11:55'),
(86, 'RainLab.Translate', 'comment', '1.0.4', 'Locale cache will clear when updating a language.', '2019-12-02 05:11:55'),
(87, 'RainLab.Translate', 'comment', '1.0.5', 'Add Spanish language and fix plugin config.', '2019-12-02 05:11:55'),
(88, 'RainLab.Translate', 'comment', '1.0.6', 'Minor improvements to the code.', '2019-12-02 05:11:55'),
(89, 'RainLab.Translate', 'comment', '1.0.7', 'Fixes major bug where translations are skipped entirely!', '2019-12-02 05:11:55'),
(90, 'RainLab.Translate', 'comment', '1.0.8', 'Minor bug fixes.', '2019-12-02 05:11:55'),
(91, 'RainLab.Translate', 'comment', '1.0.9', 'Fixes an issue where newly created models lose their translated values.', '2019-12-02 05:11:55'),
(92, 'RainLab.Translate', 'comment', '1.0.10', 'Minor fix for latest build.', '2019-12-02 05:11:55'),
(93, 'RainLab.Translate', 'comment', '1.0.11', 'Fix multilingual rich editor when used in stretch mode.', '2019-12-02 05:11:55'),
(94, 'RainLab.Translate', 'comment', '1.1.0', 'Introduce compatibility with RainLab.Pages plugin.', '2019-12-02 05:11:55'),
(95, 'RainLab.Translate', 'comment', '1.1.1', 'Minor UI fix to the language picker.', '2019-12-02 05:11:55'),
(96, 'RainLab.Translate', 'comment', '1.1.2', 'Add support for translating Static Content files.', '2019-12-02 05:11:55'),
(97, 'RainLab.Translate', 'comment', '1.1.3', 'Improved support for the multilingual rich editor.', '2019-12-02 05:11:55'),
(98, 'RainLab.Translate', 'comment', '1.1.4', 'Adds new multilingual markdown editor.', '2019-12-02 05:11:55'),
(99, 'RainLab.Translate', 'comment', '1.1.5', 'Minor update to the multilingual control API.', '2019-12-02 05:11:55'),
(100, 'RainLab.Translate', 'comment', '1.1.6', 'Minor improvements in the message editor.', '2019-12-02 05:11:55'),
(101, 'RainLab.Translate', 'comment', '1.1.7', 'Fixes bug not showing content when first loading multilingual textarea controls.', '2019-12-02 05:11:55'),
(102, 'RainLab.Translate', 'comment', '1.2.0', 'CMS pages now support translating the URL.', '2019-12-02 05:11:55'),
(103, 'RainLab.Translate', 'comment', '1.2.1', 'Minor update in the rich editor and code editor language control position.', '2019-12-02 05:11:55'),
(104, 'RainLab.Translate', 'comment', '1.2.2', 'Static Pages now support translating the URL.', '2019-12-02 05:11:55'),
(105, 'RainLab.Translate', 'comment', '1.2.3', 'Fixes Rich Editor when inserting a page link.', '2019-12-02 05:11:55'),
(106, 'RainLab.Translate', 'script', '1.2.4', 'create_indexes_table.php', '2019-12-02 05:11:55'),
(107, 'RainLab.Translate', 'comment', '1.2.4', 'Translatable attributes can now be declared as indexes.', '2019-12-02 05:11:55'),
(108, 'RainLab.Translate', 'comment', '1.2.5', 'Adds new multilingual repeater form widget.', '2019-12-02 05:11:55'),
(109, 'RainLab.Translate', 'comment', '1.2.6', 'Fixes repeater usage with static pages plugin.', '2019-12-02 05:11:55'),
(110, 'RainLab.Translate', 'comment', '1.2.7', 'Fixes placeholder usage with static pages plugin.', '2019-12-02 05:11:55'),
(111, 'RainLab.Translate', 'comment', '1.2.8', 'Improvements to code for latest October build compatibility.', '2019-12-02 05:11:55'),
(112, 'RainLab.Translate', 'comment', '1.2.9', 'Fixes context for translated strings when used with Static Pages.', '2019-12-02 05:11:55'),
(113, 'RainLab.Translate', 'comment', '1.2.10', 'Minor UI fix to the multilingual repeater.', '2019-12-02 05:11:55'),
(114, 'RainLab.Translate', 'comment', '1.2.11', 'Fixes translation not working with partials loaded via AJAX.', '2019-12-02 05:11:55'),
(115, 'RainLab.Translate', 'comment', '1.2.12', 'Add support for translating the new grouped repeater feature.', '2019-12-02 05:11:55'),
(116, 'RainLab.Translate', 'comment', '1.3.0', 'Added search to the translate messages page.', '2019-12-02 05:11:55'),
(117, 'RainLab.Translate', 'script', '1.3.1', 'builder_table_update_rainlab_translate_locales.php', '2019-12-02 05:11:55'),
(118, 'RainLab.Translate', 'script', '1.3.1', 'seed_all_tables.php', '2019-12-02 05:11:55'),
(119, 'RainLab.Translate', 'comment', '1.3.1', 'Added reordering to languages', '2019-12-02 05:11:55'),
(120, 'RainLab.Translate', 'comment', '1.3.2', 'Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.', '2019-12-02 05:11:55'),
(121, 'RainLab.Translate', 'comment', '1.3.3', 'Fix to the locale picker session handling in Build 420 onwards.', '2019-12-02 05:11:55'),
(122, 'RainLab.Translate', 'comment', '1.3.4', 'Add alternate hreflang elements and adds prefixDefaultLocale setting.', '2019-12-02 05:11:55'),
(123, 'RainLab.Translate', 'comment', '1.3.5', 'Fix MLRepeater bug when switching locales.', '2019-12-02 05:11:55'),
(124, 'RainLab.Translate', 'comment', '1.3.6', 'Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4', '2019-12-02 05:11:55'),
(125, 'RainLab.Translate', 'comment', '1.3.7', 'Fix config reference in LocaleMiddleware', '2019-12-02 05:11:55'),
(126, 'RainLab.Translate', 'comment', '1.3.8', 'Keep query string when switching locales', '2019-12-02 05:11:55'),
(127, 'RainLab.Translate', 'comment', '1.4.0', 'Add importer and exporter for messages', '2019-12-02 05:11:55'),
(128, 'RainLab.Translate', 'comment', '1.4.1', 'Updated Hungarian translation. Added Arabic translation. Fixed issue where default texts are overwritten by import. Fixed issue where the language switcher for repeater fields would overlap with the first repeater row.', '2019-12-02 05:11:55'),
(129, 'RainLab.Translate', 'comment', '1.4.2', 'Add multilingual MediaFinder', '2019-12-02 05:11:55'),
(130, 'RainLab.Translate', 'comment', '1.4.3', '!!! Please update OctoberCMS to Build 444 before updating this plugin. Added ability to translate CMS Pages fields (e.g. title, description, meta-title, meta-description)', '2019-12-02 05:11:55'),
(131, 'RainLab.Translate', 'comment', '1.4.4', 'Minor improvements to compatibility with Laravel framework.', '2019-12-02 05:11:55'),
(132, 'RainLab.Translate', 'comment', '1.4.5', 'Fixed issue when using the language switcher', '2019-12-02 05:11:55'),
(133, 'RainLab.Translate', 'comment', '1.5.0', 'Compatibility fix with Build 451', '2019-12-02 05:11:55'),
(134, 'RainLab.Translate', 'comment', '1.6.0', 'Make File Upload widget properties translatable. Merge Repeater core changes into MLRepeater widget. Add getter method to retrieve original translate data.', '2019-12-02 05:11:55'),
(135, 'RainLab.Translate', 'comment', '1.6.1', 'Add ability for models to provide translated computed data, add option to disable locale prefix routing', '2019-12-02 05:11:55'),
(136, 'RainLab.Translate', 'comment', '1.6.2', 'Implement localeUrl filter, add per-locale theme configuration support', '2019-12-02 05:11:55'),
(137, 'Legato.Pushaws', 'comment', '1.0.1', 'Initialize plugin.', '2019-12-02 05:11:55'),
(138, 'Legato.Pushaws', 'script', '1.0.2', 'create_pushaws_topic_table.php', '2019-12-02 05:11:55'),
(139, 'Legato.Pushaws', 'script', '1.0.2', 'create_pushaws_subscription_table.php', '2019-12-02 05:11:55'),
(140, 'Legato.Pushaws', 'script', '1.0.2', 'create_pushaws_endpoint_table.php', '2019-12-02 05:11:55'),
(141, 'Legato.Pushaws', 'script', '1.0.2', 'create_pushaws_complete_topic_table.php', '2019-12-02 05:11:56'),
(142, 'Legato.Pushaws', 'script', '1.0.2', 'create_pushaws_complete_individual_table.php', '2019-12-02 05:11:56'),
(143, 'Legato.Pushaws', 'comment', '1.0.2', 'init db', '2019-12-02 05:11:56');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'PopcornPHP.ImageCompress', '1.0.3', '2019-12-02 05:11:52', 0, 0),
(2, 'October.Demo', '1.0.1', '2019-12-02 05:11:52', 0, 0),
(3, 'Legato.Braintree', '1.0.1', '2019-12-02 05:11:52', 0, 0),
(4, 'Legato.SMS', '1.0.2', '2019-12-02 05:11:53', 0, 0),
(5, 'Legato.Content', '1.0.2', '2019-12-02 05:11:53', 0, 0),
(6, 'Legato.Backend', '1.0.7', '2019-12-02 05:11:53', 0, 0),
(7, 'Legato.Push', '1.0.2', '2019-12-02 05:11:54', 0, 0),
(8, 'Legato.News', '1.0.2', '2019-12-02 05:11:54', 0, 0),
(9, 'legato.Api', '1.0.3', '2019-12-02 05:11:54', 0, 0),
(10, 'Legato.TranslateRelationships', '1.0.1', '2019-12-02 05:11:54', 0, 0),
(11, 'Legato.AuditTrail', '1.0.3', '2019-12-02 05:11:55', 0, 0),
(12, 'Legato.MobileVersion', '1.0.2', '2019-12-02 05:11:55', 0, 0),
(13, 'Bedard.Debugbar', '2.0.1', '2019-12-02 05:11:55', 0, 0),
(14, 'Alxy.Captcha', '1.0.3', '2019-12-02 05:11:55', 0, 0),
(15, 'TamerHassan.ScheduleList', '1.0.1', '2019-12-02 05:11:55', 0, 0),
(16, 'Jc91715.Oss', '1.0.1', '2019-12-02 05:11:55', 0, 0),
(17, 'RainLab.Translate', '1.6.2', '2019-12-02 05:11:55', 0, 0),
(18, 'Legato.Pushaws', '1.0.2', '2019-12-02 05:11:56', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_access_token`
--

CREATE TABLE `users_access_token` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=user/2=admin',
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_logs`
--
ALTER TABLE `api_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `api_logs_created_at_index` (`created_at`),
  ADD KEY `api_logs_ref_code_index` (`ref_code`);

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_users_roles`
--
ALTER TABLE `backend_users_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_templates_source_index` (`source`),
  ADD KEY `cms_theme_templates_path_index` (`path`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `legato_audittrail_audit_logs`
--
ALTER TABLE `legato_audittrail_audit_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_audittrail_audit_logs_section_index` (`section`),
  ADD KEY `legato_audittrail_audit_logs_model_index` (`model`),
  ADD KEY `legato_audittrail_audit_logs_action_index` (`action`);

--
-- Indexes for table `legato_backend_blacklist_ip`
--
ALTER TABLE `legato_backend_blacklist_ip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_backend_blacklist_ip_ip_address_index` (`ip_address`);

--
-- Indexes for table `legato_backend_user_ip_attempts`
--
ALTER TABLE `legato_backend_user_ip_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_backend_user_ip_attempts_id_index` (`id`),
  ADD KEY `legato_backend_user_ip_attempts_ip_address_index` (`ip_address`);

--
-- Indexes for table `legato_backend_user_resetpasswords`
--
ALTER TABLE `legato_backend_user_resetpasswords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_backend_user_resetpasswords_user_id_index` (`user_id`),
  ADD KEY `legato_backend_user_resetpasswords_reset_password_code_index` (`reset_password_code`);

--
-- Indexes for table `legato_backend_whitelist_ip`
--
ALTER TABLE `legato_backend_whitelist_ip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_backend_whitelist_ip_ip_address_index` (`ip_address`);

--
-- Indexes for table `legato_content_pages`
--
ALTER TABLE `legato_content_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legato_import`
--
ALTER TABLE `legato_import`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legato_news`
--
ALTER TABLE `legato_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legato_pushaws_complete_individual`
--
ALTER TABLE `legato_pushaws_complete_individual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legato_pushaws_complete_topic`
--
ALTER TABLE `legato_pushaws_complete_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legato_pushaws_endpoint`
--
ALTER TABLE `legato_pushaws_endpoint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_pushaws_endpoint_device_id_index` (`device_id`),
  ADD KEY `legato_pushaws_endpoint_sns_status_index` (`sns_status`);

--
-- Indexes for table `legato_pushaws_subscription`
--
ALTER TABLE `legato_pushaws_subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_pushaws_subscription_subscription_id_index` (`subscription_id`),
  ADD KEY `legato_pushaws_subscription_sns_status_index` (`sns_status`);

--
-- Indexes for table `legato_pushaws_topic`
--
ALTER TABLE `legato_pushaws_topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_pushaws_topic_topic_id_index` (`topic_id`),
  ADD KEY `legato_pushaws_topic_sns_status_index` (`sns_status`);

--
-- Indexes for table `legato_push_complete_individual`
--
ALTER TABLE `legato_push_complete_individual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_complete_individual_message_id_index` (`message_id`),
  ADD KEY `legato_push_complete_individual_device_id_index` (`device_id`);

--
-- Indexes for table `legato_push_complete_topic`
--
ALTER TABLE `legato_push_complete_topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_complete_topic_message_id_index` (`message_id`),
  ADD KEY `legato_push_complete_topic_topic_id_index` (`topic_id`),
  ADD KEY `legato_push_complete_topic_status_index` (`status`);

--
-- Indexes for table `legato_push_device`
--
ALTER TABLE `legato_push_device`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_device_deleted_at_index` (`deleted_at`),
  ADD KEY `legato_push_device_status_index` (`status`),
  ADD KEY `legato_push_device_token_type_index` (`token_type`),
  ADD KEY `legato_push_device_token_index` (`token`);

--
-- Indexes for table `legato_push_message`
--
ALTER TABLE `legato_push_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legato_push_message_tag`
--
ALTER TABLE `legato_push_message_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_message_tag_name_index` (`name`);

--
-- Indexes for table `legato_push_message_tag_link`
--
ALTER TABLE `legato_push_message_tag_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_message_tag_link_message_id_index` (`message_id`),
  ADD KEY `legato_push_message_tag_link_tag_id_index` (`tag_id`);

--
-- Indexes for table `legato_push_progress_individual`
--
ALTER TABLE `legato_push_progress_individual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_progress_individual_device_id_index` (`device_id`),
  ADD KEY `legato_push_progress_individual_message_id_index` (`message_id`),
  ADD KEY `legato_push_progress_individual_status_index` (`status`);

--
-- Indexes for table `legato_push_progress_individual_user`
--
ALTER TABLE `legato_push_progress_individual_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_progress_individual_user_message_id_index` (`message_id`),
  ADD KEY `legato_push_progress_individual_user_user_id_index` (`user_id`),
  ADD KEY `legato_push_progress_individual_user_status_index` (`status`);

--
-- Indexes for table `legato_push_progress_topic`
--
ALTER TABLE `legato_push_progress_topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_progress_topic_message_id_index` (`message_id`),
  ADD KEY `legato_push_progress_topic_topic_id_index` (`topic_id`),
  ADD KEY `legato_push_progress_topic_status_index` (`status`);

--
-- Indexes for table `legato_push_subscription`
--
ALTER TABLE `legato_push_subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_subscription_device_id_index` (`device_id`),
  ADD KEY `legato_push_subscription_topic_id_index` (`topic_id`),
  ADD KEY `legato_push_subscription_status_index` (`status`),
  ADD KEY `legato_push_subscription_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `legato_push_topic`
--
ALTER TABLE `legato_push_topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_topic_language_index` (`language`),
  ADD KEY `legato_push_topic_topic_group_id_index` (`topic_group_id`),
  ADD KEY `legato_push_topic_status_index` (`status`);

--
-- Indexes for table `legato_push_topic_group`
--
ALTER TABLE `legato_push_topic_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_push_topic_group_status_index` (`status`),
  ADD KEY `legato_push_topic_group_deleted_at_index` (`deleted_at`),
  ADD KEY `legato_push_topic_group_name_index` (`name`);

--
-- Indexes for table `legato_sms_complete`
--
ALTER TABLE `legato_sms_complete`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_sms_complete_status_index` (`status`),
  ADD KEY `legato_sms_complete_create_date_index` (`create_date`),
  ADD KEY `legato_sms_complete_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_complete_archive`
--
ALTER TABLE `legato_sms_complete_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_sms_complete_archive_msg_id_index` (`msg_id`),
  ADD KEY `legato_sms_complete_archive_response_id_index` (`response_id`),
  ADD KEY `legato_sms_complete_archive_num_retry_index` (`num_retry`),
  ADD KEY `legato_sms_complete_archive_current_retry_index` (`current_retry`),
  ADD KEY `legato_sms_complete_archive_status_index` (`status`),
  ADD KEY `legato_sms_complete_archive_create_date_index` (`create_date`),
  ADD KEY `legato_sms_complete_archive_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_cores`
--
ALTER TABLE `legato_sms_cores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legato_sms_delivery`
--
ALTER TABLE `legato_sms_delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_sms_delivery_response_id_index` (`response_id`),
  ADD KEY `legato_sms_delivery_status_index` (`status`),
  ADD KEY `legato_sms_delivery_create_date_index` (`create_date`),
  ADD KEY `legato_sms_delivery_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_delivery_archive`
--
ALTER TABLE `legato_sms_delivery_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_sms_delivery_archive_response_id_index` (`response_id`),
  ADD KEY `legato_sms_delivery_archive_status_index` (`status`),
  ADD KEY `legato_sms_delivery_archive_create_date_index` (`create_date`),
  ADD KEY `legato_sms_delivery_archive_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_message`
--
ALTER TABLE `legato_sms_message`
  ADD PRIMARY KEY (`msg_id`),
  ADD KEY `legato_sms_message_schedule_time_index` (`schedule_time`),
  ADD KEY `legato_sms_message_status_index` (`status`),
  ADD KEY `legato_sms_message_create_date_index` (`create_date`),
  ADD KEY `legato_sms_message_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_message_archive`
--
ALTER TABLE `legato_sms_message_archive`
  ADD PRIMARY KEY (`msg_id`),
  ADD KEY `legato_sms_message_archive_schedule_time_index` (`schedule_time`),
  ADD KEY `legato_sms_message_archive_status_index` (`status`),
  ADD KEY `legato_sms_message_archive_create_date_index` (`create_date`),
  ADD KEY `legato_sms_message_archive_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_progress`
--
ALTER TABLE `legato_sms_progress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_sms_progress_msg_id_index` (`msg_id`),
  ADD KEY `legato_sms_progress_num_retry_index` (`num_retry`),
  ADD KEY `legato_sms_progress_current_retry_index` (`current_retry`),
  ADD KEY `legato_sms_progress_status_index` (`status`),
  ADD KEY `legato_sms_progress_create_date_index` (`create_date`),
  ADD KEY `legato_sms_progress_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_receiver`
--
ALTER TABLE `legato_sms_receiver`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_sms_receiver_msg_id_index` (`msg_id`),
  ADD KEY `legato_sms_receiver_status_index` (`status`),
  ADD KEY `legato_sms_receiver_create_date_index` (`create_date`),
  ADD KEY `legato_sms_receiver_modify_date_index` (`modify_date`);

--
-- Indexes for table `legato_sms_receiver_archive`
--
ALTER TABLE `legato_sms_receiver_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legato_sms_receiver_archive_msg_id_index` (`msg_id`),
  ADD KEY `legato_sms_receiver_archive_status_index` (`status`),
  ADD KEY `legato_sms_receiver_archive_create_date_index` (`create_date`),
  ADD KEY `legato_sms_receiver_archive_modify_date_index` (`modify_date`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_version`
--
ALTER TABLE `mobile_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_attributes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_attributes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_attributes_model_type_index` (`model_type`);

--
-- Indexes for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_indexes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_indexes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_indexes_model_type_index` (`model_type`),
  ADD KEY `rainlab_translate_indexes_item_index` (`item`);

--
-- Indexes for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_locales_code_index` (`code`),
  ADD KEY `rainlab_translate_locales_name_index` (`name`);

--
-- Indexes for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_messages_code_index` (`code`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- Indexes for table `users_access_token`
--
ALTER TABLE `users_access_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_access_token_deleted_at_index` (`deleted_at`),
  ADD KEY `users_access_token_status_index` (`status`),
  ADD KEY `users_access_token_type_index` (`type`),
  ADD KEY `users_access_token_access_token_index` (`access_token`),
  ADD KEY `users_access_token_user_id_index` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_logs`
--
ALTER TABLE `api_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_users_roles`
--
ALTER TABLE `backend_users_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_audittrail_audit_logs`
--
ALTER TABLE `legato_audittrail_audit_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_backend_blacklist_ip`
--
ALTER TABLE `legato_backend_blacklist_ip`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_backend_user_ip_attempts`
--
ALTER TABLE `legato_backend_user_ip_attempts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_backend_user_resetpasswords`
--
ALTER TABLE `legato_backend_user_resetpasswords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_backend_whitelist_ip`
--
ALTER TABLE `legato_backend_whitelist_ip`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_content_pages`
--
ALTER TABLE `legato_content_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_import`
--
ALTER TABLE `legato_import`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_news`
--
ALTER TABLE `legato_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_pushaws_complete_individual`
--
ALTER TABLE `legato_pushaws_complete_individual`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_pushaws_complete_topic`
--
ALTER TABLE `legato_pushaws_complete_topic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_pushaws_endpoint`
--
ALTER TABLE `legato_pushaws_endpoint`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_pushaws_subscription`
--
ALTER TABLE `legato_pushaws_subscription`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_pushaws_topic`
--
ALTER TABLE `legato_pushaws_topic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_complete_individual`
--
ALTER TABLE `legato_push_complete_individual`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_complete_topic`
--
ALTER TABLE `legato_push_complete_topic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_device`
--
ALTER TABLE `legato_push_device`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_message`
--
ALTER TABLE `legato_push_message`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_message_tag`
--
ALTER TABLE `legato_push_message_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_message_tag_link`
--
ALTER TABLE `legato_push_message_tag_link`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_progress_individual`
--
ALTER TABLE `legato_push_progress_individual`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_progress_individual_user`
--
ALTER TABLE `legato_push_progress_individual_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_progress_topic`
--
ALTER TABLE `legato_push_progress_topic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_subscription`
--
ALTER TABLE `legato_push_subscription`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_topic`
--
ALTER TABLE `legato_push_topic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_push_topic_group`
--
ALTER TABLE `legato_push_topic_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_complete`
--
ALTER TABLE `legato_sms_complete`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_complete_archive`
--
ALTER TABLE `legato_sms_complete_archive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_cores`
--
ALTER TABLE `legato_sms_cores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_delivery`
--
ALTER TABLE `legato_sms_delivery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_delivery_archive`
--
ALTER TABLE `legato_sms_delivery_archive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_message`
--
ALTER TABLE `legato_sms_message`
  MODIFY `msg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_message_archive`
--
ALTER TABLE `legato_sms_message_archive`
  MODIFY `msg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_progress`
--
ALTER TABLE `legato_sms_progress`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_receiver`
--
ALTER TABLE `legato_sms_receiver`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legato_sms_receiver_archive`
--
ALTER TABLE `legato_sms_receiver_archive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `mobile_version`
--
ALTER TABLE `mobile_version`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_access_token`
--
ALTER TABLE `users_access_token`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
