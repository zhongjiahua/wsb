<?php 
namespace Common\Models;

use Model;
use Maatwebsite\Excel\Facades\Excel;
use Common\Models\CommonModel;

/**
 * Import Model
 */
class Import extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_import';
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'excel_file' => 'required',
    ];


    const IMPORTED_TRUE = 1;
    const IMPORTED_FALSE = 0;
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['model'];

    /**
     * @var array Relations
     */
    public $attachOne = [
        'excel_file' => 'System\Models\File' ,'public' => false
    ];
    public $attachMany = [];

    public function beforeCreate(){
        parent::beforeCreate();
        $this->imported=self::IMPORTED_FALSE;
    }
    public function afterSave(){

    }
}
