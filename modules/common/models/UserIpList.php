<?php
namespace Common\Models;

class UserIpList extends CommonModel{

    protected $table = 'user_ip_list';
    const TYPE_WHITE = 1;
    const TYPE_BLACK = 0;

    /**
     *  Check ip exists or not.
     * @param $ip
     * @return bool
     */
    public static function checkIpExist($ip){
        $res = static::where(['ip_address' => $ip]) -> count();
        return $res > 0;
    }

    /**
     *  Check ip is white or black
     * @param $ip
     * @return mixed
     */
    public static function checkIpStatus($ip){
        $res = static::where(['ip_address' => $ip]) -> first();
        return $res -> type;
    }

    /**
     * Add white or black ip to user ip list
     * @param null $user_id
     * @param null $ip
     * @param int  $type
     * @return bool
     * @throws \ErrorException
     */

    public static function addIp($user_id = null,$ip = null,$type = 1){
        if(empty($ip)){
            throw new \ErrorException('Ip address required');
        }
        if(empty($user_id)){
            throw new  \ErrorException('User ID required');
        }

        if(!self::checkIpExist($ip)){
            $ips = new UserIpList();
            $ips -> user_id = $user_id;
            $ips -> type = $type;
            $ips -> ip_address = $ip;
            if(!$ips -> save()){
                return false;
            }
        }
        return true;
    }

    /**
     * Lock or unlock user ip
     * @param     $ip
     * @param int $type
     * @return bool
     * @throws \ErrorException
     */
    public static function changeIpStatus($ip,$type = 1){
        if(empty($ip)){
            throw new \ErrorException('Ip address required');
        }
        if(!self::checkIpExist($ip)){
            \Flash::error('No this ip address');
        }
        $res = static::where(['ip_address' => $ip]) -> update(['type' => $type]);
        if($res){
            return true;
        }
        return false;
    }
}
?>