<?php
namespace Common\Models;

class SettingCountryModel extends CommonModel{

    public $table = 'setting_country';

    /**
     * Get Country by key value
     * @param int $status
     * @return array
     */
    public static function getCountryByKeyValue($status = 1){
        $res = static::where(['status' => $status]) -> orderBy('sort','asc') -> get();
        $arr = [];
        if($res){
            foreach($res as $country){
                $arr[$country -> id] = $country -> name.' ( '.$country -> code.' ) ';
            }

        }
        return $arr;
    }
}
?>