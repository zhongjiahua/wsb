<?php
namespace Common\Models;

use Common\Classes\StatusService;
use Illuminate\Support\Facades\Lang;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\SoftDelete;
use October\Rain\Support\Traits\Singleton;
use October\Rain\Database\Traits\Validation;
use Legato\AuditTrail\Traits\Auditable;
use Common\Traits\LegatoSoftDelete;

class CommonModel extends Model{

	// Trait offered by Legato Audit Trail plugin
	use Auditable;
    use Singleton;
    use SoftDelete;
    use LegatoSoftDelete;
    use Validation;

    public $rules = [];
    public $customMessages = [];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [];
    /**
     * eager loading usage, please define in own model
     */
    public $translate_model_type = 'Common\Models\CommonModel';

    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    public $primaryKey = 'id';
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->translate_model_type = get_called_class();
    }

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Return status options
     * @return array
     */
    public function getStatusOptions(){
//        return [
//            static::STATUS_OFF  => Lang::get('common::lang.list.status.column_switch_false'),
//            static::STATUS_ON   => Lang::get('common::lang.list.status.column_switch_true')
//        ];
        $ret = array(
            StatusService::STATUS_ACTIVE => Lang::get('backend::lang.base_status.status_active'),
            StatusService::STATUS_SUSPEND => Lang::get('backend::lang.base_status.status_suspend'));
        return $ret;
    }

    /**
     *  set active status
     * @param $status
     * @return array
     */
    public function activateOrDeactivate($status){
        if((int)$this -> status === $status){
            $message = $status === static::STATUS_ON ? 'item_already_activated' : 'item_already_deactivated';
            return
                [   'status' => 0,
                    'info' => \Lang::get('common::lang.message.'.$message)
                ];
        }
        $this -> status = $status;
        $this -> forceSave();
        return ['status' => 1];
    }

    /**
     * Password validate. format : 6 length with number + letter
     * @param $post
     * @param $rules
     * @throws \ValidationException
     */
    public static function strengthenPassword($post,$rules){

        if ( isset($post['password']) && !empty($post['password'])) {
            $validator = \Validator::make(post(), $rules);
            if (!preg_match("/^(?![^a-zA-Z]+$)(?!\D+$).{6,}$/", $post['password'])) {
                $validator->errors()->add('x', 'asdfasd');
            }
            $error = $validator->messages();
            if ($error->has('x')) {
                throw new \ValidationException(['password' => 'Password must contain letter and number']);
            }
        }
    }
    /**
     * @todo testing
     */
    public function scopeWhereUt($query,$ut=null,$table = []){
        if($ut){
            if($table){
                $query->where(function($query2)use($ut,$table){
                    foreach ($table as $key => $value) {
                        $query2->orWhere($value.'.updated_at', '>', $ut);
                    }
                });
            }else{
                $query->where($this->table.'.updated_at', '>', $ut);
            }
        }
        $query->withTrashed();
    }
    
    /**
     * left join with translate field
     * e.g. 
     *  $models = Model::where('x','x')->withTranslate('en')->first();
     *  $title = $models->getTranslatedField('title','en');
     *
     * @param string $query 
     * @param string $lang  refer to rainlab_translate_locales->code 
     */
    public function scopeWithTranslate($query,$lang = null){
      if(is_null($lang)){
        $lang = \App::getLocale();
      }
      $sql_lang = str_replace('-', '_', $lang); 
      $select = ['rainlab_translate_attributes.locale as locale_'.$sql_lang,
          'rainlab_translate_attributes.model_id as model_id_'.$sql_lang,
          'rainlab_translate_attributes.model_type as model_type_'.$sql_lang,
          'rainlab_translate_attributes.attribute_data as attribute_data'];
      /*foreach ($this->translatable as $key => $value) {
          $select[] = \DB::raw('JSON_UNQUOTE(JSON_EXTRACT(attribute_data,"$.'.$value.'")) as '.$value.'_'.$sql_lang);
          //$select[] = \DB::raw('JSON_EXTRACT(attribute_data,"$.'.$value.'") as '.$value.'_'.$sql_lang);
          //$select[] = \DB::raw("TRIM(both '".'"'."' FROM JSON_EXTRACT(attribute_data,".'"$.'.$value.'")) as '.$value.'_'.$sql_lang);
      }*/
      $subq = \DB::table('rainlab_translate_attributes')
          ->select($select);
          $qqSql = $subq->toSql();
      $query->leftJoin(\DB::raw('(' . $qqSql. ') AS translate'),function($join)use($lang,$sql_lang) {
          $join->on($this->table.'.'.$this->primaryKey, '=', 'translate.model_id_'.$sql_lang);
          $join->where('translate.model_type_'.$sql_lang,$this->translate_model_type);
          $join->where('translate.locale_'.$sql_lang,$lang);
      });
    }

    /**
     * get translated field (use with withTransalte)
     * e.g. 
     *  $models = Model::where('x','x')->withTranslate('en')->first();
     *  $title = $models->getTranslatedField('title','en');
     *
     * @param string $field translatable field name
     * @param string $lang  refer to rainlab_translate_locales->code 
     */
    public function getTranslatedField($field){
        $data = json_decode($this->attribute_data);
        if(isset($data->$field)){
            return $data->$field;
        }
        return $this->$field;
    }

    /**
     * @todo 
     */
    public function translateAll(){
        
    }

    /**
     * Transform object
     *
     * @param Collection | item             $input          collection or single model item
     * @param TransformerAbstract | null    $transformer    $item->tranfomer will be used if null
     */
    protected static function transform($input, $transformer=null, $key = null){
        if($input instanceof \Illuminate\Support\Collection){
            return self::transformCollection($input, $transformer, $key = null);
        }else{
            return self::transformItem($input, $transformer, $key = null);
        }
    }

    protected static function transformCollection($collection, $transformer, $key = null)
    {
        if(is_null($transformer)){
            if($collection->count() == 0){
                return $collection;
            }
            $item = $collection->first();
            $transformer = $item->transformer;
            if(!isset($transformer)){
                return $collection;
            }
        }
        $fractal = new \League\Fractal\Manager();
        $resource = new \League\Fractal\Resource\Collection($collection, new $transformer, $key);
        return current($fractal->createData($resource)->toArray());
    }

    protected static function transformItem($item, $transformer,$key = null)
    {
        if(is_null($transformer)){
            $transformer = $item->transformer;
            if(!isset($transformer)){
                return $item;
            }
        }
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\ArraySerializer());
        $resource = new \League\Fractal\Resource\Item($item, new $transformer, $key);
        return $fractal->createData($resource)->toArray();
    }

}

?>