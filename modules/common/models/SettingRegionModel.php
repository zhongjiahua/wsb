<?php
namespace Common\Models;

class SettingRegionModel extends CommonModel{

    public $table = 'setting_region';

    /**
     * Get region by id , output array with key - value
     * @param null $id
     * @return array
     */
    public static function getRegionByKeyValue($id = null){
        $where['status'] = 1;
        if(!empty($id)){
            $where['id'] = $id;
        }
        $res  = static::where($where) -> orderBy('sort','asc') -> get();
        $arr = [];
        if($res){
            foreach($res as $region){
                $arr[$region -> id] = $region -> name;
            }
        }
        return $arr;
    }
}
?>