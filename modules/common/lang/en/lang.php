<?php
return [
    'no'        => 'No',
    'yes'       => 'Yes',
    'status'    => 'Status',
    'language_id'       => 'Language',
    'please_select'     => 'Please Select',
    'validation_successful' => 'Validation Successful',
    'email_verify_failed'   => 'An invalid link or link has been expired',
    'activate_failed_caused_system' => 'Sorry! Failed to activate your account caused by system',
    'account_activated_already'     => 'The account has been activated. No need to activate again.',
    'btn'       => [
        'create'        => 'Create',
        'activate'      => 'Activate',
        'suspend'       => 'Suspend',
        'cancel'        => 'Cancel',
        'update'        => 'Update'
    ],
    'message' => [
        'successful'        => 'Successful',
        'successfully'      => 'successfully',
        'nothing_selected' => 'Nothing Selected!',
        'activate_selected_confirm'     => 'Are you sure to activate selected?',
        'deactivate_selected_confirm'   => 'Are you sure to deactivate selected?',
        'delete_selected_confirm'       => 'Are you sure to delete selected?',
        'item_already_activated'        => 'Item activated already.',
        'item_already_deactivated'      => 'Item deactivated already.',
        'delete_confirm'                => 'Do you really want to delete this item?',
        'email_exists'                  => 'An same E-mail has been existed!'
    ],
    'mail_templates' => [
        'show_with_language' => 'List with Language',
        'successful'         => 'Successful',

    ],
    'list' =>[
        'status' => [
            'column_switch_true'    => 'Active',
            'column_switch_false'   => 'Inactive'
        ],
        'export' => [
            'export_csv'    => 'Export CSV',
            'export_excel'  => 'Export Excel',
            'export'       => 'Export'
        ]
    ],
    'form'  => [
        'label' => [
            'saving' => 'Saving'
        ]
    ],
    'button' => [
        'activate_selected'     => 'Activate selected',
        'deactivate_selected'   => 'Deactivate selected'
    ]

]
?>