<?php
return [
    'no'        => '否',
    'yes'       => '是',
    'status'    => '狀態',
    'language_id'       => '語言',
    'please_select'     => '請選擇',
    'validation_successful' => '驗證成功',
    'email_verify_failed'   => '無效的鏈接或鏈接已過期',
    'activate_failed_caused_system' => '抱歉！暫時無法激活您的帳戶',
    'account_activated_already'     => '帳戶已被激活。無需再次激活。',
    'btn'       => [
        'create'        => '創建',
        'activate'      => '激活',
        'suspend'       => '暫停',
        'cancel'        => '取消',
        'delete'        => '刪除',
        'deactivate'        => '暫停',
        'update'        => '更新',
        'selected'        => '已選中'
    ],
    'message' => [
        'successful'        => '成功',
        'successfully'      => '成功',
        'nothing_selected' => '沒有選擇！',
        'selected_confirm_1'     => '您確定要',
        'selected_confirm_2'     => '已選中嗎？',
        'activate_selected_confirm'     => '您確定要激活已選中嗎？',
        'deactivate_selected_confirm'   => '您確定要停用所選的嗎？',
        'delete_selected_confirm'       => '您確定要刪除所選的嗎？',
        'item_already_activated'        => '已激活。',
        'item_already_deactivated'      => '已停用。',
        'delete_confirm'                => '你真的想要刪除這個嗎？',
        'email_exists'                  => '電子郵件已經存在！'
    ],
    'mail_templates' => [
        'show_with_language' => '語言列表',
        'successful'         => '成功',

    ],
    'list' =>[
        'status' => [
            'column_switch_true'    => '有效',
            'column_switch_false'   => '無效'
        ],
        'import' => [
            'import_csv'    => '導入CSV',
            'import_excel'  => '導入Excel',
            'import'       => '導入'
        ],
        'export' => [
            'export_csv'    => '導出CSV',
            'export_excel'  => '導出Excel',
            'export'       => '導出'
        ]
    ],
    'form'  => [
        'label' => [
            'saving' => '保存中'
        ]
    ],
    'button' => [
        'download' => '下载',
        'delete_selected' => '刪除已選中',
        'activate_selected' => '啟用已選中',
        'deactivate_selected' => '停用已選中',
        'export_as' => '導出為',
        'import' => '導入'
    ]
]
?>
