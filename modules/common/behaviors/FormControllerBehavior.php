<?php
namespace Common\Behaviors;

use Backend\Behaviors\FormController;
use Common\Classes\StatusService;
use Common\Models\CommonModel;

class FormControllerBehavior extends FormController {

    /**
     * Custom redirect url
     * @param $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function customRedirect($model){
        $custom_params =  $this -> controller -> update_formDelete($model);
        if(!empty($custom_params)){
            $custom_params = http_build_query($custom_params);
        }
        $redirect = $this -> getRedirectUrl($this -> context);
        if(!empty($redirect)){
            $redirect = explode('/',$redirect);
            array_pop($redirect);
            array_pop($redirect);
            $redirect_url = join('/',$redirect);
            if(!empty($custom_params)){
                $redirect_url = $redirect_url.'/?'.$custom_params;
            }
        }
        return \Backend::redirect($redirect_url);
    }


    /**
     * Get redirectUrl from config_form.yaml
     * @param null $context
     * @return mixed|string
     */
    protected function getRedirectUrl($context = null)
    {
        $redirectContext = explode('-', $context, 2)[0];
        $redirectSource = ends_with($context, '-close') ? 'redirectClose' : 'redirect';
        // Get the redirect for the provided context
        $redirects = [$context => $this->getConfig("{$redirectContext}[{$redirectSource}]", '')];
        // Assign the default redirect afterwards to prevent the
        // source for the default redirect being default[redirect]
        $redirects['default'] = $this->getConfig('defaultRedirect', '');
        if (empty($redirects[$context])) {
            return $redirects['default'];
        }
        return $redirects[$context];
    }

    public function create_onSave($context = null)
    {
        $this->context = strlen($context) ? $context : $this->getConfig('create[context]', self::CONTEXT_CREATE);

        $model = $this->controller->formCreateModelObject();
        $model = $this->controller->formExtendModel($model) ?: $model;

        $this->initForm($model);

        $this->controller->formBeforeSave($model);
        $this->controller->formBeforeCreate($model);

        $modelsToSave = $this->prepareModelsToSave($model, $this->formWidget->getSaveData());
        \Db::transaction(function () use ($modelsToSave) {
            foreach ($modelsToSave as $modelToSave) {
                $modelToSave->save(null, $this->formWidget->getSessionKey());
            }
        });

        $this->controller->formAfterSave($model);
        $this->controller->formAfterCreate($model);

        \Flash::success("Create".' ' .\Lang::get('common::lang.message.successfully'));

        if ($redirect = $this->makeRedirect('create', $model)) {
            return $redirect;
        }
    }

    /**
     *  Overwrite update_onSave to add custom language
     * @param null $recordId
     * @param null $context
     * @return mixed|\Redirect
     */
    public function update_onSave($recordId = null,$context = null)
    {
        $this->context = strlen($context) ? $context : $this->getConfig('update[context]', self::CONTEXT_UPDATE);
        $model = $this->controller->formFindModelObject($recordId);
        $this->initForm($model);

        $this->controller->formBeforeSave($model);
        $this->controller->formBeforeUpdate($model);

        $modelsToSave = $this->prepareModelsToSave($model, $this->formWidget->getSaveData());
        \Db::transaction(function () use ($modelsToSave) {
            foreach ($modelsToSave as $modelToSave) {
                $modelToSave->save(null, $this->formWidget->getSessionKey());
            }
        });

        $this->controller->formAfterSave($model);
        $this->controller->formAfterUpdate($model);

        \Flash::success('Update ' . \Lang::get('common::lang.message.successful'));

        if ($redirect = $this->makeRedirect('update', $model)) {
            return $redirect;
        }
    }
    
    public function update_onBulk($recordId = null,$context = null)
    {
        $bulkAction = post('bulk');
        $bulk_detail = $this->controller->vars['bulk_list'][$bulkAction];
        $model = $this->controller->formFindModelObject($recordId);
        $this->initForm($model);
        
        $this->controller->formBeforeSave($model);
        $this->controller->formBeforeUpdate($model);

        $flag = true;
        if( (isset($bulk_detail['default']) && !$bulk_detail['default'])
            || !isset($bulk_detail['default'])
            ){
            if(isset($bulk_detail['function'])){
                //use function
                $function = $bulk_detail['function'];
                $res = $model->$function();
                if($res['status'] === StatusService::STATUS_SUSPEND){
                    \Flash::error(sprintf($res['info'],$recordId));
                    $flag = false;
                }
            }else{
                //use required and update
                if(isset($bulk_detail['required'])){
                    foreach ($bulk_detail['required'] as $field => $detail) {
                        if($model->$field != $detail['value']){
                            \Flash::error(sprintf($detail['message'],$recordId));
                            $flag = false;
                            break;              
                        }
                    }
                }
                if($flag==true){
                    foreach ($bulk_detail['update'] as $field => $value) {
                        if($model->$field != $value){
                            
                            $model->$field = $value;
                        }
                        $model->save();
                    }
                }
            }

            
        }else{
            //default bulk actions
            switch($bulkAction){
               case 'delete':
                    $model -> delete();
                    \Flash::success('Delete ' .\Lang::get('common::lang.message.successfully'));
                    //return $this -> customRedirect( $model);
                    break;
                case 'activate':
                    $res = $model -> activateOrDeactivate(CommonModel::STATUS_ON);
                    if($res['status'] === StatusService::STATUS_SUSPEND){
                        \Flash::error($res['info']);
                        $flag = false;
                    }
                    break;
                case 'deactivate':
                    $res = $model -> activateOrDeactivate(CommonModel::STATUS_OFF);
                    if($res['status'] === StatusService::STATUS_SUSPEND){
                        \Flash::error($res['info']);
                        $flag = false;
                    }
                    break;
            }

        }
        if($flag==true)
            \Flash::success($bulk_detail['label'].' ' .\Lang::get('common::lang.message.successfully'));
        if ($flag==true && $redirect = $this->makeRedirect('update', $model) ) {
            if(isset($bulk_detail['redirect']['destination'])){
                return \Backend::redirect($bulk_detail['redirect']['destination']);
            }
            return $redirect;
        }else{
            return back();
        }
    }

}
?>