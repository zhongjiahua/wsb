<?php
namespace  Common\Behaviors;

use Backend\Classes\ControllerBehavior;

class ListControllerBehavior extends ControllerBehavior{

    public $controller;
    public $listConfig;
    public $model;

    public function __construct($controller){
        parent::__construct($controller);
        $this -> controller = $controller;
        $this -> listConfig = $this -> makeConfig($controller -> listConfig);
        $this -> model = $this -> listConfig -> modelClass;
    }

    /**
     *  Export to csv
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function onExport() {
        $controllerId = $this -> controller -> getId();
        list($controller_name,$action) = explode('-',$controllerId);

        $path = '/storage/app/media/downloads';
        $file_name = $controller_name.time().'.csv';

        if(!file_exists(base_path().$path)){
            @mkdir(base_path().$path,0777,true);
        }
        @chmod(base_path().$path,0777);
        $model = $this -> model;
        $list = $model::all()->toArray();
        array_unshift($list, array_keys($list[0]));

        $FH = fopen( base_path().$path.'/'.$file_name, 'w');
        foreach ($list as $row) {
                fputcsv($FH, $row);
        }
        fclose($FH);
        return \Redirect::to($path.'/'.$file_name);
    }*/

}
?>