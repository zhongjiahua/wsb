<?php 

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class DBCommonImports extends Migration
{
    public function up()
    {
        Schema::create('legato_import', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('model',255);
            $table->tinyInteger('imported')->default(0);
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_import');
    }
}
