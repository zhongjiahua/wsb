<?php
namespace Common\Helpers;

class Functions{

    /**
     *  Get path of common directory. special used in htm layout.
     *  Usage: Common\Helpers\Functions:: common_path($path);
     * @param $path
     * @return string
     */
    public static function common_path($path){
        return base_path('modules/common/'.$path);
    }

    /**
     * Add please select for dropdown list
     * @param null $option
     * @return null
     * @throws \ErrorException
     */
    public static function addSelectedForOptions($option = null){
        if(is_null($option)){
            throw new \ErrorException('Select option needs');
        }
        array_unshift($option,\Lang::get('common::lang.please_select'));
        return $option;
    }

    public static function checkActionPermission($controller,$key_action = null){
        $user = \BackendAuth::getUser();
        $current_module_action_permissions = $controller -> requiredActionsPermissions;
        $current_user_permissions = $controller -> user_permissions;
        if(isset($current_module_action_permissions[$key_action])){
            foreach ($current_module_action_permissions[$key_action] as $permission) {
                if(!$user->hasAccess($permission)){
                    return false;
                }
            }
        }
        return true;
    }

    /** Get all constants from a class
     * @param $class
     * @return array
     */
    public static function getConstants($class){
        $oClass = new \ReflectionClass($class);
        $arr1 = $oClass->getConstants();
        return $arr1;
    }

    public static function addQuotesForArray($arr = []){
          array_walk($arr,function($item){
          return  '"'.$item.'",';
        });
        return $arr;
    }
}
?>