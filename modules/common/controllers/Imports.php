<?php namespace Common\Controllers;

use Common\Models\Import;
use Common\Classes\ImportService;

/**
 * Event Logs controller
 *
 * @package october\system
 * @author Alexey Bobkov, Samuel Georges
 */
class Imports extends \CommonController
{
    /**
     * @var array Permissions required to view this page.
     */
    public $requiredPermissions = ['system.imports_log'];

    /**
     * Constructor.
     */
    //public function __construct()
    //{
    //    parent::__construct();
//
    //    BackendMenu::setContext('October.System', 'system', 'settings');
    //    SettingsManager::setContext('October.System', 'event_logs');
    //}
    public function test($id){
        $import = Import::find($id); 
        ImportService::import_excel($import);
    }
    
}
