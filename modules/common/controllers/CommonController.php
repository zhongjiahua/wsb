<?php
namespace Common\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;
use Common\Behaviors\FormControllerBehavior;
use Common\Behaviors\ListControllerBehavior;
use Common\Classes\StatusService;
use Common\Classes\ExportService;
use Common\Classes\ImportService;
use Common\Models\CommonModel;

class CommonController extends Controller{

    use \Legato\Api\Traits\UtilityFunctions;

    public $user_permissions;
    public $requiredPermissions = [];
    public $requiredActionsPermissions=[];


    public $implement = [
        FormController::class,
        ListController::class,
        FormControllerBehavior::class,
        ListControllerBehavior::class,

    ];
    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    /**
     * Internal Usage
     */
    public $formConfigObj;
    public $listConfigObj;
    protected $importWidget;

    /**
     * Optional
     * To specific theimport behaviour 
     */
    public $import_class = null;

    public function __construct(){
        parent::__construct();

        $this->importWidget = $this->createImportWidget();
        $this->formConfigObj = $this -> makeConfig($this -> formConfig);
        $this->listConfigObj = $this -> makeConfig($this -> listConfig);

        // set global vars for layout

        // define whether to hide create new button , default: false;
        $this -> vars['hide_create'] = false;

        // define whether to  use right sidebar,default: false
        $this -> vars['with_side'] = false;

        // define whether to show save and close button , default: true
        $this -> vars['show_save_redirect'] = true;

        // define cancel button url
        if($this->formConfigObj->defaultRedirect)
            $this -> vars['cancel_url'] = $this->formConfigObj->defaultRedirect;
        else
            $this -> vars['cancel_url']='';

        // define form layout center
        $this -> vars['form_layout_center'] = true;

        // define whether to show update
        $this -> vars['show_update'] = false;

        // define form update button
        $this -> vars['show_update_btn'] = true;

        // define whether to hide import button , default: true;
        $this -> vars['hide_import'] = true;
        $this -> vars['import_url'] = \Backend::url($this -> vars['cancel_url'].'/import');

        // define whether to hide export button , default: true;
        $this -> vars['hide_export'] = true;
        $this -> vars['export_file_name'] = 'common';
        $this -> vars['export_fields_title'] = NULL;
        $this -> vars['export_column'] = NULL;
        $this -> vars['export_file_type'] = \Config::get('common.export_file_type');
        // reserved parameter
        $this -> vars['exporting']=null;

        // includes custom css or js from here
        $this -> addCss(\Url::asset('modules/common/assets/css/common.css'));
        $this -> addJs(\Url::asset('modules/common/assets/js/bulk-actions.js'));
        $this -> _bulks_list();
        $this -> _breadcrumb();

    }
    protected function createImportWidget()
    {
        $config = $this->makeConfig('modules/common/models/import/fields.yaml');
        $config->alias = 'importform';
        $config->arrayName = 'create';
        $config->model = new \Common\Models\Import;
        $widget = $this->makeWidget('Backend\Widgets\Form', $config);
        $widget->bindToController();
        return $widget;
    }
    public function import()
    {
        $hasAccess = \Common\Helpers\Functions::checkActionPermission($this,'import');
        if(!$hasAccess){
            \Flash::error('You do not have the permissions to import data.');
            return \Backend::redirect($this -> vars['cancel_url']);
        }else if($this -> vars['hide_import']){
            \Flash::error('Import function is disabled.');
            return \Backend::redirect($this -> vars['cancel_url']);
        }else{
            $this -> pageTitle = 'Import '.$this->formConfigObj->name;
            $this->vars['importWidget'] = $this->importWidget;
            if(isset($this->vars['breadcrumb'])){
                array_push($this -> vars['breadcrumb']['child'],
                    [
                        'title' => 'Import',
                    ]
                );
            }
            $this->vars['target'] = \Backend::url($this->formConfigObj->defaultRedirect.'/onImport');
            return $this->makePartial(\Common\Helpers\Functions::common_path( 'layouts/import'));
        }
    }
    public function onImport(){
        $hasAccess = \Common\Helpers\Functions::checkActionPermission($this,'import');
        if(!$hasAccess){
            \Flash::error('You do not have the permissions to import data.');
            return \Backend::redirect($this -> vars['cancel_url']);
        }else if($this -> vars['hide_import']){
            \Flash::error('Import function is disabled.');
            return \Backend::redirect($this -> vars['cancel_url']);
        }else{
            $data = $this->importWidget->getSaveData();
            $data['model'] = $this->listConfigObj->modelClass;
            $import = new \Common\Models\Import;
            $import->fill($data);
            $import->save(null,\Input::get('_session_key'));


            $message = ImportService::import_excel($import,$this->import_class);
            //$message=true;

            if(!$message){
                \Flash::success('Import Successful');
                return \Backend::redirect($this->formConfigObj->defaultRedirect);
            }else{
                \Flash::error($message);
                return \Backend::redirect($this->formConfigObj->defaultRedirect.'/import');
            }
        }
    }

    /**
     * Bulk action for index page
     * @param null $model_class
     * @param null $action_lists array
     * @return mixed
     * @throws \SystemException
     */
    public function bulkAction($model_class = null,$action_lists = null){

        if(is_null($model_class)){
            throw new \SystemException('Model name needed!');
        }
        if(is_null($action_lists)){
            throw new \SystemException('Bulk action lists needed!');
        }
        $model = new $model_class;
        if(($bulkAction = post('action')) && ($checkIds = post('checked')) &&  is_array($checkIds) && count($checkIds)){
            foreach($checkIds as $objId){

                if(!$obj = $model::withTrashed() -> find($objId)){
                    continue;
                }
                $flag = true;
                if( (isset($action_lists[$bulkAction]['default']) && !$action_lists[$bulkAction]['default'])
                    || !isset($action_lists[$bulkAction]['default'])
                    ){
                    if(isset($action_lists[$bulkAction]['function'])){
                        //use function
                        $function = $action_lists[$bulkAction]['function'];
                        $res = $obj->$function();
                        if($res['status'] === StatusService::STATUS_SUSPEND){
                            \Flash::error(sprintf($res['info'],$objId));
                            $flag = false;
                            break;
                        }
                    }else{
                        //use required and update
                        if(isset($action_lists[$bulkAction]['required'])){
                            foreach ($action_lists[$bulkAction]['required'] as $field => $detail) {
                                if($obj->$field != $detail['value']){
                                    \Flash::error(sprintf($detail['message'],$objId));
                                    $flag = false;
                                    break;
                                }
                            }
                        }
                        if($flag==true){
                            foreach ($action_lists[$bulkAction]['update'] as $field => $value) {
                                if($obj->$field != $value){
                                    $obj->$field = $value;
                                }
                                $obj->save();
                            }
                        }
                    }


                }else{
                    //default bulk actions
                    switch($bulkAction){
                        case 'delete':
                            $obj -> delete();
                            break;
                        case 'activate':
                            $res = $obj -> activateOrDeactivate(CommonModel::STATUS_ON);
                            if($res['status'] === StatusService::STATUS_SUSPEND){
                                \Flash::error($res['info']);
                                $flag = false;
                            }
                            break;
                        case 'deactivate':
                            $res = $obj -> activateOrDeactivate(CommonModel::STATUS_OFF);
                            if($res['status'] === StatusService::STATUS_SUSPEND){
                                \Flash::error($res['info']);
                            }
                            break;
                    }

                }
                if($flag == false)
                    break;

            }
            if($flag){
                \Flash::success(Ucfirst($bulkAction).' ' .\Lang::get('common::lang.message.successfully'));
            }
        }else{
            \Flash::error(\Lang::get('common::lang.message.nothing_selected'));
        }
        return true;
    }

    /**
     * Update listConfig, remove pagination for generate query for export
     */
    public function listGetConfig($definition = null){
        $ret = parent::listGetConfig($definition);
        if(isset($this->vars['exporting'])&&$this->vars['exporting']){
            $ret -> recordsPerPage = 0;
        }
        return $ret;
    }

    /**
     * Backup query for exporting usage
     */
    public function listextendQuery($query){
        $this->vars['query'] = $query;
    }

    public function index_onExport(){
        $hasAccess = \Common\Helpers\Functions::checkActionPermission($this,'export');
        if(!$hasAccess){
            \Flash::error('You do not have the permissions to export data.');
            return \Backend::redirect($this -> vars['cancel_url']);
        }else{
            if($memory_limit = \Config::get('common.export_memory_limit')){
                if(!is_null($memory_limit))
                    ini_set('memory_limit', $memory_limit);
            }
            $this->vars['exporting'] = 'true';//set gate for update list config
            $this->asExtension('ListController')->index();
            $this->listRender();
            $query = $this->vars['query'];
            $this->exportExtendquery($query);
            $rows = $query->get();
            $exportArray = $rows->toArray();
            $exportArray = $this->exportExtendarray($exportArray);
            switch ($this -> vars['export_column']) {
                case NULL:
                    break;
                
                default:
                    $export_columns = $this -> vars['export_column'];
                    foreach ($exportArray as $index => $row) {

//                        $sorted = array_only($row ,$export_columns);
//                        $sorted = array_merge(array_flip($export_columns), $sorted);
                        $temp = [];

                        foreach ($export_columns as $column) {
                            $temp[$column] = $row[$column];
                        }
                        $exportArray[$index] = $temp;
                    }
                    break;
            }

            $exportArray = $this->exportExtendarray($exportArray);
            $res = ExportService::export(post('action'),$exportArray,$this->vars['export_file_name'],$this->vars['export_fields_title']);

            if($res['status']==StatusService::STATUS_ACTIVE)
                return \Backend::redirect('exports/'.$res['file_name']);
            else
                return \Redirect::back();
        }
    }
    
    public function exportExtendquery($query){
        //$query->where('id',2);
    }
    public function exportExtendarray($exportArray){
        /*foreach ($exportArray as $key => $value) {
            $exportArray[$key]['id']=1;
        }*/
        return $exportArray;
    }

    /*public function index(){
        parent::index();
    }*/

    public function create(){
        $hasAccess = \Common\Helpers\Functions::checkActionPermission($this,'create');
        if(!$hasAccess){
            \Flash::error('You do not have the permissions to create.');
            return \Backend::redirect($this -> vars['cancel_url']);
        }else{
            parent::create();
            if(isset($this->vars['breadcrumb'])){
                array_push($this -> vars['breadcrumb']['child'],
                    [
                        'title' => 'Create',
                    ]
                );
            }
        }
    }

    public function preview($id){

        parent::preview($id);
        if(isset($this->vars['breadcrumb'])){
            array_push($this -> vars['breadcrumb']['child'],
                [
                    'title' => 'Preview',
                ]
            );
        }
        $this -> pageTitle =  $this -> getId();
    }


    public function update($id){
        $hasAccess = \Common\Helpers\Functions::checkActionPermission($this,'update');
        if(!$hasAccess){
            return \Backend::redirect($this->formConfigObj->defaultRedirect.'/preview/'.$id);
        }else{
            parent::update($id);
            if(isset($this->vars['breadcrumb'])){
                array_push($this -> vars['breadcrumb']['child'],
                    [
                        'title' => 'Update',
                    ]
                );
            }
            $this -> pageTitle = $this -> getId();
        }
    }

    public function update_formDelete($model = null){}

    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(            
                /**
                 * button label
                 */
                'label' => 'Delete',      
                /**
                 * button icon
                 */
                'icon'   => 'oc-icon-trash-o',
                /**
                 * button color, used in detail page,
                 * optional
                 */
                //'color' => '#777777', 
                /**
                 * use default action,
                 * optional (default false)
                 * (support delete,activate,deactivate)
                 */
                'default' => true,
                /**
                 * redirect after action success?
                 * optional(default false)
                 */
                'redirect' => array(
                        'enable' => true,
                        /**
                         * if destination is not setted, will redirect to list page
                         */
                        //'destination' => 'legato/news/news'
                    ),
            ),
        ];
    }

    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => $this->listConfigObj->title,
            'url'   => \Backend::url($this->formConfigObj->defaultRedirect)
        ];
        $this -> vars['breadcrumb']['child'] = [];
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            $this->bulkAction($this->listConfigObj->modelClass,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }


}
?>
