<?php namespace Common\Traits;

trait LegatoSoftDelete
{
    public static function bootLegatoSoftDelete()
    {

		static::extend(function($model) {
            $model->bindEvent('model.beforeDelete', function () use ($model) {
		        try{
		            if($model -> status != \Common\Classes\StatusService::STATUS_REMOVED){
		                $model -> status = \Common\Classes\StatusService::STATUS_REMOVED;
		                $model -> forcesave();
		            }
		        }catch(\Exception $e){
		            $className = get_class($model);
		            \Log::debug($className.' id: '.$model->id.' unable to delete because: '.$e->getMessage());
		        }
            });
        });

    }


}

?>
