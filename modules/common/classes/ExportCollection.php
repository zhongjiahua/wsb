<?php namespace Common\Classes;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;

class ExportCollection implements FromCollection, WithHeadings, WithEvents
{   
    private $heading;
    private $collection;

    // set the headings
    public function setHeadings(array $headings)
    {
        $this->heading = $headings;
    }
    // set the collection
    public function setCollection($data)
    {
        $this->collection = collect($data);
    }
    // get the headings
    public function headings(): array
    {
        return $this->heading;
    }

    // freeze the first row with headings
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    // get the data
    public function collection()
    {
        return $this->collection;
    }

}