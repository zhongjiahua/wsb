<?php
namespace Common\Classes;

use Common\Helpers\Functions;

class StatusService {

    // define common status
    const STATUS_ACTIVE         = 1;
    const STATUS_SUSPEND        = 0;
    const STATUS_REMOVED        = 9;

    // define payment status
    const PAYMENT_STATUS_PENDING    = 0;
    const PAYMENT_STATUS_PAID       = 1;
    const PAYMENT_STATUS_CANCELLED  = 2;

    // define remove from mailing
    const REMOVE_FROM_MAILING_YES   = 1;
    const REMOVE_FROM_MAILING_NO    = 0;

    const APP_USER_LOGIN_TYPE_COMMON    = 1;
    const APP_USER_LOGIN_TYPE_FACEBOOK  = 2;
    const APP_USER_LOGIN_TYPE_GOOGLE    = 3;

    // define child activity progress status
    const CHILD_ACTIVITY_STATUS_UNFINISHED   = 0;
    const CHILD_ACTIVITY_STATUS_COMPLETED    = 1;

    // define cancel survey form answer
    const FORM_CANCEL_SURVEY_ANSWER_NO      = 0;
    const FORM_CANCEL_SURVEY_ANSWER_YES     = 1;
    //  other definition
    // const ...


    /** Get a certain status by key-value;
     *
     * Usage:
     *      StatusService:: getStatusOptions('status')
     * output: Array(
                    [1] => Active
                    [0] => Suspend
                    [9] => Removed
                )
     * @param null $key_word
     * @param null $class
     * @return array
     * @throws \ErrorException
     */

    public static function getStatusOptions($key_word = null,$class = null){
        if(empty($key_word)){
            throw new \ErrorException('Please provide key words');
        }
        $key_word = strtoupper($key_word);
        $class = is_null($class) ?  __CLASS__ : $class;
        $all_consts = Functions::getConstants($class);
        $arr = [];
        if(!empty($all_consts)){
            foreach($all_consts as $key => $val){
                if(starts_with($key,$key_word)){
                    $key_var = explode('_',$key);
                    if(!isset($arr[$val])){//prevent const in parent(commonmodel) orverride const in model
                        $arr[$val] = ucfirst(strtolower(end($key_var)));
                    }
                }
            }
        }
        return $arr;
    }
}
?>