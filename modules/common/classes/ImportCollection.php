<?php

namespace Common\Classes;

use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportCollection implements ToModel, WithHeadingRow
{
    private $model_class;
    public function setModel($model){
      $this->model_class = $model;
    }
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
      $model_class = $this->model_class;
      $model = new $model_class();
      foreach ($row as $key => $value) {
        $model->$key = $value;

      }
      $model->save();
      return $model;
    }
}