<?php
namespace Common\Classes;


use Backend\Models\User;
use Common\Classes\Email\UserChangedIpConfirmation;
use Common\Helpers\Functions;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Member\Child\Models\Program;

class EmailService extends Mailable {

    public $user;
    public $params;
    public $from_email;
    public $from_name ;
    public $to ;
    public $cc ;
    public $bcc;
    public $file;

    private static $view_path ;

    public function __construct($user,$params = null){

        $this -> to = $user -> email;
        if(!empty($params)){

            if(isset($params['to']) && !empty($params['to'])){
                $this -> to = $params['to'];
            }
            if(isset($params['cc']) && !empty($params['cc'])){
                $this -> cc = $params['cc'];
            }

            if(isset($params['bcc']) && !empty($params['bcc'])){
                $this -> bcc = $params['bcc'];
            }

            if(isset($params['file']) && !empty($params['file'])){
                $this -> file = $params['file'];
            }
            $this -> params = $params;
        }
    }

    public function build(){
        if(!empty($this -> send_from)){
            $this -> from($this -> send_from);
        }
    }

    /**
     *  Send email
     * @param $target
     * @return mixed
     */
    public function sendMail($target){
        $res = Mail::to($this -> to);
        if(!empty($this -> cc)){
            $res -> cc($this -> cc);
        }
        if(!empty($this -> bcc)){
            $res -> bcc($this -> bcc);
        }
        return  $res -> send(new $target($this -> params));
    }

    /**
     * set mail template view path
     */
    public static function setMailViewPath(){
        self::$view_path = Functions::common_path('layouts/mailTemplates/');
    }

    /**
     * get common email template view path
     * @return mixed
     */
    public static function getDefaultViewPath(){
        self::setMailViewPath();
        return self::$view_path;
    }


}
?>