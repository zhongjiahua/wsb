<?php
namespace Common\Classes\Socialize;

class Socialize {

    /**
     * Register for user from other portals
     * Usage: Socialize::register($client,$params) -> signIn() or -> validateToken();
     * @param null $name
     * @param null $params
     * @return Facebook|Google
     * @throws \ErrorException
     */
    static function register($name = null , $params = null){
        if(is_null($name)){
            throw new \ErrorException('Socialization provider required. Google or Facebook ?');
        }
        if(empty($params)){
            throw new \ErrorException('User details required');
        }
        $name = strtolower($name);
        switch ($name){
            case 'google':
                return new Google($name,$params);
                break;
            case 'facebook':
                return new Facebook($name,$params);
                break;
            default:
                break;
        }
    }
}
?>