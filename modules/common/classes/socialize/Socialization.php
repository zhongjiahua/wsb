<?php
namespace Common\Classes\Socialize;

use RainLab\User\Models\User;

abstract class Socialization{

    public $details;
    public $client;

    public function __construct($client,$details)
    {
        $this -> details = $details;
        $this -> client = $client;
    }

    /**
     * Sign In for user
     * @return mixed
     */
    abstract function signIn();

    /**
     * Validate token from socialization provider ,eg. google / facebook
     * @return mixed
     */
    abstract function validateToken();

    /** Register for new user from other client;
     * @param $client
     * @param $params
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function registerMember($client,$params){

        if(User::findByEmail($params['email'])){
            return \Response::json([
                'status' => 0,
                'message' => \Lang::get('common::lang.message.email_exists')
            ]);
        }
        $google_token = $face_token =  '';
        if($client === 'google'){
            $google_token  = $params['social_token'];
        }else{
            $face_token  = $params['social_token'];
        }
        $user = \DB::table('users') -> insertGetId([
            'email'         => $params['email'],
            'username'      => $params['email'],
            'password'      => \Hash::make($params['password']),
            'region_id'     => $params['region'],
            'login_type'    => $params['login_type'],
            'activated_at'  => now(),
            'is_activated'  => 1,
            'remove_from_mailing'   => 0,
            'google_token'      => $google_token,
            'facebook_token'    => $face_token,
            'status'            => 1,
            'report_lang_id'    => isset($params['lang']) ? $params['lang'] : 1,
        ]);
        if(!$user){
            return \Response::json([
                'status'    => 0,
                'message'   => \Lang::get('common::lang.message.register_failed'),
                'data'     => null
            ]);
        }

        return \Response::json([
            'status'    => 1,
            'message'   => \Lang::get('common::lang.message.register_success'),
            'data'      => $user
        ]);
    }
}
?>