<?php
namespace Common\Classes\Socialize;

class Facebook extends Socialization{

    public function __construct($client, $details)
    {
        parent::__construct($client, $details);
    }

    /**
     * Inherit
     * @return bool|\Illuminate\Http\JsonResponse|mixed
     */
    public function signIn(){
        return parent::registerMember($this -> client,$this -> details);
    }

    /**
     * Inherit
     */
    public function validateToken()
    {
        // TODO: Implement validateToken() method.
    }

}
?>