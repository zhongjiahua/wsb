<?php
namespace Common\Classes\Notifications;

use SplObserver;

class NotificationCenter implements \SplSubject{

    public $params;
    private $_observers;
    private $_name;

    public  function __construct($name = null){
        $this -> _observers = new \SplObjectStorage();
        $this -> _name = $name;
    }

    /**
     * Receive custom params and notify observers
     * custom params includes:cc,bcc,file,subject,to
     *
     * Usage:
     *  $master = new NotificationCenter();
        $ob = new NewProgramReportAvailable();
        $master -> attach($ob);
        $master -> start([
     *      'to'    => 'custom_receiver_email' // default value up to different to scenarios
     *      'cc'    => 'custom_cc_receiver'  // string|array,
     *      'bcc'   => 'custom_bcc_receiver' // string|array,
     *      ...
            'file' => storage_path('app/uploads/public/5aa/b69/963/thumb_3_260_260_0_0_crop.jpeg')
        ]);
        $master -> detach($ob);
     *
     * @param $params
     */
    public function start($params = null){
        $this -> params = $params;
        $this -> notify();
    }

    public function attach(SplObserver $observer){
        $this -> _observers -> attach($observer);
    }

    public function detach(SplObserver $observer){
        $this -> _observers -> detach($observer);
    }

    public function notify(){
        foreach($this -> _observers as $observer){
            $observer -> update($this);
        }
        return true;
    }

    public function getName(){
        return $this -> _name;
    }
}
?>