<?php
namespace Common\Classes\Notifications;

use Common\Classes\Email\NewUserRegister;
use Common\Classes\EmailService;
use RainLab\User\Models\User;
use SplSubject;

class UserRegisterValidation implements \SplObserver{

    public function update(SplSubject $subject){
        $user = $this -> getReceiver($subject -> params['user_id']);
        $mail =   new EmailService($user,$subject -> params);
        $mail ->  sendMail(new NewUserRegister($subject -> params));
    }

    public function getReceiver($user_id){
        return   User::find($user_id);
    }
}
?>