<?php
namespace Common\Classes\Notifications;

use Common\Classes\Email\UserForgetPasswordView;
use Common\Classes\EmailService;
use RainLab\User\Models\User;
use SplSubject;

class UserForgetPassword implements \SplObserver{

    public function update(SplSubject $subject){
        $user = $this -> getReceiver($subject -> params['user_id']);
        $subject -> params['name'] = !empty($user -> name) ? $user -> name : $user -> surname;
        if(empty($subject -> params['name'])){
            $subject -> params['name'] = 'Customer';
        }
        $mail =   new EmailService($user,$subject -> params);
        $mail ->  sendMail(new UserForgetPasswordView($subject -> params));
    }

    public function getReceiver($user_id){
        return User::find($user_id);
    }
}
?>