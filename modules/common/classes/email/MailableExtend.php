<?php
namespace Common\Classes\Email;

use Illuminate\Mail\Mailable;

class  MailableExtend extends  Mailable{

    public $params;
    public $subject;

    public function __construct($params){
        $this -> params = $params;
        $this -> subject = null;
        $this -> getFiles($this -> params);
        if(isset($this -> params['subject']) && !empty($this -> params)){
            $this -> subject = $this -> params['subject'];
        }
        $this -> params['subject'] = $this -> subject;
    }

    public function getFiles($p){
        if(isset($p['file']) && !empty($p['file'])){
            if(is_array($p['file'])){
                foreach($p['file'] as $file){
                    $this -> attach($file);
                }
            }else{
                $this -> attach($p['file']);
            }
        }
    }

}
?>