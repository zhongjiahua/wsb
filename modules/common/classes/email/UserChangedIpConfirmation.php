<?php
namespace Common\Classes\Email;


use Common\Classes\EmailService;
use Illuminate\Mail\Mailable;


class UserChangedIpConfirmation extends Mailable {

    protected $params;

    public function __construct($params = null){
        $this -> params = $params;
    }

    public function build(){
        if(!empty($this -> params)){
            $this -> with([
                'name'   => $this -> params['name'],
                'new_ip' => $this -> params['new_ip'],
                'link'   => $this -> params['unlock_link']
            ]);
        }
        $this -> view(EmailService::getDefaultViewPath().'ip_changed_confirmation_to_user');
        $this -> subject(\Lang::get('Caution: CMS access location IP changed.'));

    }
}

?>