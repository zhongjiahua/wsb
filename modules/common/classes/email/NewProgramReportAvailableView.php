<?php
namespace Common\Classes\Email;

use Common\Classes\EmailService;

class NewProgramReportAvailableView extends MailableExtend {

    public function __construct($params = null){
        parent::__construct($params);
    }

    public function build(){
        $this -> subject = !empty($this -> subject) ?   $this -> subject : 'A new program report has been created!';

        if(!empty($this -> params)){
            $this -> with($this -> params);
        };
        $this -> view(EmailService::getDefaultViewPath().'new_program_report_available');

    }
}
?>