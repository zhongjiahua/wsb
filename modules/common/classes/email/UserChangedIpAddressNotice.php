<?php
namespace Common\Classes\Email;

use Common\Classes\EmailService;
use Illuminate\Mail\Mailable;
use System\Models\MailTemplate;

class UserChangedIpAddressNotice extends Mailable{

    protected $params;

    public function __construct($params = null){
        $this -> params = $params;
    }

    public function build(){
        if(!empty($this -> params)){
            $this -> with($this -> params);
        }

        $this -> view(EmailService::getDefaultViewPath().'ip_changed_to_admin');
        $this -> subject('CMS user login IP changed');
    }
}
?>