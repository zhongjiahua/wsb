<?php
namespace Common\Classes\Email;

use Common\Classes\EmailService;

class UserForgetPasswordView extends MailableExtend{

    public $params;

    public function __construct($params){
        parent::__construct($params);
        $this -> params = $params;
    }

    public function build(){
        $this -> subject = !empty($this -> subject ) ? $this -> subject : 'Get your password at Learning Time';
        if(!empty($this -> params)){
            $this -> with($this -> params);
        };
        $this -> view(EmailService::getDefaultViewPath().'user_forget_password');
        $this -> getFiles($this -> params);
    }
}
?>