<?php
namespace Common\Classes\Email;

use Common\Classes\EmailService;

class NewUserRegister extends MailableExtend{

    public $params;

    public function __construct($params){
        parent::__construct($params);
        $this -> params = $params;
    }

    public function build(){
        $this -> subject = !empty($this -> subject ) ? $this -> subject : 'Thank you for register as a member of Learning Time';
        if(!empty($this -> params)){
            $this -> with($this -> params);
        };
        $this -> view(EmailService::getDefaultViewPath().'new_user_register_validation');
        $this -> getFiles($this -> params);
    }
}
?>