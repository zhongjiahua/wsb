<?php
namespace Common\Classes\Email;

use Common\Classes\EmailService;

class NewPostFromActivityFeedView extends MailableExtend{

    public $params;

    public function __construct($params = null){
        parent::__construct($params);
        $this -> params = $params;
    }

    public function build(){
        $this -> subject = !empty($this -> subject ) ? $this -> subject : 'A new activity feed created';
        if(!empty($this -> params)){
            $this -> with($this -> params);
        };
        $this -> view(EmailService::getDefaultViewPath().'new_post_from_activity_feed');
        $this -> getFiles($this -> params);

    }
}
?>