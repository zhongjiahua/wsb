<?php
namespace Common\Classes;

use Excel;
use Storage;


class ExportService {

    public static function export($filetype, $arr,$filename,$heading=NULL){
        if(count($arr)==0){
            \Flash::error('No record found.');
            $ret=['status'=>StatusService::STATUS_SUSPEND];
            return $ret;
        }
        $action = 'export_'.$filetype;
        if($heading==NULL){
            $heading=[];
            foreach ($arr[0] as $key => $value) {
                $heading[]=$key;
            }
        }
        $handler = self::$action($arr,$filename,$heading);
        $ret=['status'=>StatusService::STATUS_ACTIVE, 'file_name'=>$handler];
        return $ret;
    }

    private static function export_html($list,$filename,$heading){
        return self::maat_export($list,$filename,$heading,'html');
    }

    private static function export_xls($list,$filename,$heading){
        return self::maat_export($list,$filename,$heading,'xls');
    }

    private static function export_xlsx($list,$filename,$heading){
        return self::maat_export($list,$filename,$heading,'xlsx');
    }

    private static function export_csv($list,$filename,$heading){
        return self::maat_export($list,$filename,$heading,'csv');
    }

    private static function createExportDirectory($path){
        if(!file_exists($path)){
            @mkdir($path,0777,true);
        }
        @chmod($path,0777);
    }

    private static function maat_export($list,$filename,$headings,$filetype){

        //self::createExportDirectory(storage_path('app').$path);

        $file_name = $filename.'_'.date('Ymd_hhmmss');
        $export = new ExportCollection();
        $export->setHeadings($headings);
        $export->setCollection($list);
        Excel::store($export, $file_name.'.'.$filetype, 'local');
        return $file_name.'.'.$filetype;

    }
    public static function downloadExportedFile($file_name){
        $path = storage_path("app/").$file_name;
        $file = \File::get($path);
        //return response()->download($path);
        return response()->download($path)->deleteFileAfterSend(true);
    }
}
?>
