<?php
namespace Common\Classes;

use Excel;
use DB;

class ImportService {
    /**
     * @param \Common\Models\Import                 $import
     * @param \Maatwebsite\Excel\Concerns\ToModel   $import_class   ref: https://docs.laravel-excel.com/3.1/imports/model.html
     */
    public static function import_excel($import,$import_class = null){
        $excel = $import->excel_file;
        $class = $import->model;
        if(is_null($import_class)){
            $collection = new ImportCollection();
            $collection->setModel($class);
        }else{
            $collection = new $import_class();
        }
        DB::beginTransaction();
        try{
            Excel::import($collection, $excel->getDiskPath());
            $import->imported = $import::IMPORTED_TRUE;
            $import->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            \Log::debug($e);
            \Flash::error($e->getMessage());
            return $e->getMessage();
        }
            
    }
}
?>
