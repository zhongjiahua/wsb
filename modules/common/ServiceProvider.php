<?php
namespace Common;


use Illuminate\Support\Facades\Event;
use October\Rain\Support\ModuleServiceProvider;
use System\Models\MailTemplate;
use Illuminate\Foundation\AliasLoader;
use App;
use October\Rain\Database\Attach\File;
use Illuminate\Database\DatabaseManager;

class ServiceProvider extends ModuleServiceProvider {

    public function register(){
        parent::register('common');
    }

    public function boot(){
        parent::boot('common');
        $alias = AliasLoader::getInstance()->alias('CommonModel', 'Common\Models\CommonModel');
        $alias = AliasLoader::getInstance()->alias('CommonController', 'Common\Controllers\CommonController');

        /**
         * Override Log file
         */
        App::register('\Common\Providers\LogServiceProvider');

        App::register('\Maatwebsite\Excel\ExcelServiceProvider');

        // Register alias
        $alias = AliasLoader::getInstance();
        $alias->alias('Excel', '\Maatwebsite\Excel\Facades\Excel');

        $this->app->singleton(DatabaseManager::class, function ($app) {
            return $app->make('db');
        });
		
		File::extend(function ($model) {
            $model->bindEvent('model.afterSave', function () use ($model) {
            	if($model->attachment_type){
                    try{
                		$parent_class = $model->attachment_type;
                		$parent = $parent_class::find($model->attachment_id);
                		$parent->updated_at = \Carbon\Carbon::now();
                		$parent->save();
                    }catch(\Exception $e){
                        \Log::debug('common/ServiceProvider/file.afterSave');
                        \Log::debug($e);
                    }
            	}
            });
            $model->bindEvent('model.beforeDelete', function () use ($model) {
            	if($model->attachment_type){
                    try{
                		$parent_class = $model->attachment_type;
                		$parent = $parent_class::find($model->attachment_id);
                		$parent->updated_at = \Carbon\Carbon::now();
                        $parent->save();
                    }catch(\Exception $e){
                        \Log::debug('common/ServiceProvider/file.beforeDelete');
                        \Log::debug($e);
                    }
            	}
            });
        });

    }
}
?>