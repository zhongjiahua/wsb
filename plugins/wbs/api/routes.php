<?php

Route::get('api/platform/getGeneral', 'WBS\api\Controllers\Platform@GetGeneral');

Route::post('api/wxlogin/wxSaveUser', 'WBS\api\Controllers\WxLogin@WxSaveUser');
Route::post('api/wxlogin/postBeMaster', 'WBS\api\Controllers\WxLogin@WxSaveApply');

Route::post('api/order/postOrder', 'WBS\api\Controllers\Order@SaveOrder');
Route::post('api/order/getOrderList', 'WBS\api\Controllers\Order@GetUserOrderList');
Route::get('api/order/getPublishOrder', 'WBS\api\Controllers\Order@GetOrderList');
Route::post('api/order/getOrderView', 'WBS\api\Controllers\Order@GetUserOrderView');

Route::post('api/order/getPickList', 'WBS\api\Controllers\Order@GetUserPickList');
Route::post('api/order/savePickOrder', 'WBS\api\Controllers\Order@SavePickOrder');
Route::post('api/order/delPickOrder', 'WBS\api\Controllers\Order@DelPickOrder');
Route::post('api/order/getPickView', 'WBS\api\Controllers\Order@GetUserPickView');
Route::post('api/order/cancelOrder', 'WBS\api\Controllers\Order@DelOrder');

Route::post('api/file/upload_img', 'WBS\api\Controllers\UpFile@upload_img');


?>
