<?php namespace Wbs\Api\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Wbs\Order\Models\PickOrder;
use Wbs\Order\Models\WxOrder;

/**
 * Order Back-end Controller
 */
class Order extends Controller
{
    use \Legato\Api\Traits\UtilityFunctions;
    public $order_check = false;

    public function GetUserOrderList()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'];
            $user   = (new Common)->Common($openId);
            if (!isset($user->id)) {
                return $user;
            }
            $limits = (new Common)->GetLimit($input);

            $where = array(
                'wx_id' => $user->id,
            );
            if (isset($input['status']) && !empty($input['status'])) {
                $where['status'] = $input['status'];
            }

//            $list = WxOrder::where($where)->limit($limits['limit'])->get()->toArray();
            $list = WxOrder::where($where)->skip($limits['page'] * $limits['limit'])->take($limits['limit'])->get()->toArray();

            return $this->successResponse(['list' => $list]);
        }

    }

    public function SaveOrder()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'] ?? null;
            $user   = (new Common)->Common($openId);

            if (!isset($user->id)) {
                return $user;
            }
            $num    = WxOrder::all()->count();
            $number = 1 + $num;
            $code   = 'WSB' . time() . rand(1000, 9999) . $number;
            $data   = array(
                'openid'        => $openId,
                'wx_id'         => $user->id,
                'order_code'    => $code,
                'order_content' => $input['order_content'],
                'push_time'     => $input['push_time'] ? strtotime($input['push_time']) : time(),
//                'section_id'    => '',
//                'item_id'       => '',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            );
            $result = WxOrder::insert($data);
            if ($result) {
                return $this->response(trans('backend::lang.wbs_wx_api.api_success'), 200);
            }
            return $this->response(trans('backend::lang.wbs_wx_api.user_not_login'), 200);
        }

    }

    public function DelOrder()
    {
        $input = Request::all();
        if ($input) {
            $order_id = $input['order_id'] ?? null;
            $openId   = $input['openId'] ?? null;
            $user     = (new Common)->Common($openId);
            if (!isset($user->id)) {
                return $user;
            }

            $order = WxOrder::where(['wx_id' => $user->id, 'id' => $order_id])->first();
            if (!$order) {
                return $this->response(trans('backend::lang.wbs_wx_api.order_is_null'), 200);
            }

            switch ($order->status) {
                case 3:
                    return $this->response(trans('backend::lang.wbs_wx_api.order_status_3') . trans('backend::lang.wbs_wx_api.not_delete'), 200);
                case 4:
                    return $this->response(trans('backend::lang.wbs_wx_api.order_status_4') . trans('backend::lang.wbs_wx_api.not_delete'), 200);
                case 5:
                    return $this->response(trans('backend::lang.wbs_wx_api.order_status_5') . trans('backend::lang.wbs_wx_api.not_delete'), 200);
            }

            $data = array(
                'status'     => 5,
                'deleted_at' => Carbon::now(),
            );

            $result = WxOrder::where(['wx_id' => $user->id, 'id' => $order_id])->update($data);
            if ($result) {
                return $this->response(trans('backend::lang.wbs_wx_api.api_success'), 200);
            }
            return $this->response(trans('backend::lang.wbs_wx_api.user_not_login'), 200);

        }

    }

    public function GetUserOrderView()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'] ?? null;
            $user   = (new Common)->Common($openId);
            if (!isset($user->id)) {
                return $user;
            }
            $where = array(
                'id'    => $input['order_id'] ?? 0,
                'wx_id' => $user->id,
            );
            if (!$where['id']) {
                return $this->response(trans('backend::lang.wbs_wx_api.orderId_is_null'), 200);
            }
            $order = WxOrder::where($where)->first() ?? array();
            if ($order) {
                if ($order->status == 1 || $order->status == 2) {
                    $order->suggestion = PickOrder::where(['order_id' => $order->id])->get()->toArray();
                }
                if ($order->status == 3 || $order->status == 4) {
                    $order->pick = (new PickOrder)->GetPickUser($order->id);
                }
            }

            return $this->successResponse(['order' => $order]);
        }
    }

    public function GetOrderList()
    {
        $input = Request::all();
//        if ($input) {
//            $openId = $input['openId'];
//            $user   = $this->Common($openId);
        $limits = (new Common)->GetLimit($input);

        $where = array(
            'status'     => 1,
            'deleted_at' => null,
        );
        $list  = WxOrder::where($where)->skip($limits['page'] * $limits['limit'])->take($limits['limit'])->get()->toArray();

        return $this->successResponse(['list' => $list]);
//        }

    }

    public function GetUserPickList()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'] ?? null;
            $user   = (new Common)->Common($openId);
            if (!isset($user->id)) {
                return $user;
            }
            $limits = (new Common)->GetLimit($input);

            $where = array(
                'wx_id' => $user->id,
            );
            if (isset($input['status']) && !empty($input['status'])) {
                $where['status'] = $input['status'];
            }

            $list = PickOrder::where($where)->skip($limits['page'] * $limits['limit'])->take($limits['limit'])->get()->toArray();

            return $this->successResponse(['list' => $list]);
        }
    }

    public function SavePickOrder()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'];
            $user   = (new Common)->Common($openId);
            if (!isset($user->id)) {
                return $user;
            }

            $order_id = $input['order_id'] ?? 0;
            $check    = $this->CheckOrder($order_id, $user);
            if (!$this->order_check) {
                return $check;
            }

            $num    = PickOrder::all()->count();
            $number = 1 + $num;
            $code   = 'WSB-PICK' . time() . rand(1000, 9999) . $number;

            $pick_price = $input['pick_price'] ?? 0;
            if (!is_numeric($pick_price)) {
                return $this->response(trans('backend::lang.wbs_wx_api.pick_price_error'), 200);
            }
            $data = array(
                'wx_id'      => $user->id,
                'pick_code'  => $code,
                'pick_price' => $pick_price,
                'order_id'   => $order_id,
                'suggestion' => $input['suggestion'] ?? null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );

            $result = PickOrder::insert($data);
            if ($result) {
                return $this->response(trans('backend::lang.wbs_wx_api.api_success'), 200);
            }
            return $this->response(trans('backend::lang.wbs_wx_api.user_not_login'), 200);
        }
    }

    public function DelPickOrder()
    {
        $input = Request::all();
        if ($input) {
            $order_id = $input['order_id'] ?? null;
            $openId   = $input['openId'] ?? null;
            $user     = (new Common)->Common($openId);
            if (!isset($user->id)) {
                return $user;
            }
            $order = PickOrder::where(['wx_id' => $user->id, 'id' => $order_id])->first();
            if (!$order) {
                return $this->response(trans('backend::lang.wbs_wx_api.order_is_null'), 200);
            }

            switch ($order->status) {
                case 2:
                    return $this->response(trans('backend::lang.wbs_wx_api.pick_status_2') . trans('backend::lang.wbs_wx_api.not_delete'), 200);
                case 3:
                    return $this->response(trans('backend::lang.wbs_wx_api.pick_status_3') . trans('backend::lang.wbs_wx_api.not_delete'), 200);
                case 4:
                    return $this->response(trans('backend::lang.wbs_wx_api.pick_status_4') . trans('backend::lang.wbs_wx_api.not_delete'), 200);
            }

            $data = array(
                'status'     => 5,
                'deleted_at' => Carbon::now(),
            );

            $result = WxOrder::where(['wx_id' => $user->id, 'id' => $order_id])->update($data);
            if ($result) {
                return $this->response(trans('backend::lang.wbs_wx_api.api_success'), 200);
            }
            return $this->response(trans('backend::lang.wbs_wx_api.user_not_login'), 200);
        }
    }

    public function GetUserPickView()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'] ?? null;
            $user   = (new Common)->Common($openId);
            if (!isset($user->id)) {
                return $user;
            }
            $where = array(
                'id'    => $input['order_id'] ?? 0,
                'wx_id' => $user->id,
            );
            if (!$where['id']) {
                return $this->response(trans('backend::lang.wbs_wx_api.orderId_is_null'), 200);
            }
            $order = PickOrder::where($where)->first();

            if ($order) {
                $order->order = WxOrder::where(['id' => $order->order_id])->get()->toArray();
            }

            return $this->successResponse(['order' => $order]);
        }
    }

    public function ConfirmOrder()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'] ?? null;
            $user   = (new Common)->Common($openId);

            if (!isset($user->id)) {
                return $user;
            }

            $order_id = $input['oder_id'] ?? 0;
            $pick_id  = $input['pick_id'] ?? 0;
            $order = WxOrder::where(['id' => $order_id, 'wx_id' => $user->id])->first();
            if (!$order) {
                return $this->response(trans('backend::lang.wbs_wx_api.order_is_null'), 200);
            }
            $pick_order = PickOrder::where(['id' => $pick_id, 'order_id' => $order_id])->first();
            if (!$pick_order) {
                return $this->response(trans('backend::lang.wbs_wx_api.pick_is_null'), 200);
            }
        }
    }

    public function CheckOrder($order_id, $user)
    {
        $order = WxOrder::where(['id' => $order_id, 'deleted_at' => null])->first();
        if (!$order) {
            return $this->response(trans('backend::lang.wbs_wx_api.order_is_null'), 200);
        }
        if ($user->role == 1) {
            return $this->response(trans('backend::lang.wbs_wx_api.user_role_error'), 200);
        }

        $pick_order = PickOrder::where([
            'order_id' => $order_id, 'deleted_at' => null, 'wx_id' => $user->id,])
            ->where('status', '!=', 4)
            ->first();
        if ($pick_order) {
            return $this->response(trans('backend::lang.wbs_wx_api.user_pick'), 200);
        }
        if ($order->wx_id == $user->id) {
            return $this->response(trans('backend::lang.wbs_wx_api.order_user'), 200);
        }

        switch ($order->status) {
            case 1:
                WxOrder::where(['id' => $order_id])->update(['status' => 2]);
                break;
            case 4:
                return $this->response(trans('backend::lang.wbs_wx_api.order_status_4') . trans('backend::lang.wbs_wx_api.not_pick_order'), 200);
            case 5:
                return $this->response(trans('backend::lang.wbs_wx_api.order_status_5') . trans('backend::lang.wbs_wx_api.not_pick_order'), 200);
        }

        return $this->order_check = true;


    }
}
