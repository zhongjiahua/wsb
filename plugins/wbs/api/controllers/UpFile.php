<?php

namespace Wbs\Api\Controllers;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;


class UpFile extends Controller
{
    use \Legato\Api\Traits\UtilityFunctions;

    public function upload_img(Request $request)
    {
        if ($request->isMethod('POST')) { //判断文件是否是 POST的方式上传
            $file = $request->file('file') ?? array();
            $data = [];
            foreach ($file as $key => $tmp) {
                if ($tmp->isValid()) { //判断文件上传是否有效
                    $FileType = $tmp->getClientOriginalExtension(); //获取文件后缀

                    $FilePath = $tmp->getRealPath(); //获取文件临时存放位置

                    //临时绝对路径
                    $realPath = $tmp->getRealPath();
                    echo $realPath;
                    $FileName = uniqid() . '.' . $FileType; //定义文件名

                    $file_relative_path = 'storage/app/uploads/public/' . date('Ymd');
                    $file_path          = public_path($file_relative_path);

                    if (!is_dir($file_path)) {
                        mkdir($file_path);
                    }

                    Storage::disk('uploads')->put($FileName, file_get_contents($FilePath)); //存储文件

                    $data[] = [
                        'status'   => 0,
//                        'file_path' => $file_path,
                        'url_path' => $_SERVER['SERVER_NAME'] . '/' . $file_relative_path . '/' . $FileName //文件路径
                    ];
                }
            }

            return $this->successResponse(['images' => $data]);
        }
    }


}