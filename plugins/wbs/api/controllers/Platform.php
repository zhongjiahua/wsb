<?php namespace Wbs\Api\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\DB;
use Wbs\Platform\Models\Banner;
use Wbs\Platform\Models\Platform AS Platform_Model;
use Wbs\Platform\Models\Section;

/**
 * Platform Back-end Controller
 */
class Platform extends Controller
{
    use \Legato\Api\Traits\UtilityFunctions;

    public function GetGeneral()
    {
        $platform = Platform_Model::all();
        foreach ($platform as $value) {
            if ($value['code'] == Platform_Model::ABOUT_US) {
                $about_us = $value['value'];
            }
            if ($value['code'] == Platform_Model::OPERATOR_EMAIL) {
                $order_remind = $value['value'];
            }
            if ($value['code'] == Platform_Model::PROTOCOL) {
                $protocol = $value['value'];
            }
            if ($value['code'] == Platform_Model::OPERATOR_EMAIL) {
                $operator['email'] = $value['value'];
            }
            if ($value['code'] == Platform_Model::OPERATOR_PHONE) {
                $operator['phone'] = $value['value'];
            }
            if ($value['code'] == Platform_Model::OPERATOR_ADDRESS) {
                $operator['address'] = $value['value'];
            }
        }
        $banner = Banner::where(['status' => 1, 'deleted_at' => null])->orderBy('sort', 'desc')->get();

//        $section = DB::table('wbs_platform_sections as s');
////            ->leftJoin('wbs_platform_items as i', 'i.section_id', '=', 's.id')
//        $section->leftJoin('wbs_platform_items as i', function($join) {
//                $join->where('i.status', '=', 1)
//                    ->where('i.deleted_at', '=', null)
//                    ->orderBy('i.sort', 'desc')
//                ;
//            });
//        $section->where(['s.status' => 1, 's.deleted_at' => null])
//            ->orderBy('s.sort', 'desc')
//            ->select('s.id as section_id', 's.title as s_title', 'i.id as item_id', 'i.title as i_title', 'i.icon')
//            ->get()->toArray();
        $section = DB::table('wbs_platform_sections')
            ->select('id as section_id', 'title as s_title')
            ->where(['status' => 1, 'deleted_at' => null])
            ->orderBy('sort', 'desc')
            ->get()->toArray();
        if($section){
            foreach ($section as &$value){
                $value->item =  DB::table('wbs_platform_items')
                    ->select('id as item_id', 'title as i_title', 'icon')
                    ->where(['status' => 1, 'deleted_at' => null])
                    ->orderBy('sort', 'desc')
                    ->get()->toArray();
            }
        }

        $data = array(
            'section'         => $section,
            'about_as'        => $about_us??'',
            'order_remind'    => $order_remind??'',
            'landing_banners' => $banner,
            'contact_us'      => $operator??array(),
            'protocol'        => $protocol??''
        );

        return $this->successResponse(['getGeneral' => $data]);

    }
}
