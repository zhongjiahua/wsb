<?php namespace Wbs\Api\Controllers;

use BackendMenu;
use Validator;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Wbs\Wuser\Models\WxApplyInfo;
use Wbs\Wuser\Models\WxUser;

/**
 * Wx Login Back-end Controller
 */
class WxLogin extends Controller
{
    use \Legato\Api\Traits\UtilityFunctions;

    public function WxSaveUser()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'];

            if (!$openId) {
                return $this->response(trans('backend::lang.wbs_wx_api.openid_is_null'), 200);
            }

            $user = WxUser::where('openId', $openId)->first();
            $data = array(
                'openid'     => $input['openId'],
                'appid'      => $input['appId'],
                'unionid'    => $input['unionId'],
                'nickName'   => $input['nickName'] ?? '',
                'avatarUrl'  => $input['avatarUrl'] ?? '',
                'gender'     => $input['gender'] ?? 0,
                'updated_at' => Carbon::now(),
            );
            if ($user) {
                WxUser::where('openId', $openId)->update($data);
                $role = $user->role;
            } else {
                $data['created_at'] = Carbon::now();
                WxUser::insert($data);
                $role = 1;
            }

            return $this->successResponse(['role' => $role]);
            // return $this->response(trans('backend::lang.wbs_wx_api.api_success'),200);

        }
//        if($result){
//            return $this->response(trans('backend::lang.auto_error.contact_success'), 200);
//            //return $this->successResponse(['status' => 1]);
//        }
//        return $this->response(trans('backend::lang.auto_error.contact_fail'), 200);
    }

    public function WxSaveApply()
    {
        $input = Request::all();
        if ($input) {
            $user = (new Common())->Common($input['openId'] ?? null);

            if (!isset($user->id)) {
                return $user;
            }
            $rules          = $this->ReturnApplyRule();
            $customMessages = $this->ReturnCustomMessages();
            $data           = array(
//                'openId'    => $input['openId'],
                'name'       => $input['name'] ?? '',
                'age'        => $input['age'] ?? '',
                'gender'     => $input['gender'] ?? 0,
                'phone'      => $input['phone'] ?? '',
                'address'    => $input['address'] ?? '',
                'describe'   => $input['describe'] ?? '',
                'specialty'  => $input['specialty'] ?? '',
                'images'     => $input['images'] ? json_encode($input['images']) : '',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );

            $validator = \Validator::make($data, $rules, $customMessages);
            if ($validator->fails()) {
                return $this->validationResponse($validator);
            }

            $openId = $input['openId'];

            $user = WxUser::where('openId', $openId)->first();

            if ($user) {
                $data['wx_id'] = $user->id;
                WxApplyInfo::insert($data);

                return $this->response(trans('backend::lang.wbs_wx_api.api_success'), 200);
            }
            return $this->response(trans('backend::lang.wbs_wx_api.user_not_login'), 200);
        }
    }

    public function GetApplyInfo()
    {
        $input = Request::all();
        if ($input) {
            $openId = $input['openId'];

            if (!$openId) {
                return $this->response(trans('backend::lang.wbs_wx_api.openid_is_null'), 200);
            }

            $user = WxApplyInfo::where('openId', $openId)->first();

            return $this->successResponse(['info' => $user]);

        }
    }

    public function ReturnCustomMessages()
    {
        $customMessages = [
            'name.required'      => 'name is required!!',
            'age.required'       => 'age is required!!',
            'gender.required'    => 'gender is required!!',
            'address.required'   => 'address is required!!',
            'describe.required'  => 'describe is required!!',
            'specialty.required' => 'specialty is required!!',
            'phone.required'     => 'phone is required!!',
        ];
        return $customMessages;
    }

    public function ReturnApplyRule()
    {
        $rules = [
            'phone'     => 'required|regex:/^1[34578]\d{9}$/',
            'name'      => 'required',
            'age'       => 'required',
            'gender'    => 'required',
            'address'   => 'required',
            'describe'  => 'required',
            'specialty' => 'required',
        ];
        return $rules;
    }
}
