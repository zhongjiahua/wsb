<?php


namespace Wbs\Api\Controllers;


use Wbs\Wuser\Models\WxUser;

class Common
{
    use \Legato\Api\Traits\UtilityFunctions;
    public function Common($openId = null)
    {
        if (!$openId) {
            return $this->response(trans('backend::lang.wbs_wx_api.openid_is_null'), 200);
        }
        $user = WxUser::where('openId', $openId)->first();

        if (!$user) {
            return $this->response(trans('backend::lang.wbs_wx_api.user_not_login'), 200);
        }
        return $user;
    }

    public function GetLimit($data = array())
    {
        return array(
            'page'  => $data['page'] ?? 0,
            'limit' => $data['num'] ?? 10,
        );
    }

}