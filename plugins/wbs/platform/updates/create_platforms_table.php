<?php namespace Wbs\Platform\Updates;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePlatformsTable extends Migration
{
    public function up()
    {
//        Schema::create('wbs_platform_platforms', function (Blueprint $table) {
//            $table->engine = 'InnoDB';
//            $table->increments('id');
//            $table->text('about_as')->nullable();
//            $table->text('order_remind')->nullable();
//            $table->text('protocol')->nullable();
//            $table->string('operator_address',64)->nullable();
//            $table->string('operator_phone',11)->nullable();
//            $table->string('operator_email',64)->nullable();
//            $table->tinyInteger('status');
//            $table->integer('created_by')->nullable();
//            $table->integer('updated_by')->nullable();
//            $table->timestamps();
//            $table->softDeletes();
//        });

        Schema::create('wbs_platform_platforms', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code',64)->nullable();
            $table->text('value')->nullable();
//            $table->string('operator_address',64)->nullable();
//            $table->text('about_as')->nullable();
//            $table->text('order_remind')->nullable();
//            $table->text('protocol')->nullable();
//            $table->string('operator_address',64)->nullable();
//            $table->string('operator_phone',11)->nullable();
//            $table->string('operator_email',64)->nullable();
            $table->tinyInteger('status');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        $data= [
            [
                'code'       => 'about_us',
                'status'     => '1',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'code'       => 'order_remind',
                'status'     => '1',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'code'       => 'protocol',
                'status'     => '1',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'code'       => 'operator_phone',
                'status'     => '1',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'code'       => 'operator_email',
                'status'     => '1',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'code'       => 'operator_address',
                'status'     => '1',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];
        $user = Db::table('wbs_platform_platforms')->insert($data);

    }

    public function down()
    {
        Schema::dropIfExists('wbs_platform_platforms');
    }
}
