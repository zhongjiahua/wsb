<?php namespace Wbs\Platform\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Common\Controllers\CommonController;
use Wbs\Platform\Models\Item;

/**
 * Items Back-end Controller
 */
class Items extends CommonController
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = ['items' => 'config_list.yaml'];

    public function __construct()
    {
        parent::__construct();

        $this -> _breadcrumb();
        /**
         * Register bulky action list(Optional)
         */
        $this -> _bulks_list();
        $this -> _vars();

        BackendMenu::setContext('Wbs.Platform', 'platform', 'items');
    }

    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => 'Items',
            'url'   => \Backend::url('/Wbs/Platform/Items/')
        ];
        $this -> vars['breadcrumb']['child'] = [];
    }

    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(
                'label' => 'Delete',
                'lang_key' => 'delete',
                'icon'   => 'oc-icon-trash-o',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/platform/items'
                ),
            ),
            'activate' => array(
                'label' => 'Activate',
                'lang_key' => 'activate',
                'icon'   => 'oc-icon-play-circle-o',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/platform/items'
                ),
            ),
            'deactivate' => array(
                'label' => 'Deactivate',
                'lang_key' => 'deactivate',
                'icon'   => 'oc-icon-stop-circle',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/platform/items'
                ),
            ),
        ];
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            parent::bulkAction(Item::class,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }

    private function _vars(){}

}
