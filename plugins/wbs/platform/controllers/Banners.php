<?php namespace Wbs\Platform\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Common\Controllers\CommonController;
use Wbs\Platform\Models\Banner;

/**
 * Banners Back-end Controller
 */
class Banners extends CommonController
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        $this -> _breadcrumb();
        /**
         * Register bulky action list(Optional)
         */
        $this -> _bulks_list();
        $this -> _vars();

        BackendMenu::setContext('Wbs.Platform', 'platform', 'banners');
    }
    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => 'Banners',
            'url'   => \Backend::url('/Wbs/Platform/Banners/')
        ];
        $this -> vars['breadcrumb']['child'] = [];
    }

    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(
                'label' => 'Delete',
                'lang_key' => 'delete',
                'icon'   => 'oc-icon-trash-o',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/platform/banners'
                ),
            ),
            'activate' => array(
                'label' => 'Activate',
                'lang_key' => 'activate',
                'icon'   => 'oc-icon-play-circle-o',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/platform/banners'
                ),
            ),
            'deactivate' => array(
                'label' => 'Deactivate',
                'lang_key' => 'deactivate',
                'icon'   => 'oc-icon-stop-circle',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/platform/banners'
                ),
            ),
        ];
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            parent::bulkAction(Banner::class,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }

    private function _vars(){}
}
