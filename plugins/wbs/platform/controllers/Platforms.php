<?php namespace Wbs\Platform\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Common\Controllers\CommonController;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;
use System\Models\MailBrandSetting;
use Wbs\Platform\Models\Platform;
use Exception;

/**
 * Platforms Back-end Controller
 */
class Platforms extends CommonController
{
    protected $formWidget;
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';

//    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Wbs.Platform', 'platform', 'platforms');
    }

    public function _breadcrumb()
    {
        $this->vars['breadcrumb']['home']  = [
            'title' => 'Platforms',
//            'url'   => \Backend::url('/Wbs/Platform/Platforms/')
        ];
        $this->vars['breadcrumb']['child'] = [];
    }

    public function index()
    {

        $this->pageTitle = 'Platforms';

        $model = (new Platform());
        $model->initPlatformData();
        $this->initWidgets($model);
    }

    public function index_onSave()
    {
        $model = (new Platform());
        $this->initWidgets($model);

        try {
            $saveData = $this->formWidget->getSaveData();
//            trace_log($saveData);
            foreach ($saveData as $attribute => $value) {
                if($attribute == 'operator_phone' && $value){
                    $check = '/^(1(([35789][0-9])|(47)))\d{8}$/';
                    if (!preg_match($check, $value)){
                        throw new ApplicationException(trans('backend::lang.wbs_platform.phone_error'));
                    }
                }
                if($attribute == 'operator_email' && $value){
                    $check = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/";
                    if (!preg_match($check, $value)){
                        throw new ApplicationException(trans('backend::lang.wbs_platform.email_error'));
                    }
                }
                Platform::where('code', $attribute)->update(["value" => $value]);
            }
            Flash::success(trans('backend::lang.wbs_platform.update_success'));
        }
        catch (Exception $ex){
            Flash::error($ex->getMessage());
        }

    }

    protected function initWidgets($model)
    {

        $config = $model->getFieldConfig();

        $config->model     = $model;
        $config->arrayName = class_basename($model);
        $config->context   = 'update';

        $widget = $this->makeWidget('Backend\Widgets\Form', $config);
        $widget->bindToController();
        $this->formWidget = $widget;
    }

    public function formRender($options = [])
    {
        if (!$this->formWidget) {
            throw new ApplicationException(Lang::get('backend::lang.form.behavior_not_ready'));
        }

        return $this->formWidget->render($options);
    }



}
