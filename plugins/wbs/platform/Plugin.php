<?php namespace Wbs\Platform;

use Backend;
use System\Classes\PluginBase;

/**
 * Platform Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Platform',
            'description' => 'No description provided yet...',
            'author'      => 'Wbs',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Wbs\Platform\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
//            'wbs.platform.some_permission' => [
//                'tab' => 'Platform',
//                'label' => 'Some permission'
//            ],
            'wbs.platform.create' => [
                'tab' => 'Platform',
                'label' => 'Create Platform'
            ],
            'wbs.platform.update' => [
                'tab' => 'Platform',
                'label' => 'Update Platform'
            ],
            'wbs.platform.delete' => [
                'tab' => 'Platform',
                'label' => 'Delete Platform'
            ],
            'wbs.banner.create' => [
                'tab' => 'Banner',
                'label' => 'Create Banner'
            ],
            'wbs.banner.update' => [
                'tab' => 'Banner',
                'label' => 'Update Banner'
            ],
            'wbs.banner.delete' => [
                'tab' => 'Banner',
                'label' => 'Delete Banner'
            ],
            'wbs.section.create' => [
                'tab' => 'Section',
                'label' => 'Create Section'
            ],
            'wbs.section.update' => [
                'tab' => 'Section',
                'label' => 'Update Section'
            ],
            'wbs.section.delete' => [
                'tab' => 'Section',
                'label' => 'Delete Section'
            ],
            'wbs.item.create' => [
                'tab' => 'Item',
                'label' => 'Create Item'
            ],
            'wbs.item.update' => [
                'tab' => 'Item',
                'label' => 'Update Item'
            ],
            'wbs.item.delete' => [
                'tab' => 'Item',
                'label' => 'Delete Item'
            ],

        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'platform' => [
                'label'       => 'backend::lang.wbs_general.platform',
                'url'         => Backend::url('wbs/platform/platforms'),
                'icon'        => 'icon-windows',
                'permissions' => ['wbs.platform.*'],
                'order'       => 600,

                'sideMenu' => [
                    'platform' => [
                        'label'       => 'backend::lang.wbs_general.platform',
                        'icon'        => 'icon-windows',
                        'url'         => Backend::url('wbs/platform/platforms'),
                        'permissions' => ['wbs.platform.*'],
//                        'class'       => 'Wbs\Platform\Models\Platform',
                    ],
                    'banner' => [
                        'label'       => 'backend::lang.wbs_general.banner',
                        'icon'        => 'icon-file-image-o',
                        'url'         => Backend::url('wbs/platform/banners'),
                        'permissions' => ['wbs.banner.*']
                    ],
                    'section' => [
                        'label'       => 'backend::lang.wbs_general.section',
                        'icon'        => 'icon-object-ungroup',
                        'url'         => Backend::url('wbs/platform/sections'),
                        'permissions' => ['wbs.section.*']
                    ]
                    ,
                    'item' => [
                        'label'       => 'backend::lang.wbs_general.item',
                        'icon'        => 'icon-object-group',
                        'url'         => Backend::url('wbs/platform/items'),
                        'permissions' => ['wbs.item.*']
                    ]
                ]
            ],
        ];
    }
}
