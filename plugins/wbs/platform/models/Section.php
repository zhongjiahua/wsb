<?php namespace Wbs\Platform\Models;

use Common\Models\CommonModel;
use Mail;
use Model;

/**
 * Section Model
 */
class Section extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'wbs_platform_sections';

    public $rules = [
        'title' => 'required|unique:wbs_platform_sections,title,NULL,id,deleted_at,NULL',

    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [

    ];
    public $hasMany = [
        'item' => [
            Item::Class,
            'order' => 'created_at',
        ],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
