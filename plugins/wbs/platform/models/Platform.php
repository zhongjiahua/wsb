<?php namespace Wbs\Platform\Models;

use Common\Models\CommonModel;
use Illuminate\Support\Facades\DB;
use Mail;
use Model;

/**
 * Platform Model
 */
class Platform extends CommonModel
{
    use \System\Traits\ConfigMaker;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'wbs_platform_platforms';

    const ABOUT_US = 'about_us';
    const ORDER_REMIND = 'order_remind';
    const PROTOCOL = 'protocol';
    const OPERATOR_PHONE = 'operator_phone';
    const OPERATOR_EMAIL = 'operator_email';
    const OPERATOR_ADDRESS = 'operator_address';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $fieldConfig;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public function initPlatformData()
    {
        $platform_info = DB::table('wbs_platform_platforms')->where('status', 1)->get();
        if ($platform_info) {
            foreach ($platform_info as $value) {
                if ($value->code === self::ABOUT_US) {
                    $this->about_us = $value->value;
                }
                if ($value->code === self::ORDER_REMIND) {
                    $this->order_remind = $value->value;
                }
                if ($value->code === self::PROTOCOL) {
                    $this->protocol = $value->value;
                }
                if ($value->code === self::OPERATOR_PHONE) {
                    $this->operator_phone = $value->value;
                }
                if ($value->code === self::OPERATOR_EMAIL) {
                    $this->operator_email = $value->value;
                }
                if ($value->code === self::OPERATOR_ADDRESS) {
                    $this->operator_address = $value->value;
                }
            }

        }
    }

    public function getFieldConfig()
    {
        return $this->fieldConfig = $this->makeConfig('fields.yaml');
    }
}
