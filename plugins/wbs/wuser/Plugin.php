<?php namespace Wbs\Wuser;

use Backend;
use System\Classes\PluginBase;

/**
 * Wuser Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Wuser',
            'description' => 'No description provided yet...',
            'author'      => 'Wbs',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Wbs\Wuser\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'wbs.wuser.some_permission' => [
                'tab' => 'Wuser',
                'label' => 'Some permission'
            ],
            'wbs.wuser.update' => [
                'tab' => 'Wx_user',
                'label' => 'Update Wx_user'
            ],
            'wbs.wuser.delete' => [
                'tab' => 'Wx_user',
                'label' => 'Delete Wx_user'
            ],
            'wbs.applyInfo.update' => [
                'tab' => 'applyInfo',
                'label' => 'Delete applyInfo'
            ],
            'wbs.applyInfo.delete' => [
                'tab' => 'applyInfo',
                'label' => 'Delete applyInfo'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'wuser' => [
                'label'       => 'backend::lang.wbs_wx_user.wx_user',
                'url'         => Backend::url('wbs/wuser/wxusers'),
                'icon'        => 'icon-user',
                'permissions' => ['wbs.wuser.*'],
                'order'       => 600,

                'sideMenu' => [
                    'wxUser' => [
                        'label'       => 'backend::lang.wbs_wx_user.wx_user',
                        'icon'        => 'icon-user',
                        'url'         => Backend::url('wbs/wuser/wxusers'),
                        'permissions' => ['wbs.wuser.*'],
//
                    ],
                    'applyInfo' => [
                        'label'       => 'backend::lang.wbs_wx_user.apply_info',
                        'icon'        => 'icon-address-book',
                        'url'         => Backend::url('wbs/wuser/wxapplyinfos'),
                        'permissions' => ['wbs.applyInfo.*'],
//
                    ],
                ]
            ],
        ];
    }
}
