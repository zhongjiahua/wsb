<?php namespace Wbs\Wuser\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWxUsersTable extends Migration
{
    public function up()
    {
        Schema::create('wbs_wx_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('appid',64);
            $table->string('openid',64);
            $table->string('nickName',64);
            $table->tinyInteger('gender')->default(0);
            $table->string('avatarUrl',128);
            $table->string('unionid',64);
            $table->tinyInteger('role')->default(1);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('wbs_wx_user');
    }
}
