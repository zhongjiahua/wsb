<?php namespace Wbs\Wuser\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWxApplyInfosTable extends Migration
{
    public function up()
    {
        Schema::create('wbs_wx_apply_infos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('wx_id');
            $table->string('name',16);
            $table->string('born',64)->nullable();
            $table->string('age',4);
            $table->tinyInteger('gender');
            $table->string('phone',11);
            $table->string('address',128);
            $table->string('describe',255)->nullable();
            $table->string('specialty',255)->nullable();
            $table->string('audit_opinion',255)->nullable();
            $table->string('images',255)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('wbs_wx_apply_infos');
    }
}
