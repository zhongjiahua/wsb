<?php namespace Wbs\Wuser\Models;

use Common\Models\CommonModel;
use Illuminate\Support\Facades\Lang;
use Model;

/**
 * WxUser Model
 */
class WxUser extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'wbs_wx_user';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getImageAttribute()
    {
        $project = $this->find($this->id);
        if ($project->avatarUrl) {
            return '<img src="'.$project->avatarUrl.'" width=30 height=30/>';
        }
    }

    public function getRoleOptions(){
//        return [
//            static::STATUS_OFF  => Lang::get('common::lang.list.status.column_switch_false'),
//            static::STATUS_ON   => Lang::get('common::lang.list.status.column_switch_true')
//        ];
        return array(
            1 => '普通用戶',
            2 => '師傅');
    }
}
