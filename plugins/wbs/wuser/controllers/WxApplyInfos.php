<?php namespace Wbs\Wuser\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Common\Controllers\CommonController;
use Wbs\Wuser\Models\WxApplyInfo;

/**
 * Wx Apply Infos Back-end Controller
 */
class WxApplyInfos extends CommonController
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        $this -> _breadcrumb();
        /**
         * Register bulky action list(Optional)
         */
        $this -> _bulks_list();
        $this -> _vars();

        BackendMenu::setContext('Wbs.Wuser', 'wuser', 'wxapplyinfos');
    }

    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => 'Banners',
            'url'   => \Backend::url('/Wbs/Wuser/WxApplyInfos/')
        ];
        $this -> vars['breadcrumb']['child'] = [];
    }

    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(
                'label' => 'Delete',
                'lang_key' => 'delete',
                'icon'   => 'oc-icon-trash-o',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/wuser/wxapplyinfos'
                ),
            ),
        ];
        $this->vars['hide_create'] = true;
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            parent::bulkAction(WxApplyInfo::class,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }

    private function _vars(){}

    public function actionGetAge()
    {
        $birthday = '2010-01-01';
        $age = date('Y', time()) - date('Y', strtotime($birthday)) - 1;
        if(date('m', time()) == date('m', strtotime($birthday))){
            if(date('d', time()) > date('d', strtotime($birthday))){
                $age++;
            }
        }
        elseif(date('m', time()) > date('m', strtotime($birthday))){
            $age++;
        }
        return $age;
    }
}
