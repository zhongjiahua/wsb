<?php namespace Wbs\Order;

use Backend;
use System\Classes\PluginBase;

/**
 * Order Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Order',
            'description' => 'No description provided yet...',
            'author'      => 'Wbs',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Wbs\Order\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
       // return []; // Remove this line to activate

        return [
            'wbs.order.some_permission' => [
                'tab' => 'Order',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'order' => [
                'label'       => 'backend::lang.wbs_general.order',
                'url'         => Backend::url('wbs/order/wxorder'),
                'icon'        => 'icon-align-left',
                'permissions' => ['wbs.order.*'],
                'order'       => 600,
                'sideMenu' => [
                    'WxOrder' => [
                        'label'       => 'backend::lang.wbs_wx_order.WxOrder',
                        'icon'        => 'icon-align-left',
                        'url'         => Backend::url('wbs/order/wxorder'),
                        'permissions' => ['wbs.wxorder.*'],
                    ],
                    'PickOrder' => [
                        'label'       => 'backend::lang.wbs_wx_order.PickOrder',
                        'icon'        => 'icon-align-right',
                        'url'         => Backend::url('wbs/order/pickorder'),
                        'permissions' => ['wbs.pickorder.*']
                    ],
                ]
            ],
        ];
    }
}
