<?php namespace Wbs\Order\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWxOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('wbs_wx_orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('openid', 64);
            $table->integer('wx_id');
            $table->string('order_code', 64);
            $table->string('order_content', 255)->nullable();
            $table->string('price', 64)->nullable();
            $table->string('pay_price', 64)->nullable();
            $table->integer('pay_at')->nullable();
            $table->tinyInteger('is_pay')->default(0);
            $table->integer('push_time');
            $table->tinyInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('wbs_wx_orders');
    }
}
