<?php namespace Wbs\Order\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePickOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('wbs_pick_orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('pick_code',64);
            $table->string('pick_price', 64)->nullable();
            $table->integer('order_id');
            $table->integer('wx_id');
            $table->string('suggestion',255);
            $table->tinyInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('wbs_pick_orders');
    }
}
