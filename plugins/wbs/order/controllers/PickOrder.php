<?php namespace Wbs\Order\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Common\Controllers\CommonController;
use Wbs\Order\Models\PickOrder AS PickOrder_Model;

/**
 * Pick Order Back-end Controller
 */
class PickOrder extends CommonController
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        $this -> _breadcrumb();
        /**
         * Register bulky action list(Optional)
         */
        $this -> _bulks_list();
        $this -> _vars();

        BackendMenu::setContext('Wbs.Order', 'order', 'pickorder');
    }

    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => 'PickOrder',
            'url'   => \Backend::url('/Wbs/Order/PickOrder/')
        ];
        $this -> vars['breadcrumb']['child'] = [];
    }

    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(
                'label' => 'Delete',
                'lang_key' => 'delete',
                'icon'   => 'oc-icon-trash-o',
                'default' => true,
                'redirect' => array(
                    'enable' => true,
                    'destination' => 'wbs/order/pickOrder'
                ),
            ),
        ];
        $this->vars['hide_create'] = true;
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            parent::bulkAction(PickOrder_Model::class,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }

    private function _vars(){}
}
