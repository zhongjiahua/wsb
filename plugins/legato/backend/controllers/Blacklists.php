<?php namespace Legato\Backend\Controllers;

use BackendMenu;
use Common\Controllers\CommonController;
use System\Classes\SettingsManager;
use Legato\Backend\Models\Blacklist;

/**
 * Blacklist Back-end Controller
 */
class Blacklists extends CommonController
{
    public $requiredPermissions=['legato.backend.blacklists'];
    public function __construct()
    {
        parent::__construct();

        //$this -> _breadcrumb();
        $this -> _bulks_list();
        BackendMenu::setContext('October.System', 'system', 'users');
        SettingsManager::setContext('Legato.Backend', 'blacklist_ip');
        $this -> _vars();
    }

    private function _vars(){
        //$create_url = 'Legato/News/News/create';
        //$this -> vars['create_url'] = $create_url;
        //$this -> vars['cancel_url'] =  'Legato/News/News/' ;
        $this -> vars['with_side'] = false;
        $this -> vars['show_save_redirect'] = true;
        $this -> vars['hide_create'] = false;
        $this -> vars['form_layout_center'] = false;
        $this -> vars['hide_import'] = true;//default true
        $this -> vars['hide_export'] = true;//default true

    }

    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => 'Blacklists',
            'url'   => \Backend::url('/Legato/Backend/Blacklists/')
        ];
        $this -> vars['breadcrumb']['child'] = [];
    }

    public function update($id){
        $this -> _breadcrumb();
        parent::update($id);
    }

    public function create(){
        $this -> _breadcrumb();
        parent::create();
    }
    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(       
                'label' => 'Delete',      
                'icon'   => 'oc-icon-trash-o',
                'default' => true,
                'redirect' => array(
                        'enable' => true,
                    ),
            ),
        ];
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            parent::bulkAction(Blacklist::class,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }
}
