<?php namespace Legato\Backend\Controllers;

use BackendMenu;
use Common\Controllers\CommonController;
use System\Classes\SettingsManager;
use Legato\Backend\Models\Whitelist;

/**
 * Whitelist Back-end Controller
 */
class Whitelists extends CommonController
{
    public $requiredPermissions=['legato.backend.whitelists'];
    public function __construct()
    {
        parent::__construct();

        //$this -> _breadcrumb();
        $this -> _bulks_list();
        BackendMenu::setContext('October.System', 'system', 'users');
        SettingsManager::setContext('Legato.Backend', 'whitelist_ip');
        //$this -> _vars();
    }

    private function _vars(){}

    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => 'WhiteList',
            'url'   => \Backend::url('/Legato/Backend/Whitelists/')
        ];
        $this -> vars['breadcrumb']['child'] = [];
    }

    public function update($id){
        $this -> _breadcrumb();
        parent::update($id);
    }

    public function create(){
        $this -> _breadcrumb();
        parent::create();
    }
    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(       
                'label' => 'Delete',      
                'icon'   => 'oc-icon-trash-o',
                'default' => true,
                'redirect' => array(
                        'enable' => true,
                    ),
            ),
        ];
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            parent::bulkAction(Whitelist::class,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }
}
