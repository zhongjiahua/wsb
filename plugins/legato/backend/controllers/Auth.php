<?php namespace Legato\Backend\Controllers;

use Mail;
use Flash;
use Backend;
use Validator;
use BackendAuth;
use Backend\Models\AccessLog;
use Backend\Classes\Controller;
use System\Classes\UpdateManager;
use ApplicationException;
use ValidationException;
use Exception;
use Backend\Controllers\Auth as BackendAuthController;
use Legato\Backend\Models\Attempt as AttemptModel;
use Legato\Backend\Models\Resetpassword as ResetpasswordModel;
use Carbon\Carbon;
use \Legato\Backend\Classes\IpBlocker;
use \Legato\Backend\Classes\BackendCaptcha;
use Common\Classes\UserService;
use Alxy\Captcha\Models\Settings as CaptchaSetting;

/**
 * Auth Back-end Controller
 */
class Auth extends BackendAuthController
{
    public function signin()
    {
        $this->bodyClass = 'signin';

        $captcha = new BackendCaptcha();
        $this-> vars['captcha'] = $captcha->render();
        try {
            if (post('postback')) {
                return $this->signin_onSubmit();
            }

            $this->bodyClass .= ' preload';
        } catch (Exception $ex) {
            Flash::error($ex->getMessage());
        }
    }

    public function signin_onSubmit()
    {
        $rules = [
            'login'    => 'required',
            'password' => 'required'
        ];
        $validation = \Validator::make(post(), $rules);
        if ($validation->fails()) {
            throw new \ValidationException($validation);
        }

        $captcha = new BackendCaptcha();
        $captcha->isValid(post());
        

        if (is_null($remember = config('cms.backendForceRemember', true))) {
            $remember = (bool) post('remember');
        }

        //Check ip before authenticate.
        $ib = new IpBlocker(\Request::ip(),post('login'));

        if(!$ib->checkWhiteList()){
            Flash::error("You are not in the trusted list. Please contact your System Administrator.");
            return Backend::redirectIntended('backend');
        }
        if(!$ib->checkBlackList()){
            Flash::error("Your IP have been blocked. Please contact your System Administrator.");
            return Backend::redirectIntended('backend');
        }
        if(!$ib->checkIpAttempt()){
        	$unblocked_at = new \Carbon\Carbon($ib->unblockedAt());
        	$unblocked_at=$unblocked_at->diffForHumans(null,true);
        	//$now = Carbon::now();

        	Flash::error('You have tried too many times. Please come back after '.$unblocked_at);
    		return Backend::redirectIntended('backend');
        }
	    

        // Authenticate user
        try{
	        $user = $this->authenticate(post('login'),post('password'), $remember);
	        $ib->clearAttemptLog();
	    }
        catch (Exception $ex) {
        	// Record IP address when login failed
        	$ib->logIpAttempt();
            Flash::error($ex->getMessage());
        }


        try {
            // Load version updates
            UpdateManager::instance()->update();
        }
        catch (Exception $ex) {
            Flash::error($ex->getMessage());
        }

        // Log the sign in event
        AccessLog::add($user);

        // Redirect to the intended page after successful sign in
        return Backend::redirectIntended('backend');
    }
    public function restore_onSubmit()
    {
        $user = BackendAuth::findUserByLogin(post('login'));
        if (!$user) {
            throw new ValidationException([
                'login' => trans('backend::lang.account.restore_error', ['login' => post('login')])
            ]);
        }
        $reset_limit = \Config::get('legato.backend::reset_password.number_of_attempts',3);
        $today = Carbon::today()->startOfDay()->toDateTimeString();
        $resetted = ResetpasswordModel::where('user_id',$user->id)->where('created_at','>=',$today)->count();
        if($resetted<$reset_limit){
            Flash::success(trans('backend::lang.account.restore_success'));

            $code = $user->getResetPasswordCode();

            $reset = new ResetpasswordModel;
            $reset->user_id = $user->id;
            $reset->reset_password_code = $code;
            $reset->save();

            $link = Backend::url('legato/backend/auth/reset/'.$user->id.'/'.$code);

            $data = [
                'name' => $user->full_name,
                'link' => $link,
            ];

            Mail::send('backend::mail.restore', $data, function ($message) use ($user) {
                $message->to($user->email, $user->full_name)->subject(trans('backend::lang.account.password_reset'));
            });
        }else{
            Flash::error('You have reach the limit. Please try again tomorrow.');

        }
        

        return Backend::redirect('backend/auth/signin');
    }

    public function reset_onSubmit()
    {
        if (!post('id') || !post('code')) {
            throw new ApplicationException(trans('backend::lang.account.reset_error'));
        }

        \Legato\Backend\Helpers\Functions::strong_password(post());

        $code = post('code');
        $user = BackendAuth::findUserById(post('id'));

        if (!$user->checkResetPasswordCode($code)) {
            throw new ApplicationException(trans('backend::lang.account.reset_error'));
        }

        // Set duration for the password reset code
        $reset_code = ResetpasswordModel::where('user_id',$user->id)->where('reset_password_code',$code)->first();
        $expired_at = new Carbon($reset_code->created_at);
        $expired_at->addHours(\Config::get('legato.backend::reset_password.valid_duration',3));
        if($expired_at->isPast()){
            throw new ApplicationException('The password reset code is expired.');
        }

        if (!$user->attemptResetPassword($code, post('password'))) {
            throw new ApplicationException(trans('backend::lang.account.reset_fail'));
        }

        $user->clearResetPassword();

        Flash::success(trans('backend::lang.account.reset_success'));

        return Backend::redirect('backend/auth/signin');
    }

    public static function check_captcha_is_valid($var){
        return \Captcha::check($var);
    }

    public function authenticate($login,$password,$remember){
        $user = \BackendAuth::authenticate([
            'login' => $login,
            'password' => $password
        ], $remember);
        return $user;
    }
}
