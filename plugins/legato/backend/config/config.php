<?php

return [
	'user' => [
		// Enable Strong password protection
		'strong_password_enable' => env('BACKEND_STRONG_PASSWORD', true),

		// Define the minimum required length of password
	    'password_min_length' => 4,
		// Define the maximum required length of password
	    'password_max_length' => 255,

		// Define the password should contain least one number
	    'at_least_one_number' => true,
		// Define the password should contain at least one upper case character
	    'at_least_one_upper_case_character' => true,
		// Define the password should contain at least one lower case character
	    'at_least_one_lower_case_character' => true,
		// Define the password should contain at least one non-word character
	    'at_least_one_special_character' => false,
	    /** 
	     * Define the list of enabled special character e.g. '!\"#$%&\'()*+,-.\/:;<=>?@\\[\\\\\\]^_`{|}~'
	     * If this list is not setted, all special chacter is allowed
	     */
	    'special_character_list' => '!\"#$%&\'()*+,-.\/:;<=>?@\\[\\\\\\]^_`{|}~',
	    // Define the comment under the password field
	    'field_comment' => '4-255 digits mixed with uppercase, lowercase, number and symbol',
    ],
    
    'login_attempt' => [
    	//Enable to Block a IP for z hours after x times unsuccessful authenticate within y hours
    	'enable' => env('BACKEND_LOGIN_ATTEMPT', true),

    	'after_x_times_failed' => 5,
    	'with_in_y_hours' => 5,
    	'block_for_z_hours' => 12,
    ],
    //Enable Trusted list protection for Backend User
    'enable_whitelisted_ip' => env('BACKEND_WHITELIST', true),
    //Enable Blacklist protection for Backend User
    'enable_blacklisted_ip' => env('BACKEND_BLACKLIST', true),
    

    'reset_password' =>[
    	// Define the valid duration of the password reset link (in hours)
		'valid_duration' => 3,
		// Define the number of attempts for resetting password allowed per day
		'number_of_attempts' => 4,
    ],


    'captcha' => [
    	// Enable Captcha in backend login page
    	'enable' => env('BACKEND_CAPTCHA_ENABLE', true),
    	/**
		 * Select type of captcha
		 * available type: 'simple','recaptcha' 
    	 */
    	'type' => env('BACKEND_CAPTCHA_TYPE','simple')
    ]
];