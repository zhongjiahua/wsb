<?php namespace Legato\Backend\Models;

use Model;
use Common\Models\CommonModel;

/**
 * Ip Model
 */
class Attempt extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_backend_user_ip_attempts';
    
    public $updated_by;
    public $created_by;

}
