<?php namespace Legato\Backend\Models;

use Model;
use Common\Models\CommonModel;
/**
 * Blacklist Model
 */
class Whitelist extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_backend_whitelist_ip';

	public $rules = [
        'ip_address' => 'required|ip',
    ];
    public $customMessages = [
    ];

    public $belongsTo = [
        'user' => [
            'Backend\Models\User',
            'key' => 'user_id'
        ],
    ];
}
