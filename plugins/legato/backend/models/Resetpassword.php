<?php namespace Legato\Backend\Models;

use Common\Models\CommonModel;

/**
 * Resetpassword Model
 */
class Resetpassword extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_backend_user_resetpasswords';
    
    public $updated_by;
    public $created_by;

}
