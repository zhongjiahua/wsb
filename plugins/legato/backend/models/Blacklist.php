<?php namespace Legato\Backend\Models;

use Model;
use Common\Models\CommonModel;
/**
 * Blacklist Model
 */
class Blacklist extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_backend_blacklist_ip';

    public $rules = [
        'ip_address' => 'required|ip',
    ];
}
