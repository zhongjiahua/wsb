<?php namespace Legato\Backend;

use Backend;
use Carbon\Carbon;
use System\Classes\PluginBase;
use Legato\Backend\Models\Blacklist;
use Legato\Backend\Classes\IpBlocker;
use System\Classes\SettingsManager;
use Zablose\Captcha\Captcha;
use Backend\Models\UserRole;


/**
 * Backend Plugin Information File
 */
class Plugin extends PluginBase
{
    //public $elevated = true;
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Backend',
            'description' => 'Extend October Backend Behaviour',
            'author'      => 'Legato',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {   
        \App::register('\Latrell\Captcha\CaptchaServiceProvider');
        $facade = \Illuminate\Foundation\AliasLoader::getInstance();
        $facade->alias('Captcha', '\Latrell\Captcha\Facades\Captcha');


        /*\App::register('\Wicochandra\Captcha\CaptchaServiceProvider');
        $facade = \Illuminate\Foundation\AliasLoader::getInstance();
        $facade->alias('Captcha', '\Wicochandra\Captcha\Facade\Captcha');

        $this->excel = \App::make('Captcha');*/

        //\App::register('\Igoshev\Captcha\Providers\CaptchaServiceProvider');
        //\App::register('\Mews\Captcha\CaptchaServiceProvider');
        /*$facade = \Illuminate\Foundation\AliasLoader::getInstance();
        $facade->alias('Captcha', '\Mews\Captcha\Facades\Captcha');

        $this->excel = \App::make('Captcha');*/
        /*$facade = \Illuminate\Foundation\AliasLoader::getInstance();
        $facade->alias('Excel', '\Maatwebsite\Excel\Facades\Excel');

        $this->excel = \App::make('excel');
        */
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        
        $this->strongPasswordForBackendUser();
        $this->backendUserMultiRoles();
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Legato\Backend\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        
        return [
            'legato.backend.blacklists' => [
                'tab' => 'system::lang.permissions.name',
                'label' => 'Blacklists Management'
            ],
            'legato.backend.whitelists' => [
                'tab' => 'system::lang.permissions.name',
                'label' => 'Whitelists Management'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'backend' => [
                'label'       => 'Backend',
                'url'         => Backend::url('legato/backend/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['legato.backend.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSchedule($schedule)
    {   
        $schedule->call(function () {
            $ib = new IpBlocker();
            $ib->unblockIPAttempt();
        })->everyMinute()->name('unblock_ip')->withoutOverlapping();
    }

    public function registerSettings()
    {
        return [
            'blacklist_ip' => [
                'label'       => 'Blacklist IP',
                'description' => 'Manage Blacklist IP',
                'category'    => SettingsManager::CATEGORY_SYSTEM,
                'icon'        => 'icon-lock',
                'url'         => Backend::url('legato/backend/blacklists'),
                'permissions' => ['legato.backend.blacklists'],
                'order'       => 999,
                'keywords'    => 'blacklists ip',
            ],
            'whitelist_ip' => [
                'label'       => 'Whitelist IP',
                'description' => 'Manage Whitelist IP',
                'category'    => SettingsManager::CATEGORY_SYSTEM,
                'icon'        => 'icon-unlock-alt',
                'url'         => Backend::url('legato/backend/whitelists'),
                'permissions' => ['legato.backend.whitelists'],
                'order'       => 999,
                'keywords'    => 'whitelists ip',
            ],
        ];
    }

    public function strongPasswordForBackendUser(){
        if(!\Config::get('legato.backend::user.strong_password_enable')){
            return false;
        }
        \Event::listen('backend.form.extendFields', function($widget){
            //if (!$widget->getController() instanceof \Backend\Models\User) return;
            if (!$widget->model instanceof \Backend\Models\User) return;
            if($comment = \Config::get('legato.backend::user.field_comment'))
                $widget->getField('password')->comment=$comment;

        });
        \Backend\Models\User::extend(function($model){ 

            $model->bindEvent('model.beforeValidate', function() use ($model) {
                $rule = 'required:create|confirmed';//
                $model->rules['password'] = $rule;
                $model->rules['password_confirmation'] = 'required_with:password';
                //$model->customMessages[strtolower($field_name).'.required']=$label_names[$key].' is required.';
                
                if(post('User')['password']){
                    \Legato\Backend\Helpers\Functions::strong_password(post('User'));
                }

                

            });
        });
    }



    public function backendUserMultiRoles(){
        \Backend\Models\User::extend(function($model){ 
            unset($model->belongsTo['role']);
            $model->belongsToMany['role'] = [UserRole::class, 'table' => 'backend_users_roles'];

            /**
             *
             * Function cannot be override from outside, therefore added in core directly
             *
             */
            /*$model->addDynamicMethod('getMergedPermissions', function($code) use ($model) {
                if (!$model->mergedPermissions) {
                    $permissions = [];
                    if ($roles = $model->getRole()) {
                        foreach ($roles as $role) {
                            if (is_array($role->permissions)) {
                                $permissions = array_merge($permissions, $role->permissions);
                            }
                        }
                    }
                    if (is_array($model->permissions)) {
                        $permissions = array_merge($permissions, $model->permissions);
                    }
                    $model->mergedPermissions = $permissions;
                }
                return $model->mergedPermissions;
            });*/
        });

        \Event::listen('backend.form.extendFields', function($widget){
            if (!$widget->getController() instanceof \Backend\Controllers\Users) return;
            if (!$widget->model instanceof \Backend\Models\User) return;
            $widget->removeField('role');
            $widget->addTabFields([
                'role' => [
                    'label' => 'Roles',
                    'context' =>   ['create', 'update'],
                    'commentAbove' => \Lang::get('backend::lang.user.role_comment'),
                    'type' => 'checkboxlist'
                ]
            ]);

        });
    }
}
