<?php namespace Legato\Backend\Classes;

use Carbon\Carbon;
use Config;
use Captcha;
use \Alxy\Captcha\Models\Settings as RecaptchaSetting;
use ReCaptcha\ReCaptcha;

class BackendCaptcha
{

    private $captcha_enable;
    private $captcha_view;
    private $captcha_check;
    private $captcha_field;

    public function __construct(){
        $captcha_field_list = ['simple'=>'captcha','recaptcha'=>'g-recaptcha-response'];
        $this->captcha_enable = Config::get('legato.backend::captcha.enable',false);
        $type = Config::get('legato.backend::captcha.type','simple');
        $this->captcha_view = $type.'CaptchaView';
        $this->captcha_check = $type.'IsValid';
        $this->captcha_field = $captcha_field_list[$type];
    }

    public function render(){
        if($this->captcha_enable){
            $view = $this->captcha_view;
            return $this->$view();

        }
    }

    public function isValid($req){
        if($this->captcha_enable){
            $check = $this->captcha_check;
            return $this->$check($req[$this->captcha_field]);
        }
        return true;
    }
    public function simpleIsValid($code){
        if(Captcha::check($code))
            return true;
        else{
            throw new \ValidationException(['x' => 'Are you a robot ?']);
        }
    }
    public function recaptchaIsValid($code){
        $reCaptcha = new ReCaptcha(RecaptchaSetting::get('secret_key'));
        $response = $reCaptcha->verify(
            $code,
            \Request::ip()
        );
        if (! $response->isSuccess()) {
            $missing_captcha = 'Are you a robot ?';
            foreach ($response->getErrorCodes() as $code) {
                if($code == 'missing-input-response'){
                    throw new \ValidationException(['x' => $missing_captcha]);
                } else{
                    throw new \ValidationException(['x' => $code]);
                }
            }
        }
        return true;
    }

    private function recaptchaCaptchaView(){
        return '<div class="g-recaptcha" 
                    data-sitekey="'.RecaptchaSetting::get('site_key').'" 
                    style="transform:scale(0.8);-webkit-transform:scale(0.8);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>';

    }

    private function simpleCaptchaView(){
        $captcha_url = \Captcha::url();
        return '<div style="width:240px;">
                    <img id="captcha" src="'.$captcha_url.'" style="cursor:pointer;" onClick="this.src=\''.$captcha_url.'?fresh=\'+Math.random();">
                    <input
                        type="text"
                        name="captcha"
                        value=""
                        class="form-control"
                        style="margin-bottom: 10px;border-top-right-radius:0px;border-top-left-radius:0px;"
                        placeholder="Captcha"
                        autocomplete="off"
                        maxlength="255" />
                </div>';

    }


}