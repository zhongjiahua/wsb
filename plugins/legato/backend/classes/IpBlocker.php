<?php namespace Legato\Backend\Classes;

use Carbon\Carbon;
use Config;

class IpBlocker
{
    protected $blackListModel = 'Legato\Backend\Models\Blacklist';
    protected $whiteListModel = 'Legato\Backend\Models\Whitelist';
    protected $ipAttemptModel = 'Legato\Backend\Models\Attempt';

    protected $login_column = 'login';

    private $ip;
    private $login_name;
    private $ip_attempt_enable;
    private $ip_attempt_x;
    private $ip_attempt_y;
    private $ip_attempt_z;
    private $whitelist_enable;
    private $blacklist_enable;

    public function __construct($ip=null,$login_name=null){
        $this->ip = $ip;
        $this->login_name = $login_name;
        
        $this->whitelist_enable = Config::get('legato.backend::enable_whitelisted_ip',false);
        $this->blacklist_enable = Config::get('legato.backend::enable_blacklisted_ip',false);

        $this->ip_attempt_enable = Config::get('legato.backend::login_attempt.enable',false);
        $this->after_x_times_failed = Config::get('legato.backend::login_attempt.after_x_times_failed',5);
        $this->with_in_y_hours = Config::get('legato.backend::login_attempt.with_in_y_hours',1);
        $this->block_for_z_hours = Config::get('legato.backend::login_attempt.block_for_z_hours',1);
    }

    /**
     * Get when will this ip unblock
     */
    public function unblockedAt(){
        $class = $this->blackListModel;
        $ip = $class::where('ip_address',$this->ip)->whereNotNull('unblocked_at')->first();
        if($ip)
            return $ip->unblocked_at;
        else
            return false;
    }

    /**
     * Log attempt record
     */
    public function logIpAttempt(){
        $class = $this->ipAttemptModel;
        $attempt = new $class;
        $attempt->ip_address= $this->ip;
        $attempt->login= $this->login_name;
        $attempt->save();

        //check the number of failed attempts of this ip
        if($this->ip_attempt_enable){
            $before = Carbon::now()->subHours($this->with_in_y_hours);
            $count = $class::where('ip_address',$this->ip)->with('login',$this->login_name)->where('created_at','>=',$before->toDateTimeString())->count();
            if($count>=$this->after_x_times_failed){
                \Log::info('Block IP:'.$this->ip.' due to too may times of login failed of account:'.$this->login_name);
                $this->putIpInBlackList();
            }
        }
    }

    /**
     * Clear attempt record
     */
    public function clearAttemptLog(){
        $class = $this->ipAttemptModel;
        $count = $class::where('ip_address',$this->ip)->where('login',$this->login_name)->delete();
    }

    public function isAccessDeny(){
        return !$this->checkWhiteList() && !$this->checkIpAttempt() && !$this->checkBlackList();
    }

    /**
     * Check is this account support white list and check is that ip in white list
     */
    public function checkWhiteList(){
        if($this->whitelist_enable){
            $class = $this->whiteListModel;
            $q1 = $class::whereHas('user', function($query){
                $query->where($this->login_column,$this->login_name);
            });
            $q2 = $class::whereNull('user_id');
            $user_in_whitelist = $q1->union($q2)->get();
            if(count($user_in_whitelist)>0){
                $ip_in_whitelist = $user_in_whitelist->contains('ip_address',$this->ip);
                if($ip_in_whitelist){
                    return true;
                }else{
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check is this IP in the black list
     */
    public function checkBlackList(){
        if($this->blacklist_enable){
            $class = $this->blackListModel;
            $black_list = $class::where('ip_address',$this->ip)->whereNull('unblocked_at')->exists();
            if($black_list){
                return false;
            }
        }
        return true;
    }

    /**
     * Check is this IP attmempt to login this account too many times
     */
    public function checkIpAttempt(){
        if($this->ip_attempt_enable){
            //check is the ip already black listed
            $class = $this->blackListModel;
            $black_list = $class::where('ip_address',$this->ip)->whereNotNull('unblocked_at')->exists();
            if($black_list){
                return false;
            }
        }
        return true;
    }

    /**
     * Put a IP in blacklist because of too many login attempt
     */
    private function putIpInBlackList(){
        $blacklist = new $this->blackListModel();
        $blacklist->ip_address = $this->ip;
        $blacklist->unblocked_at = Carbon::now()->addHours($this->block_for_z_hours)->toDateTimeString();
        $blacklist->save();
    }

    /**
     * Unlock the blacklist add from IP attempt at specific time
     */
    public function unblockIPAttempt(){
        if($this->ip_attempt_enable){
            $class = $this->blackListModel;
            $unblock_lists = $class::where('unblocked_at','<=',Carbon::now()->toDateTimeString())->get();
            foreach ($unblock_lists as $key => $value) {
                $value->delete();
            }
        }
    }

}