<?php namespace Legato\Backend\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAttemptsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_backend_user_ip_attempts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->index();
            $table->string('ip_address')->nullable()->index();
            $table->string('login')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_backend_user_ip_attempts');
    }
}
