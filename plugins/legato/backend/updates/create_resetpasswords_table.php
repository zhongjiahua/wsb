<?php namespace Legato\Backend\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateResetpasswordsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_backend_user_resetpasswords', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('user_id')->index();
            $table->string('reset_password_code')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_backend_user_resetpasswords');
    }
}
