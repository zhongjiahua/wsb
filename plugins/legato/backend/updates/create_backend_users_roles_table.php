<?php namespace Legato\Backend\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class create_backend_users_roles_table extends Migration
{
    public function up()
    {
        Schema::dropIfExists('backend_users_roles');
        Schema::create('backend_users_roles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('user_role_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('backend_users_roles');
    }

}
