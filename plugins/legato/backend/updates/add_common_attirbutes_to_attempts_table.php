<?php namespace Legato\Backend\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCommonAttirbutesToAttemptsTable extends Migration
{
    public function up()
    {
        Schema::table('legato_backend_user_ip_attempts', function ($table) {
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasColumn('legato_backend_user_ip_attempts', 'created_by')) {
            Schema::table('legato_backend_user_ip_attempts', function($table)
            {
                $table->dropColumn('created_by');
            });
        }
        if (Schema::hasColumn('legato_backend_user_ip_attempts', 'updated_by')) {
            Schema::table('legato_backend_user_ip_attempts', function($table)
            {
                $table->dropColumn('updated_by');
            });
        }
    }
}
