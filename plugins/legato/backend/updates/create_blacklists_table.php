<?php namespace Legato\Backend\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBlacklistsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_backend_blacklist_ip', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ip_address')->nullable()->index();
            $table->timestamp('unblocked_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_backend_blacklist_ip');
    }
}
