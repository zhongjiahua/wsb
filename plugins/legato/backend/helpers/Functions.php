<?php
namespace Legato\Backend\Helpers;

class Functions{

    /**
     *  Check is the password in correct format
     *  Usage: Common\Helpers\Functions:: strong_password($arr);
     * @param $path
     * @return ValidationException
     */
    public static function strong_password($var){

        $min_pw = \Config::get('legato.backend::user.password_min_length',4);
        $max_pw = \Config::get('legato.backend::user.password_max_length',255);

        $pw_num = \Config::get('legato.backend::user.at_least_one_number');

        $pw_uc = \Config::get('legato.backend::user.at_least_one_upper_case_character');
        $pw_lc = \Config::get('legato.backend::user.at_least_one_lower_case_character');
        $pw_sc = \Config::get('legato.backend::user.at_least_one_special_character');
        $pw_sc_list = \Config::get('legato.backend::user.special_character_list');
        
        if($pw_num || $pw_uc || $pw_lc || $pw_sc || $pw_sc_list){
            $regex = '/^';
            if($pw_num){
                $regex .= '(?=.*\d)';
            }
            if($pw_uc){
                $regex .= '(?=.*[A-Z])';
            }
            if($pw_lc){
                $regex .= '(?=.*[a-z])';
            }
            if($pw_sc){
                if($pw_sc_list){
                    $regex .= '(?=.*['.$pw_sc_list.'])';
                }else{
                    $regex .= '(?=.*\W)';
                }
            }
            if($pw_sc_list){
                $regex .= '([[a-zA-Z0-9'.$pw_sc_list.']+)$';
            } 
            //$regex = '/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]+)$/';
            $rules = [
                'password'    => ['regex:'.$regex.'/','required','between:'.$min_pw.','.$max_pw],
            ];
            $validation = \Validator::make($var, $rules);
            if ($validation->fails()) {
                throw new \ValidationException($validation);
            }
        }
    }

    
}
?>