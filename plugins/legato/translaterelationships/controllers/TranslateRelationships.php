<?php namespace Legato\TranslateRelationships\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Translate Relationships Back-end Controller
 */
class TranslateRelationships extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Legato.TranslateRelationships', 'translaterelationships', 'translaterelationships');
    }
}
