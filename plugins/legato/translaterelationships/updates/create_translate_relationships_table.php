<?php namespace Legato\TranslateRelationships\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTranslateRelationshipsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_translaterelationships_translate_relationships', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_translaterelationships_translate_relationships');
    }
}
