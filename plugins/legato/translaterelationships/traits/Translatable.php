<?php namespace Legato\TranslateRelationships\Traits;

use DB;

trait Translatable
{
    /**
     * Method that saves the translation data of modal.
     * TODO: Try to integrate this in translate plugin.
     *
     * @uses DB
     */
    public function legatoTranslate()
    {
        if (post("RLTranslate")) {
            foreach (post("RLTranslate") as $key => $value) {

                $data = json_encode($value);
                $obj = Db::table("rainlab_translate_attributes")
                    ->where("locale", $key)
                    ->where("model_id", $this->id)
                    ->where("model_type", get_class($this->model));

                // If the translate object exist in database update it
                if ($obj->count() > 0) {
                    $obj->update(["attribute_data" => $data]);
                }else{
                    Db::table("rainlab_translate_attributes")->insert([
                        "locale" => $key,
                        "model_id" => $this->id,
                        "model_type" => get_class($this->model),
                        "attribute_data" => $data
                    ]);
                }

            }
        }
    }

    /**
     * Boot the Translatable trait for this model.
     *
     * @return void
     */
    public static function bootTranslatable()
    {
        static::extend(function($model) {
            $model->bindEvent('model.afterSave', function () use ($model) {
                $model->legatoTranslate();
            });
        });
    }
}

?>
