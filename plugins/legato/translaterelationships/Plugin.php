<?php namespace Legato\TranslateRelationships;

use Backend;
use System\Classes\PluginBase;

/**
 * TranslateRelationships Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'TranslateRelationships',
            'description' => 'No description provided yet...',
            'author'      => 'Legato',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Legato\TranslateRelationships\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'legato.translaterelationships.some_permission' => [
                'tab' => 'TranslateRelationships',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'translaterelationships' => [
                'label'       => 'TranslateRelationships',
                'url'         => Backend::url('legato/translaterelationships/translaterelationships'),
                'icon'        => 'icon-leaf',
                'permissions' => ['legato.translaterelationships.*'],
                'order'       => 500,
            ],
        ];
    }

// 	DROP TABLE IF EXISTS `legato_translaterelationships_translate_relationships`;
// CREATE TABLE `legato_translaterelationships_translate_relationships` (
//   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
//   PRIMARY KEY (`id`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
}
