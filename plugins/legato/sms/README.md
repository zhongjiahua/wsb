                                                    October SMS Module
                                                    ------------------

1. Setup the project in Centralized Server.
	Login URL : https://sms-server.legato.co/SMS_Centralized_Server
	Username : admin
	Password : 123456

2. Click "Project Setup" in the menu, field in the information of Setup form

3. Click "Project List", you can find your created project and will show the "Key", "Secret key" and "Signature"

4. Open the sms/config/config.php file, edit follow item of the info you can find in 3.:
	'PROJECT_KEY' => 'KEY_OF_YOUR_PROJECT',
    'PROJECT_SECRET_KEY' => 'SECRET_KEY_OF_YOUR_PROJECT',
    'PROJECT_SIGNATURE' => 'SIGNATURE_OF_YOUR_PROJECT',

5. Set cronjob in terminal
	- ssh to your project server
	- sudo crontab -e
	- Add this line to the contab page
		* * * * * /YOUR/PHP/PATH/php -q /REAL_PATH/OF/THE_FILE/ARTISAN/artisan schedule:run
	- Save and exit the crontab

6. Setup for use SMS module in your page, add this two line at top of the page(Inside <php tag)
	use Legato\SMS\Models\core;
	use Legato\SMS\Models\SmsMessage;

7. Setup array for send SMS
	$data = array();
    $data['phone_no'] = array("85212345678");
    $data['message'] = "october test sms";
    $data['protocol'] = 1;					0 = HTTP(Only can use in NEXMO), 1 = SMPP
    $data['service_provider'] = 1;			0 = NEXMO(Not use now), 1 = CITIC
    SmsMessage::sendSMS($data);

Stop SMS send function in your server
	- Comment you cronjob command like this :
		#* * * * * /YOUR/PHP/PATH/php -q /REAL_PATH/OF/THE_FILE/ARTISAN/artisan schedule:run
	- Edit the file legato/sms/helpers/switch_send.txt name, e.g. switch_send2.txt
	- When you need to reopen the SMS function next time, please uncomment the cronjob and rename the file name to switch_send.txt