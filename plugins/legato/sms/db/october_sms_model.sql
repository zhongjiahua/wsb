-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- 主機: localhost
-- 產生時間： 2018 年 10 月 01 日 17:17
-- 伺服器版本: 10.1.34-MariaDB
-- PHP 版本： 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `generic-octobercms-develop`
--

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_complete`
--

CREATE TABLE `legato_sms_complete` (
  `id` int(11) NOT NULL,
  `msg_id` int(11) NOT NULL,
  `sms_token` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_token` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `protocol` varchar(100) NOT NULL,
  `service_provider` varchar(100) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_total_retry` int(11) NOT NULL,
  `error_code` varchar(200) NOT NULL,
  `error_message` varchar(200) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_complete_archive`
--

CREATE TABLE `legato_sms_complete_archive` (
  `id` int(11) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_total_retry` int(11) NOT NULL,
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_delivery`
--

CREATE TABLE `legato_sms_delivery` (
  `id` int(11) UNSIGNED NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_delivery_archive`
--

CREATE TABLE `legato_sms_delivery_archive` (
  `id` int(11) UNSIGNED NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_message`
--

CREATE TABLE `legato_sms_message` (
  `msg_id` int(11) UNSIGNED NOT NULL,
  `sms_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_message` int(11) NOT NULL,
  `num_success` int(11) NOT NULL,
  `num_fail` int(11) NOT NULL,
  `num_processing` int(11) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `num_delivered` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OA` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suffix` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_message_archive`
--

CREATE TABLE `legato_sms_message_archive` (
  `msg_id` int(11) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `num_message` int(11) NOT NULL,
  `num_success` int(11) NOT NULL,
  `num_fail` int(11) NOT NULL,
  `num_processing` int(11) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `num_delivered` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_progress`
--

CREATE TABLE `legato_sms_progress` (
  `id` int(11) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `sms_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_host_retry` int(11) NOT NULL DEFAULT '0',
  `num_total_retry` int(11) NOT NULL DEFAULT '0',
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_receiver`
--

CREATE TABLE `legato_sms_receiver` (
  `id` int(11) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `receiver_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `legato_sms_receiver_archive`
--

CREATE TABLE `legato_sms_receiver_archive` (
  `id` int(11) UNSIGNED NOT NULL,
  `msg_id` int(11) NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `legato_sms_complete`
--
ALTER TABLE `legato_sms_complete`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `legato_sms_complete_archive`
--
ALTER TABLE `legato_sms_complete_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msg_id` (`msg_id`),
  ADD KEY `num_retry` (`num_retry`),
  ADD KEY `current_retry` (`current_retry`),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`),
  ADD KEY `response_id` (`response_id`(191));

--
-- 資料表索引 `legato_sms_delivery`
--
ALTER TABLE `legato_sms_delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `response_id` (`response_id`(191)),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`);

--
-- 資料表索引 `legato_sms_delivery_archive`
--
ALTER TABLE `legato_sms_delivery_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `response_id` (`response_id`(191)),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`);

--
-- 資料表索引 `legato_sms_message`
--
ALTER TABLE `legato_sms_message`
  ADD PRIMARY KEY (`msg_id`),
  ADD KEY `schedule_time` (`schedule_time`),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`);

--
-- 資料表索引 `legato_sms_message_archive`
--
ALTER TABLE `legato_sms_message_archive`
  ADD PRIMARY KEY (`msg_id`),
  ADD KEY `schedule_time` (`schedule_time`),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`);

--
-- 資料表索引 `legato_sms_progress`
--
ALTER TABLE `legato_sms_progress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msg_id` (`msg_id`),
  ADD KEY `num_retry` (`num_retry`),
  ADD KEY `current_retry` (`current_retry`),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`);

--
-- 資料表索引 `legato_sms_receiver`
--
ALTER TABLE `legato_sms_receiver`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msg_id` (`msg_id`),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`);

--
-- 資料表索引 `legato_sms_receiver_archive`
--
ALTER TABLE `legato_sms_receiver_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msg_id` (`msg_id`),
  ADD KEY `status` (`status`),
  ADD KEY `create_date` (`create_date`),
  ADD KEY `modify_date` (`modify_date`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_complete`
--
ALTER TABLE `legato_sms_complete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_complete_archive`
--
ALTER TABLE `legato_sms_complete_archive`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_delivery`
--
ALTER TABLE `legato_sms_delivery`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_delivery_archive`
--
ALTER TABLE `legato_sms_delivery_archive`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_message`
--
ALTER TABLE `legato_sms_message`
  MODIFY `msg_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_message_archive`
--
ALTER TABLE `legato_sms_message_archive`
  MODIFY `msg_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_progress`
--
ALTER TABLE `legato_sms_progress`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_receiver`
--
ALTER TABLE `legato_sms_receiver`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `legato_sms_receiver_archive`
--
ALTER TABLE `legato_sms_receiver_archive`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
