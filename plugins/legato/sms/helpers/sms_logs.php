<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	// $host, $port, $api_key, $secret_key
	function sms_config_log($sms){
		$today = date("Y-m-d");
		$file_name = $today."_sms_logs.txt";
		$file_path = __DIR__."/../sms_logs/".$file_name;
		$add_log = "";
		if(!file_exists($file_path)){
			$file_path = fopen(__DIR__."/../sms_logs/".$file_name, "w") or die("Can't create file");
			$add_log .= "########################### Send SMS start ###########################\n\n";
			$add_log .= "Host IP : ". $sms->get_host() ."\n";
			$add_log .= "Port : ". $sms->get_port() ."\n";
			$add_log .= "API Key : ". $sms->get_api_key() ."\n";
			$add_log .= "Secret Key : ". $sms->get_secret_key() ."\n";
			$add_log .= "Service Provider : ". $sms->get_service_provider_desc() ."\n";
			$add_log .= "Protocol : ". $sms->get_protocol_desc() ."\n";
			$add_log .= "Sender : ". $sms->get_sender() ."\n";

			$phone_no = $sms->get_phone_no();
			$phone_no = $phone_no[0];

			$add_log .= "Phone no : ". $phone_no ."\n";
			$add_log .= "Message : ". $sms->get_message() ."\n";
			fwrite($file_path, $add_log);
		}else{
			$log_content = file_get_contents($file_path);
			$add_log .= "########################### Send SMS start ###########################\n\n";
			$add_log .= "Host IP : ". $sms->get_host() ."\n";
			$add_log .= "Port : ". $sms->get_port() ."\n";
			$add_log .= "API Key : ". $sms->get_api_key() ."\n";
			$add_log .= "Secret Key : ". $sms->get_secret_key() ."\n";
			$add_log .= "Service Provider : ". $sms->get_service_provider_desc() ."\n";
			$add_log .= "Protocol : ". $sms->get_protocol_desc() ."\n";
			$add_log .= "Sender : ". $sms->get_sender() ."\n";

			$phone_no = $sms->get_phone_no();
			$phone_no = $phone_no[0];
			
			$add_log .= "Phone no : ". $phone_no ."\n";
			$add_log .= "Message : ". $sms->get_message() ."\n";
			$add_log .= "\n";

			$log_content .= $add_log;
			file_put_contents($file_path,$log_content);
		}
		
	}

	function sms_log_respond($respond_id, $status){
		$today = date("Y-m-d");
		$file_name = $today."_sms_logs.txt";
		$file_path = __DIR__."/../sms_logs/".$file_name;
		$add_log = "";
		if(!file_exists($file_path)){
			$file_path = fopen(__DIR__."/../sms_logs/".$file_name, "w") or die("Can't create file");
			if($status == "success"){
				$add_log .= "Respond id : ".$respond_id ."\n";
				$add_log .= "Status : "."Success" ."\n";
			}else{
				$add_log .= "Status : "."Fail" ."\n";
			}

			fwrite($file_path, $add_log);
		}else{
			$log_content = file_get_contents($file_path);
			if($status == "success"){
				$add_log .= "Respond id : ".$respond_id ."\n";
				$add_log .= "Status : "."Success" ."\n";
			}else{
				$add_log .= "Status : "."Fail" ."\n";
			}

			$log_content .= $add_log;
			file_put_contents($file_path,$log_content);
		}
	}

	function sms_fail_log($error){
		$today = date("Y-m-d");
		$file_name = $today."_sms_logs.txt";
		$file_path = __DIR__."/../sms_logs/".$file_name;
		$add_log = "";
		if(!file_exists($file_path)){
			$file_path = fopen(__DIR__."/../sms_logs/".$file_name, "w") or die("Can't create file");
			$add_log .= "Error message : ".$error."\n";

			fwrite($file_path, $add_log);
		}else{
			$log_content = file_get_contents($file_path);
			$add_log .= "Error message : ".$error."\n";

			$log_content .= $add_log;
			file_put_contents($file_path,$log_content);
		}
	}
	
?>