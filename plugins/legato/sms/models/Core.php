<?php namespace Legato\SMS\Models;

use Model;
use DB;
use Config;
use Carbon\Carbon;
/**
 * Core Model
 */
class Core extends Model
{
    /**
     * @var string The database table used by the model.
     */
    // public $table = 'legato_sms_cores';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function add_message_receiver($sms){
        $msg_id = Db::table('legato_sms_message')->insertGetId(
            ['message' => $sms['message'], 'num_message' => $sms['num_message'], 'num_retry' => $sms['num_retry'], 
            'protocol' => $sms['protocol'], 'service_provider' => $sms['service_provider'], 'schedule_time' => $sms['schedule_time']]
        );

        $phones = $sms['phone_no'];

        for($i=0;$i<count($phones);$i++) {
            Db::table('legato_sms_receiver')->insert(['msg_id' => $msg_id, 'phone_no' => $phones[$i]]);
        }

        $status = Config::get("legato.sms::SMS_STATUS_PENDING");
        Db::table('legato_sms_message')
            ->where('msg_id', $msg_id)
            ->update(['status' => $status]);

        return $msg_id;
    }

    public static function add_progress_message(){
        $current_date = Carbon::now()->toDateTimeString();
        $find_status = Config::get("legato.sms::SMS_STATUS_PENDING");
        $record = Db::table('legato_sms_message')
                    ->where('status', '=', $find_status)
                    ->where('schedule_time', '<=', $current_date)
                    ->orderBy('create_date', 'asc')
                    ->take(1)
                    ->get();
        if(count($record) > 0) {
            $sms_array = (array)$record[0];
        }else{
            return false;
        }
        // \Log::info("add_progress_message run test : ".$sms_array);
        
        //upload sms_message and sms_receiver info to centaliz server
        $status = Config::get("legato.sms::SMS_STATUS_PROCESSING");
        Db::table('legato_sms_message')
            ->where('msg_id', $sms_array['msg_id'])
            ->update(['status' => $status]);

        Core::upload_toServer($sms_array['msg_id']);

        
        unset($record);
        unset($sms_array);
    }

    public static function upload_toServer($msg_id){
        $msg_record = Db::table('legato_sms_message')
                    ->where('msg_id', '=', $msg_id)
                    ->get();

        $message = (array)$msg_record[0];

        // $sql = "SELECT * 
        //         FROM sms_receiver
        //         WHERE msg_id = ?";
        // $sqlarray = array($message['msg_id']);
        // $receiver_list = $this->db->query($sql, $sqlarray)->result_array();
        $receiver_result = Db::table('legato_sms_receiver')
                        ->where('msg_id', '=', $msg_id)
                        ->get();
        $receiver_list = array();
        for($i=0;$i<count($receiver_result);$i++){
            $receiver_list[] = (array)$receiver_result[$i];
        }
        
        //Check project account
        $project_account_key = Config::get("legato.sms::PROJECT_KEY");
        $project_account_secret_key = Config::get("legato.sms::PROJECT_SECRET_KEY");
        $project_signature = Config::get("legato.sms::PROJECT_SIGNATURE");
        
        $data_array = array();
        $message_array = array();
        $data_array['key_str'] = $project_account_key;
        $data_array['secret_key'] = $project_account_secret_key;
        $data_array['signature'] = $project_signature;
        $data_array['message'] = $message;
        $data_array['receiver_list'] = $receiver_list;
        $json_array = json_encode($data_array);
        $data_array = array("data" => $json_array);

        $url = Config::get("legato.sms::CENT_URL");
        $url = $url."sms_api";
        $curl = curl_init();
        // curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_array);
        // curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
        //     'Content-Type: application/json',                                                                                
        //     'Content-Length: ' . strlen($json_array))                                                                       
        // );                                  

        $result = curl_exec($curl);
        $result = json_decode($result, TRUE);

        // update return token
        Db::table('legato_sms_message')
            ->where('msg_id', $result['message']['msg_id'])
            ->update(['sms_token' => $result['message']['sms_token'],
                        'OA' => $result['message']['OA'],
                        'suffix' => $result['message']['suffix'],
                        'sender' => $result['message']['sender']]);

        for($i=0;$i<count($result['receiver_list']);$i++){
            Db::table('legato_sms_receiver')
            ->where('id', $result['receiver_list'][$i]['id'])
            ->update(['receiver_token' => $result['receiver_list'][$i]['receiver_token']]);
        }

        // var_dump($result);

    }

    public static function update_msg_status(){
        $status = Config::get("legato.sms::SMS_STATUS_PROCESSING");
        $messages_result = Db::table('legato_sms_message')
                        ->where('status', '=', $status)
                        ->orderBy('create_date', 'asc')
                        ->get();
        $messages = array();
        for($i=0;$i<count($messages_result);$i++){
            $messages[] = (array)$messages_result[$i];
        }

        $project_account_key = Config::get("legato.sms::PROJECT_KEY");
        $project_account_secret_key = Config::get("legato.sms::PROJECT_SECRET_KEY");

        foreach ($messages as $message){
            $data_array = array();
            $data_array['key_str'] = $project_account_key;
            $data_array['secret_key'] = $project_account_secret_key;
            $data_array['sms_token'] = $message['sms_token'];
            $json_array = json_encode($data_array);
            $data_array = array("data" => $json_array);

            $url = Config::get("legato.sms::CENT_URL");
            $url = $url."update_status_api";
            $curl = curl_init();
            // curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_array);
            // curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
            //     'Content-Type: application/json',                                                                                
            //     'Content-Length: ' . strlen($json_array))                                                                       
            // );                                  

            $result = curl_exec($curl);
            $result_data = json_decode($result, TRUE);
            var_dump($result_data);
            $active_status = Config::get("legato.sms::STATUS_ACTIVE");
            if($result_data['status'] == $active_status){
                $sqlarray = array(
                                "num_success" => $result_data['msg_update']['msg_info']["num_success"],
                                "num_processing" => $result_data['msg_update']['msg_info']["num_processing"],
                                "num_fail" => $result_data['msg_update']['msg_info']["num_fail"],
                            );

                for($i=0;$i<count($result_data['msg_update']['complete_result']);$i++){
                    // Insert to local
                    var_dump("1");
                    date_default_timezone_set('Asia/Hong_Kong');
                    $current_date = date("Y-m-d h:i:s");
                    Db::table('legato_sms_complete')->insert(['msg_id' => $message['msg_id'], 'sms_token' => $result_data['msg_update']['complete_result'][$i]['sms_token'], 
                        'receiver_token' => $result_data['msg_update']['complete_result'][$i]['receiver_token'], 'message' => $result_data['msg_update']['complete_result'][$i]['message'], 
                        'response_id' => $result_data['msg_update']['complete_result'][$i]['response_id'], 'phone_no' => $result_data['msg_update']['complete_result'][$i]['phone_no'], 
                        'protocol' => $result_data['msg_update']['complete_result'][$i]['protocol'], 'service_provider' => $result_data['msg_update']['complete_result'][$i]['service_provider'], 
                        'num_retry' => $result_data['msg_update']['complete_result'][$i]['num_retry'], 'current_retry' => $result_data['msg_update']['complete_result'][$i]['current_retry'],
                        'num_total_retry' => $result_data['msg_update']['complete_result'][$i]['num_total_retry'], 'error_code' => $result_data['msg_update']['complete_result'][$i]['error_code'], 
                        'error_message' => $result_data['msg_update']['complete_result'][$i]['error_message'], 'start_time' => $result_data['msg_update']['complete_result'][$i]['start_time'], 
                        'end_time' => $result_data['msg_update']['complete_result'][$i]['end_time'], 'status' => $result_data['msg_update']['complete_result'][$i]['status'],
                        'create_date' => $current_date, 'modify_date' => $current_date]);

                    // $sql_insert = "INSERT into sms_complete (msg_id, sms_token, receiver_token, message, response_id, phone_no, protocol, service_provider, num_retry, current_retry, num_total_retry, error_code, error_message,
                    //                 start_time, end_time, status, create_date, modify_date) 
                    //                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now())";
                    // $sql_insert_array = array($message['msg_id'], $result_data['msg_update']['complete_result'][$i]['sms_token'], $result_data['msg_update']['complete_result'][$i]['receiver_token'], $result_data['msg_update']['complete_result'][$i]['message'],
                    //                 $result_data['msg_update']['complete_result'][$i]['response_id'], $result_data['msg_update']['complete_result'][$i]['phone_no'], $result_data['msg_update']['complete_result'][$i]['protocol'], 
                    //                 $result_data['msg_update']['complete_result'][$i]['service_provider'], $result_data['msg_update']['complete_result'][$i]['num_retry'], $result_data['msg_update']['complete_result'][$i]['current_retry'],
                    //                 $result_data['msg_update']['complete_result'][$i]['num_total_retry'], $result_data['msg_update']['complete_result'][$i]['error_code'], $result_data['msg_update']['complete_result'][$i]['error_message'], 
                    //                 $result_data['msg_update']['complete_result'][$i]['start_time'], $result_data['msg_update']['complete_result'][$i]['end_time'], $result_data['msg_update']['complete_result'][$i]['status']);
                    // $this->db->query($sql_insert, $sql_insert_array);
                }

                $complete_status = Config::get("legato.sms::SMS_STATUS_COMPLETE");
                $sqlarray["status"] = $complete_status;
                $sqlarray["start_time"] = $result_data['msg_update']['start_time'];
                $sqlarray["end_time"] = $result_data['msg_update']['end_time'];
                // $this->db->where("msg_id", $message["msg_id"])->update("sms_message", $sqlarray);

                Db::table('legato_sms_message')
                    ->where('msg_id', $message["msg_id"])
                    ->update(['status' => $sqlarray["status"], 'start_time' => $sqlarray["start_time"], 'end_time' => $sqlarray["end_time"]]);
                // $this->db->where("msg_id", $message["msg_id"])->delete("sms_progress", $sqlarray);
            }
            
        }
    }

    // public static function send_progress_message(){


    // }

    // public static function add_complete_message($sms){


    // }

    // public static function add_error_message($sms){


    // }

    // public static function retry_progress_message($sms) {
        
    // }

    // public static function reset_retry_progress_message($sms) {
        
    // }

    // public function add_total_retry_message($sms) {
        
    // }

    // public function delete_progress_message($sms) {
        
    // }

    // public function update_sent_record() {
        
    // }

    // public function get_status($msg_id){
        
    // }

    // public function get_all_records($filters) {
        
    // }

    public function archive_message(){
        
    }

    public function archive_receiver(){
        
    }

    public function archive_complete_message(){
        
    }

    public function archive_delivery(){
        
    }

    public function set_http_delivery($data) {
        
    }

    public function get_smpp_delivery($host_num) {
        
    }

    private function update_delivery_record($response_id) {
        
    }
}
