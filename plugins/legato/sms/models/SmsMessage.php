<?php namespace Legato\SMS\Models;

use Model;
use Legato\SMS\Models\Core;
use Common\Models\CommonModel;
use Config;
/**
 * SmsMessage Model
 */
class SmsMessage extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_sms_message';

    public $primaryKey = 'msg_id';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function sendSMS($data){ //setup sms insert to sms_message
        $sms = array();
        $sms['message'] = $data['message'];
        $sms['num_message'] = count($data['phone_no']);
        $sms['num_retry'] = Config::get("legato.sms::NUM_RETRY");
        $sms['protocol'] = $data['protocol'];   //http = 0, smpp = 1
        $sms['service_provider'] = $data['service_provider']; // NEXMO = 0, CITIC = 1
        if($data['schedule_time'] == "" || !$data['schedule_time']){
            $sms['schedule_time'] = "1993-01-01 00:00:00";
        }else{
            $sms['schedule_time'] = $data['schedule_time'];
        }
        $sms['phone_no'] = $data['phone_no'];
        // var_dump("sendSMS");
        $msg_id = Core::add_message_receiver($sms);

        // if($msg_id !== false)
        //     $result = array("Result" => SMS_STATUS_SUCCESS, "EMsg" => "Success", "Message_id" => $msg_id);
        // else
        //     $result = array("Result" => SMS_STATUS_FAIL, "EMsg" => "Add message receiver fails");
    }

    public static function send(){
        while(@file_exists(Config::get("legato.sms::SENDSWITCH"))){
            Core::add_progress_message();
            sleep(Config::get("legato.sms::SLEEP_SECOND_SEND"));
        }
        // sleep(self::SLEEP_SECOND_SEND);
    }

    public static function updateRecordServer(){
        Core::update_msg_status();
        // sleep(self::SLEEP_SECOND_SENDTASK);
    }
}
