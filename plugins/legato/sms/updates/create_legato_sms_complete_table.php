<?php namespace Legato\SMS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use DB;

class CreateLegatoSmsCompleteTable extends Migration
{
    public function up()
    {
        Schema::create('legato_sms_complete', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('msg_id');
            $table->string('sms_token', 200)->nullable()->default(NULL);
            $table->string('receiver_token', 200)->nullable()->default(NULL);
            $table->text('message');
            $table->string('response_id', 200);
            $table->string('phone_no', 200);
            $table->string('protocol', 100);
            $table->string('service_provider', 100);
            $table->integer('num_retry');
            $table->integer('current_retry');
            $table->integer('num_total_retry');
            $table->string('error_code', 200);
            $table->string('error_message', 200);
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('status')->default(2)->index();
            $table->integer('create_user');
            $table->integer('modify_user');
            $table->dateTime('create_date')->default(DB::raw('CURRENT_TIMESTAMP'))->index();
            $table->timestamp('modify_date')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_sms_complete');
    }
}
