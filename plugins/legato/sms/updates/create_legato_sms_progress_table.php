<?php namespace Legato\SMS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use DB;

class CreateLegatoSmsProgressTable extends Migration
{
    public function up()
    {
        Schema::create('legato_sms_progress', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('msg_id')->index();
            $table->string('sms_token', 200)->nullable()->default(NULL);
            $table->string('receiver_token', 200)->nullable()->default(NULL);
            $table->text('message');
            $table->string('phone_no', 200);
            $table->string('sender', 200);
            $table->integer('num_retry')->index();
            $table->integer('current_retry')->index();
            $table->integer('num_host_retry')->default(0);
            $table->integer('num_total_retry')->default(0);
            $table->string('protocol', 100);
            $table->string('service_provider', 100);
            $table->dateTime('start_time');
            $table->integer('status')->default(2)->index();
            $table->integer('create_user');
            $table->integer('modify_user');
            $table->dateTime('create_date')->default(DB::raw('CURRENT_TIMESTAMP'))->index();
            $table->timestamp('modify_date')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_sms_progress');
    }
}