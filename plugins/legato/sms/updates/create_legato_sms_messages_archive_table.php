<?php namespace Legato\SMS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use DB;

class CreateLegatoSmsMessagesArchiveTable extends Migration
{
    public function up()
    {
        Schema::create('legato_sms_message_archive', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('msg_id');
            $table->string('message')->nullable();
            $table->integer('num_message');
            $table->integer('num_success');
            $table->integer('num_fail');
            $table->integer('num_processing');
            $table->integer('num_retry');
            $table->integer('num_delivered');
            $table->string('protocol', 100);
            $table->string('service_provider', 100);
            $table->string('sender', 200);
            $table->dateTime('schedule_time')->index();
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('status')->default(2)->index();
            $table->integer('create_user');
            $table->integer('modify_user');
            $table->dateTime('create_date')->default(DB::raw('CURRENT_TIMESTAMP'))->index();
            $table->timestamp('modify_date')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_sms_message_archive');
    }
}