<?php namespace Legato\SMS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCoresTable extends Migration
{
    public function up()
    {
        Schema::create('legato_sms_cores', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_sms_cores');
    }
}
