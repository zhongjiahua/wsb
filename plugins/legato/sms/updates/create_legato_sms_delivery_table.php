<?php namespace Legato\SMS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use DB;

class CreateLegatoSmsDeliveryTable extends Migration
{
    public function up()
    {
        Schema::create('legato_sms_delivery', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('response_id', 200)->index();
            $table->dateTime('delivery_time')->default('0000-00-00 00:00:00');
            $table->dateTime('submit_time')->default('0000-00-00 00:00:00');
            $table->string('error_code', 200);
            $table->string('error_message', 200);
            $table->integer('status')->default(1)->index();
            $table->integer('create_user');
            $table->integer('modify_user');
            $table->dateTime('create_date')->default(DB::raw('CURRENT_TIMESTAMP'))->index();
            $table->timestamp('modify_date')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_sms_delivery');
    }
}