<?php namespace Legato\SMS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use DB;

class CreateLegatoSmsCompleteArchiveTable extends Migration
{
    public function up()
    {
        Schema::create('legato_sms_complete_archive', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('msg_id')->index();
            $table->text('message');
            $table->string('response_id', 200)->index();
            $table->string('phone_no', 200);
            $table->string('protocol', 100);
            $table->string('service_provider', 100);
            $table->integer('num_retry')->index();
            $table->integer('current_retry')->index();
            $table->integer('num_total_retry');
            $table->string('error_code', 200);
            $table->string('error_message', 200);
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('status')->default(2)->index();
            $table->integer('create_user');
            $table->integer('modify_user');
            $table->dateTime('create_date')->default(DB::raw('CURRENT_TIMESTAMP'))->index();
            $table->timestamp('modify_date')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_sms_complete_archive');
    }
}
