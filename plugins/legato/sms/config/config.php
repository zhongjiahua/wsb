<?php

return [
    'STATUS_ACTIVE' => 1,
    
	'protocols' => array("http", "smpp"),
    'service_providers' => array("nexmo", "citic"),

    'nexmo_http_host' => array("https://rest.nexmo.com/sms/json?"),
    'nexmo_http_api_key' => array("a3e5acb4"),
    'nexmo_http_secret_key' => array("ece308711be605a1"),
    'nexmo_http_port' => array("8000"),

    'nexmo_smpp_host' => array("smpp3.nexmo.com"),
    'nexmo_smpp_api_key' => array("aa98246e"),
    'nexmo_smpp_secret_key' => array("5be9d916dca03b30"),
    'nexmo_smpp_port' => array("8000"),

    'citic_smpp_host' => array("202.68.193.40", "202.68.206.41"),
    'citic_smpp_api_key' => array("lgg03cit", "lgg04cit"),
    'citic_smpp_secret_key' => array("Bqn49bGs", "Bqn49bGs"),
    'citic_smpp_port' => array(2775, 2775),
    'citic_oa_sender' => '85264521839',

    'SMS_STATUS_SUCCESS' => 1,
    'SMS_STATUS_PENDING' => 2,
    'SMS_STATUS_COMPLETE' => 3,
    'SMS_STATUS_PROCESSING' => 4,
    'SMS_STATUS_DELIVERED' => 5,
    'SMS_STATUS_SENDER_TOO_LONG' => 6,
    'SMS_STATUS_INVALID_USER' => 7,
    'SMS_STATUS_FAIL' => 9,

    'SMS_PROVIDER_STATUS_SUCCESS' => "0",

    'NEXMO' => 0,
    'CITIC' => 1,

    'HTTP' => 0,
    'SMPP' => 1,

    'SMS_IS_UPDATE' => 2,
    'SMS_NON_UPDATE' => 1,

    'NEXMO_SMPP_DELIVERY' => 0,

    'PROJECT_KEY' => 'OqfIjCZ5v3kG6utSMStest',
    'PROJECT_SECRET_KEY' => 'NlpQBaKyushYm4VSMStest',
    'PROJECT_SIGNATURE' => 'ysn2ZRvtx3ozhpbcF57D',
    'CENT_URL' => "https://sms-server.legato.co/SMS_Centralized_Server/sms/",

    'SLEEP_SECOND_SEND' => 1,
    'SLEEP_SECOND_SENDTASK' => 1,

    'SENDSWITCH' => __DIR__."/../helpers/switch_send.txt",
    'NUM_RETRY' => 3,
    'SLEEP_SECOND_SEND' => 1,
    ];
