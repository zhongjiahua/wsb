<?php namespace Legato\SMS;

use Backend;
use System\Classes\PluginBase;
use Legato\SMS\Models\smsmessage;
use Legato\SMS\Models\core;

/**
 * SMS Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'SMS',
            'description' => 'No description provided yet...',
            'author'      => 'Legato',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Legato\SMS\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'legato.sms.some_permission' => [
                'tab' => 'SMS',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'sms' => [
                'label'       => 'SMS',
                'url'         => Backend::url('legato/sms/SmsMessages'),
                'icon'        => 'icon-comment-o',
                'permissions' => ['legato.sms.*'],
                'order'       => 510,
                'sideMenu' => [
                    'smsmessages' => [
                        'label' => 'SMS',
                        'icon' => 'icon-comment-o',
                        'url' => Backend::url('legato/sms/SmsMessages'),
                        'permissions' => ['legato/sms/SmsMessages'],
                    ],
                ],
            ],
        ];
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            SmsMessage::send();
        })->name('SMSMessage')
        ->withoutOverlapping();

        $schedule->call(function () {
            SmsMessage::updateRecordServer();
        })->name('SMSMessageUpdateRecordServer')
        ->withoutOverlapping();
    }
}
