<?php namespace Legato\SMS\Controllers;

use BackendMenu;
use Common\Controllers\CommonController;
use Legato\SMS\Models\core;
use Legato\SMS\Models\SmsMessage;
/**
 * Sms Messages Back-end Controller
 */
class SmsMessages extends CommonController
{
    public $requiredPermissions=['legato.sms.view'];
    
    public $requiredActionsPermissions=[
      'create'=>['legato.sms.create'],
      'update'=>['legato.sms.update'],
      'delete'=>['legato.sms.delete'],
      'import'=>['legato.sms.import'],
      'export'=>['legato.sms.export'],

      'suspend'=>['legato.sms.update'],
      'publish'=>['legato.sms.update'],
    ];
    
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Legato.SMS', 'sms', 'smsmessages');
    }

    public function create($context = null){
        // var_dump("test:");
        $data = array();
        $data['phone_no'] = array("85291759043");
        $data['message'] = "october sms test";
        $data['protocol'] = 1;
        $data['service_provider'] = 1;
        SmsMessage::sendSMS($data);

        parent::create();
    }

    public function create_onSave($context = null){
       
    }

    // public function updateSentRecord(){
    //     Core::update_sent_record();
    //     // sleep(self::SLEEP_SECOND_SENDTASK);
    // }

    public function archive(){

    }

    public function deliveryCallback(){

    }

    public function updateDeliveryTask(){

    }

    private function check_cli_request(){

    }
}
