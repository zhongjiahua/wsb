<?php

$library_dir = APPPATH . "modules/sms/libraries/php-smpp-master/";
require_once $library_dir ."smppclient.class.php";
require_once $library_dir ."gsmencoder.class.php";


class Sms_smpp_client {

	private $socket_transport = null;
	private $smppClient = null;

	const RECIEVER_TIMEOUNT = 10000;
	const SEND_TIMEOUNT = 10000;

	public function __construct($host = "", $port = 8000) {
		$this->initialize($host, $port);
	}

	private function initialize($host, $port) {
		$this->socket_transport = new SocketTransport(array($host), $port);
		$this->socket_transport->setRecvTimeout(self::RECIEVER_TIMEOUNT);
		$this->socket_transport->debug = true;
		$this->socket_transport->open();

		$this->smppClient = new SmppClient($this->socket_transport);
		$this->smppClient->debug = true;
	}

	public function set_transmitter($api_key, $secret_key) {
		$this->smppClient->bindTransmitter($api_key, $secret_key);
	}

	public function set_receiver($api_key, $secret_key) {
		$this->smppClient->bindReceiver($api_key, $secret_key);
	}

	public function send_request($sms) {
		if($sms->is_unicode()) {
			$encoding = SMPP::DATA_CODING_UCS2;
			$msg_text 	= iconv('UTF-8', 'UCS-2BE', $sms->get_message());
		}
		else {
			$encoding = SMPP::DATA_CODING_DEFAULT;
			$msg_text = $sms->get_message();
		}

		SmppClient::$sms_registered_delivery_flag = SMPP::REG_DELIVERY_SMSC_BOTH;
		SmppClient::$csms_method = SmppClient::CSMS_8BIT_UDH;

		if(is_numeric($sms->get_sender())){
			$addr_from 	= new SmppAddress($sms->get_sender(), SMPP::TON_INTERNATIONAL, SMPP::NPI_E164);
		}else{
			$addr_from 	= new SmppAddress($sms->get_sender(), SMPP::TON_ALPHANUMERIC);
		}
		$addr_to 	= new SmppAddress($sms->get_phone_no()[0], SMPP::TON_INTERNATIONAL, SMPP::NPI_E164);

		$message_id = $this->smppClient->sendSMS($addr_from, $addr_to, $msg_text, null, $encoding);
		$message_id = rtrim($message_id);
		
		$this->smppClient->close();

		return $message_id;
	}

	public function get_delivery() {
		$deliveries = array();
		do {
			$sms = $this->smppClient->readSMS();
			if($sms != "" && $sms != false){
				if($sms->id != null){
					$deliveries[] = $sms;
				}
			}
		} while($sms !== false);

		return $deliveries;
	}
}



