<?php

class Sms_http_client {

	private $host = "";

	function __construct($host = "") {
		$this->host = $host;
	}

	public function send_request($sms) {
		$data = array(
						'api_key' 		=> $sms->get_api_key(),
						'api_secret' 	=> $sms->get_secret_key(),
						'to' 			=> $sms->get_phone_no()[0],
						'from' 			=> $sms->get_sender(),
						'text' 			=> $sms->get_message(),
						"type"			=> $sms->is_unicode() ? "unicode":"text"
				);
	   $Parameters_String = '';
	   foreach($data as $key=>$value) { $Parameters_String .= $key.'='.urlencode($value).'&'; }
	   $Parameters_String = rtrim($Parameters_String,'&');
	   $ch = curl_init();
	   
	   //set the url, number of POST vars, POST data
	   curl_setopt($ch,CURLOPT_URL, $this->host);
	   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	   curl_setopt($ch,CURLOPT_POST,count($data));
	   curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameters_String);
	   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	   if ($uname != "") {
	    curl_setopt($ch,CURLOPT_USERPWD, $uname);
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	   }
	 
	   //execute post
	   $result = curl_exec($ch);
	   curl_close($ch);
	   return $result;  

		 
	}

}





?>