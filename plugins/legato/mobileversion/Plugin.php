<?php namespace Legato\MobileVersion;

use Backend;
use System\Classes\PluginBase;

/**
 * MobileVersion Plugin Information File
 *
 * 2018-03-26 Sean Lee
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'Mobile Version',
            'description' => 'Mobile Version Module',
            'author' => 'Legato',
            'icon' => 'icon-mobile-phone'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        app()->make('router')->aliasMiddleware('appVer', \Legato\MobileVersion\Middleware\AppVersionMiddleware::class);

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Legato\MobileVersion\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'legato.mobileversion.delete_mobile_version' => [
                'label' => 'Delete Mobile Version',
                'tab' => 'Mobile Version',
            ],
            'legato.mobileversion.edit_mobile_version' => [
                'label' => 'Edit Mobile Version',
                'tab' => 'Mobile Version',
            ],
            'legato.mobileversion.create_mobile_version' => [
                'label' => 'Create Mobile Version',
                'tab' => 'Mobile Version',
            ],
            'legato.mobileversion.manage_mobile_version' => [
                'label' => 'Manage Mobile Version List',
                'tab' => 'Mobile Version',
            ],
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            'status' => function ($value) {
                switch ($value) {
                    case 0:
                        return 'Disable';
                    case 1:
                        return 'Active';
                }
                return '';
            },
            'yes_no' => function ($value) {
                switch ($value) {
                    case 0:
                        return 'No';
                    case 1:
                        return 'Yes';
                }
                return '';
            },
            'platform' => function ($value) {
                switch ($value) {
                    case 1:
                        return 'iOS';
                    case 2:
                        return 'Android';
                }
                return '';
            }
        ];
    }

    /**
     * Registers back-end settings items for this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'regions' => [
                'label'       => 'Mobile Version',
                'description' => 'Manage mobile versions.',
                'icon'        => 'icon-mobile-phone',
                'url'         => Backend::url('legato/mobileversion/mobileversion'),
                'order'       => 500,
                'category'    => 'Mobile Version',
                'permissions' => ['legato.mobileversion.manage_mobile_version']
            ]
        ];
    }
}
