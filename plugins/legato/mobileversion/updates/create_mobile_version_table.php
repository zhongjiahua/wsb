<?php namespace Legato\MobileVersion\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMobileVersionTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('mobile_version');
        Schema::create('mobile_version', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('version', 20);
            $table->smallInteger('force_update')->default(0)->comment('1=Yes;0=No');
            $table->smallInteger('platform')->comment('1=iOS;2=Android');
            $table->string('popup_message', 128)->nullable();
            $table->string('description', 255)->nullable();
            $table->text('download_link')->nullable();
            $table->smallInteger('status')->default(1)->comment('0=disabled;1=active');
            $table->timestamps();
            $table->string('created_by', 20);
            $table->string('updated_by', 20);
        });
    }

    public function down()
    {
        Schema::dropIfExists('mobile_version');
    }
}
