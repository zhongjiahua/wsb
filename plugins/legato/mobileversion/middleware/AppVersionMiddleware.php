<?php namespace Legato\MobileVersion\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Validation\Rule;
use Legato\MobileVersion\Models\MobileVersion;
use Legato\MobileVersion\Controllers\MobileVersion as MobileVersionController;

class AppVersionMiddleware
{
    use \Legato\Api\Traits\UtilityFunctions;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rules = [
            /**
             * @todo check app_kind format
             */
            'app_ver' => 'required',
            'device_kind' => [ 'required', Rule::in(MobileVersion::TYPE_IOS, MobileVersion::TYPE_ANDROID)],
        ];
        $customMessages = [
            'device_kind.in' => 'Device kind should be '.MobileVersion::TYPE_IOS.'(for ios) or '.MobileVersion::TYPE_ANDROID.'(for android)',
        ];
        $data = $request->toArray();
        $validator = \Validator::make($data, $rules,$customMessages);

        if ($validator->fails()) {
            return $this->validationResponse($validator);
        }

        /**
         * Stop request directly if force_update is required
         */
        $app_ver = $this->checkAppVersion($data['app_ver'],$data['device_kind']);
        if($app_ver['force_update']==1){
            return $this->response('Update is required',400,$app_ver);
        }

        $response = $next($request);

        /**
         * Append have_update info to api response if api do not return exception
         */
        if(!$response->exception){
            $res_data = json_decode($response->getContent(),true);
            $res_data['data']['mobile_version'] = $app_ver;
            $response->setContent($res_data);
        }        
        return $response;

    }
    public function checkAppVersion($app_ver,$platform){
        $result = MobileVersionController::checkMobileVersion($app_ver,$platform);

        $res_array = array();
        if ($result['need_update']) {    
                $res_array = array (
                    "Ver" => $result['mobile_version']->version,
                    "DL" => $result['mobile_version']->download_link,
                    "Msg" => $result['mobile_version']->lang(\App::getLocale())->popup_message
                );
        }
        return array("need_update"=>$result['need_update'], 'force_update'=> $result['force_update'], "mobile_version"=> $res_array);
    }

}