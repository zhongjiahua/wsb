<?php
/**
 * Created by PhpStorm.
 * User: seanlee
 * Date: 26/3/2018
 * Time: 6:57 PM
 */

use Legato\MobileVersion\Controllers\MobileVersion;

/**
 *  Example: http://localhost/path/to/mobileversion?version=1.0&platform=1
 */
Route::get('mobileversion', function () {

    $version = Input::get('version');
    $platform = Input::get('platform');

    $result = MobileVersion::checkMobileVersion($version, $platform);

    dump($result);
});