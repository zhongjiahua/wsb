<?php namespace Legato\MobileVersion\Models;

use Model;
use Validator;
use ValidationException;
use October\Rain\Database\Traits\Validation;
use Legato\AuditTrail\Traits\Auditable;

/**
 * MobileVersion Model
 *
 * 2018-03-26 Sean Lee
 */
class MobileVersion extends Model
{

    use Validation;
    use Auditable;
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
      'popup_message',
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'mobile_version';

    public $rules = [
        'version' => [
            'required',
            'regex:(^[0-9]+\.[0-9]{1,3}$)'],
        'force_update' => 'required',
        'platform' => 'required',
        'popup_message' => 'required',
        'download_link' => 'required|url',
        'status' => 'required'
    ];
    
    public $customMessages = [
        'version.required' => 'The Version Name is required.',
        'version.regex' => 'Please input a valid version name format(x.xxx).',
        'force_update.required' => 'Please select the force update option.',
        'platform.required' => 'Please select the platform.',
        'popup_message.required' => 'Please input the popup message.',
        'download_link.required' => 'Please input the download link.',
        'download_link.url' => 'Please input a valid download link.',
        'status.required' => 'Please select the status.',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'updated_by_user' => [
            'Backend\Models\User',
            'key' => 'updated_by'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public function beforeValidate(){
        $count = self::where('id','!=',$this->id)->where('version',$this->version)->where('platform',$this->platform)->count();

        if($count){
            throw new ValidationException(['x'=>'version is already existed']);
        }
    }

    public function getStatusOptions() {
        $res = array(0=>'Disable',1=>'Active');
        return $res;
    }

    public function getPlatformOptions() {
        $res = array(1=>'iOS',2=>'Android');
        return $res;
    }
}
