<?php namespace Legato\MobileVersion\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Legato\MobileVersion\Models\MobileVersion as MobileVersionModel;

/**
 * Mobile Version Back-end Controller
 *
 * 2018-03-26 Sean Lee
 */
class MobileVersion extends Controller
{
    const TRUE = 1;
    const FALSE = 0;
    const PLATFORM_IOS = 1;
    const PLATFORM_ANDROID = 2;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Legato.MobileVersion', 'mobileversion', 'mobileversion');
    }

    public function formBeforeSave(MobileVersionModel $mobileVersionModel){
        $mobileVersionModel->updated_by = $this->user->id;
    }

    public function formBeforeCreate(MobileVersionModel $mobileVersionModel){
        $mobileVersionModel->created_by = $this->user->id;
    }

    /**
     * @param String $version Mobile version name
     * @param int $platform Platform ID (iOS -> 1; Android -> 2)
     * @return array
     * [
     *      "need_update" => 0
     *      "force_update" => 1
     *      "mobile_version" => [
     *          "id" => 3
     *          "version" => "1.12"
     *          "force_update" => 0
     *          "platform" => 1
     *          "popup_message" => "1.12"
     *          "description" => "1.12"
     *          "download_link" => "http://xxx.xxx"
     *          "status" => 1
     *          "deleted_at" => null
     *          "created_at" => "2018-03-26 19:20:37"
     *          "created_by" => ""
     *          "updated_at" => "2018-03-26 19:20:37"
     *          "updated_by" => ""
     *      ]
     *  ]
     */
    public static function checkMobileVersion(String $version, int $platform): array{

        $mobileVersions = MobileVersionModel::where('status', self::TRUE)
            ->where('platform', $platform)
            ->where('version', '>', $version)
            ->orderBy('version', 'desc')
            ->get();

        $mobileVersionResult = array();

        if(count($mobileVersions)>0){
            $count = $mobileVersions
                ->where('force_update', self::TRUE)
                ->count();
            $mobileVersion = $mobileVersions->first();

            $mobileVersionResult['need_update'] = self::TRUE;
            $mobileVersionResult['force_update'] = $count > 0 ? self::TRUE: self::FALSE;
            $mobileVersionResult['mobile_version'] = $mobileVersion;
        }else{
            $mobileVersionResult['need_update'] = self::FALSE;
            $mobileVersionResult['force_update'] = self::FALSE;
        }

        return $mobileVersionResult;
    }
}
