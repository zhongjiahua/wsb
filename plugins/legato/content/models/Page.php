<?php namespace Legato\Content\Models;

use Db;
use Url;
use App;
use Str;
use Html;
use Lang;
use Model;
use Markdown;
use BackendAuth;
use ValidationException;
use Backend\Models\User;
use Carbon\Carbon;
use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;
use Common\Models\CommonModel;
use Legato\Content\Transformers\PageTransformer;

/**
 * Page Model
 */
class Page extends CommonModel
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_content_pages';
	public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $transformer = PageTransformer::class;

	/*
     * Validation
     */
	public $rules = [
		'title' => 'required',
		'slug' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:legato_content_pages'],
		'content' => 'required'
	];

	/**
	 * @var array Attributes that support translation, if available.
	 */
	public $translatable = [
		'title',
		'content',
		'content_html'
		//'excerpt',
		//['slug', 'index' => true]
	];

	/**
	 * The attributes that should be mutated to dates.
	 * @var array
	 */
	protected $dates = ['published_at'];

	/**
	 * The attributes on which the post list can be ordered
	 * @var array
	 */
	public static $allowedSortingOptions = [
		'title asc' => 'Title (ascending)',
		'title desc' => 'Title (descending)',
		'created_at asc' => 'Created (ascending)',
		'created_at desc' => 'Created (descending)',
		'updated_at asc' => 'Updated (ascending)',
		'updated_at desc' => 'Updated (descending)',
		'published_at asc' => 'Published (ascending)',
		'published_at desc' => 'Published (descending)',
		'random' => 'Random'
	];

	/*
     * Relations
     */
    public $belongsTo = [
        'user' => ['Backend\Models\User']
    ];

	public $attachMany = [
		'content_images' => ['System\Models\File']
	];

	/**
	 * @var array The accessors to append to the model's array form.
	 */
	//protected $appends = ['summary', 'has_summary'];

	public $preview = null;

	/**
     * Limit visibility of the published-button
     *
     * @param      $fields
     * @param null $context
     *
     * @return void
     */
    public function filterFields($fields, $context = null)
    {
        if (!isset($fields->published, $fields->published_at)) {
            return;
        }

        $user = BackendAuth::getUser();

        if (!$user->hasAnyAccess(['legato.content.access_publish'])) {
            $fields->published->hidden = true;
            $fields->published_at->hidden = true;
        }
        else {
            $fields->published->hidden = false;
            $fields->published_at->hidden = false;
        }
    }

	public function afterValidate()
	{
		if ($this->published && !$this->published_at) {
			throw new ValidationException([
			   'published_at' => Lang::get('legato.content::lang.page.published_validation')
			]);
		}
	}

	/**
	 * Sets the "url" attribute with a URL to this object
	 * @param string $pageName
	 * @param Cms\Classes\Controller $controller
	 */
	public function setUrl($pageName, $controller)
	{
		$params = [
			'id'   => $this->id,
			'slug' => $this->slug,
		];

		// if (array_key_exists('categories', $this->getRelations())) {
		// 	$params['category'] = $this->categories->count() ? $this->categories->first()->slug : null;
		// }

		//expose published year, month and day as URL parameters
		if ($this->published) {
			$params['year'] = $this->published_at->format('Y');
			$params['month'] = $this->published_at->format('m');
			$params['day'] = $this->published_at->format('d');
		}

		return $this->url = $controller->pageUrl($pageName, $params);
	}

	/**
	 * Used to test if a certain user has permission to edit post,
	 * returns TRUE if the user is the owner or has other posts access.
	 * @param User $user
	 * @return bool
	 */
	public function canEdit(User $user)
	{
		return ($this->user_id == $user->id) || $user->hasAnyAccess(['legato.content.access_other_pages']);
	}

	// public static function formatHtml($input, $preview = false)
	// {
	// 	$result = Markdown::parse(trim($input));
	//
	// 	if ($preview) {
	// 		$result = str_replace('<pre>', '<pre class="prettyprint">', $result);
	// 	}
	//
	// 	$result = TagProcessor::instance()->processTags($result, $preview);
	//
	// 	return $result;
	// }

	//
	// Scopes
	//

	public function scopeIsPublished($query)
	{
		return $query
			->whereNotNull('published')
			->where('published', true)
			->whereNotNull('published_at')
			->where('published_at', '<', Carbon::now())
		;
	}

	/**
	 * Lists posts for the front end
	 *
	 * @param        $query
	 * @param  array $options Display options
	 *
	 * @return Post
	 */
	public function scopeListFrontEnd($query, $options)
	{
		/*
		 * Default options
		 */
		extract(array_merge([
			'page'       => 1,
			'perPage'    => 30,
			'sort'       => 'created_at',
			//'categories' => null,
			//'category'   => null,
			'search'     => '',
			'published'  => true,
			'exceptPost' => null,
		], $options));

		$searchableFields = ['title', 'slug', 'content'];

		if ($published) {
			$query->isPublished();
		}

		/*
		 * Ignore a post
		 */
		if ($exceptPost) {
			if (is_numeric($exceptPost)) {
				$query->where('id', '<>', $exceptPost);
			}
			else {
				$query->where('slug', '<>', $exceptPost);
			}
		}

		/*
		 * Sorting
		 */
		if (!is_array($sort)) {
			$sort = [$sort];
		}

		foreach ($sort as $_sort) {

			if (in_array($_sort, array_keys(self::$allowedSortingOptions))) {
				$parts = explode(' ', $_sort);
				if (count($parts) < 2) {
					array_push($parts, 'desc');
				}
				list($sortField, $sortDirection) = $parts;
				if ($sortField == 'random') {
					$sortField = Db::raw('RAND()');
				}
				$query->orderBy($sortField, $sortDirection);
			}
		}

		/*
		 * Search
		 */
		$search = trim($search);
		if (strlen($search)) {
			$query->searchWhere($search, $searchableFields);
		}

		/*
		 * Categories
		 */
		// if ($categories !== null) {
		// 	if (!is_array($categories)) $categories = [$categories];
		// 	$query->whereHas('categories', function($q) use ($categories) {
		// 		$q->whereIn('id', $categories);
		// 	});
		// }

		/*
		 * Category, including children
		 */
		// if ($category !== null) {
		// 	$category = Category::find($category);
		//
		// 	$categories = $category->getAllChildrenAndSelf()->lists('id');
		// 	$query->whereHas('categories', function($q) use ($categories) {
		// 		$q->whereIn('id', $categories);
		// 	});
		// }

		return $query->paginate($perPage, $page);
	}


    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    //public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    //public $attachMany = [];

	public static function getSupportivePages($locale = 'en')
	{

		if ($locale == 'en'){

			$pages = self::all();

		} else {

			$pages = DB::table('legato_content_pages')
			->join('rainlab_translate_attributes', 'legato_content_pages.id', '=', 'rainlab_translate_attributes.model_id')
			->select('legato_content_pages.*', 'rainlab_translate_attributes.*')
			->where('rainlab_translate_attributes.model_type', 'Legato\Content\Models\Page')
			->where('rainlab_translate_attributes.locale', $locale)
			->get();

			$pages = Page::getTranslation($pages);

		}

		return $pages;
	}

	public static function getTranslation($result){
       if ($result != null) {
            foreach($result as $key => $value){
                if ($value->attribute_data != null) {
                    $attribute_data = json_decode($value->attribute_data, true);
                    if ($attribute_data != null) {
                        foreach($attribute_data as $attribute_data_key => $attribute_data_value){
                            if ($attribute_data_value != '') {
                                $result[$key]->$attribute_data_key = $attribute_data_value;

                            }
                        }
                    }
                }
                unset($value->attribute_data);
            }
        }
        return $result;
    }

    public static function api($ut,$lang){
    	$collection = self::whereUt($ut)->withTranslate($lang)->get();
    	$tranformed = self::transform($collection);
    	return $tranformed;
    }

}
