<?php namespace Legato\Content\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePagesTable extends Migration
{
    public function up()
    {
        Schema::create('legato_content_pages', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->string('title', 255)->nullable();
			$table->string('slug', 255);
			$table->longText('content')->nullable();
            $table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_content_pages');
    }
}
