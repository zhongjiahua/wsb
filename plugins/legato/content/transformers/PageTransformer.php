<?php namespace Legato\Content\Transformers;

use League\Fractal\TransformerAbstract;
use Legato\Content\Models\Page;
use App;


class PageTransformer extends TransformerAbstract
{
	public function transform(Page $page) 
	{
		return [
			'id' => (int)$page->id,
			//'title' => $page->lang(App::getLocale())->title,
			'title' => $page->getTranslatedField('title'),
			'slug' => $page->slug,
			//'content' => $page->lang(App::getLocale())->content,
			'content' => $page->getTranslatedField('content'),
		];

	}

}