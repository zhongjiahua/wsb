<?php namespace Legato\Content;

use Backend;
use System\Classes\PluginBase;

/**
 * Content Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
		return [
			'name'        => 'legato.content::lang.plugin.name',
			'description' => 'legato.content::lang.plugin.description',
			'author'      => 'Legato',
			'icon'        => 'icon-leaf',
			'homepage'	  => 'https://www.legato.co'
		];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Legato\Content\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
	public function registerPermissions()
 	{
 		return [
 			'legato.content.access_pages' => [
 				'tab'   => 'legato.content::lang.content.tab',
 				'label' => 'legato.content::lang.content.access_pages'
 			],
 			'legato.content.access_other_pages' => [
 				'tab'   => 'legato.content::lang.content.tab',
 				'label' => 'legato.content::lang.content.access_other_pages'
 			],
 			'legato.content.access_publish' => [
 				'tab'   => 'legato.content::lang.content.tab',
 				'label' => 'legato.content::lang.content.access_publish'
 			]
 		];
 	}

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
	public function registerNavigation()
	{
		return [
		 'content' => [
		     'label'       => 'legato.content::lang.content.menu_label',
		     'url'         => Backend::url('legato/content/pages'),
		     'icon'        => 'icon-copy',
		     //'iconSvg'     => 'plugins/legato/content/assets/images/content-icon.svg',
		     'permissions' => ['legato.content.*'],
		     'order'       => 505,

		     'sideMenu' => [
		         'pages' => [
		             'label'       => 'legato.content::lang.content.menu_label',
		             'icon'        => 'icon-copy',
		             'url'         => Backend::url('legato/content/pages'),
		             'permissions' => ['legato.content.access_pages']
		         ]
		     ]
		 ]
		];
	}
}
