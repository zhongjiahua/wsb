<?php

return [
    'plugin' => [
        'name' => 'Legato Supportive Pages',
        'description' => 'A content management plugin for adding supportive pages.'
    ],
    'content' => [
        'menu_label' => 'Supportive Pages',
        'menu_description' => 'Manage Content Pages',
        'pages' => 'Pages',
        'create_page' => 'Content page',
        'tab' => 'Content',
        'access_pages' => 'Manage the content pages',
        'access_other_pages' => 'Manage other users content pages',
        'access_publish' => 'Allowed to publish pages',
        'delete_confirm' => 'Are you sure?',
        'chart_published' => 'Published',
        'chart_drafts' => 'Drafts',
        'chart_total' => 'Total'
    ],
    'pages' => [
        'list_title' => 'Manage the content pages',
        'filter_published' => 'Published',
        'filter_date' => 'Date',
        'new_page' => 'New page'
    ],
    'page' => [
        'title' => 'Title',
        'title_placeholder' => 'New page title',
        'content' => 'Content',
        'content_html' => 'HTML Content',
        'slug' => 'Slug',
        'slug_placeholder' => 'new-page-slug',
        'author_email' => 'Author Email',
        'created' => 'Created',
        'created_date' => 'Created date',
        'updated' => 'Updated',
        'updated_date' => 'Updated date',
        'published' => 'Published',
        'published_date' => 'Published date',
        'published_validation' => 'Please specify the published date',
        'tab_edit' => 'Edit',
        'tab_manage' => 'Manage',
        'published_on' => 'Published on',
        'excerpt' => 'Excerpt',
        'summary' => 'Summary',
        'featured_images' => 'Featured Images',
        'delete_confirm' => 'Delete this page?',
        'close_confirm' => 'The page is not saved.',
        'return_to_pages' => 'Return to pages list'
    ],
    'menuitem' => [
        'content_page' => 'Content page',
        'all_content_pages' => 'All content pages'
    ],
    'settings' => [
        'page_title' => 'Page',
        'page_description' => 'Displays a content page on the page.',
        'page_slug' => 'Page slug',
        'page_slug_description' => "Look up the content page using the supplied slug value.",
        'pages_title' => 'Page List',
        'pages_description' => 'Displays a list of latest content pages on the page.',
        'pages_pagination' => 'Page number',
        'pages_pagination_description' => 'This value is used to determine what page the user is on.',
        'pages_per_page' => 'Pages per page',
        'pages_per_page_validation' => 'Invalid format of the pages per page value',
        'pages_no_pages' => 'No pages message',
        'pages_no_pages_description' => 'Message to display in the content page list in case if there are no pages. This property is used by the default component partial.',
        'pages_order' => 'Page order',
        'pages_order_description' => 'Attribute on which the pages should be ordered',
        'pages_page' => 'Page page',
        'pages_page_description' => 'Name of the content page page file for the "Learn more" links. This property is used by the default component partial.',
        'pages_except_page' => 'Except page',
        'pages_except_page_description' => 'Enter ID/URL or variable with page ID/URL you want to except',
    ]
];
