<?php namespace Legato\Push;

use Legato\Push\Models\Progress_individual_user;
use RainLab\Translate\Models\Locale;
use System\Classes\PluginBase;
use Lang;

class Plugin extends PluginBase
{
    /**
     * @var array Plugin dependencies
     */
    public $require = ['October.Drivers'];

    public function boot()
    {
      \Event::listen('backend.menu.extendItems', function($manager) {
          $manager->removeMainMenuItem('legato.push', 'push-menu-message');
          $manager->removeMainMenuItem('legato.push', 'push-menu-tag');
      });
    }
    public function pluginDetails()
    {
        return [
            'name'        => 'push',
            'description' => 'legato.push::lang.legato.push.description',
            'author'      => 'legato'
        ];
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerListColumnTypes()
    {
        return [
            // Convert special offer values to text
            'status' => function($value) {
                $map = [
                    0 => Lang::get('legato.push::lang.legato.push.deleted'),
                    1 => Lang::get('legato.push::lang.legato.push.active')
                ];
                return $map[$value];
            },
            'token_type' => function($value) {
                $map = [
                    0 => Lang::get('legato.push::lang.legato.push.unknown'),
                    1 => Lang::get('legato.push::lang.legato.push.dev'),
                    2 => Lang::get('legato.push::lang.legato.push.inhouse_pro'),
                    3 => Lang::get('legato.push::lang.legato.push.store_pro')
                ];
                return $map[$value];
            },
            'os_type' => function($value) {
                $map = [
                    0 => Lang::get('legato.push::lang.legato.push.unknown'),
                    1 => Lang::get('legato.push::lang.legato.push.ios'),
                    2 => Lang::get('legato.push::lang.legato.push.android')
                ];
                return $map[$value];
            },
            'platform' => function($value){
                $map =[
                    0 => Lang::get('legato.push::lang.legato.push.unknown'),
                    1 => Lang::get('legato.push::lang.legato.push.local'),
                    2 => Lang::get('legato.push::lang.legato.push.aws'),
                    3 => Lang::get('legato.push::lang.legato.push.azure')
                ];
                return $map[$value];
            }
        ];


        /*
        return [
            // A local method, i.e $this->evalUppercaseListColumn()
            'uppercase' => [$this, 'evalUppercaseListColumn'],

            // Using an inline closure
            'loveit' => function($value) { return 'I love '. $value; }
        ];
        */
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            Progress_individual_user::cronUserToDevice();
        })->name('cronUserToDevice')->withoutOverlapping();

    }
}
