<?php namespace Legato\Push\Models;

use Legato\Push\classes\Constant;
use Model;
use Illuminate\Support\Facades\Validator;
use Legato\Push\classes\CustomValidator;
use Illuminate\Support\Facades\Lang;
use Legato\Push\Models\Message;
use Illuminate\Support\Facades\DB;
//
//Validator::extend('device', function($attribute, $value, $parameters) {
//    Log::info("extend");
//    var_dump($this->data);
////    Log::info("Topic:".Input::get('topic'));
////    Log::info(var_dump($parameters));
//    return $value == 'foo';
//});
//Validator::resolver(function($translator, $data, $rules, $messages, $customAttributes) {
////    Log::info(var_dump($data));
//    $messages['device_sub'] = Lang::get('legato.push::lang.legato.push.duplicate_record');
//    return new CustomValidator($translator,$data,$rules,$messages,$customAttributes);
//});

/**
 * Model
 */
class Subscription extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $fillable = ['device_id','topic_id'];

    public $belongsTo = [
        'device' => 'Legato\Push\Models\Device',
        'topic' => 'Legato\Push\Models\Topic'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
//        'device_id' => ['deviceSub']
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_subscription';

    public static function createDefaultSubscription($device_id,$language){
        $defaultTopic = Topic_group::where("name",Constant::NAME_DEFAULT_ALL_TOPIC)->first();
        if ($defaultTopic) {
            self::createNewSubscriptionWithGroupIdAndLang($device_id, $defaultTopic->id, $language);
        }
    }

    public static function createNewSubscriptionWithGroupId($device_id,$topic_group_id){
        $topics = Topic::where('topic_group_id',$topic_group_id)
            ->where('status',Constant::PUSH_STATUS_ACTIVE)->get();

        foreach ($topics as $topic){
            self::createSubscriptionWithTopicId($device_id,$topic);
        }
    }

    public static function createNewSubscriptionWithGroupIdAndLang($device_id,$topic_group_id,$language){

        $topic = Topic::where('topic_group_id',$topic_group_id)
            ->where('language',$language)
            ->where('status',Constant::PUSH_STATUS_ACTIVE)->first();

        self::createSubscriptionWithTopicId($device_id,$topic);

    }

    public static function createSubscriptionWithTopicId($device_id,$topic){
        $subscription = self::firstOrNew(['device_id'=>$device_id,'topic_id' => $topic->id]);
        $subscription->status = Constant::PUSH_STATUS_ACTIVE;
        $subscription->save();
    }

    public static function removeSubscriptionWithGroupId($device_id,$topic_group_id){
        $topics = Topic::where('topic_group_id',$topic_group_id)
            ->where('status',Constant::PUSH_STATUS_ACTIVE)->get();

        foreach ($topics as $topic){
            self::removeSubscriptionWithTopicId($device_id,$topic);
        }
    }

    public static function removeNewSubscriptionWithGroupIdAndLang($device_id,$topic_group_id,$language){

        $topic = Topic::where('topic_group_id',$topic_group_id)
            ->where('language',$language)
            ->where('status',Constant::PUSH_STATUS_ACTIVE)->first();

        self::removeSubscriptionWithTopicId($device_id,$topic);

    }

    public static function removeSubscriptionWithTopicId($device_id,$topic){
        $subscription = Subscription::firstOrNew(['device_id'=>$device_id,'topic_id' => $topic->id]);
        $subscription->status = Constant::PUSH_STATUS_INACTIVE;
        $subscription->save();
    }

    public static function removeSubscriptionByCustomFieldWithTopicGroupId($custom_field, $topic_group_id){
        $devices = Device::where('custom_field',$custom_field)->get();

        foreach ($devices as $device){
            self::removeSubscriptionWithGroupId($device->id,$topic_group_id);
        }

    }

    public static function getSubscribedTopicGroupIdByDevice($device_id)
    {   
        return Subscription::select("topic_group_id")
            ->leftJoin("legato_push_topic", 'legato_push_subscription.topic_id', '=', 'legato_push_topic.id')
            ->where('device_id', $device_id)
            ->where('legato_push_subscription.status', Constant::PUSH_STATUS_ACTIVE)
            ->groupBy("legato_push_subscription.topic_id")->get();
    }

    public static function getActiveSubscription($topic_id){
        return self::where('topic_id',$topic_id)
                                ->where('status',Constant::PUSH_STATUS_ACTIVE);
    }


    public static function isSubscriptionExist($device_id,$topic_id)
    {
        $subscription = self::where('device_id',$device_id)
                        ->where('topic_id',$topic_id)
                        ->where('status',1)
                        ->whereNull('deleted_at')->first();

        return $subscription;
    }

    public static function setSubscriptionStatusByDevice($id,$status)
    {
        self::where('device_id',$id)->update(['status' => $status]);
    }

}
