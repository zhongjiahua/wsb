<?php namespace Legato\Push\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Legato\Push\classes\Constant;
use Model;
use Echome\Inbox\Models\Inbox;

/**
 * Model
 */
class Progress_individual_user extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'message' => 'Legato\Push\Models\Message',
        'user' => [
            'Sinoclub\Member\Models\Session'
            //'key' => 'id',
            //'otherKey'  => 'membership_id',
        ]
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_progress_individual_user';

    public static function createBatchByUserIds($user_ids=array(),$message_id){

        $data = array();
        foreach ($user_ids as $user_id){
            $data[] = array("message_id" => $message_id,"user_id" => $user_id);
        }

        Progress_individual_user::insert($data);
    }

    public static function getPendingMessage()
    {

        return Progress_individual_user::with('message', 'user')
            ->where('status', Constant::PUSH_STATUS_ACTIVE)->whereHas('message', function ($query) {
                $query->where('status', Constant::PUSH_STATUS_ACTIVE)
                    ->where('push_platform',Constant::PLATFORM_AWS)
                    ->where('schedule_time', '<=', Carbon::now());
            })->whereHas('user', function ($query) {
                //$query->where('status', Constant::PUSH_STATUS_ACTIVE);
                //$query->where('is_activated',Constant::PUSH_STATUS_ACTIVE);
            })->get();

//        Progress_topic::with('message', 'topic', 'topic.aws_topic')
//            ->where('sns_status', Constant::SNS_STATUS_PROGRESSING)->whereHas('message', function ($query) {
//                $query->where('status', \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE);
//            })->whereHas('topic', function ($query) {
//                $query->where('status', \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE);
//            })->limit(2000)->lockForUpdate()->get();
    }

    public static function cronUserToDevice(){
        $records = Progress_individual_user::getPendingMessage();
        foreach ($records as $record){
            $tokens = array();

            $devices = Db::table('user_device_key')->select('push_token')->where('user_id',$record->user_id)
                ->where('status',Constant::PUSH_STATUS_ACTIVE)->get();

            foreach ($devices as $device){
                $tokens[] = $device->push_token;
            }
            $record->message->sendMessageToDeviceByTokenArr($tokens);
            Inbox::insertMessage($record->message,$record->user_id);
            $record->delete();
        }


    }
}
