<?php namespace Legato\Push\Models;

use Model;
/**
 * Created by PhpStorm.
 * User: legato
 * Date: 16/3/2018
 * Time: 12:20 PM
 */

class PushLocale extends \RainLab\Translate\Models\Locale
{
    protected static $cacheById;
    /**
     * Lists available locales, used on the back-end.
     * @return array
     */
    public static function listAvailable()
    {
        if (parent::$cacheListAvailable) {
            return parent::$cacheListAvailable;
        }

        return parent::$cacheListAvailable = parent::order()->get();
    }

    /**
     * Locate a locale table by its code, cached.
     * @param  string $code
     * @return Model
     */
    public static function findById($id = null)
    {
        if (!$id) {
            return null;
        }

        if (isset(self::$cacheById[$id])) {
            return self::$cacheById[$id];
        }

        return self::$cacheById[$id] = self::where('id',$id)->first();
    }

}