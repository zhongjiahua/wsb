<?php namespace Legato\Push\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Legato\Push\classes\Constant;
use Model;

/**
 * Model
 */
class Device extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $fillable = ['token'];

    protected $dates = ['deleted_at'];

    public $hasMany = [
        'subscription' => 'Legato\Push\Models\Subscription'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
//        'token' => 'unique:legato_push_device'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_device';

    public static function getDeviceByCustomField($custom_field){
        return self::where('custom_field',$custom_field)->get();
    }

    public static function getDeviceByCustomFields($custom_fields){
        return self::whereIn('custom_field',$custom_fields)->get();
    }

    public static function getDeviceByTokenArr($tokens){
//        $ids = array();
        $devices = self::whereIn('token',$tokens)->get();
//        foreach ($devices as $device){
//            $ids[] = $device->id;
//        }

        return $devices;
    }

    public static function getDeviceByToken($token){
        $device = self::where('token',$token)->first();
        return $device;
    }



    public static function updateDeviceCustomFieldByToken($token, $custom_field){
        try {
            $device = self::where('token', $token)->firstOrFail();
            $device->custom_field = $custom_field;
            $device->save();

        } catch (ModelNotFoundException $e)
        {
            return Response::json([Constant::KEY_RESULT => 0,Constant::KEY_MSG => 'Token not found exception']);
        }
    }

    public static function createOrUpdateDevice($pushToken, $osType, $language = 0, $user_id, $tokenType = Constant::TOKEN_TYPE_INHOUSE_PRO,$status = 1, $customField = 0)
    {
        $isLangChanged = false;
        
        $device = self::firstOrCreate(['token' => $pushToken]);

        if ($device->language != $language) {
            $isLangChanged = true;
        }

        $device->token = $pushToken;
        $device->token_type = $tokenType;
        $device->language = $language;
        $device->os_type = $osType;
        $device->status = $status;
        $device->save();

        Subscription::createDefaultSubscription($device->id,$device->language);
        if ($isLangChanged) {
            self::changeSubscribedTopicLang($device->id,$language);
        }
        /**
         * @todo
         * audience list handling shd be extended from audience list plugin
         */
        \Sinoclub\Push\Models\AudienceList::updateSubscriptionForAudienceList($device->token, $user_id);

    }


    public static function updateCustomFieldByToken($token,$custom_field){
        self::where('token',$token)->update(['custom_field' => $custom_field]);
    }

    public static function changeSubscribedTopicLang($id,$language){
//        $device = Device::with('subscription','subscription.topic','subscription.topic.topic_group')->find($id);
//        $subscriptions = $device->subscription;
//
//        foreach ($subscriptions as $subscription)
//        {
//
//        }

        $topic_group_id_arr = Subscription::getSubscribedTopicGroupIdByDevice($id);
        foreach ($topic_group_id_arr as $topic_group_id)
        {
            Subscription::setSubscriptionStatusByDevice($id,Constant::PUSH_STATUS_INACTIVE);
            Subscription::createNewSubscriptionWithGroupIdAndLang($id,$topic_group_id->topic_group_id,$language);
        }



    }

}
