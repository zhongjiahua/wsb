<?php namespace Legato\Push\Models;

use Model;

/**
 * Model
 */
class Complete_topic extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $fillable = ['message_id','topic_id','status'];
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_complete_topic';
}
