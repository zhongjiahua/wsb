<?php namespace Legato\Push\Models;

use Model;
use Illuminate\Support\Facades\Log;
// use RoundAsset\Parent\Models\InboxIndividual;
// use RoundAsset\Push\Models\Individual;
use Illuminate\Support\Facades\DB;
use Echome\Inbox\Models\Inbox;

/**
 * Model
 */
class Progress_individual extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'message' => 'Legato\Push\Models\Message',
        'topic' => 'Legato\Push\Models\Topic',
        'device' => 'Legato\Push\Models\Device'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_progress_individual';

    public static function createBatchByDevices($devices=array(),$message_id){

        $data = array();
        foreach ($devices as $device){
            $data[] = array("message_id" => $message_id,"device_id" => $device->id);
        }
        self::insert($data);
    }

    public function moveToComplete()
    {
        $complete_individual = new Complete_individual();
        $complete_individual = $complete_individual->firstOrCreate(['message_id' => $this->message_id , 'topic_id' =>$this->topic_id, 'device_id' => $this->device_id , 'status' => $this->status]);
        $this->delete();
        $this->insertInBox();

        return $complete_individual;
    }

    public function insertInBox(){
        $device = Db::table('user_device_key')
                    ->where('push_token',$this->device->token)->first();
        if ($device)
            Inbox::insertMessage($this->message,$device->user_id);

    }
}
