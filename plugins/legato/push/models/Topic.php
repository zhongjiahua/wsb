<?php namespace Legato\Push\Models;

use Model;

/**
 * Model
 */
class Topic extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $belongsTo = [
        'topic_group' => 'Legato\Push\Models\Topic_group'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_topic';



    /**
     *================= Custom method ===================
     */

    public static function createOrGetUniqueTopic($topic_group_id,$language)
    {
        $topic = Topic::where('topic_group_id',$topic_group_id)->where('language',$language)->first();
        if (is_null($topic))
        {
            $topic = new Topic;
            $topic->name = env('SNS_STAGE','').$topic_group_id."_topic_".$language;
            $topic->topic_group_id = $topic_group_id;
            $topic->language = $language;
            $topic->save();
            return $topic;
        }
        else{
            return $topic;
        }

    }

    public static function getTopicByGroupId($topic_group_id){
        $result = Topic::where('topic_group_id',$topic_group_id)->get();
        return $result;
    }

    public static function getTopicByGroupIdAndLangId($topic_group_id, $language_id) {
        return Topic::where('topic_group_id', $topic_group_id)
                        ->where('language', $language_id)
                        ->first();
    }

}
