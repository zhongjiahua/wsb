<?php namespace Legato\Push\Models;

use Model;
use Illuminate\Support\Facades\Log;
use Legato\Push\classes\Constant;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Common\Traits\LegatoSoftDelete;

/**
 * Model
 */

/*
 * $user = Message::first();

// Sets the name in the default language
$Message->title = 'English';

$user->translateContext('fr');

// Sets the name in French
$user->title = 'Anglais';
 */
class Message extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;
    use LegatoSoftDelete;

    protected $dates = ['deleted_at'];

    public $belongsToMany = [
        'Tags' => ['Legato\Push\Models\Tag',
                    'table'     =>  'legato_push_message_tag_link',
                    'key'       =>  'message_id',
                    'otherKey'  =>  'tag_id'] ,
        'Topic_progress' =>  ['Legato\Push\Models\Topic',
                                'table'     =>  'legato_push_progress_topic',
                                'key'       =>  'message_id',
                                'otherKey'  =>  'topic_id'] ,
        'Topic_complete' =>  ['Legato\Push\Models\Topic',
            'table'     =>  'legato_push_complete_topic',
            'key'       =>  'message_id',
            'otherKey'  =>  'topic_id'],
        'Individual_progress' => ['Legato\Push\Models\Device',
            'table'     =>  'legato_push_progress_individual',
            'key'       =>  'message_id',
            'otherKey'  =>  'device_id'],
        'Individual_complete' =>  ['Legato\Push\Models\Device',
            'table'     =>  'legato_push_complete_individual',
            'key'       =>  'message_id',
            'otherKey'  =>  'individual_id'],
        'User_progress' => ['Sinoclub\Member\Models\Session',
            'table'     =>  'legato_push_progress_individual_user',
            'key'       =>  'message_id',
            'otherKey'  =>  'user_id']

    ];


    protected $jsonable = ['extra_data'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'member_ids'    => 'json',
        'subject'       => 'max:100',
        'title'         => 'max:500',
        'message_type'  => 'in:0,1,2,3',
        'badge'         => 'nullable|integer'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_message';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'body'
    ];

    /****************************
     * Implement method
     ****************************/

    /*
     * If the message is deleted , remove all pending for publish record
     */
    public function beforeDelete()
    {
        $this->detachTarget();

    }

    /*public function afterSave(){
        $extra_data = $this->extra_data;
        $type = 0;
        $ref_id = 0;

//            trace_log($extra_data);

        foreach ($extra_data as $extra_datum){
            if ($extra_datum['extra_data_key'] == \Echome\Push\classes\Constant::KEY_TYPE){
                $type = $extra_datum['extra_data_value'];
            }
            if ($extra_datum['extra_data_key'] == \Echome\Push\classes\Constant::KEY_RECORD){
                $ref_id = $extra_datum['extra_data_value'];
                break;
            }
        }

        if ($type == 5){
            Db::table('legato_push_message')
                ->where('id', $this->id)
                ->update(['extra_data' => \GuzzleHttp\json_encode(Message::arrayToOctoberFormat(array(\Echome\Push\classes\Constant::KEY_TYPE=> 5 , \Echome\Push\classes\Constant::KEY_RECORD => $this->id)))]);

        }
    }*/

    /*****************************
     * Custom methods
     *****************************/

    /*****************************
     * Create
     *****************************/

    public static function initWithData($title,$body,$other_titles,$other_bodies,$schedule_time,$push_platform,
                                        $badge=0,$sound="",$ttl=3600,$content_available = 0,$mutable_content = 0, $attachment_url = "",
                                        $category = "",$extra_data = array(),$status = Constant::PUSH_STATUS_ACTIVE,
                                        $create_user = 0, $modify_user =0){

        $msg = new Message();
        $msg->title = $title;
        $msg->body = $body;
        $msg->schedule_time = $schedule_time;
        $msg->push_platform = $push_platform;
        $msg->badge = $badge;
        $msg->sound = $sound;
        $msg->ttl = $ttl;
        $msg->content_available = $content_available;
        $msg->mutable_content = $mutable_content;
        $msg->attachment_url = $attachment_url;
        $msg->category = $category;
        $msg->status = $status;
        $msg->extra_data = Message::arrayToOctoberFormat($extra_data);
        $msg->create_user = $create_user;
        $msg->modify_user = $modify_user;
        $msg->save();

        if (is_array($other_titles)){
            foreach ($other_titles as $code => $value){
                $msg->setAttributeTranslated('title', $value, $code);
            }
        }

        if (is_array($other_bodies)){
            foreach ($other_bodies as $code => $value){
                $msg->setAttributeTranslated('body', $value, $code);
            }
        }

        return $msg;


    }


    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    public function setOtherLangTitleWithArray($other_titles){
        if (is_array($other_titles)){
            foreach ($other_titles as $code => $value){
                $this->setAttributeTranslated('title', $value, $code);
            }
        }
        return $this;
    }

    public function setBody($body){
        $this->body = $body;
        return $this;
    }

    public function setOtherLangBodyWithArray($other_bodies){

        if (is_array($other_bodies)){
            foreach ($other_bodies as $code => $value){
                $this->setAttributeTranslated('body', $value, $code);
            }
        }
        return $this;
    }

    public function setScheduleTime($schedule_time){
        $this->schedule_time = $schedule_time;
        return $this;
    }

    public function setPushPlatform($push_platform){
        $this->push_platform = $push_platform;
        return $this;
    }

    public function setBadge($badge){
        $this->badge = $badge;
        return $this;
    }

    public function setSound($sound){
        $this->sound = $sound;
        return $this;
    }

    public function setTTL($ttl){
        $this->ttl = $ttl;
        return $this;
    }

    public function setContentAvailable($content_available){
        $this->content_available = $content_available;
        return $this;
    }

    public function setMutableContent($mutable_content){
        $this->mutable_content = $mutable_content;
        return $this;
    }

    public function setAttachmentUrl($attachment_url){
        $this->attachment_url = $attachment_url;
        return $this;
    }

    public function setCategory($category){
        $this->category = $category;
        return $this;
    }

    public function setStatus($status){
        $this->status = $status;
        return $this;
    }

    public function setExtraData($extra_data){
        $this->extra_data = Message::arrayToOctoberFormat($extra_data);;
        return $this;
    }

    public function setCreateUser($create_user){
        $this->create_user = $create_user;
        return $this;
    }

    public function setModifiedUser($modify_user){
        $this->modify_user = $modify_user;
        return $this;
    }

    /*****************************
     * Get
     *****************************/
    public static function getMessageByTag($tag_arr=array())
    {
        if (!count($tag_arr)) { //if array is empty
            return Response::json([Constant::KEY_RESULT => 0,Constant::KEY_MSG => 'Tag array cannot be blank']);
        }

        $messages = Message::whereHas('Tags', function ($query) use ($tag_arr) {
//            Log::info("Tag_arr",$tag_arr);
            $query->whereIn('name', $tag_arr);
        })->get();

//        Log::info(json_encode($messages));

        return $messages;
    }

    /****************************
     * Sending methods
     * This function is tailor-made for RA project
     * Get device from RA table and get the ID from push device table
     *****************************/


    public function sendMessageToDeviceByCustomField($customFields = array()){
//        if (!count($customFields)) { //if array is empty
//            return Response::json([Constant::KEY_RESULT => 0, Constant::KEY_MSG => 'Array cannot be blank']);
//        }
//
//        $devices = Device::getDeviceByCustomFields($customFields);
//        Progress_individual::createBatchByDevices($devices,$this->id);
//
//        return Response::json([Constant::KEY_RESULT => 1,Constant::KEY_MSG => 'Success']);

        if (!count($customFields)) { //if array is empty
            return Response::json([Constant::KEY_RESULT => 0, Constant::KEY_MSG => 'Array cannot be blank']);
        }

        Progress_individual_user::createBatchByUserIds($customFields,$this->id);

        return Response::json([Constant::KEY_RESULT => 1,Constant::KEY_MSG => 'Success']);
    }


    /**
     * Create Push message to individual device
     */
    public function sendMessageToDeviceByTokenArr($token_arr = array())
    {
        if (!count($token_arr)) { //if array is empty
            return Response::json([Constant::KEY_RESULT => 0,Constant::KEY_MSG => 'Array cannot be blank']);
        }

        $devices = Device::getDeviceByTokenArr($token_arr);
        Progress_individual::createBatchByDevices($devices,$this->id);

        return Response::json([Constant::KEY_RESULT => 1,Constant::KEY_MSG => 'Success']);

    }


    /**
     * Create Push message to topic group
     */
    public function sendMessageToTopicGroupById($topic_group_id_arr = array())
    {
        if (!count($topic_group_id_arr)) { //if array is empty
            return Response::json([Constant::KEY_RESULT => 0,Constant::KEY_MSG => 'Array cannot be blank']);
        }

        foreach ($topic_group_id_arr as $topic_group_id) {
            $topic_group = Topic_group::find($topic_group_id);
            $this->sendMessageToTopicGroup($topic_group);
        }

        return Response::json([Constant::KEY_RESULT => 1,Constant::KEY_MSG => 'Success']);
    }

    public function sendMessageToDefaultTopic(){

//        $defaultTopicGroup = Topic_group::where("name",Constant::NAME_DEFAULT_ALL_TOPIC)->first();
        self::sendMessageToTopicGroupByName(array(Constant::NAME_DEFAULT_ALL_TOPIC));

    }

    public function sendMessageToTopicGroupByName($topic_group_name_arr = array())
    {
        if (!count($topic_group_name_arr)) { //if array is empty
            return Response::json([Constant::KEY_RESULT => 0,Constant::KEY_MSG => 'Array cannot be blank']);
        }

        foreach ($topic_group_name_arr as $topic_group_name) {
            $topic_group = Topic_group::where('name', $topic_group_name)->first();
            $this->sendMessageToTopicGroup($topic_group);
        }

        return Response::json([Constant::KEY_RESULT => 1,Constant::KEY_MSG => 'Success']);
    }

    private function sendMessageToTopicGroup($topic_group){
        foreach ($topic_group->topics as $topic)
        {
            if ($this->topicValidation($topic->id,$this->id))
                $this->Topic_progress()->attach($topic->id);
        }
    }

    private function topicValidation($topic_id,$msg_id){
        $result = Progress_topic::where('topic_id',$topic_id)
                                    ->where('message_id',$msg_id)->first();
        if ($result)
        {
            return false;
        }

        $result = Complete_topic::where('topic_id',$topic_id)
            ->where('message_id',$msg_id)->first();

        if ($result)
        {
            return false;
        }

        return true;

    }

    private static function arrayToOctoberFormat($inputs = array()){
        $result = array();

        foreach ($inputs as $key => $value){
            $in_arr = array();
            $in_arr['extra_data_key'] = $key;
            $in_arr['extra_data_value'] = $value;

            $result[] = $in_arr;
        }

        return $result;
    }

    /**
     * @todo extend from sinoclub\push\models
     * @todo send to individual device
     * For Sino case only
     * handle member list for each message
     */
    public static function getInitMessage(){
        return self::where('progress_status',self::PROGRESS_INIT)->get();

    }

    public function getProgressStatusOptions(){
        return [
            self::PROGRESS_INIT             => 'init',
            self::PROGRESS_PROGRESSING      => 'progressing',
            self::PROGRESS_TARGET_ATTACHED  => 'attached',
        ];
    }
    const PROGRESS_INIT                 = 1;
    const PROGRESS_PROGRESSING          = 2;
    const PROGRESS_TARGET_ATTACHED      = 3;
    //const PROGRESS_SENT                 = 4;

    public function attachTarget(){
        $this->progress_status = self::PROGRESS_PROGRESSING;
        $this->save();

        $topic_group_id = null;
        $member_ids = $this->member_ids;
        if($member_ids){
            $members = json_decode($member_ids,true);
            if (count($members)>0) {
                if(!$this->audience_list_id){
                    $audience_list = new \Sinoclub\Push\Models\AudienceList();
                    $audience_list->name = 'auto_list';
                    $audience_list->status = 1;
                    $audience_list->upload_csv = 0;
                    $audience_list->is_system = 1;
                    $audience_list->save();
                    $audience_list->createTopic();
                    $audience_list->createSubscription();

                    $this->audience_list_id = $audience_list->id;
                    $this->save();
                }else{
                    $audience_list = \Sinoclub\Push\Models\AudienceList::find($this->audience_list_id);
                }
                $audience_users = $audience_list->audienceItems;
                foreach ($members as $key => $value) {
                    $exist = $audience_users->where('user_id',$value)->first();
                    if(!$exist){
                        $audienceUser = new \Sinoclub\Push\Models\AudienceUser();
                        $audienceUser->audience_list_id = $audience_list->id;
                        $audienceUser->user_id = $value;
                        $audienceUser->save();
                    }
                }
                $old_users = $audience_users->whereNotIn('user_id',$members)->all();

                foreach ($old_users as $key => $old_user) {
                    $old_user->delete();
                }
                $topic_group_id = $audience_list->topic_group_id;
            }else{
            }
        }
        if($topic_group_id)
            $this->sendMessageToTopicGroupById([$topic_group_id]);
        else
            $this->sendMessageToDefaultTopic();

        $this->progress_status = self::PROGRESS_TARGET_ATTACHED;
        $this->save();
    }

    public function detachTarget(){
        $this->Topic_progress()->detach();
        $this->Individual_progress()->detach();
        $this->User_progress()->detach();
    }

    public function beforeSave(){
        if($this->isDirty('member_ids')){
            switch ($this->progress_status) {
                case self::PROGRESS_PROGRESSING:
                    $this->detachTarget();
                
                default:
                    # code...
                    break;
            }
            $this->progress_status = self::PROGRESS_INIT;


        }
    }

}
