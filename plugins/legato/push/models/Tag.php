<?php namespace Legato\Push\Models;

use Model;

/**
 * Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $belongsToMany = [
        'Messages' => ['legato\push\models\Message',
            'table'     =>  'legato_push_message_tag_link',
            'key'       =>  'tag_id',
            'otherKey'  =>  'message_id']

    ];

    protected $fillable = ['name'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_message_tag';
}
