<?php namespace Legato\Push\Models;

use Illuminate\Support\Facades\DB;
use Model;

/**
 * Model
 */
class Progress_topic extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'message' => 'Legato\Push\Models\Message',
        'topic' => 'Legato\Push\Models\Topic'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_progress_topic';

    public function moveToComplete()
    {
        $complete_topic = new Complete_topic();
//        $complete_topic->message_id = $this->message_id;
//        $complete_topic->topic_id = $this->topic_id;
//        $complete_topic->status = $this->status;
//
        $complete_topic = $complete_topic->firstOrCreate(['message_id' => $this->message_id , 'topic_id' =>$this->topic_id, 'status' => $this->status]);
        $this->delete();
        //$this->insertInBox();

        return $complete_topic;
    }

    public function insertInBox(){


    }

    public function getGroupTopic(){
        //return AudienceList::where('topic_group_id',$this->topic->topic_group_id)->first();
    }

}
