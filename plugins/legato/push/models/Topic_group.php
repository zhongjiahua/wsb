<?php namespace Legato\Push\Models;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Legato\Push\classes\CustomValidator;
use Model;
use Log;


// Validator::resolver(function($translator, $data, $rules, $messages, $customAttributes) {
// //    Log::info(var_dump($data));
//     $messages['topic_group_name'] = Lang::get('legato.push::lang.legato.push.defaultTopicNameUsed');
//     // return new CustomValidator($translator,$data,$rules,$messages,$customAttributes);
// });
/**
 * Model
 */
class Topic_group extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $fillable = ['name'];

    protected $dates = ['deleted_at'];


//    public $belongsToMany = [
//        'topics' => [
//            'Legato\Push\Models\Topic',
//            'table' => 'rainlab_blog_posts_categories',
//            'order' => 'name'
//        ]
//    ];

    public $hasMany = [
        'topics' => [
            'Legato\Push\Models\Topic',
            'table' => 'legato_push_topic',
            'key'  =>  'topic_group_id'
        ]
    ];


    /**
     * @var array Validation rules
     */
    public $rules = [
        //'name' => ['TopicGroupName']
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_push_topic_group';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];


    public function beforeSave(){
//        Log::info('Before Save:'.json_encode($this));
//        $sessionKey = uniqid('session_key', true);
//
////        $topic = Topic::firstOrCreate(['topic_group_id' => $this->id,'language' => 1]);
//
//        $topic = new Topic;
//        $topic->name = "sub-topic";
//        $topic->topic_group_id = $this->id;
//        $topic->language = 1;
//        $topic->save();
//
//        $this->topics()->add($topic,$sessionKey);
    }

    public function afterSave()
    {
//        Log::info('After Save:' . json_encode($this->id));

        /**
         * Current locale
         */
        if (class_exists('Rainlab\translate\Models\Locale')) {

            $locales = PushLocale::listAvailable();
            foreach ($locales as $locale)
            {
                Topic::createOrGetUniqueTopic($this->id,$locale->id);
            }

//            Log::info("Available locales:".json_encode($locales));
        } else {
            $topic = Topic::createOrGetUniqueTopic($this->id,0);
        }

    }

    public function afterDelete()
    {
//        Log::info('After Delete:' . json_encode($this->id));

        Topic::where('topic_group_id', $this->id)
            ->update(['status' => 0]);

    }
}
