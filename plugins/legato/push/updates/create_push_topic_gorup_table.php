<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushTopicGroupTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_topic_group', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',200);

            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->index('status');
            $table->index('deleted_at');
            $table->index('name');


        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_topic_group');
    }
}
