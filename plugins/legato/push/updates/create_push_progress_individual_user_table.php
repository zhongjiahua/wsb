<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushProgressIndividualUserTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_progress_individual_user', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('topic_id')->nullable();//useless?
            $table->unsignedInteger('message_id');
            $table->unsignedInteger('user_id');
            $table->tinyInteger('status')->default(1);
            //$table->unsignedInteger('created_by')->nullable();
            //$table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            //$table->foreign('message_id')->references('id')->on('legato_push_message');
            $table->index('message_id');
            //$table->foreign('user_id')->references('id')->on('users');
            $table->index('user_id');
            $table->index('status');
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_progress_individual_user');
    }
}
