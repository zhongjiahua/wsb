<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushMessageTagTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_message_tag', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',200);

            $table->index('name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_message_tag');
    }
}
