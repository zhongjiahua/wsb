<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushTopicTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_topic', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',200);

            $table->unsignedInteger('language');
            $table->unsignedInteger('topic_group_id');

            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            //$table->foreign('language')->references('id')->on('rainlab_translate_locales');
            $table->index('language');
            //$table->foreign('topic_group_id')->references('id')->on('legato_push_topic_group');
            $table->index('topic_group_id');
            $table->index('status');


        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_topic');
    }
}
