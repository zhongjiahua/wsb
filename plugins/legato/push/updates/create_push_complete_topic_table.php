<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushCompleteTopicTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_complete_topic', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('topic_id');
            $table->unsignedInteger('message_id');
            $table->tinyInteger('status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            //$table->foreign('message_id')->references('id')->on('legato_push_message');
            //$table->foreign('topic_id')->references('id')->on('legato_push_topic');
            $table->index('message_id');
            $table->index('topic_id');
            $table->index('status');
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_complete_topic');
    }
}
