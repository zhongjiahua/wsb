<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushMessageTagLinkTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_message_tag_link', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->unsignedInteger('message_id')->nullable();
            $table->unsignedInteger('tag_id')->nullable();
            
            //$table->unsignedInteger('created_by')->nullable();
            //$table->unsignedInteger('updated_by')->nullable();
            //$table->timestamps();
            //$table->softDeletes();

            //$table->foreign('message_id')->references('id')->on('legato_push_message');
            //$table->foreign('tag_id')->references('id')->on('legato_push_message_tag');
            $table->index('message_id');
            $table->index('tag_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_message_tag_link');
    }
}
