<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushDeviceTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_device', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('token',300);
            $table->tinyInteger('token_type')->default(1);
            $table->integer('language')->default(0);
            $table->integer('os_type')->default(0);
            $table->string('custom_field',200);
            $table->tinyInteger('status')->default(1);
            $table->integer('platform_arn_position')->default(0);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('deleted_at');
            $table->index('status');
            $table->index('token_type');
            $table->index('token');
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_device');
    }
}
