<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushMessageTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_message', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title',45)->default('');
            $table->string('body',300);
            $table->dateTime('schedule_time');
            $table->tinyInteger('push_platform')->default(2);
            $table->integer('badge')->nullable();
            $table->string('sound',200)->nullable();
            $table->bigInteger('ttl')->default(172800);
            
            $table->tinyInteger('content_available')->default(0);
            $table->tinyInteger('mutable_content')->default(0);

            $table->string('attachment_url',100)->nullable();
            $table->string('category',200)->nullable();

            $table->tinyInteger('status')->default(1);
            
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_message');
    }
}
