<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushSubscriptionTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_subscription', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->unsignedInteger('device_id');
            $table->unsignedInteger('topic_id');

            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();

            //$table->foreign('device_id')->references('id')->on('legato_push_device');
            //$table->foreign('topic_id')->references('id')->on('legato_push_topic');
            $table->index('device_id');
            $table->index('topic_id');
            $table->index('status');
            $table->index('deleted_at');


        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_subscription');
    }
}
