<?php namespace Legato\Push\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushProgressIndividualTable extends Migration
{
    public function up()
    {
        Schema::create('legato_push_progress_individual', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('topic_id')->nullable();//useless?
            $table->unsignedInteger('message_id');
            $table->unsignedInteger('device_id');
            $table->tinyInteger('status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            //$table->foreign('device_id')->references('id')->on('legato_push_device');
            //$table->foreign('message_id')->references('id')->on('legato_push_message');
            $table->index('device_id');
            $table->index('message_id');
            $table->index('status');

        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_push_progress_individual');
    }
}
