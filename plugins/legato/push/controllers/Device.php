<?php namespace Legato\Push\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Legato\Push\classes\Constant;

class Device extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'push_master' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Legato.Push', 'push-menu-message', 'push-side-menu-device');
    }

    public function test()
    {
//        $device = \Legato\Push\Models\Device::find(2);
//        echo $device->endpoint->arn;
//        $device->endpoint->arn = "1122";
//        $device->save();
        return \Legato\Push\Models\Device::changeSubscribedTopicLang(22,1);
    }

    public function testCreate()
    {
        for($i = 0 ; $i <10 ; $i++)
        {
            \Legato\Push\Models\Device::updateOrCreateDevice(str_random(16),Constant::TOKEN_TYPE_DEV,1,Constant::OS_TYPE_IOS,5,Constant::PLATFORM_AWS);
        }

//        \Legato\Push\Models\Device::updateOrCreateDevice("c1c7ef6209d788b8a3d21d23c0dab795957b4a7d964dfcd95bf53555521bd1c9",Constant::TOKEN_TYPE_DEV,1);
    }
}
