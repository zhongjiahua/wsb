<?php namespace Legato\Push\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Message extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'push_master' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Legato.Push', 'push-menu-message');
    }

    public function test()
    {
//        return "ABC";
//        $message = new \Legato\Push\Models\Message();
        $message = \Legato\Push\Models\Message::find(1);
//        return $message->sendMessageToTopicGroupById(array("26"));
//        return $message->sendMessageToTopicGroupByName(array("G13"));
//        \Legato\Push\Models\Message::getMessageByTag(array("Tag1"));

        $message->sendMessageToDeviceByTokenArr(array("ptk","ptk1","token1","1111","c1c7ef6209d788b8a3d21d23c0dab795957b4a7d964dfcd95bf53555521bd1c9"));

    }
}
