<?php namespace Legato\Push\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Topic extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'push_master' 
    ];

    public function __construct()
    {
        parent::__construct();
    }
}
