<?php namespace Legato\Push\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Topic_group extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'push_master' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Legato.Push', 'push-menu-message', 'push-side-menu-topic_group');
    }

    public function initialSetup(){
        $topic_group = \Legato\Push\Models\Topic_group::firstOrNew(['name' => 'Default_All']);
        $topic_group->save();
    }
}
