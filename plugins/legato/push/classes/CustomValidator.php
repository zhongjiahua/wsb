<?php namespace Legato\Push\classes;

use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Log;
use Legato\Push\Models\Subscription;
/**
 * Created by PhpStorm.
 * User: ryanng
 * Date: 20/3/2018
 * Time: 12:59 PM
 */

class CustomValidator extends Validator
{

    public function validateDeviceSub($attribute, $value, $parameters)
    {
        $device_id = $value;
        $topic_id = $this->data['topic_id'];
        $subscription = Subscription::isSubscriptionExist($device_id,$topic_id);
        return is_null($subscription);
    }

    public function validateTopicGroupName($attribute, $value, $parameters)
    {
        $name = $value;
        return !($name == Constant::NAME_DEFAULT_ALL_TOPIC);
    }

}