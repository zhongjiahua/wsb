<?php namespace Legato\Push\classes;

/**
 * Created by PhpStorm.
 * User: ryanng
 * Date: 22/3/2018
 * Time: 1:09 PM
 */

class Constant
{
    const PUSH_LOG_PREFIX            = "[Core Push plugin]:";

    const PUSH_STATUS_INACTIVE  = 0;
    const PUSH_STATUS_ACTIVE    = 1;

    const KEY_RESULT = "result";
    const KEY_MSG = "msg";

    const TOKEN_TYPE_DEV = 1;
    const TOKEN_TYPE_INHOUSE_PRO = 2;
    const TOKEN_TYPE_PRO = 3;

    const OS_TYPE_UNKNOWN = 0;
    const OS_TYPE_IOS = 1;
    const OS_TYPE_ANDROID = 2;

    const PLATFORM_LOCAL = 1;
    const PLATFORM_AWS = 2;
    const PLATFORM_AZURE = 3;

    const NAME_DEFAULT_ALL_TOPIC = "Default_All";

}