                                                    Mobile Application Push Notification
                                                    ------------------------------------

* Please run "{SERVER}/legato/push/topic_group/initialSetup" to create default topic first

I. Device
---------------
* Register mobile device: 
```
Device::updateOrCreateDevice($token,$token_type,$language, $osType,$custom_field,$platform_arn_position)
```

###Arguments:

Argument 		| Description 	| Possible Value
------------ | ------------- | -----------
token | PK & Push token from device | String
token_type | Development environment of device | `Constant::TOKEN_TYPE_DEV` `Constant::TOKEN_TYPE_INHOUSE_PRO` `Constant::TOKEN_TYPE_PRO`
language | The language of the device. | Integer. ID of the language
osType | The operation system of device |  `Constant::OS_TYPE_UNKNOWN` `Constant::OS_TYPE_IOS` `Constant::OS_TYPE_ANDROID`
custom_field | Free text for developver to use. Can act as remark field.Can be used to store member id | String / Integer
platform\_arn\_position | To support multiple bundle/package in single push server | Integer

###Example:

```php
use Legato\Push\Models\Device;
use Legato\Push\classes\Constant;

class Testing extends Controller
{
	public function test(){
		Device::updateOrCreateDevice('abcdefghiljkl',Constant::TOKEN_TYPE_DEV,1,Constant::OS_TYPE_IOS,10,0);
	}
}
```
----------

* Change Device language:
```
Device::changeDeviceLangByToken($token,$language)
```

###Arguments:

Argument | Description | Possible Value
-------- | ------------ | -------------
token | PK & Push token from device | String
language | The language of the device. | Integer. ID of the language

###Example:
```php
use Legato\Push\Models\Device;
use Legato\Push\classes\Constant;

class Testing extends Controller
{
	public function test(){
		Device::changeDeviceLangByToken('abcdefghiljkl',2);
	}
}
```
----------
* Change Device custom field value. Can used to remove/add device related to member id :
```
Device::updateDeviceCustomFieldByToken($token, $custom_field)
```

###Arguments:

Argument | Description | Possible Value
-------- | ------------ | -------------
token | PK & Push token from device | String
custom_field | Free text for developver to use. Can act as remark field.Can be used to store member id | String / Integer

###Example:
```php
use Legato\Push\Models\Device;
use Legato\Push\classes\Constant;

class Testing extends Controller
{
	public function test(){
		Device::updateDeviceCustomFieldByToken('abcdefghiljkl',21);
	}
}
```
----------
II. Message
---------------
* Create a new push message: 

###Setter:

Setter 		| Description 	| Possible Value | Default Value
------------ | ------------- | ----------- | ------- 
setTile() | Set the default push title | String | **_Required_**
setOtherLangTitleWithArray() | Multilingual support for push title. Array with code as key, string as value | `array('zh' => '中文')`; | 
setBody() | Set the default push body | String | **_Required_**
setOtherLangBodyWithArray() | Multilingual support for push body. Array with code as key, string as value |  `array('zh' => '中文')`
setScheduleTime() | Set the scheduled push date time. | String. `2018-01-01 01:01:01` | 0000-00-00 00:00:00
setPushPlatform() | Set the push service to be used. | `Constant::PLATFORM_AWS` | 0
setBadge() | Set the badge number. Effecetive for iOS only | Integer | Null
setSound() | Set the specified sound to be played when push arrived.[Audio file should be placed in application in advance] | String | Null
setTTL() | Set the time to live in second | Integer | 3600
setContentAvailable() | Set the push is slient push or not | 0: Normal push , 1 = Slient push | 0
setMutableContent() | Set the push contains media attachment or not | 0: NO , 1 = Yes. If YES, please set the `attachment_url` | 0
setAttachmentUrl() | Set the attachment url. For Rich notification to use. | String | Null
setCategory() | Set the category of the push. Effective for iOS only | String | Null
setStatus() | Set the status of push | `Constant::PUSH_STATUS_INACTIVE` `Constant::PUSH_STATUS_ACTIVE` | 1 
setExtraData() | Extra information to be passed to application though push | Array. `array('Key1' => 'Value1', 'Key2' => 'Value2')` | Null
setCreateUser() | Set the ID of create user | Integer. ID of user. | Null
setModifiedUser() | Set the ID of modify user | Integer. ID of user. | Null

**_Please call `->save()` at the end_** 
###Example:

```php
use Legato\Push\Models\Message;
use Legato\Push\classes\Constant;

class Testing extends Controller
{
	public function test(){
		$message = new Message();
		$message->setTitle("test_t2")->setOtherLangTitleWithArray(array("zh"=>"Chinese title 1"))
        ->setBody("test_b2")->setOtherLangBodyWithArray(array("zh" => "Chinese body 2"))
        ->setScheduleTime("2018-04-17 11:55:00")->setPushPlatform(Constant::PLATFORM_AWS)
        ->setBadge(10)->setSound("s1.mp3")->setTTL(1000)->setContentAvailable(1)->setMutableContent(1)
        ->setAttachmentUrl("attachment_url2")->setCategory("c1")->setStatus(0)->setExtraData($extra_data)
        ->setCreateUser(10)->setModifiedUser(10)->save();
	}
}
```
----------
Message can be sent though following methods.
You can use 1 or All method at the same time.

* Send push message by custom field.
```
Message::sendMessageToDeviceByCustomField($customFields);
```
* Send push message by token.
```
Message::sendMessageToDeviceByTokenArr($token_arr);
```
* Send push message by topic name.
```
Message::sendMessageToTopicGroupById($topic_group_name_arr);
```

###Arguments:

Argument | Description | Possible Value
-------- | ------------ | -------------
customFields | A list of custom field value | Array. `array('A','B')`
token_arr | A list of targeted push token | Array. `array('Device1','Device2')`
topic\_group\_name\_arr | A list of topic name | Array. `array('Topic1','Topic2')`

###Example:
```php
use Legato\Push\Models\Message;
use Legato\Push\classes\Constant;

class Testing extends Controller
{
	public function test(){
		$message = new Message();
		$message->setTitle("test_t2")->->setBody("test_b2")
		->setScheduleTime("2018-04-17 11:55:00")->setPushPlatform(Constant::PLATFORM_AWS)->save();
    	$message->sendMessageToDeviceByCustomField(array("1"));
    	$message->sendMessageToDeviceByTokenArr(array("ptk","ptk1","token1","1111","c1c7ef6209d788b8a3d21d23c0dab795957b4a7d964dfcd95bf53555521bd1c9"));
    	$message->sendMessageToTopicGroupByName(array("G13"));
	}
}
```
----------

