CREATE TABLE `legato_braintree_error_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(191) DEFAULT NULL,
  `braintree_message` varchar(191) DEFAULT NULL,
  `braintree_result` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `legato_braintree_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `recurring_billing_refer_id` int(10) UNSIGNED DEFAULT NULL,
  `currency_iso_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merchant_account_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `braintree_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `braintree_customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `legato_braintree_error_log_id` int(10) UNSIGNED DEFAULT NULL,
  `braintree_message` text COLLATE utf8mb4_unicode_ci,
  `transaction_date` datetime DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `legato_braintree_error_log`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `legato_braintree_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recurring_billing_refer_id` (`recurring_billing_refer_id`),
  ADD KEY `legato_braintree_error_log_id` (`legato_braintree_error_log_id`);


ALTER TABLE `legato_braintree_error_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `legato_braintree_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;


ALTER TABLE `legato_braintree_payment`
  ADD CONSTRAINT `legato_braintree_payment_ibfk_1` FOREIGN KEY (`recurring_billing_refer_id`) REFERENCES `legato_braintree_payment` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `legato_braintree_payment_ibfk_2` FOREIGN KEY (`legato_braintree_error_log_id`) REFERENCES `legato_braintree_error_log` (`id`) ON DELETE SET NULL;
