<?php namespace Legato\Braintree\Models;

use Model;

/**
 * Model
 */
class Payment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_braintree_payment';

    public static function getById($id){
        \DB::enableQueryLog();

        return \DB::table('legato_braintree_payment')->where('id', $id)->first();
    }
}
