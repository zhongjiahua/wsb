<?php namespace Legato\Braintree\Library\Lib;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'Braintree.php';

class Braintree_library {
    private $gateway;
    const STATUS_SUCCESS    = 1;
    const STATUS_FAIL       = 0;

    public function __construct(){
        $environment    = \Config::get('legato.braintree::environment');
        $merchantId     = \Config::get('legato.braintree::merchantId');
        $publicKey      = \Config::get('legato.braintree::publicKey');
        $privateKey     = \Config::get('legato.braintree::privateKey');

        $this->gateway = new \Braintree_Gateway([
            'environment'   => $environment,
            'merchantId'    => $merchantId,
            'publicKey'     => $publicKey,
            'privateKey'    => $privateKey
        ]);
    }

    public function create_client_token(){
        return $this->gateway->clientToken()->generate();
    }

    public function transaction($amount, $nonce, $first_name, $last_name, $email, $merchant_account){
        $return_result = array();
        $return_result['result'] = FALSE;
        $return_result['data'] = array();

        //Create Braintree Customer
        $create_braintee_customer_result = $this->create_braintree_customer($nonce, $first_name, $last_name, $email);
        if(!$create_braintee_customer_result['result']){
            $return_result['result'] = FALSE;
            $return_result['data']['braintree_message']             = $create_braintee_customer_result['data']['braintree_message'];
            $return_result['data']['legato_braintree_error_log_id'] = $create_braintee_customer_result['data']['legato_braintree_error_log_id'];
            return $return_result;
        }
        $return_result['data']['braintree_customer_id'] = $create_braintee_customer_result['data']['braintree_customer_id'];

        //Create Braintree Transaction
        $create_braintree_transaction_result = $this->create_braintree_transaction($amount, $create_braintee_customer_result['data']['braintree_customer_id'], $merchant_account);
        $return_result['result'] = $create_braintree_transaction_result['result'];
        $return_result['data'] = array_merge_recursive($return_result['data'], $create_braintree_transaction_result['data']);

        //Insert Data to DB
        $legato_braintree_payment_id = \DB::table('legato_braintree_payment')->insertGetId($return_result['data']);
        $return_result['data']['legato_braintree_payment_id'] = $legato_braintree_payment_id;

        return $return_result;
    }

    public function create_braintree_customer($nonce, $first_name, $last_name, $email){
        $return_result = array();
        $return_result['result'] = FALSE;
        $return_result['data'] = array();

        $result = $this->gateway->customer()->create([
            'paymentMethodNonce' => $nonce,
            'firstName' => $first_name,
            'lastName' => $last_name,
            'email' => $email,
        ]);

        if ($result->success) {
            $return_result['result'] = TRUE;
            $return_result['data']['braintree_customer_id'] = $result->customer->id;
        } else {
            $braintree_message = '';

            $return_result['result'] = FALSE;

            foreach($result->errors->deepAll() AS $error) {
                $braintree_message += $error->code . ": " . $error->message . "\n";
            }
            $return_result['data']['braintree_message'] = $braintree_message;

            //insert to db:legato_braintree_error_log
            $legato_braintree_error_log = array();
            $legato_braintree_error_log['action'] = 'create_braintree_customer';
            $legato_braintree_error_log['braintree_result'] = $result;
            $legato_braintree_error_log['braintree_message'] = $braintree_message;
            $return_result['data']['legato_braintree_error_log_id'] = \DB::table('legato_braintree_error_log')->insertGetId($legato_braintree_error_log);
        }

        return $return_result;
    }

    public function create_braintree_transaction($amount, $braintree_customer_id, $merchant_account){
        $return_result = array();
        $return_result['result'] = FALSE;
        $return_result['data'] = array();

        $transaction_result = $this->gateway->transaction()->sale([
            'amount'				=>	$amount,
            'customerId'            =>  $braintree_customer_id,
            'merchantAccountId'		=>	$merchant_account,
            'options' 				=>	[
                'submitForSettlement' => True
            ]
        ]);

        $return_result['data']['amount']                    = $transaction_result->transaction->amount;
        $return_result['data']['braintree_transaction_id']  = $transaction_result->transaction->id;
        $return_result['data']['transaction_date']          = $transaction_result->transaction->createdAt->setTimezone(new \DateTimeZone('Asia/Hong_Kong'))->format('Y-m-d H:i:s');
        $return_result['data']['merchant_account_id']       = $transaction_result->transaction->merchantAccountId;
        $return_result['data']['currency_iso_code']         = $transaction_result->transaction->currencyIsoCode;

        if($transaction_result->success){
            $return_result['result'] = TRUE;
            $return_result['data']['status'] = SELF::STATUS_SUCCESS;
        }else{
            $return_result['result'] = FALSE;
            $return_result['data']['braintree_message'] = $transaction_result->message;
            $return_result['data']['status'] = SELF::STATUS_FAIL;

            //insert to db:legato_braintree_error_log
            $legato_braintree_error_log = array();
            $legato_braintree_error_log['action'] = 'create_braintree_transaction';
            $legato_braintree_error_log['braintree_result'] = $transaction_result;
            $legato_braintree_error_log['braintree_message'] = $transaction_result->message;
            $return_result['data']['legato_braintree_error_log_id'] = \DB::table('legato_braintree_error_log')->insertGetId($legato_braintree_error_log);
        }

        return $return_result;
    }

    public function recurring_billing($amount, $legato_braintree_payment_id){
        $return_result = array();
        $return_result['result'] = FALSE;
        $return_result['data'] = array();

        $payment_detail = \Legato\Braintree\Models\Payment::getById($legato_braintree_payment_id);
        if(!$payment_detail){
            return $return_result;
        }
        $braintree_customer_id = $payment_detail->braintree_customer_id;
        $return_result['data']['recurring_billing_refer_id']    = $payment_detail->id;
        $return_result['data']['braintree_customer_id']         = $payment_detail->braintree_customer_id;

        //Create Braintree Transaction
        $create_braintree_transaction_result = $this->create_braintree_transaction($amount, $braintree_customer_id,$payment_detail->merchant_account_id);
        $return_result['result']    = $create_braintree_transaction_result['result'];
        $return_result['data']      = array_merge_recursive($return_result['data'], $create_braintree_transaction_result['data']);

        //Insert Data to DB
        $legato_braintree_payment_id = \DB::table('legato_braintree_payment')->insertGetId($return_result['data']);
        $return_result['data']['legato_braintree_payment_id'] = $legato_braintree_payment_id;

        return $return_result;
    }
}
