<?php namespace Legato\Braintree\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Legato\Braintree\Library\Lib;

class Payment extends Controller
{
    public $implement = [        
        'Backend\Behaviors\FormController',
    	'Backend\Behaviors\ReorderController'    
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    private $Braintree_library;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Legato.Braintree', 'main-menu-item', 'Payment');

        $this->Braintree_library = new Lib\Braintree_library();
        $this -> vars['token'] =  $this -> create_client_token();
    }

    public function index(){
//        echo '<pre>';
//            $recurring_billing_result = $this->recurring_billing(5, 1);
//            print_r($recurring_billing_result);
//        echo '</pre>';
    	die;
    }

    public function create_client_token(){
        $clientToken = $this->Braintree_library -> create_client_token();

        return $clientToken;
    }

    public function submit_payment($amount, $nonce, $first_name, $last_name, $email, $merchant_account){
        $transaction_result = $this->Braintree_library->transaction($amount, $nonce, $first_name, $last_name, $email,$merchant_account);

        return $transaction_result;
    }

    public function recurring_billing($amount, $legato_braintree_payment_id){
        $transaction_result = $this->Braintree_library->recurring_billing($amount, $legato_braintree_payment_id);

        return $transaction_result;
    }
}
