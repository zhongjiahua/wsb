**Braintree Payment**

--------------------------------------------------

1.  Configuration
	1.1) Place this folder to '/plugins/legato/'
	1.2) Goto './config/config.php' set the parameter from Braintree Console
	1.3) Import './db_script.sql' into database
	
--------------------------------------------------

2.  Create Client Token
	2.1) Function: create_client_token()
	2.2) Input parameters:
		2.2.1) NULL
	2.3) Success Response:
		2.3.1) clientToken  = [String]  {String from braintree, for setup client script}
	2.4) Sample Call:
		use Legato\Braintree\Controllers;
		$braintree_payment = new Payment();
		$braintree_client_token = $braintree_payment->create_client_token();
		

3.  Submit Payment
	3.1) Function: submit_payment($amount, $nonce)
	3.2) Input parameters:
		3.2.1) amount       = [double]  {Payment amount}
		3.2.2) nonce        = [String]  {String from client script, generated from braintree}
	3.3) Success Response:
		[array]
		3.3.1) result       = TRUE[boolean] {Transaction result}
		3.3.2) data         = [array]
			3.3.2.1) braintree_customer_id          = [String]      {Braintree customer ID}
			3.3.2.2) amount                         = [double]      {Transaction amount}
			3.3.2.3) braintree_transaction_id       = [String]      {Braintree transaction ID}
			3.3.2.4) transaction_date               = [String]      {Braintree transaction date}
			3.3.2.5) merchant_account_id            = [String]      {Braintree merchant account ID}
			3.3.2.6) currency_iso_code              = [String]      {Currency ISO Code}
			3.3.2.7) status                         = 1[Integer]    {0:Fail|1:Success}
			3.3.2.8) legato_braintree_payment_id    = [Integer]     {DB legato_braintree_payment record ID}
	3.4) Transaction Failure Response:
    		[array]
    		3.4.1) result   = FALSE[boolean] {Transaction result}
    		3.4.2) data     = [array]
    			3.4.2.1)    braintree_customer_id          = [String]      {Braintree customer ID}
    			3.4.2.2)    amount                         = [double]      {Transaction amount}
    			3.4.2.3)    braintree_transaction_id       = [String]      {Braintree transaction ID}
    			3.4.2.4)    transaction_date               = [String]      {Braintree transaction date}
    			3.4.2.5)    merchant_account_id            = [String]      {Braintree merchant account ID}
    			3.4.2.6)    currency_iso_code              = [String]      {Currency ISO Code}
    			3.4.2.7)    status                         = 0[Integer]    {0:Fail|1:Success}
    			3.4.2.8)    legato_braintree_error_log_id  = [Integer]     {DB legato_braintree_error_log record ID}
    			3.4.2.9)    braintree_message              = [String]      {Braintree error message}
    			3.3.2.10)   legato_braintree_payment_id    = [Integer]     {DB legato_braintree_payment record ID}
    3.5) Create Braintree Customer Failure Response:
        		[array]
        		3.5.1) result   = FALSE[boolean] {Create Braintree Customer result}
        		3.5.2) data     = [array]
        			3.5.2.1)    braintree_message           = [String]      {Braintree error message}
        			3.5.2.2)   legato_braintree_payment_id  = [Integer]     {DB legato_braintree_error_log record ID}
    3.6) Sample Call:
    		use Legato\Braintree\Controllers;
    		$braintree_payment = new Payment();
    		$braintree_client_token = $braintree_payment->submit_payment($amount, $nonce);

4.  Recurring Billing
	4.1) Function: recurring_billing($amount, $legato_braintree_payment_id)
	4.2) Input parameters:
		4.2.1) $legato_braintree_payment_id = [Integer] {DB record ID}
		4.2.2) amount                       = [double]  {Payment amount}
    4.3) Success Response:
    		[array]
    		4.3.1) result       = TRUE[boolean] {Transaction result}
    		4.3.2) data         = [array]
    			4.3.2.1) braintree_customer_id          = [String]      {Braintree customer ID}
    			4.3.2.2) amount                         = [double]      {Transaction amount}
    			4.3.2.3) braintree_transaction_id       = [String]      {Braintree transaction ID}
    			4.3.2.4) transaction_date               = [String]      {Braintree transaction date}
    			4.3.2.5) merchant_account_id            = [String]      {Braintree merchant account ID}
    			4.3.2.6) currency_iso_code              = [String]      {Currency ISO Code}
    			4.3.2.7) status                         = 1[Integer]    {0:Fail|1:Success}
    			4.3.2.8) legato_braintree_payment_id    = [Integer]     {DB legato_braintree_payment record ID}
    	4.4) Failure Response:
        		[array]
        		4.4.1) result   = FALSE[boolean] {Transaction result}
        		4.4.2) data     = [array]
        			4.4.2.1)    braintree_customer_id          = [String]      {Braintree customer ID}
        			4.4.2.2)    amount                         = [double]      {Transaction amount}
        			4.4.2.3)    braintree_transaction_id       = [String]      {Braintree transaction ID}
        			4.4.2.4)    transaction_date               = [String]      {Braintree transaction date}
        			4.4.2.5)    merchant_account_id            = [String]      {Braintree merchant account ID}
        			4.4.2.6)    currency_iso_code              = [String]      {Currency ISO Code}
        			4.4.2.7)    status                         = 0[Integer]    {0:Fail|1:Success}
        			4.4.2.8)    legato_braintree_error_log_id  = [Integer]     {DB legato_braintree_error_log record ID}
        			4.4.2.9)    braintree_message              = [String]      {Braintree error message}
        			4.3.2.10)   legato_braintree_payment_id    = [Integer]     {DB legato_braintree_payment record ID}
        4.5) Sample Call:
        		use Legato\Braintree\Controllers;
        		$braintree_payment = new Payment();
        		$braintree_client_token = $braintree_payment->recurring_billing($amount, $legato_braintree_payment_id);