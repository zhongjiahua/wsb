<?php namespace Legato\Pushaws;

use Legato\Push\Models\Complete_individual;
use Legato\Push\Models\Complete_topic;
use Legato\Push\Models\Device;
use Legato\Push\Models\Message;
use Legato\Push\Models\Progress_topic;
use Legato\Push\Models\Subscription;
use Legato\Push\Models\Topic;
use Legato\Pushaws\classes\Constant;
use Legato\Pushaws\Models\AWSprogress_individual;
use Legato\Pushaws\Models\AWSprogress_topic;
use Legato\Pushaws\Models\Endpoint;
use System\Classes\PluginBase;
use Illuminate\Support\Facades\Log;
use Lang;

class Plugin extends PluginBase
{
    public $require = ['October.Drivers',"Legato.Push"];

    public function pluginDetails()
    {
        return [
            'name'        => 'pushaws',
            'description' => '',
            'author'      => 'Legato',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {

            Endpoint::cronCreateEndpointInAWS();

        })->name('uploadEndpointToAws')->withoutOverlapping();

        $schedule->call(function () {

            \Legato\Pushaws\Models\Topic::cronCreateTopic();

        })->name('uploadTopicToAws')->withoutOverlapping();

        $schedule->call(function () {

            \Legato\Pushaws\Models\Topic::cronDeleteTopic();

        })->name('deleteTopicFromAws')->withoutOverlapping();

        $schedule->call(function () {

            \Legato\Pushaws\Models\Subscription::cronCreateSubscription();

        })->name('uploadSubscriptionToAws')->withoutOverlapping();

        $schedule->call(function () {

            \Legato\Pushaws\Models\Subscription::cronDeleteSubscription();

        })->name('deleteSubscriptionFromAws')->withoutOverlapping();

        $schedule->call(function () {

            AWSprogress_topic::cronPublishMessageToTopic();

        })->name('sendTopicMessage')->withoutOverlapping();

        $schedule->call(function () {

            AWSprogress_individual::cronPublishMessageToEndpoint();

        })->name('sendIndividualMessage')->withoutOverlapping();
    }

    public function boot()
    {
        Device::extend(function ($model) {
            $model->hasOne['endpoint'] = ['Legato\Pushaws\Models\Endpoint'];
//            $model->addDynamicProperty('platformarn', null);
//
//            $model->addDynamicMethod('getPlatformarnAttribute', function($value) {
//                return $value;
//            });
//
//            $model->addDynamicMethod('setPlatformarnAttribute', function($value) use ($model) {
//                $model->platformarn = $value;
//            });
//
            $model->bindEvent('model.afterSave', function () use ($model) {

                if (is_null($model->endpoint)) {
                    $model->endpoint()->add(new Endpoint());
                } else {
                    $endPoint = Endpoint::where('device_id', $model->id)->first();
                    $endPoint->sns_status = Constant::SNS_STATUS_PENDING;
                    $endPoint->error_reason = "";
                    $endPoint->save();
                }
            });
        });

        Subscription::extend(function ($model) {
            $model->hasOne['aws_subscription'] = ['Legato\Pushaws\Models\Subscription'];

            $model->bindEvent('model.afterSave', function () use ($model) {
//                trace_log($model->aws_topic);
                if (is_null($model->aws_subscription)) {
                    $model->aws_subscription()->add(new \Legato\Pushaws\Models\Subscription());
                } else {
                    $aws_topic = \Legato\Pushaws\Models\Subscription::where('subscription_id', $model->id)->first();
                    $aws_topic->sns_status = Constant::SNS_STATUS_PENDING;
                    $aws_topic->error_reason = "";
                    $aws_topic->save();
                }
            });
        });

        Message::extend(function($model) {
            $model->addDynamicMethod('getPrettyExtraData', function() use ($model) {

                $result = array();

                if ($model->extra_data) {
                    foreach ($model->extra_data as $extra_datum){
                        $result[$extra_datum['extra_data_key']] = $extra_datum['extra_data_value'];
                    }
                }

                return $result;
            });
        });

        Topic::extend(function ($model) {
            $model->hasOne['aws_topic'] = ['Legato\Pushaws\Models\Topic'];

            $model->bindEvent('model.afterSave', function () use ($model) {
//                trace_log($model->aws_topic);
                if (is_null($model->aws_topic)) {
                    $model->aws_topic()->add(new \Legato\Pushaws\Models\Topic());
                } else {
                    $aws_topic = \Legato\Pushaws\Models\Topic::where('topic_id', $model->id)->first();
                    $aws_topic->sns_status = Constant::SNS_STATUS_PENDING;
                    $aws_topic->error_reason = "";
                    $aws_topic->save();
                }
            });
        });

        Complete_topic::extend(function ($model) {
            $model->hasOne['aws_complete_topic'] = ['Legato\Pushaws\Models\AWScomplete_topic'];
        });

        Complete_individual::extend(function ($model) {
            $model->hasOne['aws_complete_individual'] = ['Legato\Pushaws\Models\AWScomplete_individual'];
        });

//
//        Message::extendFormFields(function($form, $model, $context) {
//            if (!$model instanceof \Legato\Push\Models\Message) {
//                return;
//            }
//
//            $platformField = $form->getField('push_platform');
//            $test = (array)$platformField;
//            $currentOptions = (array)$platformField->options;
////            $platformField->options = $currentOptions;
//            trace_log($test['options']);
//        });


    }
}
