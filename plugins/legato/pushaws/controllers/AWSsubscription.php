<?php namespace Legato\Pushaws\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Legato\Pushaws\Models\Subscription;

class AWSsubscription extends Controller
{
    public $implement = [    ];
    
    public function __construct()
    {
        parent::__construct();
    }

    public function test()
    {
//        Subscription::changeNewPendingToProcessing();
        Subscription::cronDeleteSubscription();
        return Subscription::cronCreateSubscription();
    }
}
