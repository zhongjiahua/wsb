<?php namespace Legato\Pushaws\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Legato\Pushaws\Models\AWSprogress_individual;

class AWSprogress_topic extends Controller
{
    public $implement = [    ];
    
    public function __construct()
    {
        parent::__construct();
    }

    public function test(){
//        \Legato\Pushaws\Models\AWSprogress_topic::cronPublishMessageToTopic();
        AWSprogress_individual::cronPublishMessageToEndpoint();
    }
}
