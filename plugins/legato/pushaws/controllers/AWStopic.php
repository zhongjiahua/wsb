<?php namespace Legato\Pushaws\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Legato\Pushaws\Models\Topic;

class AWStopic extends Controller
{
    public $implement = [    ];
    
    public function __construct()
    {
        parent::__construct();
    }

    public function test()
    {
        Topic::cronCreateTopic();
        Topic::cronDeleteTopic();
    }
}
