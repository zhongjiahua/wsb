<?php namespace Legato\Pushaws\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePushAwsTopicTable extends Migration
{
    public function up()
    {
        Schema::create('legato_pushaws_topic', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('arn',300)->nollable();
            $table->unsignedInteger('topic_id');

            $table->smallInteger('sns_status')->default(1);
            $table->text('error_reason')->nollable();

            $table->timestamps();

           //$table->index('arn');
            $table->index('topic_id');
            $table->index('sns_status');

        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_pushaws_topic');
    }
}
