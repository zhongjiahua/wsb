<?php namespace Legato\Pushaws\Models;

use Legato\Push\classes\Constant;
use Model;
use Legato\Pushaws\classes\SNS;

/**
 * Model
 */
class Subscription extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_pushaws_subscription';

    public $belongsTo = [
        'subscription' => 'Legato\Push\Models\Subscription'
    ];


    public static function changeNewPendingToProcessing()
    {
        Subscription::whereHas('subscription', function ($query) {
            $query->where('status', Constant::PUSH_STATUS_ACTIVE);
        })->where('sns_status', \Legato\Pushaws\classes\Constant::SNS_STATUS_PENDING)
            ->update(['sns_status' => \Legato\Pushaws\classes\Constant::SNS_STATUS_PROGRESSING]);

    }

    public static function changeDeletedPendingToProcessing()
    {
        Subscription::whereHas('subscription', function ($query) {
            $query->where('status', Constant::PUSH_STATUS_INACTIVE);
        })->where('sns_status', \Legato\Pushaws\classes\Constant::SNS_STATUS_PENDING)
            ->update(['sns_status' => \Legato\Pushaws\classes\Constant::SNS_STATUS_PROGRESSING]);

    }

    public static function getNewProcessing()
    {
        $new_subscriptions = \Legato\Push\Models\Subscription::with('aws_subscription', 'device.endpoint', 'topic.aws_topic')
            ->whereHas('aws_subscription', function ($query) {
                $query->where('sns_status', \Legato\Pushaws\classes\Constant::SNS_STATUS_PROGRESSING);
            })->where('status', Constant::PUSH_STATUS_ACTIVE)->limit(2000)->get();
        return $new_subscriptions;
    }

    public static function getDeletedSubscriptions()
    {
        $deleted_subscriptions = \Legato\Push\Models\Subscription::with('aws_subscription', 'device.endpoint', 'topic.aws_topic')
            ->whereHas('aws_subscription', function ($query) {
                $query->where('sns_status', \Legato\Pushaws\classes\Constant::SNS_STATUS_PROGRESSING)->orWhere('sns_status', \Legato\Pushaws\classes\Constant::SNS_STATUS_COMPLETED);
            })->where('status', Constant::PUSH_STATUS_INACTIVE)->limit(2000)->get();


//        $deleted_subscriptions = \Legato\Push\Models\Subscription::where('status',Constant::PUSH_STATUS_INACTIVE)
//            ->with('aws_subscription')
//            ->whereHas('aws_subscription', function ($query) {
//                $query->where('sns_status', \Legato\Pushaws\classes\Constant::SNS_STATUS_COMPLETED);
//            })->get();


        return $deleted_subscriptions;
    }

    public static function cronCreateSubscription()
    {
//        trace_sql();
        $mySNS = new SNS();
        $mySNS -> initialize();

        Subscription::changeNewPendingToProcessing();
        $newSubscriptions = Subscription::getNewProcessing();

        foreach ($newSubscriptions as $newSubscription) {
            $awsSubscription = $newSubscription->aws_subscription;
            try {
                $device = $newSubscription->device;
                $topic = $newSubscription->topic;

                if ($device->status != Constant::PUSH_STATUS_ACTIVE || $topic->status != Constant::PUSH_STATUS_ACTIVE) {
                    trace_log('cronCreateSubscription1');
                    trace_log('$device->status'.$device->status);
                    trace_log('$topic->status'.$topic->status);
                    trace_log('Constant::PUSH_STATUS_ACTIVE'.Constant::PUSH_STATUS_ACTIVE);
                    $awsSubscription->error_reason = "Device or topic is not enabled";
                    $awsSubscription->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                    $awsSubscription->save();
                    continue;
                }

                if ($device->endpoint->sns_status == \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR || $topic->aws_topic->sns_status == \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR) {
                    $awsSubscription->error_reason = "Device or topic is in error status";
                    $awsSubscription->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                    $awsSubscription->save();
                    continue;
                }

                if ($device->endpoint->sns_status == \Legato\Pushaws\classes\Constant::SNS_STATUS_COMPLETED && $topic->aws_topic->sns_status == \Legato\Pushaws\classes\Constant::SNS_STATUS_COMPLETED) {
                    $subscriptionArn = $mySNS->subscribe($topic->aws_topic->arn, $device->endpoint->arn);
                } else {
                    /**
                     * We skip it first
                     * Do again next time
                     */
                    continue;
                }

                if ($subscriptionArn) {
                    $awsSubscription->arn = $subscriptionArn;
                    $awsSubscription->error_reason = "";
                    $awsSubscription->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_COMPLETED;
                } else {
                    $awsSubscription->error_reason = "Subscription cannot be created. Please check.";
                    $awsSubscription->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                }


            } catch (\Exception $e) {
                $awsSubscription->error_reason = "Unknown Error";
                $awsSubscription->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
            }
            Subscription::where('id', $awsSubscription->id)
                ->where('sns_status', \Legato\Pushaws\classes\Constant::SNS_STATUS_PROGRESSING)
                ->update(['arn' => $awsSubscription->arn, 'error_reason' => $awsSubscription->error_reason, 'sns_status' => $awsSubscription->sns_status]);

        }

    }

    public static function cronDeleteSubscription()
    {
        $mySNS = new SNS();
        $mySNS -> initialize();

        Subscription::changeDeletedPendingToProcessing();
        $deletedSubscriptions = Subscription::getDeletedSubscriptions();

        foreach ($deletedSubscriptions as $deletedSubscription) {
            $awsSubscription = $deletedSubscription->aws_subscription;
//            $awsTopic->topic_id = $newTopic->id;


            if (strlen($awsSubscription->arn) <10){
                Subscription::find($awsSubscription->id)->delete();

            }

            try {
                $result = $mySNS->unSubscribe($awsSubscription->arn);

                if ($result)
                {
                    Subscription::find($awsSubscription->id)->delete();
                }else{
                    $awsSubscription->error_reason = "Subscription cannot be delete. Please check";
                    $awsSubscription->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                    $awsSubscription->save();
                    continue;
                }

            }
            catch (\Exception $e) {
//                trace_log($e);
                $awsSubscription->error_reason = "Unknown Error";
                $awsSubscription->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                $awsSubscription->save();
                continue;
            }


        }

    }


}
