<?php namespace Legato\Pushaws\Models;

use Legato\Push\Models\Device;
use Model;
use Legato\Push\classes\Constant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Legato\Pushaws\classes\SNS;
use Illuminate\Support\Facades\Log;
/**
 * Model
 */
class Endpoint extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $fillable = ['arn','sns_status','error_reason'];
    /**
     * @var array Validation rules
     */
    public $rules = [
//        'device_id' => 'unique:legato_pushaws_endpoint'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_pushaws_endpoint';

    public $belongsTo = [
//        'device' => 'Legato\Push\Models\Device'
    ];



    /**
     * Function to get new device without enabled SNS endpoint
     * @return array
     */
    public static function getNewToken() {

        $new_tokens = Device::whereNull('deleted_at') //Do not get deleted device
                    ->where('legato_push_device.status',Constant::PUSH_STATUS_ACTIVE) //Device must be active
                    ->whereHas('endpoint', function ($query){
                        $query->where('sns_status',\Legato\Pushaws\classes\Constant::SNS_STATUS_PENDING);
                    })
                    ->take(2000)
                    ->get();

        return $new_tokens;


//        $new_tokens = Db::table('legato_push_device')
//                    ->select('legato_push_device.*')
//                    ->leftjoin('legato_pushaws_endpoint','legato_push_device.id', '=' ,'legato_pushaws_endpoint.device_id')
//                    ->where('legato_pushaws_endpoint.sns_status',\Legato\Pushaws\classes\Constant::SNS_STATUS_PENDING) // Device is not exist in endpoint table
//                    ->whereNull('deleted_at') //Do not get deleted device
//                    ->where('legato_push_device.status',Constant::PUSH_STATUS_ACTIVE) //Device must be active
//                    ->take(2000)
//                    ->get();
//        return $new_tokens;
    }

    public static function cronCreateEndpointInAWS()
    {
//        Log::info("=============== Task Start ================");

        $mySNS = new SNS();
        $mySNS -> initialize();

        $error = false;
        $platformArn_iOS_dev                = Config::get("legato.pushaws::AWS_DEV_IOS_APPLICATION_ARN");
        $platformArn_Android_dev            = Config::get("legato.pushaws::AWS_DEV_ANDROID_APPLICATION_ARN");

        $platformArn_iOS_inhouse_pro        = Config::get("legato.pushaws::AWS_INHOUSE_PRO_IOS_APPLICATION_ARN");
        $platformArn_Android_inhouse_pro    = Config::get("legato.pushaws::AWS_INHOUSE_PRO_ANDROID_APPLICATION_ARN");

        $platformArn_iOS_pro                = Config::get("legato.pushaws::AWS_PRO_IOS_APPLICATION_ARN");
        $platformArn_Android_pro            = Config::get("legato.pushaws::AWS_PRO_ANDROID_APPLICATION_ARN");


        $newTokens = Endpoint::getNewToken();

        foreach ($newTokens as $newToken)
        {
            $endpoint = Endpoint::where('device_id',$newToken->id)->first();

            $platformArn = null;

            if ($newToken->token_type == Constant::TOKEN_TYPE_DEV)
            {
                if ($newToken->os_type == Constant::OS_TYPE_IOS)
                {
                    $platformArn = $platformArn_iOS_dev[$newToken->platform_arn_position];
                }
                else if ($newToken->os_type == Constant::OS_TYPE_ANDROID)
                {
                    $platformArn = $platformArn_Android_dev[$newToken->platform_arn_position];
                }
                else
                {
                    $error = true;
                }
            }
            else if ($newToken->token_type == Constant::TOKEN_TYPE_INHOUSE_PRO)
            {
                if ($newToken->os_type == Constant::OS_TYPE_IOS)
                {
                    $platformArn = $platformArn_iOS_inhouse_pro[$newToken->platform_arn_position];
                }
                else if ($newToken->os_type == Constant::OS_TYPE_ANDROID)
                {
                    $platformArn = $platformArn_Android_inhouse_pro[$newToken->platform_arn_position];
                }
                else
                {
                    $error = true;
                }
            }
            else if ($newToken->token_type == Constant::TOKEN_TYPE_PRO)
            {
                if ($newToken->os_type == Constant::OS_TYPE_IOS)
                {
                    $platformArn = $platformArn_iOS_pro[$newToken->platform_arn_position];
                }
                else if ($newToken->os_type == Constant::OS_TYPE_ANDROID)
                {
                    $platformArn = $platformArn_Android_pro[$newToken->platform_arn_position];
                }
                else
                {
                    $error = true;
                }
            }
            else
            {
                $error = true;
            }

            /*******
             * Protection
             */

            if (is_null($platformArn) || $error)
            {
                $endpoint->error_reason = "Please check device token type OR OS type.";
                $endpoint->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                $endpoint->save();
                continue;
            }


            try {

                $endPointArn = $mySNS->createEndPoint($newToken->token, $platformArn);
                if (!$endPointArn) {
                    $mySNS->enableEndPoint($newToken->token, $endpoint->arn);
                }
            }
            catch (\Exception $e) {
//                trace_log($e);
                $endpoint->error_reason = "Unknown Error";
                $endpoint->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                $endpoint->save();
                continue;
            }

            if ($endPointArn)
            {
                $endpoint->arn = $endPointArn;
                $endpoint->error_reason = "";
                $endpoint->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_COMPLETED;
                $endpoint->save();
            }
            else{
                $endpoint->error_reason = "Cannot get AWS Endpoint ARN";
                $endpoint->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                $endpoint->save();
            }

        }
    }

}
