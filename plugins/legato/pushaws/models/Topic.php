<?php namespace Legato\Pushaws\Models;

use Model;
use Legato\Pushaws\classes\SNS;

/**
 * Model
 */
class Topic extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $fillable = ['arn','sns_status','error_reason'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_pushaws_topic';



    public static function getNewTopic()
    {
        $new_topics = \Legato\Push\Models\Topic::where('status',1)
                                ->has('aws_topic','>=',0)->get();
        return $new_topics;
    }

    public static function getDeletedTopic()
    {
        $deleted_topics = \Legato\Push\Models\Topic::where('status',0)
            ->has('aws_topic','>=',1)->get();

        return $deleted_topics;
    }



    public static function cronCreateTopic()
    {
        $mySNS = new SNS();
        $mySNS -> initialize();

        $newTopics = Topic::getNewTopic();

        foreach ($newTopics as $newTopic) {
            $awsTopic = $newTopic->aws_topic()->first();

            try {
                $topicArn = $mySNS->createTopic($newTopic->name);

                if ($topicArn)
                {
                    $awsTopic->arn = $topicArn;
                    $awsTopic->error_reason = "";
                    $awsTopic->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_COMPLETED;
                    $awsTopic->save();
                }else{
                    $awsTopic->error_reason = "Please check the name of topic. (Up to 256 alphanumeric characters, hypthens(-),and underscores(_) are allowed)";
                    $awsTopic->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                    $awsTopic->save();
//                    continue;
                }

            }
            catch (\Exception $e) {
//                trace_log($e);
                $awsTopic->error_reason = "Unknown Error";
                $awsTopic->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                $awsTopic->save();
//                continue;
            }

//            $newTopic->aws_topic()->add($awsTopic);

        }

    }

    public static function cronDeleteTopic()
    {
        $mySNS = new SNS();
        $mySNS -> initialize();

        $deletedTopics = Topic::getDeletedTopic();

        foreach ($deletedTopics as $deletedTopic) {
            $awsTopic = $deletedTopic->aws_topic()->first();
//            $awsTopic->topic_id = $newTopic->id;

            try {
                $result = $mySNS->deleteTopic($awsTopic->arn);

                if ($result)
                {
                    $awsTopic->delete();
                }else{
                    $awsTopic->error_reason = "Topic cannot be delete. Please check";
                    $awsTopic->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                    $awsTopic->save();
                    continue;
                }

            }
            catch (\Exception $e) {
//                trace_log($e);
                $awsTopic->error_reason = "Unknown Error";
                $awsTopic->sns_status = \Legato\Pushaws\classes\Constant::SNS_STATUS_ERROR;
                $awsTopic->save();
                continue;
            }


        }

    }
}
