<?php
/**
 * Created by PhpStorm.
 * User: ryanng
 * Date: 11/4/2018
 * Time: 3:40 PM
 */

namespace Legato\Pushaws\Models;

use Carbon\Carbon;
use Legato\Push\Models\Progress_topic;
use Legato\Push\Models\PushLocale;
use Legato\Pushaws\classes\Constant;
use Legato\Pushaws\classes\SNS;
use Illuminate\Support\Facades\Config;

class AWSprogress_topic extends Progress_topic
{
    public static function changePendingToProcessing($array)
    {
//        Db:
//        select('select legato_push_progress_topic progress
//                            LEFT JOIN legato_push_message message ON progress.message_id = message.id
//                            LEFT JOIN legato_push_topic topic ON progress.topic_id = topic.id
//                            LEFT JOIN legato_pushaws_topic aws_topic ON topic.id = aws_topic.topic_id
//                            WHERE message.status = ?
//                            AND message.schedule_time <= now()
//                            AND topic.status = ?
//                            AND aws_topic.sns_status =?',[ \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE, \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE, Constant::SNS_STATUS_COMPLETED]);

        Progress_topic::whereIn('id',$array)
                        ->update(['status' => Constant::SNS_STATUS_PROGRESSING]);
    }

    public static function getPendingMessage()
    {

        return Progress_topic::with('message', 'topic', 'topic.aws_topic')
            ->where('status', Constant::SNS_STATUS_PENDING)->whereHas('message', function ($query) {
                $query->where('status', \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE)
                    ->where('push_platform',\Legato\Push\classes\Constant::PLATFORM_AWS)
                    ->where('schedule_time', '<=', Carbon::now());
            })->whereHas('topic', function ($query) {
                $query->where('status', \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE);
            })->limit(2000)->lockForUpdate()->get();

//        Progress_topic::with('message', 'topic', 'topic.aws_topic')
//            ->where('sns_status', Constant::SNS_STATUS_PROGRESSING)->whereHas('message', function ($query) {
//                $query->where('status', \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE);
//            })->whereHas('topic', function ($query) {
//                $query->where('status', \Legato\Push\classes\Constant::PUSH_STATUS_ACTIVE);
//            })->limit(2000)->lockForUpdate()->get();
    }

    public static function getIdArrFromObjArr($objArr)
    {
        $idArr = array();
        foreach ($objArr as $item)
        {
            $idArr[]=$item->id;
        }
        return $idArr;
    }

    public static function cronPublishMessageToTopic()
    {
        $mySNS = new SNS();
        $mySNS -> initialize();

        $processingMsgs = AWSprogress_topic::getPendingMessage();
        AWSprogress_topic::changePendingToProcessing(AWSprogress_topic::getIdArrFromObjArr($processingMsgs));

        foreach ($processingMsgs as $processingMsg) {
            $aws_complete_topic = new AWScomplete_topic();
            $msg = $processingMsg->message;
            $topic = $processingMsg->topic;
            $aws_topic = $topic->aws_topic;
            $awsMessageArn = false;
            $isError = false;


            switch ($aws_topic->sns_status){
                case Constant::SNS_STATUS_PROGRESSING:
                case Constant::SNS_STATUS_PENDING:
                    $processingMsg->status = Constant::SNS_STATUS_PENDING;
                    $processingMsg->save();
                    continue;
                    break;

                case Constant::SNS_STATUS_ERROR:
                    $isError = true;
                    break;

                default:
                    break;
            }


            if (!$isError) {
                if (Config::get("legato.pushaws::TESTING_MODE")) {
                    $awsMessageArn = "Testing Mode is enabled";
                    $processingMsg->status = \Legato\Push\classes\Constant::PUSH_STATUS_INACTIVE;
                } else {

                    if ($topic->language != 0)
                    {
                        /**
                         * Have language
                         */
                        $locale = PushLocale::findById($topic->language);
                        $msg->translateContext($locale->code);
                    }


                    $awsMessageArn = $mySNS->sendPushByArg($aws_topic->arn, $msg->title, $msg->body, $msg->content_available, $msg->ttl, $msg->badge, $msg->sound, $msg->mutable_content, $msg->attachment_url, $msg->getPrettyExtraData(), $msg->category);
                }
            }else{
                $processingMsg->status = \Legato\Push\classes\Constant::PUSH_STATUS_INACTIVE;
            }


            $completeMsg = $processingMsg->moveToComplete();

            if ($awsMessageArn) {
                $aws_complete_topic->arn = $awsMessageArn;
                $aws_complete_topic->sns_status = Constant::SNS_STATUS_COMPLETED;
            } else {
                $aws_complete_topic->error_reason = "Cannot sent out the message, Please check";
                $aws_complete_topic->sns_status = Constant::SNS_STATUS_ERROR;
            }

            $completeMsg->aws_complete_topic()->add($aws_complete_topic);


        }



    }
}