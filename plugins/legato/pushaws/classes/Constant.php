<?php namespace Legato\Pushaws\classes;

/**
 * Created by PhpStorm.
 * User: ryanng
 * Date: 22/3/2018
 * Time: 1:09 PM
 */

class Constant
{
    const AWS_PUSH_LOG_PREFIX            = "[AWS Push plugin]:";

    const SNS_STATUS_PENDING        = 1;
    const SNS_STATUS_PROGRESSING    = 2;
    const SNS_STATUS_COMPLETED      = 3;
    const SNS_STATUS_ERROR          = 4;

}