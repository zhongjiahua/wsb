<?php namespace Legato\Pushaws\classes;

use Aws\Sns\SnsClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;


class SNS {
	
	
	private $client       = NULL;

    function __construct($config = array())
	{
		if ( ! empty($config))
		{
			$this->initialize($config);
		}
		else{

            $access_key = Config::get('legato.pushaws::AWS_SNS_ACCESS');
            $secret_key = Config::get('legato.pushaws::AWS_SNS_SECRET');
            $region     = Config::get('legato.pushaws::AWS_SNS_REGION');

            $this->initialize(array('access_key' => $access_key , 'secret_key' =>$secret_key , 'region' => $region));
        }

	}

	// --------------------------------------------------------------------

	/**
	 * Initialize preferences
	 *
	 * @access	public
	 * @param	array Config. Need to contain access key , secret key , region
	 * @return	void
	 */
	function initialize($config = array())
	{
        if ( empty($config))
        {
            $access_key = Config::get('legato.pushaws::AWS_SNS_ACCESS');
            $secret_key = Config::get('legato.pushaws::AWS_SNS_SECRET');
            $region     = Config::get('legato.pushaws::AWS_SNS_REGION');

        }

		extract($config);

		if ( ! empty($access_key) AND ! empty($secret_key) AND ! empty($region))
		{
			$this->client = new SnsClient([
    		'region'   => $region,
    		'version' => '2010-03-31',
    		'credentials' => [
       			'key'      => $access_key,
        		'secret'   => $secret_key
    		]
    	]);
		
		}
		else
			{
//				Log::error('SNS Class Initialized Empty');
			}

	}

//	function createAndroidApplication($name="",$apiKey="")
//	{
//		try{
//
//			if (strlen($name) == 0 || strlen($apiKey) ==0)
//			{
//				return false;
//			}
//
//
//			$result = $this->_sns->createPlatformApplication(array(
//  					'Name'       => $name,
//  					'Platform'   => 'GCM',
//  					'Attributes' => array('PlatformCredential' => $apiKey),
//					));
//
//			$result['PlatformApplicationArn'];
//		}
//		catch (\Exception $e)
//		{
//			return false;
//		}
//	}

//	function createiOSApplication($isPro=true , $name="",$key="", $cert="")
//	{
//
//		try{
//			if (strlen($name) == 0 || strlen($key) ==0 || strlen($cert) ==0)
//			{
//				return false;
//			}
//
//			$platform = ($isPro) ? 'APNS' : 'APNS_SANDBOX';
//
//
//			$result = $this->_sns->createPlatformApplication(array(
//    				'Name'       => $name,
//    				'Platform'   => $platform,
//    				'Attributes' => array(
//      					'PlatformCredential' => $key,
//      					'PlatformPrincipal'  => $cert,
//   						),
//  					));
//
//			$result['PlatformApplicationArn'];
//		}
//		catch (\Exception $e)
//		{
//			return false;
//		}
//	}
	
	/**
	 * Create SNS Topic
	 *
	 * @param string $topicName Topic name
	 * @return string TopicArn / false
	 */
	
	function createTopic($topicName)
	{
		try {

            if (!empty($topicName)) {

                $result = $this->client->createTopic(array(
                    // Name is required
                    'Name' => $topicName,
                ));

                return $result['TopicArn'];

            } else {
//                Log::warning('Topic name empty');
                return false;

            }
        }
		catch (\Exception $e)
		{
//            trace_log($e);
			return false;
		}
	}
	
	/**
	 * Delete SNS Topic
	 *
	 * @param string $topicArn TopicArn
	 * @return string TopicArn / false
	 */
	
	function deleteTopic($topicArn)
	{
		try{
		
		if (!empty($topicArn))
		{
			$result = $this->client->deleteTopic(array(
    		// TopicArn is required
    		'TopicArn' => $topicArn,
			));
			
			if ($result->get('@metadata')['statusCode'] == '200')
			{
				return true;
			}
			else 
			{
				return false;		
			}
			
		}
		else {
//            Log::warning('Topic Arn empty');
			return false;
		}
		}
		catch (\Exception $e)
		{
//            trace_log($e);
			return false;
			
		}
	}
	
	/**
	 * Create endpoint
	 *
	 * @param string $token Push token
	 * @param string $platformArn Platform ARN (iOS / GCM)
	 * @return string TopicArn / false
	 */
	
	function createEndPoint($token,$platformArn)
	{
		try{
		$result = $this->client->createPlatformEndpoint(array(
    								// PlatformApplicationArn is required
   									 'PlatformApplicationArn' => $platformArn,
    								// Token is required
    								'Token' => $token,
    								//'CustomUserData' => ''
								));
								
		 return $result['EndpointArn'];
		}
		catch (\Exception $e)
		{
//            trace_log($e);
			return false;
		}
	}
	
	function enableEndPoint($token,$arn)
	{
		try{
			 $result = $this->client->setEndpointAttributes(array(
										// PlatformApplicationArn is required
										 'EndpointArn' => $arn,
										'Attributes' => array(
											'Token' => $token,
        									'Enabled' => 'true'
										),
									));
			 return true;
		}
		catch (\Exception $e)
		{
//            trace_log($e);
			return false;
		}
	}
	
	
	/**
	 * Delete endpoint
	 *
	 * @param string $endPointArn Endpoint ARN
	 * @return string Endpoint ARN / false
	 */
	
	function deleteEndPoint($endPointArn)
	{
		try{
			$result = $this->client->deleteEndpoint(array(
    		// EndpointArn is required
    		'EndpointArn' => $endPointArn,
			));
			//APILog('SNS', 'delete endpoint : Result: '.$result);
			//APILog('SNS', 'delete endpoint : Result: '.$result->get('@metadata'));
			//APILog('SNS', 'delete endpoint : Result: '.$result->get('@metadata')['statusCode']);
			if ($result->get('@metadata')['statusCode'] == '200')
			{
				return true;
			}
			else 
			{
				return false;		
			}
			//return $result['EndpointArn'];
		}
		catch(\Exception $e)
		{
//            trace_log($e);
			return false;
			
		}
	}
	
	/**
	 * Subscribe endpoint to topic
	 *
	 * @param string $topicArn Topic ARN
	 * @param string $endPointArn Endpoint ARN
	 * @return string Subscription ARN / false
	 */
	
	function subscribe($topicArn, $endPointArn)
	{
		try{
			$result = $this->client->subscribe(array(
    			// TopicArn is required
    			'TopicArn' => $topicArn,
    			// Protocol is required
    			'Protocol' => 'application',
    			'Endpoint' => $endPointArn,
				));
			
			return $result['SubscriptionArn'];
		}
		catch(\Exception $e)
		{
//            trace_log($e);
			return false;
			
		}
		
	}
	
	/**
	 * Unsubscribe endpoint to topic
	 *
	 * @param string $subArn subscription ARN
	 * @return string Subscription ARN / false
	 */
	
	function unSubscribe($subArn)
	{
		try{
		
			$result = $this->client->unsubscribe(array(
    		// SubscriptionArn is required
    		'SubscriptionArn' => $subArn,
			));
			
			if ($result->get('@metadata')['statusCode'] == '200')
			{
				return true;
			}
			else 
			{
				return false;		
			}
		}
		catch (\Exception $e)
		{
//            trace_log($e);
			return false;
			
		}
	}



	function sendPushByArg($targetArn="" , $title ="",$body="" ,$content_available=0,$ttl=0, $badge=0 , $sound="" , $mutable_content=0, $attachment_url="", $extra_arr=array(),$category="")
	{
		//============================================================== iOS Format Start ========================================================
		$apnsArr = array();
		if ($content_available !=1)
		{
			/**
			 * Normal push
			 */
			
			$pushSound = "default";
			if (strlen($sound) >0)
				$pushSound = $sound;

			$alert = array(
            				'title' => $title,
            				'body' => $body
            			);
			$apnsArr['aps'] = array(
            				'alert' => $alert,
            				'badge' => (int)$badge,
            				'sound' => $pushSound
            			);

			//$apnsArr['badge'] = (int)$badge;
		}
		else
		{
			/**
			 * Silent push
			 */
			$apnsArr['aps'] = array(
            				'content-available' => 1
            			);
		}

		/**
		 * Add Rich content
		 */
		$apnsArr['aps']['mutable-content'] =(int)$mutable_content;
		$apnsArr['attachment_url'] = $attachment_url;

		/**
		 * Add category for custom action
		 */
		$apnsArr['aps']['category'] = $category;
		//============================================================== iOS Format End ========================================================


		//============================================================== Android Format Start ========================================================
		$gcmArr =array(
 			'data' => array(
 				'message' => array('title' => $title , 'body' => $body),
 				'content-available' => $content_available,
 				'badge'	=> $badge,
 				'sound'	=> $sound,
 				'mutable-content' => (int)$mutable_content,
 				'attachment_url' => $attachment_url
			)
			);
		//============================================================== Android Format End ========================================================



		/**
		 * Add Custom key & value
		 */
		foreach ($extra_arr as $key => $value) {
			$apnsArr[$key] = (string) $value;
			$gcmArr['data'][$key] = (string) $value;
		}

		return $this->sendPush($targetArn,$apnsArr,$gcmArr,$ttl);

	}
	
	/**
	 * Send Push
	 *
	 * @param string $topicArn TopicArn
	 * @param array $apnsArr iOS push content
	 * Example: array(
            		'aps' => array(
                		'alert' => 'Message'
            		),
            		// Custom payload parameters can go here
            		'Custom_field1' => 'Custom_value1',
           			'Custom_field2' => 'Custom_value2'
       	 			)
	 * @param array $gcmArr Android push content
	 * Example: array(
       	 			'data' => array(
       	 				'message' => 'Custom Message',
       	 				'CustomField1'	=> 'CustomValue1',
       	 				'CustomField2'	=> 'CustomValue2'		
					)
					)
	 * @return string TopicArn / error
	 */
	
	function sendPush($targetArn,$apnsArr,$gcmArr,$ttl)
	{

		try{

		    $payLord = array(
                'TargetArn' => $targetArn,
                'MessageStructure' => 'json',
                'Message' => json_encode(array(
                    'default' => 'test',
                    'APNS' => json_encode($apnsArr),
                    'APNS_SANDBOX' => json_encode($apnsArr),
                    'GCM' => json_encode($gcmArr)
                )),
                'MessageAttributes'=> array(
                    'AWS.SNS.MOBILE.GCM.TTL'=> array(
                        'DataType' => 'String',
                        'StringValue' => $ttl,
                    ),
                    'AWS.SNS.MOBILE.APNS.TTL'=> array(
                        'DataType' => 'String',
                        'StringValue' => $ttl,
                    )
                )
            );

		    trace_log($payLord);

			$result = $this->client->publish($payLord);
				
			$msgID = $result['MessageId'];
			
			return $msgID;
			
		}
		catch (\Exception $e)
		{
//            trace_log($e);
			return false;
		}
	}

}
