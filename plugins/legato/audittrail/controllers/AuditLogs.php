<?php namespace Legato\AuditTrail\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;
use Legato\AuditTrail\Models\AuditLog;


/**
 * Audit Logs Back-end Controller
 */
class AuditLogs extends \CommonController
{
	public $requiredPermissions = ['legato.audittrail.audittrail'];

    public function __construct()
    {
        parent::__construct();
        //BackendMenu::setContext('Legato.AuditTrail', 'audittrail', 'auditlogs');
		//$this -> _vars();
        BackendMenu::setContext('October.System', 'system', 'users');
        SettingsManager::setContext('Legato.AuditTrail', 'AuditTrail');
        $this -> vars['hide_export'] = false;
        $this -> vars['hide_create'] = true;

        $this -> vars['export_column'] = ['section','model','record_id','action','backend_users_id','created_at'];
    }
    public function _bulks_list (){}

    public function clean($days){
        AuditLog::cleanOldLogsByDays($days);


    }

}
