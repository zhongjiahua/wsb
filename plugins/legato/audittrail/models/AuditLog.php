<?php namespace Legato\AuditTrail\Models;

use Model;
use Common\Models\CommonModel;
use Common\Helpers\Functions;
use Backend\Models\User as User;
/**
 * AuditLog Model
 */
class AuditLog extends CommonModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_audittrail_audit_logs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

	public $belongsTo = [
		'foreign_key_backend_users' => [
			'Backend\Models\User',
			'key' => 'backend_users_id'
		]
	];

    public function getSectionOptions(){
        $collections = self::groupBy('section')->get(['section']);
        $res = [];
        foreach ($collections as $key => $model) {
            $res[$model->section] = $model->section; 
        }
        return $res;
    }

    public function getModelOptions(){
        $collections = self::groupBy('model')->get(['model']);
        $res = [];
        foreach ($collections as $key => $model) {
            $res[$model->model] = $model->model; 
        }
        return $res;
    }

    public function getActionOptions(){
        $res = [
            'Created' => 'Created',
            'Updated' => 'Updated',
            'Deleted' => 'Deleted',
        ];
        return $res;
    }

    /**
     * Remove Old Logs
     */
    public static function cleanOldLogsByDays($days){
        $target_date = \Carbon\Carbon::now();
        $target_date->subDays($days);
        $logs = self::where('created_at','<',$target_date)->get();
        foreach ($logs as $key => $log) {
            $log->delete();
        }
    }   

}
