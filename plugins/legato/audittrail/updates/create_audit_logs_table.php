<?php namespace Legato\AuditTrail\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAuditLogsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_audittrail_audit_logs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->string('section', 255);
			$table->string('model', 255);
			$table->integer('record_id')->unsigned();
			$table->string('action', 255);
			$table->integer('backend_users_id')->unsigned();;
            $table->timestamps();
			$table->softDeletes();

            $table->index('section');
            $table->index('model');
            $table->index('action');
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_audittrail_audit_logs');
    }
}
