<?php namespace Legato\AuditTrail\Traits;

use Legato\AuditTrail\Models\AuditLog as AuditLog;
use Carbon\Carbon;

trait Auditable
{

    public static function bootAuditable()
    {

		static::created(function ($model) {

			$backend_users_id = self::getBackendUsersId();
			$section = self::getSection($model);
			try{
				$audit_log = AuditLog::insert([
	                'record_id' => $model->id,
					'section' => $section,
					'model' => get_class($model),
					'action' => 'Created',
					'backend_users_id' => $backend_users_id,
					'created_at' => Carbon::now()
	            ]);
			}catch(\Exception $e){
				\Log::debug($e);
			}
		});

		static::updated(function ($model) {

			$backend_users_id = self::getBackendUsersId();
			$section = self::getSection($model);
			try{
				if($model->status != \Common\Classes\StatusService::STATUS_REMOVED){
					$audit_log = AuditLog::insert([
						'record_id' => $model->id,
						'section' => $section,
						'model' => get_class($model),
						'action' => 'Updated',
						'backend_users_id' => $backend_users_id,
						'created_at' => Carbon::now()
		            ]);
				}
			}catch(\Exception $e){
				\Log::debug($e);
			}
		});

		static::deleted(function ($model) {

			$backend_users_id = self::getBackendUsersId();
			$section = self::getSection($model);
			try{
				$audit_log = AuditLog::insert([
					'record_id' => $model->id,
					'section' => $section,
					'model' => get_class($model),
					'action' => 'Deleted',
					'backend_users_id' => $backend_users_id,
					'created_at' => Carbon::now()
	            ]);
			}catch(\Exception $e){
				\Log::debug($e);
			}
		});

		static::saved(function ($model) {

			$backend_users_id = self::getBackendUsersId();
			$section = self::getSection($model);

			if((($model->created_at) != date('Y-m-d H:i:s')) && (($model->updated_at) != date('Y-m-d H:i:s')))
			{
				try{

					if($model->status != \Common\Classes\StatusService::STATUS_REMOVED){
						$audit_log = AuditLog::insert([
							'record_id' => $model->id,
							'section' => $section,
							'model' => get_class($model),
							'action' => 'Updated',
							'backend_users_id' => $backend_users_id,
							'created_at' => Carbon::now()
			            ]);
			        }
				}catch(\Exception $e){
					\Log::debug($e);
				}
			}
		});

		static::extend(function($model) {
            $model->bindEvent('model.beforeCreate', function () use ($model) {
            	try{
			        $user_id = self::getBackendUsersId();
			        if($user_id)
			            $model -> created_by = $user_id;
		        }catch(\Exception $e){
				}
            });
        });

        static::extend(function($model) {
            $model->bindEvent('model.beforeSave', function () use ($model) {
            	try{
			        $user_id = self::getBackendUsersId();
			        if($user_id)
			            $model -> updated_by = $user_id;
			    }catch(\Exception $e){
				}
            });
        });

    }

	public static function getSection($model)
	{
		$section = '';
		$class = get_class($model);
		if($class)
		{
			$array = explode("\\", $class);
			if(count($array) == 4)
			{
				$section = $array[1].' - '.$array[3];
			}
		}
		return $section;
	}

	public static function getBackendUsersId()
    {
        $backend_user = \BackendAuth::getUser();
        if ($backend_user) {
        	$backend_users_id =  $backend_user->id;
      	} else {
        	$backend_users_id = 0;
      	}
        return $backend_users_id;
    }
}

?>
