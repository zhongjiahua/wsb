<?php namespace Legato\AuditTrail;

use Backend;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Legato\AuditTrail\Models\AuditLog;

/**
 * AuditTrail Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'AuditTrail',
            'description' => 'No description provided yet...',
            'author'      => 'Legato',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Legato\AuditTrail\Components\MyComponent' => 'myComponent',
        ];
    }

    public function registerSettings()
    {
        return [
            'AuditTrail' => [
                'label'       => 'AuditTrail',
                'description' => 'Audit Trail',
                'category'    => SettingsManager::CATEGORY_LOGS,
                'icon'        => 'icon-book',
                'url'         => Backend::url('legato/audittrail/auditlogs'),
                'permissions' => ['legato.audittrail.*'],
                //'permissions' => ['echome.flashsales.flashsales_notification_remarks']
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'legato.audittrail.audittrail' => [
                'tab' => 'AuditTrail',
                'label' => 'AuditTrail Management'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        #return [
        #    'audittrail' => [
        #        'label'       => 'AuditTrail',
        #        'url'         => Backend::url('legato/audittrail/auditlogs'),
        #        'icon'        => 'icon-leaf',
        #        'permissions' => ['legato.audittrail.*'],
        #        'order'       => 900,
        #    ],
        #];
    }


    public function registerSchedule($schedule)
    {   
        /**
         * Remove Auidt log 6 month before
         */
        $schedule->call(function () {
            try{
                AuditLog::cleanOldLogsByDays(180);
            }catch(\Exception $e){
                \Log::debug('remove_old_audit_log fail');
                \Log::debug($e);
            }
          
        })->dailyAt('01:00')->name('remove_old_audit_log')->withoutOverlapping();
    }
}
