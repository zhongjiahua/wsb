<?php namespace Legato\Api\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Response;
use October\Rain\Exception\AjaxException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Carbon\Carbon;

class ApiLogMiddleware
{
    use \Legato\Api\Traits\UtilityFunctions;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if(env('API_LOG', TRUE) == false){
            return $next($request);
        }
        $api_start_time         = Carbon::now();
        $api_start_microtime    = microtime(true);
        
        if(env('API_QUERY_LOG', TRUE)){
            \Db::connection()->enableQueryLog();
        }

        $response =  $next($request);
        if($response->exception){
            $result = '';
            $exception = $response->exception;
            $status_code = $response->getStatusCode();
        }else{
            $result = $response;
            $exception = null;
            $status_code = $response->getStatusCode();
        }
        $binded = [];
        if(env('API_QUERY_LOG', TRUE)){
            $query_log = \DB::getQueryLog();
            \Db::connection()->disableQueryLog();
            foreach ($query_log as $key => $item) {
                $binded[$key]['query'] = $item['query'];
                $binded[$key]['bindings'] = json_encode($item['bindings']);
                $binded[$key]['time'] = $item['time'];
                /*$sql = str_replace('?', '%s', $item['query']);
                $bindings = [];
                foreach ($item['bindings'] as $key => $value) {
                    $bindings[] = '"'.$value.'"';
                }
                $sql = sprintf($sql, ...$bindings);
                $query_log[$key]['query'] = $sql;

                $binded = \Illuminate\Support\Str::replaceArray('?', $item['bindings'], $item['query']);
                $query_log[$key]['query'] = $binded;*/
            }
        }

        $user = $request->getUser();
        if ($user == \Config::get('legato.api::username', FALSE)){
            $user_id = 0;//guest
        } else {
            $user_id = $request->getUser();
        }
        $params = $request->toArray();
        unset($params['password']);
        /**
         * @todo add optional screening parameter
         */


        $api_end_time               =   Carbon::now();
        $api_end_microtime          =   microtime(true);

        $api_log                    =   new \Legato\Api\Models\ApiLog;
        $api_log->uri               =   $request->path();
        $api_log->method            =   $request->method();
        $api_log->params            =   json_encode($params);
        $api_log->ip_address        =   $_SERVER['REMOTE_ADDR'];
        $api_log->result            =   $result;
        $api_log->exception         =   $exception;
        $api_log->request_time      =   $api_start_time->toDateTimeString();
        $api_log->end_time          =   $api_end_time->toDateTimeString();
        $api_log->rtime             =   $api_end_microtime - $api_start_microtime;
        $api_log->response_code     =   $status_code;
        $api_log->authorized        =   $user_id;
        $api_log->user_agent        =   $_SERVER['HTTP_USER_AGENT'];
        $api_log->query_log         =   $binded;
        $api_log->save();

        if($response->exception){
            /**
             * @todo email alert
             */
            if(!env('API_DEBUG', false))
                return $this->response(\Lang::get('legato.api::lang.api.internal_server_error',['log_id'=>$api_log->ref_code]), 500);
        }
        return $response;

    }

    /**
     * Check if api version correct
     * 
     * @param string $ver
     * @return boolean
     */
    private function checkApiVersion($ver){
        $required_version = \Config::get('legato.api::version', 'V1');
        if($ver == $required_version){
            return true;
        } else {
            return false;
        }
    }
}