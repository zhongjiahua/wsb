<?php namespace Legato\Api\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Validation\Rule;

class MultiLangMiddleware
{
    use \Legato\Api\Traits\UtilityFunctions;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $lang_code = $request->lang;
        if($lang_code){
            $config_langs = \Config::get('legato.api::supported_lang');
            if(count($config_langs)){
                $rules = [
                    'lang' => [ 'required', Rule::in($config_langs)]
                ];
            }else{
                $rules = [
                    'lang' => 'required|exists:rainlab_translate_locales,code'
                ];
            }
            $customMessages = [
                'lang.required'  => \Lang::get('legato.api::lang.api.unsupported_lang_code'),
                'lang.exists' => \Lang::get('legato.api::lang.api.unsupported_lang_code'),
            ];
            $validator = \Validator::make($request->toArray(), $rules,$customMessages);

            if ($validator->fails()) {
                return $this->validationResponse($validator);
            }

            /*$lang_model = \Rainlab\Translate\Models\Locale::where('code',$lang_code)->where('is_enabled',1)->first();
            if(!$lang_model){

                return $this->response(\Lang::get('legato.api::lang.api.unsupported_lang_code'), \Config::get('legato.api::HTTP_STATUS_UNAUTHORIZED'));
            }*/

        }
        \App::setlocale($lang_code);

        return $next($request);

    }

}