<?php namespace Legato\Api\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Response;
use October\Rain\Exception\AjaxException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Carbon\Carbon;
use Legato\Api\Models\AccessToken;

class BasicMiddleware
{
    use \Legato\Api\Traits\UtilityFunctions;

    public $roles = ['Guest','User','Admin'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next,$role = null)
    {   
        if (!$this->checkApiVersion($request->ver)) {
            return $this->response(\Lang::get('legato.api::lang.api.version_outdated'), \Config::get('legato.api::HTTP_STATUS_HTTP_VERSION_NOT_SUPPORTED'));
        }
        $user_name  = $request->getUser();
        $password   = $request->getPassword();
        switch ($role) {
            case 'all':
                foreach ($this->roles as $key => $value) {
                    $function = 'check'.$value;
                    $result = $this->$function($user_name,$password);
                    if($result==true)
                        break;
                }
                break;
            default:
                $role = ucfirst($role);
                $function = 'check'.$role;
                $result = $this->$function($user_name,$password);
                break;
        }
        if(!$result){
            return $this->response(\Lang::get('legato.api::lang.api.basic_auth_failed'), \Config::get('legato.api::HTTP_STATUS_UNAUTHORIZED'));
        }

        return $next($request);

    }

    private function checkGuest($request_username = NULL, $request_password = NULL)
    {
        $username = \Config::get('legato.api::username', FALSE);
        $password = \Config::get('legato.api::password', FALSE);
        if ($username === $request_username && $password === $request_password &&
            !empty($request_username) && !empty($request_password)) {
            return TRUE;
        } else {
            return false;
        }
    }
    private function checkUser($user_id = NULL, $access_token = NULL)
    {
        return $this->checkNonGuest($user_id,$access_token,AccessToken::type_user);
    }
    private function checkAdmin($user_id = NULL, $access_token = NULL)
    {
        return $this->checkNonGuest($user_id,$access_token,AccessToken::type_admin);
    }

    private function checkNonGuest($user_id, $access_token, $type)
    {
        $exist = AccessToken::where('user_id',$user_id)
            ->where('type',$type)
            ->where('access_token',$access_token)
            ->count();
        if ($exist==1) {
            return TRUE;
        } else {
            return false;
        }
    }

    /**
     * Check if api version correct
     * 
     * @param string $ver
     * @return boolean
     */
    private function checkApiVersion($ver){
        $required_version = \Config::get('legato.api::version', 'V1');
        if($ver == $required_version){
            return true;
        } else {
            return false;
        }
    }
}