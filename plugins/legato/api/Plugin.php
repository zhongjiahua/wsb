<?php namespace legato\Api;

use System\Classes\PluginBase;
use Db;
use Legato\Api\Models\ApiLog;

/**
 * Api Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Api',
            'description' => 'Provide API for Other Project',
            'author'      => 'Legato',
            'icon'        => 'icon-leaf'
        ];
    }

    public function boot()
    {
        app()->make('router')->aliasMiddleware('apiLog', \Legato\Api\Middleware\ApiLogMiddleware::class);
        app()->make('router')->aliasMiddleware('basic', \Legato\Api\Middleware\BasicMiddleware::class);
        app()->make('router')->aliasMiddleware('multiLang', \Legato\Api\Middleware\MultiLangMiddleware::class);
    }

    public function registerPermissions()
    {

        return [
            'legato.api.apilog' => [
                'tab' => 'Api Log',
                'label' => 'ApiLog Management'
            ],
        ];
    }


    public function registerSettings()
    {
        return [
            'ApiLog' => [
                'label'       => 'ApiLog',
                'description' => 'Api Log',
                'category'    => \System\Classes\SettingsManager::CATEGORY_LOGS,
                'icon'        => 'icon-book',
                'url'         => \Backend::url('legato/api/apilogs'),
                'permissions' => ['legato.apilog.*'],
                //'permissions' => ['echome.flashsales.flashsales_notification_remarks']
            ]
        ];
    }



    public function registerSchedule($schedule)
    {   
        /**
         * Remove Auidt log 6 month before
         */
        $schedule->call(function () {
            try{
                ApiLog::cleanOldLogsByDays(180);
            }catch(\Exception $e){
                \Log::debug('remove_old_api_log fail');
                \Log::debug($e);
            }
          
        })->dailyAt('01:00')->name('remove_old_api_log')->withoutOverlapping();
    }
}
