<?php namespace Legato\Api\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Legato\Api\Models\AccessToken;

class CreateAccessTokenTable extends Migration
{
    public function up()
    {
        Schema::create('users_access_token', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('type')->default(AccessToken::type_user)
                ->comment(AccessToken::type_user.'=user/'.AccessToken::type_admin.'=admin');
            $table->string('access_token');
            $table->integer('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('deleted_at');
            $table->index('status');
            $table->index('type');
            $table->index('access_token');
            $table->index('user_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users_access_token');
    }
}
