<?php namespace Legato\Api\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateApiLogsTable extends Migration
{
    public function up()
    {
        Schema::create('api_logs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('ref_code');
            $table->string('uri');
            $table->string('method');
            $table->text('params')->nullable();
            $table->string('ip_address',45);
            $table->longText('result')->nullable();
            $table->longText('exception')->nullable();
            $table->datetime('request_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->float('rtime');
            $table->smallInteger('response_code');
            $table->text('authorized')->nullable();
            $table->string('user_agent',200);
            $table->longText('query_log')->nullable();
            $table->timestamps();


            $table->index('created_at');
            $table->index('ref_code');
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('api_logs');
    }
}
