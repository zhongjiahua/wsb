<?php 
return [
	'plugin' => [
		'name' 			=> 'API',
		'description' 	=> ''
	],
	'api' =>[
		'version_outdated' 			=> 	'Version outdated.',
		'basic_auth_failed' 		=> 	'Basic Auth failed.',
		'method_not_exist' 			=> 	'Method not exist',
		'access_token_invalid' 		=> 	'Access token invalid.',
		'missing_key_field' 		=> 	'Missing Key fields.',
		'field_empty' 				=> 	'Fields must not empty.',
		'success' 					=> 	'Success!!!',
		'internal_server_error'		=> 	'Internal Server Error. Please contact customer service with reference id: :log_id',
		'unsupported_lang_code'		=> 	'The selected language is invalid',
		'ut'	=> [
			'required'		=> 'ut is required!!',
			'date_format'	=> 'ut format should be Y-m-d\TH:i:s',
		]
	],
];