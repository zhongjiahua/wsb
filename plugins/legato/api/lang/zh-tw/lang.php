<?php 
return [
	'plugin' => [
		'name' 			=> 'API',
		'description' 	=> ''
	],
	'api' =>[
		'version_outdated' 			=> '版本已過時。',
		'basic_auth_failed' 		=> '基本身份驗證失敗。',
		'method_not_exist' 			=> '方法不存在',
		'access_token_invalid' 		=> '訪問令牌無效。',
		'missing_key_field' 		=> '缺少關鍵字段。',
		'field_empty' 				=> '字段不能為空。',
		'success' 					=> '成功！！！'
	],
];