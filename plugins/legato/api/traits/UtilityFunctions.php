<?php
namespace Legato\Api\Traits;    

use Config;
use Log;
use Response;
use Common\Classes\StatusService;

trait UtilityFunctions
{
    function htmlsan($htmlsanitize){
             return str_replace(array("'"), array("&#39;"), htmlspecialchars($htmlsanitize, ENT_QUOTES, 'UTF-8'));
        }
        
        function urlsafe_b64encode($string) {
            $data = base64_encode($string);
            $data = str_replace(array('+','/','='),array('-','_',''),$data);
            return $data;
        }

        function urlsafe_b64decode($string) {
            $data = str_replace(array('-','_'),array('+','/'),$string);
            $mod4 = strlen($data) % 4;
            if ($mod4) {
                $data .= substr('====', $mod4);
            }
            return base64_decode($data);
        }
        
        function unpadPKCS7($data, $blockSize) {
            $length = strlen ( $data );
            if ($length > 0) {
                $first = substr ( $data, - 1 );
                
                if (ord ( $first ) <= $blockSize) {
                    for($i = $length - 2; $i > 0; $i --)
                        if (ord ( $data [$i] != $first ))
                            break;
                    return substr ( $data, 0, $i+1 );
                }
            }
            return $data;
        }

        function decrypt128($data,$key='01234567890abcde',$option = 'cbc'){
            $data = str_replace(" ","+",$data);
            $result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($data), $option );
            return unpadPKCS7($result ,128);
        }

        function encrypt128($data,$key='01234567890abcde',$option = MCRYPT_MODE_CBC){
            return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, $option));
        }


        function obtainCurrentDateTime() {
            if(PHP_VERSION > '5.1') {
                    function_exists('date_default_timezone_set') ? @date_default_timezone_set(TimeZone_HongKong) : '';
                    $Time_Zone_Difference = 0;
            }
            else
            {
                $Time_Zone_Difference = LocalTimeZone * 3600; 
            }   
            $DateTime = date("Y-m-d H:i:s", time()+ $Time_Zone_Difference);
            return $DateTime;
        }

        function obtainCurrentTime() {
            if(PHP_VERSION > '5.1') {
                    function_exists('date_default_timezone_set') ? @date_default_timezone_set(TimeZone_HongKong) : '';
                    $Time_Zone_Difference = 0;
            }
            else
            {
                $Time_Zone_Difference = LocalTimeZone * 3600; 
            }   
            $DateTime = date("H:i:s", time()+ $Time_Zone_Difference);
            return $DateTime;
        }

        function obtainCurrentDate($format) {
            if(PHP_VERSION > '5.1') {
                    function_exists('date_default_timezone_set') ? @date_default_timezone_set(TimeZone_HongKong) : '';
                    $Time_Zone_Difference = 0;
            }
            else
            {
                $Time_Zone_Difference = LocalTimeZone * 3600; 
            }   
            $DateTime = date($format, time()+ $Time_Zone_Difference);
            return $DateTime;
        }

        function obtainDate($format, $datediff) {
            $DateTime = date($format, strtotime("+".$datediff." days"));
            return $DateTime;
        }

        function addDatetoDate($format, $old_date, $datediff) {
            $date = new DateTime($old_date);
            $date->add(new DateInterval('P'.$datediff.'D'));
            $expire_date = $date->format($format);
            return $expire_date;
        }

        function wrap($string, $width=75, $break="\n", $cut=true)
        {
          if($cut) {
            // Match anything 1 to $width chars long followed by whitespace or EOS,
            // otherwise match anything $width chars long
            $search = '/(.{1,'.$width.'})(?:\s|$)|(.{'.$width.'})/uS';
            $replace = '$1$2'.$break;
          } else {
            // Anchor the beginning of the pattern with a lookahead
            // to avoid crazy backtracking when words are longer than $width
            $pattern = '/(?=\s)(.{1,'.$width.'})(?:\s|$)/uS';
            $replace = '$1'.$break;
          }
          return preg_replace($search, $replace, $string);
        }

        function deletephoto($parray) {
            if ($parray != "") {
                unlink($parray['full_path']);
            }
        }
    
        function random_gen($length)
        {
                $random= "";
                $char_list = "ABCDEFGHJKLMNPQRSTUVWXYZ";
                $char_list .= "abcdefghijkmnpqrstuvwxyz";
                $char_list .= "23456789";
                // Add the special characters to $char_list if needed

                for($i = 0; $i < $length; $i++)  
                {    
                    $random .= substr($char_list,(rand()%(strlen($char_list))), 1);  
                }  
                return $random;
        }
    
         function random_gen_int($min, $max)
         {
                if ($min >= 0 && $max >= $min) {
                    srand((double)microtime()*1000000);
                    return rand($min,$max);
                }
                else {
                    return 0;
                }
        }
        
        function validateDateTimeStr($string) {
            return preg_match("/^[0-9\-]{1,10}$/", $string);
        }
    
        function validateDate( $date, $format='YYYY-MM-DD')
        {
            switch( $format )
            {
                case 'YYYY/MM/DD':
                case 'YYYY-MM-DD':
                list( $y, $m, $d ) = preg_split( '/[-\.\/ ]/', $date );
                break;

                case 'YYYY/DD/MM':
                case 'YYYY-DD-MM':
                list( $y, $d, $m ) = preg_split( '/[-\.\/ ]/', $date );
                break;

                case 'DD-MM-YYYY':
                case 'DD/MM/YYYY':
                list( $d, $m, $y ) = preg_split( '/[-\.\/ ]/', $date );
                break;

                case 'MM-DD-YYYY':
                case 'MM/DD/YYYY':
                list( $m, $d, $y ) = preg_split( '/[-\.\/ ]/', $date );
                break;

                case 'YYYYMMDD':
                $y = substr( $date, 0, 4 );
                $m = substr( $date, 4, 2 );
                $d = substr( $date, 6, 2 );
                break;

                case 'YYYYDDMM':
                $y = substr( $date, 0, 4 );
                $d = substr( $date, 4, 2 );
                $m = substr( $date, 6, 2 );
                break;

                default:
                throw new Exception( "Invalid Date Format" );
            }
            if (strlen($m) == 2 && strlen($d) == 2 && strlen($y) == 4)
                return checkdate( $m, $d, $y );
            else
                return false;
        }
        
        function is_integer($v) {
            if(preg_match("/^\d+$/", $v))
                return TRUE;
            return FALSE;
        }

        function validateTwoDecimals($number)
        {
           if(preg_match('/^[0-9]+\.[0-9]{2}$/', $number))
             return true;
           else
             return false;
        }
        
        function checkTime($h, $m, $s)
        {
            if ($h >= 0  && $h <24 && $m >= 0 && $m < 60 && $s >= 0 && $s < 60)
                return true;
            else
                return false;
        }
        
        function validateDateTime( $datetime, $format='YYYY-MM-DD HH:MM:SS')
        {
            switch( $format )
            {
                case 'YYYY-MM-DD HH:MM:SS':
                
                $splitary = preg_split("/ /",$datetime);
                if (count($splitary) == 2 ) {
                     list( $date, $time ) = $splitary;
                }
                else {
                    return false;
                }
                if (!$this->validateDate($date,'YYYY-MM-DD')) {
                    return false;
                }
                if ($time != "") {
                     list( $h, $m, $s ) = preg_split( '/:/', $time );
                }
                else {
                    return false;
                }
                     
                break;

                default:
                throw new Exception( "Invalid DateTime Format" );
            }
            if (strlen($h) == 2 && strlen($m) == 2 && strlen($s) == 2) {
                return $this->checktime( $h, $m, $s );
            }
            else
                return false;
        }
        
        /*
        function my_json_encode($in) {
              $_escape = function ($str) {
                return addcslashes($str, "\v\t\n\r\f\"\\/");
              };
              $out = "";
              if (is_object($in)) {
                $class_vars = get_object_vars(($in));
                $arr = array();
                foreach ($class_vars as $key => $val) {
                  $arr[$key] = "\"{$_escape($key)}\":\"{$val}\"";
                }
                $val = implode(',', $arr);
                $out .= "{{$val}}";
              }elseif (is_array($in)) {
                $obj = false;
                $arr = array();
                foreach($in AS $key => $val) {
                  if(!is_numeric($key)) {
                    $obj = true;
                  }
                  $arr[$key] = $this->my_json_encode($val);
                }
                if($obj) {
                  foreach($arr AS $key => $val) {
                    if (is_numeric($val))
                        $arr[$key] = "\"{$_escape($key)}\":\"{$val}\"";
                    else
                        $arr[$key] = "\"{$_escape($key)}\":{$val}";
                  }
                  $val = implode(',', $arr);
                  $out .= "{{$val}}";
                }else {
                  $val = implode(',', $arr);
                  $out .= "[{$val}]";
                }
              }elseif (is_bool($in)) {
                $out .= $in ? 'true' : 'false';
              }elseif (is_null($in)) {
                $out .= 'null';
              }elseif (is_string($in)) {
                $out .= "\"{$_escape($in)}\"";
              }else {
                $out .= $in;
              }
              return "{$out}";
        } */
        
        function microtime_float()
        {
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }
        
        function convertUTF8toBig5($arow) {
            for ($i = 0; $i < count($arow) ; $i++) {
                $arow[$i] = iconv("UTF-8", "big5//IGNORE", $arow[$i] ); 
            }
            return $arow;
        }
    
        function outputordercsv($ProductArray) {
                $filename = $ProductArray['UserID']."_".$ProductArray['OrderID']."_".date("YmdHi",time()).".csv";
                $fp = fopen(FOLDER_ORDER.$filename, 'w');
                
                for ($i = 0; $i < count($ProductArray['ProductCode']) ; $i++) {
                    $arow = array();
                    $arow[] = $ProductArray['CustCode'];
                    $arow[] = $ProductArray['CustName'];
                    $arow[] = $ProductArray['OrderID']."|".$ProductArray['ProductID'][$i]."|".$ProductArray['LinkedProductID'][$i];
                    $arow[] = $ProductArray['ProductCode'][$i];
                    $arow[] = $ProductArray['ProductQty'][$i];
                    $arow[] = $ProductArray['A_CODE'];
                    $arow[] = $ProductArray['DelName'];
                    $arow[] = $ProductArray['ADD_1'];
                    $arow[] = $ProductArray['ADD_2'];
                    $arow[] = $ProductArray['ADD_3'];
                    $arow[] = $ProductArray['ADD_4'];
                    $arow[] = $ProductArray['ATTN'];
                    $mobile = "";
                    if ($ProductArray['MNO'] != 0)
                        $mobile .= $ProductArray['MNO'];
                    if ($ProductArray['HNO'] != 0) {
                        if ( $mobile != "" ) {
                            $mobile .= " / ".$ProductArray['HNO'];
                        }
                        else {
                            $mobile .= $ProductArray['HNO'];
                        }
                    }   
                    $arow[] = $mobile;
                    $arow[] = $ProductArray['DeliverDate'];
                    $arow[] = $ProductArray['DocType'];
                    $arow = convertUTF8toBig5($arow);
                    fputcsvline($fp, $arow);
                }
                fclose($fp);
        }
    
        function fputcsvline(&$handle, $fields = array(), $delimiter = ',', $enclosure = '"') {

                // Sanity Check
                if (!is_resource($handle)) {
                    trigger_error('fputcsv() expects parameter 1 to be resource, ' .
                        gettype($handle) . ' given', E_USER_WARNING);
                    return false;
                }
        
                if ($delimiter!=NULL) {
                    if( strlen($delimiter) < 1 ) {
                        trigger_error('delimiter must be a character', E_USER_WARNING);
                        return false;
                    }elseif( strlen($delimiter) > 1 ) {
                        trigger_error('delimiter must be a single character', E_USER_NOTICE);
                    }
        
                    /* use first character from string */
                    $delimiter = $delimiter[0];
                }
        
                if( $enclosure!=NULL ) {
                     if( strlen($enclosure) < 1 ) {
                        trigger_error('enclosure must be a character', E_USER_WARNING);
                        return false;
                    }elseif( strlen($enclosure) > 1 ) {
                        trigger_error('enclosure must be a single character', E_USER_NOTICE);
                    }
        
                    /* use first character from string */
                    $enclosure = $enclosure[0];
               }
        
                $i = 0;
                $csvline = '';
                $escape_char = '\\';
                $field_cnt = count($fields);
                $enc_is_quote = in_array($enclosure, array('"',"'"));
                reset($fields);
        
                foreach( $fields AS $field ) {
        
                    /* enclose a field that contains a delimiter, an enclosure character, or a newline */
                    if( is_string($field) && ( 
                        strpos($field, $delimiter)!==false ||
                        strpos($field, $enclosure)!==false ||
                        strpos($field, $escape_char)!==false ||
                        strpos($field, "\n")!==false ||
                        strpos($field, "\r")!==false ||
                        strpos($field, "\t")!==false ||
                        strpos($field, ' ')!==false ) ) {
        
                        $field_len = strlen($field);
                        $escaped = 0;
        
                        $csvline .= $enclosure;
                        for( $ch = 0; $ch < $field_len; $ch++ )    {
                            if( $field[$ch] == $escape_char && $field[$ch+1] == $enclosure && $enc_is_quote ) {
                                continue;
                            }elseif( $field[$ch] == $escape_char ) {
                                $escaped = 1;
                            }elseif( !$escaped && $field[$ch] == $enclosure ) {
                                $csvline .= $enclosure;
                            }else{
                                $escaped = 0;
                            }
                            $csvline .= $field[$ch];
                        }
                        $csvline .= $enclosure;
                    } else {
                        $csvline .= $enclosure;
                        $csvline .= $field;
                        $csvline .= $enclosure;
                    }
        
                    $i++;
                    if( $i != $field_cnt ) {
                        $csvline .= $delimiter;
                    }
                }
        
                $csvline .= "\r\n";
        
                return fwrite($handle, $csvline);
        }
    
        function scanfolderforlatestfile($folder_dir, $folder_storage_dir) {
            $filepath = "";
            if ( $handle = opendir($folder_dir) )
            {
                $comparedate = "";
                $last_minute = date ( "Y-m-d H:i:s", strtotime ( '-30 seconds' . date("Y-m-d H:i:s") ) ); 
                
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db" ){
                        $tempfilepath = $folder_dir.$file;
                        
                        $last_modified = filemtime($tempfilepath);
                        $last_modified_str= date("Y-m-d H:i:s", $last_modified);
                        
                        if ($last_modified_str <= $last_minute) {
                            //echo "C Date: ".$comparedate."   Last modify date: ".$last_modified_str."<br>";
                            if($comparedate == "" || $comparedate < $last_modified_str)  {
                                  //echo "delete file:".$filepath." then set file:".$tempfilepath."<br>";
                                  if ($filepath != "") {
                                        copy ($filepath, $folder_storage_dir.basename($filepath));  
                                        unlink($filepath);
                                  }
                                  $filepath = $tempfilepath;
                                  $comparedate = $last_modified_str;
                            }  
                            else {
                                  //echo "delete file:".$tempfilepath."<br>";
                                  copy ($tempfilepath, $folder_storage_dir.$file);   
                                  unlink($tempfilepath);
                            }
                        }
                    }
                } 
                closedir($handle);
            }
            return $filepath;
        }
        
        function scanfilesfromfolder($folder_dir, $diffsec) {
            $filepath = array();
            if ( $handle = opendir($folder_dir) )
            {
                $comparedate = "";
                $last_minute = date ( "Y-m-d H:i:s", strtotime ( "-".$diffsec." seconds" . date("Y-m-d H:i:s") ) ); 
                
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db" ){
                    
                        $last_modified = filemtime($folder_dir.$file);
                        $last_modified_str= date("Y-m-d H:i:s", $last_modified);
                        if ($last_modified_str <= $last_minute) {
                            $tempfilepath = $folder_dir.$file;
                            $filepath[] = $tempfilepath;
                        }
                    }
                } 
                closedir($handle);
            }
            return $filepath;
        }
        
        function deletebackupfiles($folder_dir, $days) {
            if ( $handle = opendir($folder_dir) )
            {
                $comparedate = "";
                $filedays = date ( "Y-m-d H:i:s", strtotime ( $days.' days' . date("Y-m-d H:i:s") ) ); 
                
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db" ){
                        //echo $folder_dir.$file;
                        $last_modified = filemtime($folder_dir.$file);
                        $last_modified_str= date("Y-m-d H:i:s", $last_modified);
                        
                        if ($last_modified_str < $filedays) {
                            unlink($folder_dir.$file);
                        }
                    }
                } 
                closedir($handle);
            }
        }
        
        function sendPOSTDataByCurl($url, $timeout, $data = null , $uname="" ) {
            if (is_array($data)) {
                //url-ify the data for the POST
                $Parameters_String = '';
                foreach($data as $key=>$value) { $Parameters_String .= $key.'='.urlencode($value).'&'; }
                $Parameters_String = rtrim($Parameters_String,'&');

                $ch = curl_init();
                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch,CURLOPT_POST,count($data));
                curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameters_String);
                curl_setopt($ch,CURLOPT_TIMEOUT,$timeout);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                if ($uname != "") {
                    curl_setopt($ch,CURLOPT_USERPWD, $uname);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                }
        
                //execute post
                $result = curl_exec($ch);
                
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($httpCode != 200) {
                     $output = array('Result' => ERROR_CURL_ERROR, 'EMsg' => "Error : ".$httpCode." ".curl_error($ch), 'HttpCode' => $httpCode);
                     curl_close($ch);
                     return my_json_encode($output);
                }
                curl_close($ch);
                return $result;     
            } else {
                $output = array('Result' => ERROR_FAIL, 'EMsg' => "CURL Data invalid");
                return my_json_encode($output);
            }
        }

        function sendPOSTJsonByCurl($url, $timeout, $data = null , $uname="" ) {
            //url-ify the data for the POST
            // $Parameters_String = '';
            // foreach($data as $key=>$value) { $Parameters_String .= $key.'='.urlencode($value).'&'; }
            // $Parameters_String = rtrim($Parameters_String,'&');
            $ch = curl_init();
            $allt = json_encode($data);
            
            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch,CURLOPT_POST,count($data));
            curl_setopt($ch,CURLOPT_POSTFIELDS,$allt);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch,CURLOPT_TIMEOUT,$timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            if ($uname != "") {
                curl_setopt($ch,CURLOPT_USERPWD, $uname);
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            }

            //execute post
            $result = curl_exec($ch);
            
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            APILog_bgtask("httpCode : ".$httpCode);
            if($httpCode != 200) {
                 $output = array('Result' => ERROR_CURL_ERROR, 'EMsg' => "Error : ".$httpCode." ".curl_error($ch), 'HttpCode' => $httpCode);
                 curl_close($ch);
                     return my_json_encode($output);
            }
            curl_close($ch);
            return $result;     
            
        }
        
        function sendGetDataByCurl($url, $timeout, $uname="" ) {
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch,CURLOPT_TIMEOUT,$timeout);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                if ($uname != "") {
                    curl_setopt($ch,CURLOPT_USERPWD, $uname);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                }
        
                //execute post
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($httpCode != 200) {
                     $output = array('Result' => ERROR_CURL_ERROR, 'EMsg' => "Error : ".$httpCode." ".curl_error($ch), 'HttpCode' => $httpCode);
                     curl_close($ch);
                     return my_json_encode($output);
                }
                curl_close($ch);
                return $result;     
        }
        
       function convertimagetorgb($filepath) {
                 $img = new Imagick($filepath);
                 if ($img->getImageColorspace() == Imagick::COLORSPACE_CMYK) { 
                       $profiles = $img->getImageProfiles('*', false); 
                       // we're only interested if ICC profile(s) exist 
                       $has_icc_profile = (array_search('icc', $profiles) !== false); 
                       // if it doesnt have a CMYK ICC profile, we add one 
                       if ($has_icc_profile === false) { 
                           $icc_cmyk = file_get_contents(asset_url(6).'USWebUncoated.icc'); 
                           $img->profileImage('icc', $icc_cmyk); 
                           unset($icc_cmyk); 
                       } 
                       // then we add an RGB profile 
                       $icc_rgb = file_get_contents(asset_url(6).'sRGB_v4_ICC_preference.icc'); 
                       $img->profileImage('icc', $icc_rgb); 
                       unset($icc_rgb); 
                       
                       $img->setImageColorspace(13);
                       $img->negateImage(false, Imagick::CHANNEL_ALL);

                       $result = $img->stripImage(); // this will drop down the size of the image dramatically (removes all profiles) 
                       $result = $img->writeImage($filepath);
                } 
        }
    
    function sendemail ($content, $to, $cc, $bcc, $subject, $replyto, $submit_app_id='', $applicant_id='') {        
        $dbc = openDB();
        $query = "Insert into email_progress (content, mailto, cc, bcc, subject, reply_to, status, created_at, submit_app_id, applicant_id)
             values ('".mysqli_real_escape_string($dbc,$content)."', '".mysqli_real_escape_string($dbc,$to)."', 
             '".mysqli_real_escape_string($dbc,$cc)."', '".mysqli_real_escape_string($dbc,$bcc)."',
             '".mysqli_real_escape_string($dbc,$subject)."', '".mysqli_real_escape_string($dbc,$replyto)."',
             ".STATUS_ACTIVE.", now(), '".mysqli_real_escape_string($dbc,$submit_app_id)."', '".mysqli_real_escape_string($dbc,$applicant_id)."')";
        mysqli_query($dbc, $query);
        if (mysqli_affected_rows($dbc) > 0) {
            return ERROR_SUCCESS;
        }
        closeDB($dbc);
        return ERROR_FAIL;
    }
    
    function insertcmslog($obj, $logby, $atype, $akind, $name, $tcname, $scname, $ref_id) {
        $obj->db->set('log_by', $logby);
        $obj->db->set('action_type', $atype);
        $obj->db->set('action_kind', $akind);
        $obj->db->set('name', $name);
        //$obj->db->set('tcname', $tcname);
        //$obj->db->set('scname', $scname);
        $obj->db->set('ref_id', $ref_id);
        $obj->db->insert('log_cms_action');
    }
    
    function get_all_string_between($string, $start, $end)
    {
        $result = array();
        $string = " ".$string;
        $offset = 0;
        while(true)
        {
            $ini = strpos($string,$start,$offset);
            if ($ini == 0)
                break;
            $ini += strlen($start);
            $len = strpos($string,$end,$ini) - $ini;
            $result[] = substr($string,$ini,$len);
            $offset = $ini+$len;
        }
        return $result;
    }
    
    function parseurlparams($c) {
          $output = array();
          $pairs = explode('&', $c);
          foreach($pairs as $pair) {
                $part = explode('=', $pair);
                $output[$part[0]] = urldecode($part[1]);
          }
          return $output;
    }
    
    function decrypt256($data,$key,$option = MCRYPT_MODE_CBC){
        if ($key == "" || $data == "") {
            return $data;
        }
        $iv = pack('H*', "00000000000000000000000000000000");
        $result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, urlsafe_b64decode($data), $option, $iv );
      
        $r = unpadPKCS7($result ,128);
        
        if ($r == "") {
            return "";
        }
        return $r;
    }

    function encrypt256($data,$key,$option = MCRYPT_MODE_CBC){
        if ($key == "" || $data == "") {
            return $data;
        }
        $block = mcrypt_get_block_size (MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = $block - (strlen($data) % $block);
        $data .= str_repeat(chr($pad), $pad);
        
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = pack('H*', "00000000000000000000000000000000");
        
        return urlsafe_b64encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv));
    }
    
    function encryptdbstr($data){
        return encrypt256($data,DB_ENC_KEY);
    }
    
    function encryptdbstrwithcase($data){
        return encrypt256(strtoupper($data),DB_ENC_KEY);
    }
    
    function decryptdbstr($data){
        return decrypt256($data,DB_ENC_KEY);
    }
    
    function encryptmobilestr($data){
        //return $data;
        return encrypt256($data,SERVER_MOBILE_ENC_KEY);
    }

    function decryptmobilestr($data){
        //return $data;
        return decrypt256($data,SERVER_MOBILE_ENC_KEY);
    }
    
    function getage($bdate){
         $date = new DateTime($bdate);
         $now = new DateTime();
         $interval = $now->diff($date);
         return $interval->y;
    }

    public static function stringEndWith($Haystack, $Needle)
    {
        return strrpos($Haystack, $Needle) === strlen($Haystack) - strlen($Needle);
    }


    /**
     * Return response
     *
     * @param string $message
     * @param int    $status_code
     * @param *      $data
     *
     * @return \Response
     */
    function response($message, $status_code, $data = NULL){
        $result = array();
        if(!empty($message)){
            $result += array('message' => $message);
        }
        $result += array('server_time' => \Carbon\Carbon::now()->toDateTimeString());
        $contents = array('result' => $result);
        if(!empty($data)){
            $contents += array('data' => $data);
        }
        return Response::make($contents, $status_code)->header('Content-Type', 'application/json');
    }
    /**
     * Return success response
     *
     * @param *      $data
     *
     * @return \Response
     */
    function successResponse($data = null){
        return $this->response(\Lang::get('legato.api::lang.api.success'), 200, $data);
    }

    /**
     * Return validation error response
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return \Response
     */
    function validationResponse($validator){
        $result = array();
        $errors = array();
        
        foreach ($validator->errors()->all() as $key => $msg) {
            if( $key==0)    
                $result += array('message' => $msg);
            $errors[] = $msg;
        }
        $result += array('server_time' => $this->getServerTime());
        $contents = array('result' => $result);
        $data = [ 'errors' => $errors];
        
        if(!empty($data)){
            $contents += array('data' => $data);
        }
        return Response::make($contents, 400)->header('Content-Type', 'application/json');
    }

    /**
     * Transform object
     *
     * @param Collection | item             $input          collection or single model item
     * @param TransformerAbstract | null    $transformer    $item->tranfomer will be used if null
     */
    protected function transform($input, $transformer=null, $key = null){
        if($input instanceof \Illuminate\Support\Collection){
            return $this->transformCollection($input, $transformer, $key = null);
        }else{
            return $this->transformItem($input, $transformer, $key = null);
        }
    }

    protected function transformCollection($collection, $transformer, $key = null)
    {
        if(is_null($transformer)){
            if($collection->count() == 0){
                return $collection;
            }
            $item = $collection->first();
            $transformer = $item->transformer;
            if(!isset($transformer)){
                return $collection;
            }
        }
        $fractal = new \League\Fractal\Manager();
        $resource = new \League\Fractal\Resource\Collection($collection, new $transformer, $key);
        return current($fractal->createData($resource)->toArray());
    }

    protected function transformItem($item, $transformer,$key = null)
    {
        if(is_null($transformer)){
            $transformer = $item->transformer;
            if(!isset($transformer)){
                return $item;
            }
        }
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\ArraySerializer());
        $resource = new \League\Fractal\Resource\Item($item, new $transformer, $key);
        return $fractal->createData($resource)->toArray();
    }

    function checkKeyFieldsExist($dataArray, $fieldsArray){
        foreach ($fieldsArray as $value) {
            if(!array_key_exists($value, $dataArray)){
                return FALSE;
            }
        }
        return TRUE;
    }

    function checkArrayValueNotEmpty($dataArray){
        foreach ($dataArray as $value) {
            if(empty($value)){
                return FALSE;
            }
        }
        return TRUE;
    }

    function getServerTime(){
        $results = \DB::select(\DB::raw('SELECT NOW() AS server_time'));
        return $results[0]->server_time;
    }
    
}