<?php namespace Legato\Api\Models;

use Common\Models\CommonModel;
use \October\Rain\Database\Traits\Validation;
use Str;

/**
 * Category Model
 */
class AccessToken extends CommonModel
{
    use Validation;

    const type_admin            = 2;
    const type_user             = 1;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'users_access_token';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $hidden = [
        'id','status','created_by','updated_by','created_at','updated_at','deleted_at'
    ];

    public static function generateToken($user_id,$type = self::type_user){
        $access_token = Str::random(32);
        $access_token_collection = self::where('user_id', $user_id)->where('type',$type)->orderBy('updated_at', 'asc')->get();
        $limited_token = \Config::get('legato.api::maximum_multi_login',1);
        if($access_token_collection->count() < $limited_token){ // Check only 1 token for same user.
            // New access token record.
            $access_token_model = new self;
            $access_token_model->user_id = $user_id;
            $access_token_model->access_token = $access_token;
            $access_token_model->type = $type;
            $access_token_model->status = 1;
            $access_token_model->save();
        }else{
            // Update oldest record.
            $access_token_model = $access_token_collection->first();
            $access_token_model->user_id = $user_id;
            $access_token_model->access_token = $access_token;
            $access_token_model->type = $type;
            $access_token_model->status = 1;
            $access_token_model->save();
        }
        return $access_token_model;

    }

    public static function generateAdminToken($user_id){
        return self::generateToken($user_id,self::type_admin);
    }

    public static function generateUserToken($user_id){
        return self::generateToken($user_id,self::type_user);
    }

 
}
