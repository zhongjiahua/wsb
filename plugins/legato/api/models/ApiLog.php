<?php namespace legato\api\Models;

use Common\Models\CommonModel;
use October\Rain\Database\Model;
use \October\Rain\Database\Traits\Validation;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;

/**
 * Category Model
 */
class ApiLog extends Model
{
    //use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'api_logs';

    public $jsonable = ['query_log'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public function beforeCreate(){
        //$this->uuid = Uuid::uuid4();
        $this->ref_code = Str::random(12);
    }

    public function getUriOptions(){
        $collections = self::groupBy('uri')->get(['uri']);
        $res = [];
        foreach ($collections as $key => $model) {
            $res[$model->uri] = $model->uri; 
        }
        return $res;
    }

    public function getMethodOptions(){
        $collections = self::groupBy('method')->get(['method']);
        $res = [];
        foreach ($collections as $key => $model) {
            $res[$model->method] = $model->method; 
        }
        return $res;
    }

    public function getIpAddressOptions(){
        $collections = self::groupBy('ip_address')->get(['ip_address']);
        $res = [];
        foreach ($collections as $key => $model) {
            $res[$model->ip_address] = $model->ip_address; 
        }
        return $res;
    }

    
    /**
     * Remove Old Logs
     */
    public static function cleanOldLogsByDays($days){
        $target_date = \Carbon\Carbon::now();
        $target_date->subDays($days);
        $logs = self::where('created_at','<',$target_date)->get();
        foreach ($logs as $key => $log) {
            $log->delete();
        }
    }   
}
