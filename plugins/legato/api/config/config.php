<?php

return [
    'version'                                       =>      env('API_VERSION','V1'),
    'maximum_multi_login'                           =>      env('API_MULTI_LOGIN',1),
    'username'                                      =>      env('API_USER','api_basic_auth_username'),
    'password'                                      =>      env('API_PASSWORD','api_basic_auth_password'),

    'general_modules'                               =>      env('API_GENERAL_MODULES') ? array_filter(explode(',', env('API_GENERAL_MODULES'))) : [],
    'supported_lang'                                =>      env('API_LANG') ? array_filter(explode(',', env('API_LANG'))) : [],

    'BASIC_VALIDATION_FAIL'                         =>       0,
    'BASIC_VALIDATION_SUCCESS'                      =>       10,
    'ACCESS_TOKEN_VALIDATION_SUCCESS'               =>       11,
    'API_ACCESS_UNAUTHORIZED'                       =>       20,

    'HTTP_STATUS_CONTINUE'                          =>      100,
    'HTTP_STATUS_SWITCHING_PROTOCOLS'               =>      101,

    'HTTP_STATUS_OK'                                =>      200,
    'HTTP_STATUS_CREATED'                           =>      201,
    'HTTP_STATUS_ACCEPTED'                          =>      202,
    'HTTP_STATUS_NON_AUTHORITATIVE_INFORMATION'     =>      203,
    'HTTP_STATUS_NO_CONTENT'                        =>      204,
    'HTTP_STATUS_RESET_CONTENT'                     =>      205,
    'HTTP_STATUS_PARTIAL_CONTENT'                   =>      206,

    'HTTP_STATUS_MULTIPLE_CHOICES'                  =>      300,
    'HTTP_STATUS_MOVED_PERMANENTLY'                 =>      301,
    'HTTP_STATUS_FOUND'                             =>      302,
    'HTTP_STATUS_SEE_OTHER'                         =>      303,
    'HTTP_STATUS_NOT_MODIFIED'                      =>      304,
    'HTTP_STATUS_USE_PROXY'                         =>      305,
    'HTTP_STATUS_UNUSED'                            =>      306,
    'HTTP_STATUS_TEMPORARY_REDIRECT'                =>      307,

    'HTTP_STATUS_BAD_REQUEST'                       =>      400,
    'HTTP_STATUS_UNAUTHORIZED'                      =>      401,
    'HTTP_STATUS_PAYMENT_REQUIRED'                  =>      402,
    'HTTP_STATUS_FORBIDDEN'                         =>      403,
    'HTTP_STATUS_NOT_FOUND'                         =>      404,
    'HTTP_STATUS_METHOD_NOT_ALLOWED'                =>      405,
    'HTTP_STATUS_NOT_ACCEPTABLE'                    =>      406,
    'HTTP_STATUS_PROXY_AUTHENTICATION_REQUIRED'     =>      407,
    'HTTP_STATUS_REQUEST_TIMEOUT'                   =>      408,
    'HTTP_STATUS_CONFLICT'                          =>      409,
    'HTTP_STATUS_GONE'                              =>      410,
    'HTTP_STATUS_LENGTH_REQUIRED'                   =>      411,
    'HTTP_STATUS_PRECONDITION_FAILED'               =>      412,
    'HTTP_STATUS_REQUEST_ENTITY_TOO_LARGE'          =>      413,
    'HTTP_STATUS_REQUEST_URL_TOO_LONG'              =>      414,
    'HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE'            =>      415,
    'HTTP_STATUS_REQUESTED_RANGE_NOT_SATISFIABLE'   =>      416,
    'HTTP_STATUS_EXPECTATION_FAILED'                =>      417,

    'HTTP_STATUS_INTERNAL_SERVER_ERROR'             =>      500,
    'HTTP_STATUS_NOT_IMPLEMENTED'                   =>      501,
    'HTTP_STATUS_BAD_GATEWAY'                       =>      502,
    'HTTP_STATUS_SERVICE_UNAVAILABLE'               =>      503,
    'HTTP_STATUS_GATEWAY_TIMEOUT'                   =>      504,
    'HTTP_STATUS_HTTP_VERSION_NOT_SUPPORTED'        =>      505,

    'MOBILE_KIND_IOS'                               =>      1,
    'MOBILE_KIND_ANDROID'                           =>      2,

    'API_GENERAL'                                   =>      1,

    'PUSH_TYPE_FORUM_COMMENT'                       =>      1,
    'PUSH_TYPE_NEW_PROGRAM_REPORT_AVALIABLE'        =>      2,
    'PUSH_TYPE_REMINDER'                            =>      5,
    'PUSH_TYPE_ADHOC_ALL'                           =>      6,
    'PUSH_TYPE_ADHOC_GROUP'                         =>      7,
    'PUSH_TYPE_ADHOC_INDIVIDUAL'                    =>      8,

];
