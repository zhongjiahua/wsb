<?php namespace Legato\Api\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;

/**
 * Api Logs Back-end Controller
 */
class GeneralApi extends Controller
{
    use \Legato\Api\Traits\UtilityFunctions;
    
    public function generalInfo(){
        $data = \Request::all();
        $rules = [
            'ut' => 'required|date_format:Y-m-d\TH:i:s',
        ];
        /*$customMessages = [
            'ut.required'    => 'ut is required',
            'ut.date_format' => 'ut format should be Y-m-d\TH:i:s',
        ];*/
        $validator = \Validator::make($data, $rules);
        if ($validator->fails()) {
            return $this->validationResponse($validator);
        }
        
        $ut      = str_replace('T', ' ', $data['ut']);
        $modules = \Config::get('legato.api::general_modules');
        $data = [];
        foreach ($modules as $key => $value) {
            # code...
            $data[class_basename($value)] = $value::Api($ut,\App::getLocale());
        }

        return $this->successResponse($data);

    }
}
