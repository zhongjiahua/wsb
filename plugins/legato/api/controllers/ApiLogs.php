<?php namespace Legato\Api\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;

/**
 * Api Logs Back-end Controller
 */
class ApiLogs extends \CommonController
{
    public $requiredPermissions = ['legato.api.apilog'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('October.System', 'system', 'users');
        SettingsManager::setContext('Legato.Api', 'ApiLog');
        $this -> vars['hide_create'] = true;
        $this -> vars['hide_export'] = false;
    }

    public function preview($id)
    {
        $this->addCss('/modules/system/assets/css/eventlogs/exception-beautifier.css', 'core');
        $this->addJs('/modules/system/assets/js/eventlogs/exception-beautifier.js', 'core');
        $this->addJs('/modules/system/assets/js/eventlogs/exception-beautifier.links.js', 'core');
        return parent::preview($id);
    }
}
