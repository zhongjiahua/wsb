<?php namespace Legato\Api\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Common\Controllers\CommonController;
use Legato\Content\Transformers\PageTransformer;

/**
 * Cities Back-end Controller
 */
class Sample extends Controller
{
    use \Legato\Api\Traits\UtilityFunctions;

    public function guestGetPage(){
        $data = \Request::all();
        $rules = [
            'ut' => 'required|date_format:Y-m-d\TH:i:s',
        ];
        $customMessages = [
            'ut.required'    => \Lang::get('legato.api::lang.api.ut.required'),
            'ut.date_format' => \Lang::get('legato.api::lang.api.ut.date_format'),
        ];
        $validator = \Validator::make($data, $rules,$customMessages);
        if ($validator->fails()) {
            return $this->validationResponse($validator);
        }

    	$pages = \Legato\Content\Models\Page::whereUt($data['ut'])->withTranslate()->get();
        $pages = $this->transform($pages);

        return $this->successResponse(['pages' => $pages]);
    }
    public function userGetPage(){
        $data = \Request::all();
        $pages = \Legato\Content\Models\Page::whereUt($data['ut'])->withTranslate()->get();
        $pages = $this->transform($pages,PageTransformer::class);
        
        return $this->successResponse(['pages' => $pages]);
    }

    public function fakeUserAccessToken(){
        $user_id = 0;
        $token = \Legato\Api\Models\AccessToken::generateUserToken($user_id);
        return $this->successResponse(['access_token' => $token]);
    }

    public function fakeAdminAccessToken(){
        $user_id = 0;
        $token = \Legato\Api\Models\AccessToken::generateAdminToken($user_id);
        return $this->successResponse(['access_token' => $token]);
    }

    public function exceptionCase(){
        throw new \Exception('sample');
    }
}
