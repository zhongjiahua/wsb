<?php
//use legato\Api\Controllers\Api;
//
//Route::group(['prefix' => 'api/group-1/{version}/'], function () {
//	// get all.
//	Route::post('regPush',function($version){
//		$api = new Api();
//		return $api->handle($version, 'regPush');
//	});
//
//	Route::get('getSupportivePages',function($version){
//		$api = new Api();
//		return $api->handle($version, 'getSupportivePages');
//	});
//
//	Route::post('updateAccessToken/{user_id}',function($version, $user_id){
//		$api = new Api();
//		return $api->handle($version, 'updateAccessToken', $user_id);
//	});
//});
//
//Route::group(['prefix' => 'api/group-2/{version}/'], function () {
//});

Route::get('api/{ver}/sample/guest', 'Legato\Api\Controllers\Sample@guestGetPage')
	->middleware('apiLog','basic:guest','multiLang','appVer');
Route::get('api/{ver}/sample/user', 'Legato\Api\Controllers\Sample@userGetPage')
	->middleware('apiLog','basic:user','multiLang');
Route::get('api/{ver}/sample/admin', 'Legato\Api\Controllers\Sample@userGetPage')
	->middleware('apiLog','basic:admin','multiLang');

Route::get('api/{ver}/sample/fakeToken/user', 'Legato\Api\Controllers\Sample@fakeUserAccessToken')
	->middleware('apiLog','basic:guest','multiLang');
Route::get('api/{ver}/sample/fakeToken/admin', 'Legato\Api\Controllers\Sample@fakeAdminAccessToken')
	->middleware('apiLog','basic:guest','multiLang');


Route::get('api/{ver}/general','Legato\Api\Controllers\GeneralApi@generalInfo')
	->middleware('apiLog','basic:all','multiLang','appVer');
?>
