<?php namespace Legato\Pushazure\Models;

use Model;
use Legato\Pushazure\classes\Constant as AzureConstant;
use Legato\Pushazure\classes\NotificationHub;

/**
 * Subscription Model
 */
class Subscription extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_pushazure_subscriptions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'subscription' => [
            '\Legato\Push\Models\Subscription'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeCreate(){
        $this->sns_status = AzureConstant::SNS_STATUS_PENDING;
    }

    public function createOrUpdateRegistration(){
        $this->sns_status = AzureConstant::SNS_STATUS_PROGRESSING;
        $this->save();
        $sub = $this->subscription;
        $device = $sub->device;
        $topic = $sub->topic;
        $tag = $topic->name;
        $disable = null;
        if($device->token == -1 ||
            $device->status == 0
        ){
            $disable = "device: {$device->id} is disabled";
        }else if($sub->status == 0){
            $disable = "subscription is disabled";

        }
        $hub = NotificationHub::initClient();
        try{
            if($disable != NULL){
                if($this->registration_id && $this->sns_status != AzureConstant::SNS_STATUS_DISABLED){
                    //$this->deleteRegistratrion();
                    $hub->updateRegistration(
                        $device->getDeviceKind(), 
                        $this->registration_id, 
                        $device->token, 
                        NULL
                    );
                }

                $this->sns_status = AzureConstant::SNS_STATUS_DISABLED;
                $this->error_reason = $disable;
                $this->save();
            }else{
                if(!$this->registration_id){
                    $registration_id = $hub->createRegistration(
                        $device->getDeviceKind(), 
                        $device->token, 
                        $tag
                    );
                    $this->sns_status = AzureConstant::SNS_STATUS_COMPLETED;
                    $this->registration_id = $registration_id;
                    $this->save();
                }else {
                    $hub->updateRegistration(
                        $device->getDeviceKind(), 
                        $this->registration_id, 
                        $device->token, 
                        $tag
                    );
                    $this->sns_status = AzureConstant::SNS_STATUS_COMPLETED;
                    $this->save();
                }
            }
        }catch(\Exception $e){
            \Log::debug('Fail to register azure subscription id:'.$this->id);
            \Log::debug($e);
            $this->sns_status = AzureConstant::SNS_STATUS_ERROR;
            $this->save();
        }
    }


    public function deleteRegistratrion(){
        $hub = NotificationHub::initClient();
        if($this->registration_id){
            $hub->deleteRegistration(
                $this->registration_id
            );
            $this->sns_status = AzureConstant::SNS_STATUS_DELETED;
            //$this->registration_id = NULL;
            $this->save();
        }
    }



    public static function getPendingSubscription(){
        return self::where('sns_status',AzureConstant::SNS_STATUS_PENDING)
            ->with('subscription')
            ->with('subscription.device')
            ->with('subscription.topic')
            ->get();
    }

}
