<?php
/**
 * Created by PhpStorm.
 * User: ryanng
 * Date: 11/4/2018
 * Time: 3:40 PM
 */

namespace Legato\Pushazure\Models;

use Carbon\Carbon;
use Legato\Push\Models\Progress_topic;
use Legato\Push\Models\PushLocale;
use Legato\Pushazure\classes\NotificationHub;
use Legato\Pushazure\classes\Constant as AzureConstant;
use Legato\Push\classes\Constant as PushConstant;

class ProgressTopic extends Progress_topic
{
    public static function changePendingToProcessing($array)
    {
        self::whereIn('id',$array)
                        ->update(['status' => AzureConstant::SNS_STATUS_PROGRESSING]);
    }

    public static function getPendingMessage()
    {

        return self::with('message', 'topic')
            ->where('status', AzureConstant::SNS_STATUS_PENDING)->whereHas('message', function ($query) {
                $query->where('status', PushConstant::PUSH_STATUS_ACTIVE)
                    ->where('push_platform',PushConstant::PLATFORM_AZURE)
                    ->where('schedule_time', '<=', Carbon::now());
            })->whereHas('topic', function ($query) {
                $query->where('status', PushConstant::PUSH_STATUS_ACTIVE);
            })->limit(2000)->lockForUpdate()->get();
    }

    public static function getIdArrFromObjArr($objArr)
    {
        $idArr = array();
        foreach ($objArr as $item)
        {
            $idArr[]=$item->id;
        }
        return $idArr;
    }

    public static function cronPublishMessageToTopic()
    {
        $hub = NotificationHub::initClient();
        $processingMsgs = self::getPendingMessage();
        self::changePendingToProcessing(self::getIdArrFromObjArr($processingMsgs));

        foreach ($processingMsgs as $processingMsg) {
            $azure_complete_topic = new CompleteTopic();
            $msg = $processingMsg->message;
            $topic = $processingMsg->topic;
            //$azure_topic = $topic->azure_topic;
            $awsMessageArn = false;
            $isError = false;


            /*switch ($azure_topic->sns_status){
                case AzureConstant::SNS_STATUS_PROGRESSING:
                case AzureConstant::SNS_STATUS_PENDING:
                    $processingMsg->status = AzureConstant::SNS_STATUS_PENDING;
                    $processingMsg->save();
                    continue;
                    break;

                case AzureConstant::SNS_STATUS_ERROR:
                    $isError = true;
                    break;

                default:
                    break;
            }*/

            
            if (!$isError) {
                if (\Config::get("legato.pushazure::TESTING_MODE",false)) {
                    $processingMsg->status = PushConstant::PUSH_STATUS_INACTIVE;

                    $azure_complete_topic->error_reason = "Testing Mode is enabled";
                    $azure_complete_topic->sns_status = AzureConstant::SNS_STATUS_COMPLETED;
                } else {
                    
                    if ($topic->language != 0)
                    {
                        /**
                         * Have language
                         */
                        $locale = PushLocale::findById($topic->language);
                        $msg->translateContext($locale->code);
                    }
                    //send out the message
                    $res = $hub->sendToTag($msg,$topic->name);

                    $azure_complete_topic->response = $res;
                }
            }else{
                $processingMsg->status = PushConstant::PUSH_STATUS_INACTIVE;
            }


            $completeMsg = $processingMsg->moveToComplete();

            /*if ($awsMessageArn) {
                $aws_complete_topic->arn = $awsMessageArn;
                $aws_complete_topic->sns_status = Constant::SNS_STATUS_COMPLETED;
            } else {
                $aws_complete_topic->error_reason = "Cannot sent out the message, Please check";
                $aws_complete_topic->sns_status = Constant::SNS_STATUS_ERROR;
            }*/

            $completeMsg->azure_complete_topic()->add($azure_complete_topic);


        }



    }
}