<?php namespace Legato\Pushazure;

use Backend;
use System\Classes\PluginBase;
use Legato\Pushazure\Models\Registration;
use Legato\Push\Models\Device;
use Legato\Push\classes\Constant as PushConstant;
use Legato\Pushazure\classes\Constant as AzureConstant;
use Legato\Pushazure\Models\Tag;
/**
 * PushAzure Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PushAzure',
            'description' => 'No description provided yet...',
            'author'      => 'Legato',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Device::extend(function ($model) {
//            $model->addDynamicProperty('platformarn', null);
//
//            $model->addDynamicMethod('getPlatformarnAttribute', function($value) {
//                return $value;
//            });
//
//            $model->addDynamicMethod('setPlatformarnAttribute', function($value) use ($model) {
//                $model->platformarn = $value;
//            });

            $model->addDynamicMethod('getDeviceKind', function() use ($model) {
                switch ($model->os_type) {
                    case PushConstant::OS_TYPE_IOS:
                        $device_kind = AzureConstant::notification_type_ios;
                        break;
                    case PushConstant::OS_TYPE_ANDROID:
                        $device_kind = AzureConstant::notification_type_gcm;
                        break;
                    case PushConstant::OS_TYPE_UNKNOWN:
                        //invalid device type
                    default:
                        # code...
                        throw new \Exception("Unsupported os type");
                        break;
                }
                return $device_kind;
            });
//            /**
//             * Create Registration in azure
//             */
//            $model->hasOne['registration'] = ['Legato\Pushazure\Models\Registration'];
//            $model->bindEvent('model.afterSave', function () use ($model) {
//
//                if (is_null($model->registration)) {
//                    $model->registration()->add(new Registration());
//                } else {
//                    $registration = Registration::where('device_id', $model->id)->first();
//                    $registration->sns_status = AzureConstant::SNS_STATUS_PENDING;
//                    $registration->save();
//                }
//            });
            
            /**
             * Trigger azure registration when device is disabled push
             */
            $model->bindEvent('model.afterSave', function () use ($model) {
                if ($model->isDirty('status')){
                    $subs = $model->subscription;
                    foreach ($subs as $key => $sub) {
                        $azure_sub = $sub->azure_subscription;

                        if($model->status == PushConstant::PUSH_STATUS_INACTIVE && 
                            $azure_sub->sns_status == AzureConstant::SNS_STATUS_DISABLED){
                            //nothing gonna do if the azure subscription is already disabled
                        }else{
                            $azure_sub->sns_status = AzureConstant::SNS_STATUS_PENDING;
                            $azure_sub->save();
                        }
                    }
                }
            });



        });

        \Legato\Push\Models\Subscription::extend(function ($model) {
            $model->hasOne['azure_subscription'] = ['Legato\Pushazure\Models\Subscription'];

            $model->bindEvent('model.afterSave', function () use ($model) {
                if (is_null($model->azure_subscription)) {
                    $model->azure_subscription()->add(new \Legato\Pushazure\Models\Subscription());
                } else {
                    $azure_sub = \Legato\Pushazure\Models\Subscription::where('subscription_id', $model->id)->first();
                    if($model->status == PushConstant::PUSH_STATUS_INACTIVE && 
                        $azure_sub->sns_status == AzureConstant::SNS_STATUS_DISABLED){
                        //nothing gonna do if the azure subscription is already disabled
                    }else{
                        $azure_sub->sns_status = AzureConstant::SNS_STATUS_PENDING;
                        $azure_sub->error_reason = "";
                        $azure_sub->save();
                    }
                }
            });
        });
        
        \Legato\Push\Models\Message::extend(function($model) {
            $model->belongsToMany['azure_tags'] = [
                Tag::class,
                'table'     =>  'legato_pushazure_message_tags',
                'key'       =>  'message_id',
                'otherKey'  =>  'tag_id'
            ];
            $model->hasMany['azure_message_tags'] = [
                'Legato\Pushazure\Models\MessageTag',
            ];
            $model->addDynamicMethod('createOrUpdateAzureTag', function($member_ids) use ($model) {
                $tag = $model->azure_tags()->first();
                if(!$tag){
                    $tag = new Tag();
                    $tag->save();
                    $model->azure_tags()->add($tag);
                }
                $tag->updateUsers($member_ids);
                
            });
            $model->addDynamicMethod('removeTag', function() use ($model) {
                $msg_tags = $model->azure_message_tags;
                $tags = $model->azure_tags;
                
                foreach ($msg_tags as $key => $msg_tag) {
                    $msg_tag->delete();
                }
                foreach ($tags as $key => $tag) {
                    $tag->delete();
                }
            });
        });
        
        /*Topic::extend(function ($model) {
            $model->hasOne['aws_topic'] = ['Legato\Pushazure\Models\Topic'];

            $model->bindEvent('model.afterSave', function () use ($model) {
               // trace_log($model->aws_topic);
                if (is_null($model->aws_topic)) {
                    $model->aws_topic()->add(new \Legato\Pushazure\Models\Topic());
                } else {
                    $aws_topic = \Legato\Pushazure\Models\Topic::where('topic_id', $model->id)->first();
                    $aws_topic->sns_status = Constant::SNS_STATUS_PENDING;
                    $aws_topic->error_reason = "";
                    $aws_topic->save();
                }
            });
        });*/

        \Legato\Push\Models\Complete_topic::extend(function ($model) {
            $model->hasOne['azure_complete_topic'] = ['Legato\Pushazure\Models\CompleteTopic'];
        });

        /*Complete_individual::extend(function ($model) {
            $model->hasOne['aws_complete_individual'] = ['Legato\Pushaws\Models\AWScomplete_individual'];
        });*/



        //$connectionString = \Config::get('legato.pushazure::hub_connection_string');
        //$hub_namespace = \Config::get('legato.pushazure::hub_namespace');
        //$hubname = \Config::get('legato.pushazure::hub');
        //$hub = new \Legato\PushAzure\Classes\NotificationHub($connectionString, $hubname); 
        //$sas = $hub->generateSasToken("https://{$hub_namespace}.servicebus.windows.net/{$hubname}/registrations/?api-version=2015-01");
        //trace_log($sas);
        /*$alert = '{"aps":{"alert":"Hello from PHP!"}}';
        //$notification = new \Legato\PushAzure\Classes\Notification("apple", $alert);
        //$hub->sendNotification($notification, null);

        $message = '{"data":{"message":"Hello from PHP!"}}';
        $notification = new \Legato\PushAzure\Classes\Notification("gcm", $message);
        //$hub->sendNotification($notification, null);*/

    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            //\Legato\Pushazure\Models\Registration::proceedRegistration();
        })->name('createRegistrationIDFromAzure')->withoutOverlapping();
        $schedule->call(function () {

            $subs = \Legato\Pushazure\Models\Subscription::getPendingSubscription();
            foreach ($subs as $key => $sub) {
                try{
                    $sub->createOrUpdateRegistration();
                }catch(\Exception $e){
                    \Log::debug('push_azure_registration');
                    \Log::debug($e);
                }
            }
        })->name('push_azure_registration')->withoutOverlapping();

        $schedule->call(function () {
            \Legato\Pushazure\Models\ProgressTopic::cronPublishMessageToTopic();
        })->name('push_azure_publish_group_message')->withoutOverlapping();

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'legato.pushazure.some_permission' => [
                'tab' => 'PushAzure',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'pushazure' => [
                'label'       => 'PushAzure',
                'url'         => Backend::url('legato/pushazure/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['legato.pushazure.*'],
                'order'       => 500,
            ],
        ];
    }
}
