<?php
/**
 * Created by PhpStorm.
 * User: ryanng
 * Date: 29/3/2018
 * Time: 3:57 PM
 */

return [

    'hub_namespace'                 => env('PUSH_AZURE_HUB_NAMESPACE',''),
    'hub'                           => env('PUSH_AZURE_HUB',''),
    'hub_connection_string'         => env('PUSH_AZURE_HUB_CONNECTION_STRING',''),
    /**
     * @todo
     */
    //'hub_namespace_sandbox'         => 'sinoclub-dev',
    //'hub_sandbox'                   => 'sinoclub-dev-sandbox',
    //'hub_connection_string_sandbox' => 'Endpoint=sb://sinoclub-dev.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=9rDyrJavnsKFPSXjkykRJMsu/+ABG4e5kIGR8diEcgw=',

];