<?php namespace Legato\Pushazure\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Legato\Push\Models\PushLocale;
use Legato\Pushazure\classes\NotificationHub;

/**
 * Progress Topics Back-end Controller
 */
class ProgressTopics extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Legato.Pushazure', 'pushazure', 'progresstopics');
    }

    public function send($msg_id,$topic_group_id){
        $hub = NotificationHub::initClient();
        $msg = \Legato\Push\Models\Message::find($msg_id);
        $topic_group = \Legato\Push\Models\Topic_group::find($topic_group_id);
        $topics = $topic_group->topics;
        foreach ($topics as $key => $topic) {
            if ($topic->language != 0)
            {
                /**
                 * Have language
                 */
                $locale = PushLocale::findById($topic->language);
                $msg->translateContext($locale->code);
            }
            //send out the message
            $res = $hub->sendToTag($msg,$topic->name);
        }
        

    }
}
