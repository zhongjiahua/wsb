<?php namespace Legato\Pushazure\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Legato\Pushazure\Models\Subscription;

/**
 * Subscriptions Back-end Controller
 */
class Subscriptions extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Legato.Pushazure', 'pushazure', 'subscriptions');
    }

    public function registration($id){
        $sub = Subscription::find($id);
        $sub->createOrUpdateRegistration();
    }
}
