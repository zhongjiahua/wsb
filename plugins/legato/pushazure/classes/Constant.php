<?php namespace Legato\Pushazure\classes;

/**
 * Created by PhpStorm.
 * User: ryanng
 * Date: 22/3/2018
 * Time: 1:09 PM
 */

class Constant
{

    const SNS_STATUS_PENDING        = 1;
    const SNS_STATUS_PROGRESSING    = 2;
    const SNS_STATUS_COMPLETED      = 3;
    const SNS_STATUS_ERROR          = 4;
    const SNS_STATUS_DELETED        = 5;
    const SNS_STATUS_DISABLED       = 0;

    const notification_type_ios 			= 'apple';
    const notification_type_windows 		= 'windows';
    const notification_type_gcm 			= 'gcm';
    const notification_type_windows_phone 	= 'windowsphone';

}