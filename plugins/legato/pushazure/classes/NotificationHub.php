<?php namespace Legato\Pushazure\classes;

use Exception;
use RainLab\Translate\Models\Locale;

class NotificationHub {
	const API_VERSION = "?api-version=2013-10";
	private $endpoint;
	private $hubPath;
	private $sasKeyName;
	private $sasKeyValue;
	function __construct($connectionString, $hubPath) {
		$this->hubPath = $hubPath;
		$this->parseConnectionString($connectionString);
	}
	private function parseConnectionString($connectionString) {
		$parts = explode(";", $connectionString);
		if (sizeof($parts) != 3) {
			throw new Exception("Error parsing connection string: " . $connectionString);
		}
		foreach ($parts as $part) {
			if (strpos($part, "Endpoint") === 0) {
				$this->endpoint = "https" . substr($part, 11);
			} else if (strpos($part, "SharedAccessKeyName") === 0) {
				$this->sasKeyName = substr($part, 20);
			} else if (strpos($part, "SharedAccessKey") === 0) {
				$this->sasKeyValue = substr($part, 16);
			}
		}
	}
	public function generateSasToken($uri) {
		$targetUri = strtolower(rawurlencode(strtolower($uri)));
		$expires = time();
		$expiresInMins = 60;
		$expires = $expires + $expiresInMins * 60;
		$toSign = $targetUri . "\n" . $expires;
		$signature = rawurlencode(base64_encode(hash_hmac('sha256', $toSign, $this->sasKeyValue, TRUE)));
		$token = "SharedAccessSignature sr=" . $targetUri . "&sig="
					. $signature . "&se=" . $expires . "&skn=" . $this->sasKeyName;
		//trace_log($token);
		return $token;
	}
	public function broadcastNotification($notification) {
		$this->sendNotification($notification, "");
	}
	public function sendNotification(Notification $notification, $tagsOrTagExpression) {
		//echo $tagsOrTagExpression."<p>";
		if (is_array($tagsOrTagExpression)) {
			$tagExpression = implode(" || ", $tagsOrTagExpression);
		} else {
			$tagExpression = $tagsOrTagExpression;
		}
		# build uri
		$uri = $this->endpoint . $this->hubPath . "/messages" . NotificationHub::API_VERSION;
		//echo $uri."<p>";
		$ch = curl_init($uri);
		if (in_array($notification->format, ["template", "apple", "gcm"])) {
			$contentType = "application/json";
		} else {
			$contentType = "application/xml";
		}
		$token = $this->generateSasToken($uri);
		$headers = [
		    'Authorization: '.$token,
		    'Content-Type: '.$contentType,
		    'ServiceBusNotification-Format: '.$notification->format
		];
		if ("" !== $tagExpression) {
			$headers[] = 'ServiceBusNotification-Tags: '.$tagExpression;
		}
		# add headers for other platforms
		if (is_array($notification->headers)) {
			$headers = array_merge($headers, $notification->headers);
		}
		
		curl_setopt_array($ch, array(
		    CURLOPT_POST => TRUE,
		    CURLOPT_RETURNTRANSFER => TRUE,
		    CURLOPT_SSL_VERIFYPEER => FALSE,
		    CURLOPT_HTTPHEADER => $headers,
		    CURLOPT_POSTFIELDS => $notification->payload
		));
		// Send the request
		$response = curl_exec($ch);
		// Check for errors
		if($response === FALSE){
		    throw new Exception(curl_error($ch));
		}
		$info = curl_getinfo($ch);
		if ($info['http_code'] <> 201) {
			throw new Exception('Error sending notificaiton: '. $info['http_code'] . ' msg: ' . $response);
		}
		return $info['http_code'];
		//print_r($info);
		//echo $response;
	} 

	public function readRegistration($registrationId) {
		$uri = "{$this->endpoint}{$this->hubPath}/registrations/{$registrationId}?api-version=2015-01";
		//trace_log($uri);
		$ch = curl_init($uri);

		$contentType = "application/xml";
		$token = $this->generateSasToken($uri);
		$headers = [
		    'Authorization: '.$token,
		    'Content-Type: '.$contentType
		];

		curl_setopt_array($ch, array(
	        CURLOPT_CUSTOMREQUEST => 'GET',
	        CURLOPT_RETURNTRANSFER => TRUE,
	        CURLOPT_SSL_VERIFYPEER => FALSE,
	        CURLOPT_HTTPHEADER => $headers,
	    ));

		// Send the request
		$response = curl_exec($ch);
		// Check for errors
		if($response === FALSE){
		    throw new Exception(curl_error($ch));
		}
		$info = curl_getinfo($ch);
		if ($info['http_code'] == 200) {
			$xml = $this->getXMLData($response);
			return $xml;
		}

		throw new Exception( $info['http_code'] . ' msg: ' . $response);
	}

	/**
	 * Create Registration in Azure
	 *
	 * @param string 				$device_kind 			apple/gcm/windows/windowsphone
	 * @param string 				$deviceToken
	 * @param string|array|null 	$tagsOrTagExpression
	 *
	 * @return string 		$registration_id
	 * @throws \Exception
	 */
	public function createRegistration($device_kind, $deviceToken, $tagsOrTagExpression = null) {
		if (is_array($tagsOrTagExpression)) {
			$tagExpression = implode(",", $tagsOrTagExpression);
		} else {
			$tagExpression = $tagsOrTagExpression;
		}
		# build uri
		$uri = $this->endpoint . $this->hubPath . "/registrations/?api-version=2015-01";
		$ch = curl_init($uri);

		$contentType = "application/xml";
		
		$token = $this->generateSasToken($uri);
		$headers = [
		    'Authorization: '.$token,
		    'Content-Type: '.$contentType
		];
		switch ($device_kind) {
			case 'apple':
				$body = $this->getXmlIOS($deviceToken,$tagExpression);
				break;
			case 'gcm':
				$body = $this->getXmlGCM($deviceToken,$tagExpression);
				break;
			case 'windows':
				//$body = $this->getXmlIOS($deviceToken);
				//break;
			case 'windowsphone':
				//$body = $this->getXmlIOS($deviceToken);
				//break;
			
			default:
				# code...
				throw new \Exception("Unsupported device kind");
			
				break;
		}
		

		curl_setopt_array($ch, array(
	        CURLOPT_CUSTOMREQUEST => 'POST',
	        CURLOPT_RETURNTRANSFER => TRUE,
	        CURLOPT_SSL_VERIFYPEER => FALSE,
	        CURLOPT_HTTPHEADER => $headers,
	        CURLOPT_POSTFIELDS => $body
	    ));

		// Send the request
		$response = curl_exec($ch);
		// Check for errors
		if($response === FALSE){
		    throw new Exception(curl_error($ch));
		}
		$info = curl_getinfo($ch);
		if ($info['http_code'] == 200) {
			$xml = $this->getXMLData($response);
			return $xml->title;
		}

		throw new Exception( $info['http_code'] . ' msg: ' . $response);
	} 


	/**
	 * Create Registration in Azure
	 *
	 * @param string 				$device_kind 			apple/gcm/windows/windowsphone
	 * @param string 				$registration_id
	 * @param string 				$deviceToken
	 * @param string|array|null 	$tagsOrTagExpression
	 *
	 * @return string 		$registration_id
	 * @throws \Exception
	 */
	public function updateRegistration($device_kind, $registration_id, $deviceToken, $tagsOrTagExpression = null) {
		if (is_array($tagsOrTagExpression)) {
			$tagExpression = implode(",", $tagsOrTagExpression);
		} else {
			$tagExpression = $tagsOrTagExpression;
		}
		# build uri
		$uri = $this->endpoint . $this->hubPath . "/registrations/".$registration_id."?api-version=2015-01";
		$ch = curl_init($uri);

		$contentType = "application/xml";
		
		$token = $this->generateSasToken($uri);
		$headers = [
		    'Authorization: '.$token,
		    'Content-Type: '.$contentType
		];
		switch ($device_kind) {
			case 'apple':
				$body = $this->getXmlIOS($deviceToken,$tagExpression);
				break;
			case 'gcm':
				$body = $this->getXmlGCM($deviceToken,$tagExpression);
				break;
			case 'windows':
				//$body = $this->getXmlIOS($deviceToken);
				//break;
			case 'windowsphone':
				//$body = $this->getXmlIOS($deviceToken);
				//break;
			
			default:
				# code...
				throw new \Exception("Unsupported device kind");
			
				break;
		}
		
		curl_setopt_array($ch, array(
	        CURLOPT_CUSTOMREQUEST => 'PUT',
	        CURLOPT_RETURNTRANSFER => TRUE,
	        CURLOPT_SSL_VERIFYPEER => FALSE,
	        CURLOPT_HTTPHEADER => $headers,
	        CURLOPT_POSTFIELDS => $body
	    ));

		// Send the request
		$response = curl_exec($ch);
		// Check for errors
		if($response === FALSE){
		    throw new Exception(curl_error($ch));
		}
		$info = curl_getinfo($ch);
		if ($info['http_code'] == 200) {
			$xml = $this->getXMLData($response);
			return $xml->title;
		}
		throw new Exception( $info['http_code'] . ' msg: ' . $response);
	} 

	/**
	 * @todo incomplete
	 */
	public function deleteRegistration($registration_id) {
		$uri = $this->endpoint . $this->hubPath . "/registrations/".$registration_id."?api-version=2015-01";
		$ch = curl_init($uri);

		$contentType = "application/xml";
		
		$token = $this->generateSasToken($uri);
		$headers = [
		    'Authorization: '.$token,
		    'Content-Type: '.$contentType,
		];

		curl_setopt_array($ch, array(
	        CURLOPT_CUSTOMREQUEST => 'DELETE',
	        CURLOPT_RETURNTRANSFER => TRUE,
	        CURLOPT_SSL_VERIFYPEER => FALSE,
	        CURLOPT_HTTPHEADER => $headers,
	        //CURLOPT_POSTFIELDS => $body
	    ));

		// Send the request
		$response = curl_exec($ch);
		// Check for errors
		if($response === FALSE){
		    throw new Exception(curl_error($ch));
		}
		$info = curl_getinfo($ch);
		if ($info['http_code'] == 200) {
			$xml = $this->getXMLData($response);
			return $xml->title;
		}

		throw new Exception( $info['http_code'] . ' msg: ' . $response);
	} 

	private function getXmlIOS($registrationId,$tag = null){
    	return $body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
			<entry xmlns=\"http://www.w3.org/2005/Atom\">
			    <content type=\"application/xml\">
			        <AppleRegistrationDescription xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.microsoft.com/netservices/2010/10/servicebus/connect\">
			            <Tags>{$tag}</Tags>
			            <DeviceToken>{$registrationId}</DeviceToken> 
			        </AppleRegistrationDescription>
			    </content>
			</entry>";
 	}

	private function getXmlGCM($registrationId,$tag = null){
    	return $body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
			<entry xmlns=\"http://www.w3.org/2005/Atom\">
			    <content type=\"application/xml\">
			        <GcmRegistrationDescription xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.microsoft.com/netservices/2010/10/servicebus/connect\">
			            <Tags>{$tag}</Tags>
			            <GcmRegistrationId>{$registrationId}</GcmRegistrationId> 
			        </GcmRegistrationDescription>
			    </content>
			</entry>";
 	}

 	private function getXMLData($xml){
 		$stringBody = (string) $xml;
        $xml = new \SimpleXMLElement($stringBody);
        return $xml;
 	}

 	private function createMessage($message){
 		$alert = array(
			'title' => $message->title,
			'body' => $message->body
		);
		$apnsArr['aps'] = array(
			'alert' => $alert,
			'badge' => $message->badge,
			'sound' => $message->sound
		);

		$gcmArr =array(
 			'data' => array(
 				'message' => array('title' => $message->title , 'body' => $message->body),
 				'content-available' => 0,
 				'badge'	=> $message->badge,
 				'sound'	=> $message->sound,
 				'mutable-content' => 0,
 				'attachment_url' => ''
			)
		);

		if($message->extra_data){
			$extra = $message->extra_data;
			$extra[] = [
				'extra_data_key' => 'message_guid',
				'extra_data_value'	=> $message->guid,
			];

			$gcmArr['data']['extra_data'] = $extra;
			$apnsArr['extra_data'] = $extra;
		}
		\Log::info($apnsArr);
 	    $alert = json_encode($apnsArr);
        $ios_message = new Notification("apple", $alert);

 		
		\Log::info($gcmArr);
        $message = json_encode($gcmArr);
        //$message = '{"data":{"message":"'.$message->title.'"}}';
        $gcm_message = new Notification("gcm", $message);
        
		return ['apns'=> $ios_message, 'gcm' => $gcm_message];
 	}

 	public function sendToTag(\Legato\Push\Models\Message $message, string $tag){
		$notifications = $this->createMessage($message);
		$httpStatus = 201;

		foreach ($notifications as $key => $notification) {
			$res = $this->sendNotification($notification, $tag);
			if($res != 201){
				$httpStatus = $res;
			}
		}
		return $httpStatus;
 	}

 	public static function initClient(){

    	//$device_kind = $this->getDeviceKind();
    	$connectionString = \Config::get('legato.pushazure::hub_connection_string');
        $hub_namespace = \Config::get('legato.pushazure::hub_namespace');
        $hubname = \Config::get('legato.pushazure::hub');
        $hub = new self($connectionString, $hubname); 
 		return $hub;
 	}
}
?>