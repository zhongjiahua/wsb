<?php namespace Legato\Pushazure\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_pushazure_subscriptions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('registration_id',255)->nullable();
            $table->integer('subscription_id')->nullable();
            $table->tinyInteger('sns_status');
            $table->string('error_reason',255)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_pushazure_subscriptions');
    }
}
