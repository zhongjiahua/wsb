<?php namespace Legato\Pushazure\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMessageTagsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_pushazure_message_tags', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('message_id')->nullable();
            $table->bigInteger('tag_id')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_pushazure_message_tags');
    }
}
