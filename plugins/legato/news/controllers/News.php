<?php namespace Legato\News\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Common\Controllers\CommonController;
use \Legato\News\Models\News as News_model;

/**
 * News Back-end Controller
 */
class News extends CommonController
{ 
    /** 
     * Register permission of modules (Optional)
     */
    public $requiredPermissions=['legato.news.view'];
    /** 
     * Register permission of actions (Optional)
     * Supported action list:
     * Create / Update / Delete / Import / Export / bulk action
     */
    public $requiredActionsPermissions=[
      'create'=>['legato.news.create'],
      'update'=>['legato.news.update'],
      'delete'=>['legato.news.delete'],
      'import'=>['legato.news.import'],
      'export'=>['legato.news.export'],

      'suspend'=>['legato.news.update'],
      'publish'=>['legato.news.update'],
    ];

    public function __construct()
    {
        parent::__construct();

        $this -> _breadcrumb();
        /** 
         * Register bulky action list(Optional)
         */
        $this -> _bulks_list();
        $this -> _vars();

        BackendMenu::setContext('Legato.News', 'news', 'news');

        /**
         * optional
         * only necessary when customize import service is needed
         */
        $this->import_class = \Legato\News\Classes\NewsImport::class;
    }

    private function _vars(){
        //$this -> vars['create_url'] = 'Legato/News/News/create';
        //$this -> vars['cancel_url'] =  'Legato/News/News/' ;
        /**
         * Unlock import button
         * default true
         */
        $this -> vars['hide_import'] = false;
        /**
         * Unlock export button
         * default true
         */
        $this -> vars['hide_export'] = false;
        $this -> vars['export_file_name'] = 'news';
        /** 
         * Customize column name of the export file (Optional)
         */
        //$this -> vars['export_fields_title'] = array('idd','titleee','short_description','description','is_published','published_at');

    }
    public function exportExtendquery($query){
        //$query->select('id', 'is_published');
    }
    public function exportExtendarray($exportArray){
        $status = \Common\Classes\StatusService::getStatusOptions('status',News_model::class);
        foreach ($exportArray as $key => $value) {
          if($exportArray[$key]['is_published']){
            $exportArray[$key]['is_published']='Yes';
          }else{
            $exportArray[$key]['is_published']='No';
          }
          $exportArray[$key]['status'] = $status[$exportArray[$key]['status']];
        }
        return $exportArray;
    }

    /*public function index(){
        $this -> _breadcrumb();
        parent::index();
    }
    public function update($id){
        $this -> _breadcrumb();
        parent::update($id);
    }
    public function create(){
        $this -> _breadcrumb();
        parent::create();
    }*/

    public function _breadcrumb (){
        $this -> vars['breadcrumb']['home'] = [
            'title' => 'News',
            'url'   => \Backend::url('/Legato/News/News/')
        ];
        $this -> vars['breadcrumb']['child'] = [];
        /*$this -> vars['breadcrumb']['child'] = [
            [

                  'title' => 'User2',
                  'url'   => \Backend::url('/rainlab/user/users2')
            ]
        ];*/
    }

    public function _bulks_list (){
        $this -> vars['bulk_list'] = [
            'delete' => array(            
                /**
                 * button label
                 */
                'label' => 'Delete',      
                /**
                 * button icon
                 */
                'icon'   => 'oc-icon-trash-o',
                /**
                 * button color, used in detail page,
                 * optional
                 */
                //'color' => '#777777', 
                /**
                 * use default action,
                 * optional (default false)
                 * (support delete,activate,deactivate)
                 */
                'default' => true,
                /**
                 * redirect after action success?
                 * optional(default false)
                 */
                'redirect' => array(
                        'enable' => true,
                        /**
                         * if destination is not setted, will redirect to list page
                         */
                        'destination' => 'legato/news/news'
                    ),
            ),
            'suspend' => array(
                'label' => 'Suspend',
                'icon'   => 'oc-icon-calendar-minus-o',
                'color' => '#7c8fad',
                'update' => array(
                        /**
                         * set status to News_model::STATUS_SUSPEND when suspending a News
                         */
                        'status' => News_model::STATUS_SUSPEND,
                    ),
                'required' => array(
                        /**
                         * A news can only suspend if status==News_model::STATUS_PUBLISHED && is_published==News_model::PUBLISHED
                         * optional
                         */
                        'status' => array(
                                'value' => News_model::STATUS_PUBLISHED,
                                'message' => 'News ID:%d cannot be suspend because only published news can be suspend.'
                            ),
                        'is_published' => array(
                                'value' => News_model::PUBLISHED,
                                'message' => 'News ID:%d cannot be suspend because only published news can be suspend.'
                            ),

                    ),
            ),
            'publish' => array(
                'label' => 'Publish',
                'icon'   => 'oc-icon-calendar-check-o',
                'color' => '#1991d1',
                /**
                 * use publish function in Legato\News\Models\News
                 */
                'function' => 'publish'
            ),
        ];
    }

    public function index_onBulkAction(){
        if(isset($this->vars['bulk_list'])){
            parent::bulkAction(News_model::class,$this->vars['bulk_list']);
            return $this   -> listRefresh();
        }
    }
}
