<?php

return [
    'image_support_language' => false,//true or false
    'image_field_name' => 'Image',
    'image_size_limitation' => 8000,//kilobytes
    'image_required' => false,//true or false
];