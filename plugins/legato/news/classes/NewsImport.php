<?php

namespace Legato\News\Classes;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Legato\News\Models\News;

/**
 * (optional)
 */
class NewsImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {   
        switch ($row['status']) {
            case 'Active':
                $status = News::STATUS_ACTIVE;
                break;
            case 'Published':
                $status = News::STATUS_PUBLISHED;
                break;
            case 'Suspend':
                $status = News::STATUS_SUSPEND;
                break;
            
            default:
                throw new \Exception('status should only be Active/Suspend/Published');
                break;
        }
        switch ($row['is_published']) {
            case 'Yes':
                $is_published = News::PUBLISHED;
                break;
            case 'No':
                $is_published = News::NOT_PUBLISHED;
                break;
            
            default:
                throw new \Exception('is_published should only be Yes/No');
                break;
        }
        return new News([
            'title' 		=> $row['title'],
            'excerpt' 		=> $row['excerpt'],
            'html_content' 	=> $row['html_content'],
            'is_published' 	=> $is_published,
            'published_at' 	=> $row['published_at'],
            'status' 		=> $status,
        ]);
    }
}