<?php namespace Legato\News;

use Backend;
use System\Classes\PluginBase;
use \Legato\News\Models\News;
use Carbon\Carbon;
/**
 * news Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'News',
            'description' => 'Demo Plugin',
            'author'      => 'Legato',
            'icon'        => 'icon-newspaper-o'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        News::extend(function($model){   
            $model->bindEvent('model.beforeSave', function() use ($model) {
                if($model->is_published==$model::PUBLISHED){
                    if($model->getOriginal('is_published')==$model::NOT_PUBLISHED){
                        //push event
                    }
                }
            });
        });

        if(\Config::get('legato.news::image_support_language',false)){
            $langs = \RainLab\Translate\Models\Locale::listEnabled();
        }else{
            $langs=[];
        }
        $field_name=\Config::get('legato.news::image_field_name','Image');
        $field_names=[];
        $label_names=[];
        if(count($langs)>0){
            foreach ($langs as $key => $lang) {
                $field_names[]=strtolower($field_name).'_'.$key;
                $label_names[]=$field_name.'('.$key.')';
            }
        }else{
            $field_names[]=strtolower($field_name);
            $label_names[]=$field_name;
        }
        $image_required=\Config::get('legato.news::image_required',true);
        $image_size=\Config::get('legato.news::image_size_limitation',1000);

        if(count($langs)>1){
            News::extend(function($model){
                $model->implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
                $model->translatable = [
                  'title',
                  'excerpt',
                  'html_content'
                ];
            });
        }
        News::extend(function($model)use($field_names,$label_names,$image_size,$image_required){ 
            foreach ($field_names as $key => $field_name) {
                $model->attachOne[strtolower($field_name)] = ['System\Models\File','delete' => true,'public'=>true];

                $model->bindEvent('model.beforeValidate', function() use ($model,$label_names,$field_name,$key,$image_size,$image_required) {
                    if($image_required&&$key==0){
                        $model->rules[strtolower($field_name)] = 'required|between:0,'.$image_size;
                        $model->customMessages[strtolower($field_name).'.required']=$label_names[$key].' is required.';
                    }else{
                        $model->rules[strtolower($field_name)] = 'between:0,'.$image_size;
                        $model->customMessages[strtolower($field_name).'.required']=$label_names[$key].' is required.';
                    }
                });
            }
        });
        \Event::listen('backend.form.extendFields', function($widget)use($field_names,$label_names,$image_size,$image_required) {
            if (!$widget->getController() instanceof \Legato\News\Controllers\News) return;
            if (!$widget->model instanceof \Legato\News\Models\News) return;

            foreach ($field_names as $key => $field_name) {
                $required=false;
                if($image_required){
                    if($key==0){
                        $required=true;
                    }
                }
                $widget->addFields([
                    $field_name => [
                        'label' => $label_names[$key],
                        'fileTypes' => 'jpeg,jpg,png',
                        'mode' => 'image',
                        'comment' => 'Image should not be larger than '.$image_size.'kb',
                        'type' => 'fileupload',
                        'required' => $required,
                        'span' => 'auto'      
                    ]
                ]);
            }

        });

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Legato\News\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'legato.news.view' => [
                'tab' => 'News',
                'label' => 'View News'
            ],
            'legato.news.create' => [
                'tab' => 'News',
                'label' => 'Create News'
            ],
            'legato.news.update' => [
                'tab' => 'News',
                'label' => 'Update News'
            ],
            'legato.news.delete' => [
                'tab' => 'News',
                'label' => 'Delete News'
            ],
            'legato.news.import' => [
                'tab' => 'News',
                'label' => 'Import News'
            ],
            'legato.news.export' => [
                'tab' => 'News',
                'label' => 'Export News'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        
        return [
            'news' => [
                'label'       => 'News',
                'url'         => Backend::url('legato/news/news'),
                'icon'        => 'icon-newspaper-o',
                'permissions' => ['legato.news.*'],
                'order'       => 500,

                'sideMenu' => [
                    'news' => [
                        'label' => 'News',
                        'icon' => 'icon-laptop',
                        'url' => Backend::url('legato/news/news'),
                    ],
                ],
            ],
        ];
    }

    public function registerSchedule($schedule)
    {   
        $schedule->call(function () {
            $news = News::where('status',News::STATUS_ACTIVE)->get();
            foreach ($news as $key => $news_item) {
                $dt = new Carbon($news_item->published_at);
                //trace_log($dt);
                if($dt->isPast()){
                    $news_item->publish();
                }
            }
          
        })->everyMinute()->name('publish_news')->withoutOverlapping();
    }

}
