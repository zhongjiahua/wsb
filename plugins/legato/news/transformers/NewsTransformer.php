<?php namespace Legato\News\Transformers;

use League\Fractal\TransformerAbstract;
use Legato\News\Models\News;
use App;


class NewsTransformer extends TransformerAbstract
{
	public function transform(News $model) 
	{
		$res = [
			'id' 			=> (int)$model->id,
			'title' 		=> $model->getTranslatedField('title'),
			'excerpt' 		=> $model->getTranslatedField('excerpt'),
			'html_content' 	=> $model->getTranslatedField('html_content'),
			'status' 		=> $model->status,
		];
		$image_names = $model->attachOne;
        foreach ($image_names as $key => $name) {
        	$res[$key] = isset($model->$key) ? $model->$key->getpath() : null;
        }

		return $res;

	}

}