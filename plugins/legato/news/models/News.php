<?php namespace Legato\News\Models;

use Model;
use Common\Classes\StatusService;
use Common\Models\CommonModel;
use \October\Rain\Database\Traits\Validation;
use Carbon\Carbon;
use Legato\News\Transformers\NewsTransformer;
/**
 * news Model
 */
class News extends CommonModel
{
    public $transformer = NewsTransformer::class;
    use Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'legato_news';
    public $rules = [
        'title' => 'required|max:255',
        'html_content' => 'required',
        'published_at' => 'required|date',
        'is_published' => 'integer',
        'status' => 'integer',
    ];
    public $customMessages = [
        'is_published.integer'                => 'is_publish flag should be 0(non-published) or 1(published).',
        'status.integer'                => 'Status flag should be 0(suspend) or 1(active).',
        'published_at.required'                => 'Published data is required.',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title','html_content','excerpt','published_at','is_published','status'
    ];

    /**
     * @var array Relations
     */


    const STATUS_ACTIVE = 2;//
    const STATUS_PUBLISHED = 1;//
    const STATUS_SUSPEND = 0;//
    const STATUS_REMOVED = 9;//

    const NOT_PUBLISHED = 0;//
    const PUBLISHED = 1;//

    public $belongsTo = [
        'updated_by_admin' => [
            'Backend\Models\User',
            'key' => 'updated_by'
        ],
        'created_by_admin' => [
            'Backend\Models\User',
            'key' => 'created_by'
        ],
    ];
    /*public function beforeDelete(){
        parent::beforeDelete();
    }*/

    public function beforeValidate(){
        if(!isset($this->status))
            $this->status=$this::STATUS_ACTIVE;

        if(!isset($this->is_published))
            $this->is_published=$this::NOT_PUBLISHED;
    }
    public function getStatusOptions() {
        $ret = array(
            $this::STATUS_ACTIVE=>'Active',
            $this::STATUS_PUBLISHED=>'Published',
            $this::STATUS_SUSPEND=>'Suspend',);
        return $ret;
    }
    public function filterFields($fields, $context = null){

        if ($context == 'update') {
            if (strtotime($this->published_at) <= time()) {
                $fields->published_at->readOnly = true;
            }
        }
    }
    public function publish(){
        try{
            if($this->status==$this::STATUS_ACTIVE){
                $this->status = $this::STATUS_PUBLISHED;
                $this->is_published=$this::PUBLISHED;
                $this->published_at=Carbon::now();
                $this->save();
                return array('status'=>StatusService::STATUS_ACTIVE);
            }else if($this->status==$this::STATUS_SUSPEND){
                $this->status = $this::STATUS_PUBLISHED;
                $this->save();
                return array('status'=>StatusService::STATUS_ACTIVE);
            }else{
                return array('status'=>StatusService::STATUS_SUSPEND,'info'=> 'News ID:'.$this->id.' is already published');   
            }
        }catch (\Exception $e){
            \Log::debug('Legato\News\Models\News\publish()');
            \Log::debug($e);
        }
        return array('status'=>StatusService::STATUS_SUSPEND,'info'=>$e->getMessage());   
    }

    public static function api($ut,$lang){
        $image_names = (new self())->attachOne;
        $with = [];
        foreach ($image_names as $key => $name) {
            $with[] = $key;
        }
        $collection = self::whereUt($ut)->withTranslate($lang)->with($with)->get();
        $tranformed = self::transform($collection);
        return $tranformed;
    }
}
