<?php namespace Legato\News\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNewsTable extends Migration
{
    public function up()
    {
        Schema::create('legato_news', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title',255)->nullable();
            $table->text('excerpt')->nullable();
            $table->text('html_content')->nullable();
            $table->tinyInteger('is_published')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->tinyInteger('status');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legato_news');
    }
}
