<?php

namespace Basic\Tools\Classes;

class AesSecurity
{
    public static function encrypt($input)
    {
        $key = \Config::get('app.aes-key');
        $data = openssl_encrypt($input, 'aes-128-ecb', $key, OPENSSL_PKCS1_PADDING);
        $data = base64_encode($data);
        return $data;
    }

    public static function decrypt($sStr)
    {
        //$key = \Config::get('basic.tools::setting.aes-key');
        $key = \Config::get('app.aes-key');
        $decrypted = openssl_decrypt(base64_decode($sStr), 'aes-128-ecb', $key, OPENSSL_PKCS1_PADDING);
        return $decrypted;
    }
}