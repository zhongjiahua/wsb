CREATE TABLE `legato_audittrail_audit_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `section` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `record_id` int(11) unsigned NOT NULL,
  `action` varchar(255) NOT NULL,
  `backend_users_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4