CREATE TABLE `legato_sms_delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_id` (`response_id`(191)),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci