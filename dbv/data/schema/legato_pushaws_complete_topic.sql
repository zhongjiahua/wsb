CREATE TABLE `legato_pushaws_complete_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arn` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complete_topic_id` int(11) NOT NULL,
  `sns_status` smallint(6) NOT NULL DEFAULT '1',
  `error_reason` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci