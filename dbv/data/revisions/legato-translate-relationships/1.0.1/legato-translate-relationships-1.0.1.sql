DROP TABLE IF EXISTS `legato_translaterelationships_translate_relationships`;
CREATE TABLE `legato_translaterelationships_translate_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;