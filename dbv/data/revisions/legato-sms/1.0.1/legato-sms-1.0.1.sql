DROP TABLE IF EXISTS `legato_sms_complete`;
CREATE TABLE `legato_sms_complete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `sms_token` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_token` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `protocol` varchar(100) NOT NULL,
  `service_provider` varchar(100) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_total_retry` int(11) NOT NULL,
  `error_code` varchar(200) NOT NULL,
  `error_message` varchar(200) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `legato_sms_complete_archive`;
CREATE TABLE `legato_sms_complete_archive` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_total_retry` int(11) NOT NULL,
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `msg_id` (`msg_id`),
  KEY `num_retry` (`num_retry`),
  KEY `current_retry` (`current_retry`),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`),
  KEY `response_id` (`response_id`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `legato_sms_delivery`;
CREATE TABLE `legato_sms_delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_id` (`response_id`(191)),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `legato_sms_delivery_archive`;
CREATE TABLE `legato_sms_delivery_archive` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `response_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_id` (`response_id`(191)),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `legato_sms_message`;
CREATE TABLE `legato_sms_message` (
  `msg_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sms_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `num_message` int(11) NOT NULL,
  `num_success` int(11) NOT NULL,
  `num_fail` int(11) NOT NULL,
  `num_processing` int(11) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `num_delivered` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OA` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suffix` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`msg_id`),
  KEY `schedule_time` (`schedule_time`),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `legato_sms_message_archive`;
CREATE TABLE `legato_sms_message_archive` (
  `msg_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8mb4_unicode_ci,
  `num_message` int(11) NOT NULL,
  `num_success` int(11) NOT NULL,
  `num_fail` int(11) NOT NULL,
  `num_processing` int(11) NOT NULL,
  `num_retry` int(11) NOT NULL,
  `num_delivered` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`msg_id`),
  KEY `schedule_time` (`schedule_time`),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `legato_sms_progress`;
CREATE TABLE `legato_sms_progress` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `sms_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_retry` int(11) NOT NULL,
  `current_retry` int(11) NOT NULL,
  `num_host_retry` int(11) NOT NULL DEFAULT '0',
  `num_total_retry` int(11) NOT NULL DEFAULT '0',
  `protocol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `msg_id` (`msg_id`),
  KEY `num_retry` (`num_retry`),
  KEY `current_retry` (`current_retry`),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `legato_sms_receiver`;
CREATE TABLE `legato_sms_receiver` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `receiver_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `msg_id` (`msg_id`),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `legato_sms_receiver_archive`;
CREATE TABLE `legato_sms_receiver_archive` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `phone_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user` int(11) NOT NULL,
  `modify_user` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `msg_id` (`msg_id`),
  KEY `status` (`status`),
  KEY `create_date` (`create_date`),
  KEY `modify_date` (`modify_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;