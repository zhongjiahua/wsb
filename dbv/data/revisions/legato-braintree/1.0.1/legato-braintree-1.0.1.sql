DROP TABLE IF EXISTS `legato_braintree_error_log`;
CREATE TABLE `legato_braintree_error_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(191) DEFAULT NULL,
  `braintree_message` varchar(191) DEFAULT NULL,
  `braintree_result` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `legato_braintree_payment`;
CREATE TABLE `legato_braintree_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `recurring_billing_refer_id` int(10) unsigned DEFAULT NULL,
  `currency_iso_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merchant_account_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `braintree_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `braintree_customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `legato_braintree_error_log_id` int(10) unsigned DEFAULT NULL,
  `braintree_message` text COLLATE utf8mb4_unicode_ci,
  `transaction_date` datetime DEFAULT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recurring_billing_refer_id` (`recurring_billing_refer_id`),
  KEY `legato_braintree_error_log_id` (`legato_braintree_error_log_id`),
  CONSTRAINT `legato_braintree_payment_ibfk_1` FOREIGN KEY (`recurring_billing_refer_id`) REFERENCES `legato_braintree_payment` (`id`) ON DELETE SET NULL,
  CONSTRAINT `legato_braintree_payment_ibfk_2` FOREIGN KEY (`legato_braintree_error_log_id`) REFERENCES `legato_braintree_error_log` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;