## Minimum System Requirements

October CMS has a few system requirements:

* PHP version 7.0 or higher
* PDO PHP Extension
* cURL PHP Extension
* OpenSSL PHP Extension
* Mbstring PHP Library
* ZipArchive PHP Library
* GD PHP Library

As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
When using Ubuntu, this can be done via ``apt-get install php5-json``.

## Installation Guide - Legato Generic October CMS

1) Obtaining the source code

	- Please make sure you checked out the source code from the correct Git repository and branch.

	- For testing purpose, you should use the source code from the 'develop' git branch.

	- For the development of Generic October CMS, you should use the feature branch, which is branched out from 'develop'.

	- For CMS development of Legato projects, you should **FORK** from the 'develop' git branch. When Generic October CMS is updated, you can create merge request from Generic October CMS to your project. Therefore, you **SHOULD** always prevent to modify the code from Generic October CMS, to prevent unresolvable conflict. 

2) Create a new database

	- By either one of the following method
		- Import the database located at script->database->develop->develop.sql
		- Run command php artisan october:up

3) Configurating the October CMS

	- Make a copy of .env.sample at the root directory and rename it as .env
	- Edit the configs

4) Setting up cron job

	- SSH to the server via Terminal (for Mac)
	- $ sudo crontab -e
	- Insert the cron script located at script->cron->crontab.txt
