<?php

return [
	/**
	 * support file type:
	 * csv xlsx xls html
	 */
    'export_file_type' => env('EXPORT_FILE_TYPE') ? array_filter(explode(',', env('EXPORT_FILE_TYPE'))) : ['csv','xlsx'],
    /**
	 * conifg memory_limit for export usage
	 * e.g. '512M'
     */
    'export_memory_limit' => env('EXPORT_MEMORY_LIMIT',NULL)
];